import numpy as np
from scipy.linalg import solve_triangular


def poly_inner(p1, p2):
    dp1 = np.polyder(p1)
    dp2 = np.polyder(p2)
    ip = np.polyint(p1*p2)
    idp = np.polyint(dp1*dp2)
    return ip(1)-ip(-1) + idp(1)-idp(-1)

def monomial(k):
    return np.poly1d([1]+[0]*k)

def gramian(degree):
    dim = degree+1
    gram = np.empty((dim,dim), dtype=np.float)
    for j in range(dim):
        for k in range(j+1):
            gram[j,k] = gram[k,j] = poly_inner(monomial(j), monomial(k))
    return gram

def coeffs(degree):
    gram = gramian(degree)
    L = np.linalg.cholesky(gram)
    I = np.eye(degree+1)
    return solve_triangular(L, I, lower=True)
