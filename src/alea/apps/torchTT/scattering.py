# coding: utf-8

def af(image, wave, num):
    if num <= 0:
        return image
    return af(abs_trafo(image, wave), wave, num-1)
    
def abs_trafo(image, wave):
    lowpass, others = pywt.dwt2(image, wave)
    filtered = np.block([[lowpass, others[0]], list(others[1:])])
    return abs(filtered)
    
import pywt
scale = lambda x: (x - x.min()) / (x.max() - x.min())
from scipy.misc import face
image = face(True)
cmap = cm.viridis
cmap.set_bad(cmap.colors[0])
from matplotlib.colors import LogNorm
plt.imshow(scale(af(image, 'haar', 3)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
#plt.imshow(scale(af(image, 'haar', np.inf)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
np.log2(image.shape)
np.floor(np.log2(image.shape))
num = np.min(np.floor(np.log2(image.shape)))
plt.imshow(scale(af(image, 'haar', num)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(scale(af(image, 'haar', num-1)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(scale(af(image, 'haar', num-1)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
def re(scatter, wave):
    L,R = np.split(scatter, 2, axis=0)
    UL,LL = np.split(L, 2, axis=1)
    UR,LR = np.split(R, 2, axis=1)
    lowpass = UL
    others = (UR, LL, LR)
    return pywt.idwt2((lowpass, others), wave)
    
re(abs_trafo(image, wave), wave)
absim = re(abs_trafo(image, 'haar'), 'haar')
plt.imshow(absim)
def ab(scatter, wave, num):
    if num <= 0:
        return image
    return ab(re(scatter, wave), wave, num-1)
    
plt.imshow(ab(af(image, 'haar', 1), 'haar', 1))
plt.imshow(ab(af(image, 'haar', 1), 'haar', ))
\plt.imshow(ab(af(image, 'haar', 1), 'haar', ))
num = 2
plt.imshow(ab(af(image, 'haar', num), 'haar', num))
num = 3
plt.imshow(ab(af(image, 'haar', num), 'haar', num))
max_num = np.min(np.floor(np.log2(image.shape)))
max_num
num = max_num
plt.imshow(ab(af(image, 'haar', num), 'haar', num))
num = max_num
plt.imshow(ab(af(image, 'haar', num), 'haar', num))
num = max_num
plt.imshow(ab(af(image, 'haar', num), 'haar', num))
num = max_num
plt.imshow(af(image, 'haar', num))
plt.imshow(scale(af(image, 'haar', num-1)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(scale(af(image, 'haar', max_num)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(ab(scale(af(image, 'haar', max_num)), 'haar', max_num), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(ab((af(image, 'haar', max_num)), 'haar', max_num), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
num = max_num
plt.imshow(ab(scale(af(image, 'haar', num)), 'haar', num))
from scipy.misc import ascent
plt.imshow(ab(scale(af(ascent(), 'haar', 9)), 'haar', 9))
plt.imshow(ab(scale(af(ascent(), 'haar', 9)), 'haar', 9))
plt.imshow(scale(af(ascent(), 'haar', 1)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(scale(af(ascent(), 'haar', 2)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(scale(af(ascent(), 'haar', 8)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
plt.imshow(scale(af(ascent(), 'haar', 9)), cmap=cmap, norm=LogNorm(vmin=1e-4, vmax=1)); plt.colorbar()
#TODO: du darfst glaube ich auch nur max_num-1 viele splits machen!
def ab(scatter, wave, num):
    if num <= 0:
        return scatter
    return ab(re(scatter, wave), wave, num-1)
    
plt.imshow(ab(scale(af(ascent(), 'haar', 1)), 'haar', 1))
plt.imshow(ab(scale(af(ascent(), 'haar', 1)), 'haar', 1))
num = 1
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 2
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 9
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 8
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 7
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 6
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 5
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 4
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 3
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 4
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 5
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 6
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 7
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 8
plt.imshow(ab(scale(af(ascent(), 'haar', num)), 'haar', num))
num = 8
plt.imshow(ab((af(ascent(), 'haar', num)), 'haar', num))
num = 8
plt.imshow(ab((af(ascent(), 'haar', num)), 'haar', num))
num = 8
plt.imshow(ab((af(image, 'haar', num)), 'haar', num))
num = 4
plt.imshow(ab((af(image, 'haar', num)), 'haar', num))
num = 2
plt.imshow(ab((af(image, 'haar', num)), 'haar', num))
num = 4
plt.imshow(ab((af(image, 'haar', num)), 'haar', num))
num = 6
plt.imshow(ab((af(image, 'haar', num)), 'haar', num))
num = 5
plt.imshow(ab((af(image, 'haar', num)), 'haar', num))
get_ipython().run_line_magic('save', 'scattering.py')
get_ipython().run_line_magic('save', 'scattering.py 0-73')
