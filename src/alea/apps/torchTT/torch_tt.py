import torch
import torch.nn as nn
from typing import Union, List


def part(a, b):
    """
    Returns True iff a <= b component-wise.
    """
    return len(a) == len(b) and all(ai <= bi for ai,bi in zip(a,b))


#TODO: join this TorchTT class and my MeasurementList class (rename it into measured tensor or stuff like that)
class TorchTT(nn.Module):
    def __init__(self,
            input_dimensions : Union[None, List[int]]          = None,
            output_dimension : Union[None, int]                = None,
            ranks            : Union[None, List[int]]          = None,
            components       : Union[None, List[torch.Tensor]] = None,
            device           : Union[None, torch.device]       = None
            ):
        super(TorchTT, self).__init__()

       #TODO: in l.32, l.33, l.81 and l.82: use torch.as_tensor?

        if input_dimensions is None:
            assert components is not None
            input_dimensions = [comp.shape[1] for comp in components[1:]]
        if output_dimension is None:
            assert components is not None
            output_dimension = components[0].shape[1]
        dimensions = torch.cat((torch.tensor([output_dimension], dtype=torch.int32),
                                torch.tensor(input_dimensions, dtype=torch.int32)), 0)
        assert all(dim > 0 for dim in dimensions)

        if ranks is None:
            assert components is not None
            ranks = [comp.shape[0] for comp in components]
            ranks.append(components[-1].shape[2])
        assert all(rank > 0 for rank in ranks)
        assert ranks[0] == 1 and ranks[-1] == 1
        assert len(dimensions)+1 == len(ranks), f"{len(dimensions)+1} == {len(ranks)}"

        if device is None:
            if components is not None:
                device = components[0].device
                assert all(comp.device == device for comp in components)
            else:
                device = torch.device("cpu")

        if components is None:
           components = [torch.ones((1,output_dimension,1), dtype=torch.float, device=device)]
           #TODO: this also underpins the basic assumption that the first basis function is constant...
           # so apparently it is okay to have some basic assumptions. just list them in the docs.
        else:
            components = list(components)[:len(dimensions)]
        mode_defect = max((len(dimensions) - len(components)), 0)
        components += [torch.ones((1,1,1), dtype=torch.float, device=device)]*mode_defect

        compose = lambda f,g: lambda x: f(g(x))
        def common_slices(ta, tb):
            a,b = ta.shape,tb.shape
            assert len(a) == len(b)
            return tuple(map(compose(slice, min), zip(a,b)))
            # return tuple(map(slice, map(min, zip(a,b))))
            # return tuple(slice(min(ai,bi)) for ai,bi in zip(a,b))

        assert len(components) == len(dimensions)
        # assert that the output dimension is not only smaller (as checked below)
        assert components[0].shape[1] == output_dimension, f"Shape condition failed: {components[0].shape[:2]} == (1, {output_dimension})"
        self.components = nn.ParameterList()
        for pos in range(len(components)):
            shape = ranks[pos], dimensions[pos], ranks[pos+1]
            init = components[pos].to(device)
            comp = 1e-9 * torch.randn(shape, dtype=torch.float, device=device)
            s = common_slices(init, comp)
            comp[s] += init[s]
            self.components.append(nn.Parameter(nn.Parameter(comp)))
        #TODO: make self.components readonly (or at least check that the shapes stay consistent)

        self.__dimensions = torch.tensor(dimensions, dtype=torch.int32)
        self.__ranks = torch.tensor(ranks, dtype=torch.int32)
        self.__device = device

        assert all(comp.shape[0] == rank for comp,rank in zip(self.components, self.ranks[:-1])), f"{[comp.shape[0] for comp in self.components]} vs {self.ranks[:-1]}"
        assert all(comp.shape[2] == rank for comp,rank in zip(self.components, self.ranks[1:])), f"{[comp.shape[2] for comp in self.components]} vs {self.ranks[1:]}"
        assert all(comp.shape[1] == dim for comp,dim in zip(self.components, self.dimensions)), f"{[comp.shape[1] for comp in self.components]} vs {self.dimensions}"

    def forward(self, *input_data):
        assert len(input_data)+1 == len(self.components)
        batch_size = input_data[0].shape[0]
        for pos in range(len(input_data)):
            assert input_data[pos].shape == (batch_size, self.dimensions[pos+1])

        retval = torch.ones((1, batch_size), dtype=torch.float, device=self.device)
        for pos in reversed(range(1, len(self.components))):
            # the first component is the output...
            comp = self.components[pos]
            meas = input_data[pos-1]

            if meas.shape[1] > comp.shape[2]:
                contraction = torch.einsum('ldr,bd -> lbr', comp, meas)
                retval = torch.einsum('lbr,rb -> lb', contraction, retval)
                # ops: ldrb + lbr (meas.shape[1] == d > r == comp.shape[2] by assumption)
            else:
                retval = torch.einsum('ldr,rb -> lbd', comp, retval)
                retval = torch.einsum('lbd,bd -> lb', retval, meas)
                # ops: ldrb + lbd (meas.shape[1] == d <= r == comp.shape[2] by assumption)
        retval = torch.matmul(self.components[0], retval)[0].T
        assert retval.shape == (batch_size, self.output_dimension)

        return retval

    @property
    def device(self): return self.__device

    @property
    def dimensions(self): return self.__dimensions.clone().detach()

    @property
    def ranks(self): return self.__ranks.clone().detach()

    @property
    def input_dimensions(self): return self.__dimensions[1:].clone().detach()

    @property
    def output_dimension(self): return self.__dimensions[0]

    def count_parameters(self):
        for p in self.parameters():
            assert p.requires_grad
        n_params = sum(p.numel() for p in self.parameters())
        assert n_params == sum(self.ranks[:-1] * self.dimensions * self.ranks[1:]), f"{n_params} == {sum(self.ranks[:-1] * self.dimensions * self.ranks[1:])}"
        return n_params

    #TODO: provide staticmethod from_xerus
    #TODO: provide method to_xerus
