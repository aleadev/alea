import os, argparse
from collections import deque
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_tt import TorchTT
from torchvision import datasets, transforms
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)  # suppress tensorboard FutureWarning
from torch.utils.tensorboard import SummaryWriter
from fMap import reshape, scatter, MNIST_PCA, MNIST_scaling
from H1ONB import coeffs


degree = 4
rank = -1
scattering_levels = 3  # max value: 6
npcs = 20
assert scattering_levels == 3 and npcs == 20  # otherwise the precomputed values are wrong


file_name = os.path.splitext(os.path.basename(__file__))[0]
experiment_name = f"{file_name}/rank-{rank}_degree-{degree}"  # {file_name}/... is degree 2 and rank 20; {file_name}/degree_{degree} is rank 20
os.makedirs(experiment_name, exist_ok=True)


torch.manual_seed(0)
writer = SummaryWriter(f"runs/{experiment_name}")


def define(fnc):
    return fnc()

#TODO: add kw-argument to enable/disable caching --- for this you should use a decorator-decorator
def compute(fnc):
    try:
        return torch.load(f"{file_name}/{fnc.__name__}.pt")
    except FileNotFoundError:
        print(f"Computing {fnc.__name__}")
        ret = fnc()
        if not os.path.isdir(f"{file_name}"):
            os.mkdir(f"{file_name}")
        torch.save(ret, f"{file_name}/{fnc.__name__}.pt")
        return ret

log = lambda x: float(torch.log(torch.tensor(float(x))))
ln2 = log(2)

class ModuleSum(nn.Module):
    def __init__(self, summands, input_dim, output_dim, feature_dim, device=None):
        super(ModuleSum, self).__init__()
        if device is None:
            device = torch.device("cpu")
        self.device = device
        self.summands = nn.ModuleList(summands)#.to(device)
        self.input_dim = input_dim  #TODO: only needed since PolynomiallyMeasuredTT has the line `assert x.ndim == 2 and x.shape[1] == self.tt.input_dim`
        self.output_dim = output_dim  # needed for empty sum
        self.feature_dim = feature_dim  #TODO: only needed since PolynomiallyMeasuredTT has the line `assert tt.feature_dim == degree+1`
        for summand in summands:
            assert summand.input_dim == input_dim
            assert summand.output_dim == output_dim
            assert summand.feature_dim == feature_dim

    def forward(self, input):
        # print("In: ModuleSum.forward")
        assert input.ndim == 3 and input.shape[1] == self.input_dim and input.shape[2] == self.feature_dim
        ret = torch.zeros((input.shape[0], self.output_dim,), dtype=torch.float, device=self.device)
        for summand in self.summands:
            ret += summand(input)
        return ret

class PolynomiallyMeasuredTT(nn.Module):  #TODO: rename: this has nothing todo with TTs!
    def __init__(self, tt, degree, offset=None):
        super(PolynomiallyMeasuredTT, self).__init__()
        assert tt.feature_dim == degree+1, f"{tt.feature_dim} vs {degree+1}"
        self.tt = tt
        self.degree = degree
        if offset is None:
            self.offset = lambda x: 0
        else:
            self.offset = offset
            for param in self.offset.parameters():
                param.requires_grad = False
        self.exponents = torch.arange(self.degree+1, dtype=torch.float, device=self.tt.device)  #TODO: device?
        self.coefficients = torch.tensor(coeffs(degree), dtype=torch.float, device=self.tt.device)  #TODO: device?
        assert self.coefficients.shape == (degree+1, degree+1)

    def forward(self, x):
        assert x.ndim == 2 and x.shape[1] == self.tt.input_dim  # (batch_size, input_dim)
        # print("In: PolynomiallyMeasuredTT.forward")
        # print("Calling: self.offset:")
        # print("\t"+str(self.offset).replace("\n", "\n\t"))
        offset = self.offset(x)
        # print("Returning from: self.offset")
        # print("\t"+str(self.offset).replace("\n", "\n\t"))
        # print("offset.shape:", torch.tensor(offset).shape)
        x = x[:, :, None] ** self.exponents
        # x = torch.tensordot(self.coefficients, x, dims=([-1], [-1]))
        x = torch.einsum('oi,bdi -> bdo', self.coefficients, x)
        # print(1, x.shape, torch.tensor(offset).shape)
        x = self.tt(x) + offset
        # print(2, x.shape, torch.tensor(offset).shape)
        return F.log_softmax(x, dim=1)

def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    data_points_seen = (epoch-1) * len(train_loader.dataset)
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        data_points_seen += len(data)
        if batch_idx % args.log_interval == 0:
            writer.add_scalar('Loss/train', loss.item(), data_points_seen)  #TODO: epoch?
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

def test(args, model, device, test_loader, data_points_seen):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    writer.add_scalar('Loss/test', test_loss, data_points_seen)
    writer.add_scalar('Accuracy/test', correct/len(test_loader.dataset), data_points_seen)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

    return test_loss, correct/len(test_loader.dataset)

if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--initial_epoch', type=int, default=1,
                        help='initial epoch to start training (default: 1)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--initial_rank', type=int, default=0,
                        help='rank of the loaded tensor before first update (default: 0)')
    parser.add_argument('--maximal_rank', type=int, default=10,
                        help='maximal rank to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the Model')
    parser.add_argument('--load-model', action='store_true', default=False,
                        help='For Loading the Model')
    parser.add_argument('--gpu', type=int, default=0,
                        help='GPU to use (use -1 for CPU)')
    args = parser.parse_args()

    print("Arguments:") 
    mlk = max(map(len, args.__dict__.keys())) 
    for k,v in args.__dict__.items(): 
        space = " "*(mlk-len(str(k))+1) 
        print(f"    {k}:{space}{v}") 

    use_cuda = args.gpu >= 0 and torch.cuda.is_available()
    if args.gpu >= torch.cuda.device_count():
        raise RuntimeError(f"GPU index too large ({args.gpu} > {torch.cuda.device_count()-1})")

    device = torch.device(f"cuda:{args.gpu}" if use_cuda else "cpu")
    print(f"\nRunning on device:    {device}\n")

    trafo_list = [transforms.ToTensor(),  # converts PIL images with shape (H,W,C) and range [0,255] to tensors with shape (C,H,W) and range [0,1]
                  transforms.Lambda(reshape(28,28)),
                  transforms.Lambda(scatter("haar", scattering_levels))]

    mnist_pca = MNIST_PCA(trafo_list, device, file_name)
    mean = mnist_pca.mean.to('cpu')
    projection = mnist_pca.eigenvectors.to('cpu')[:npcs]
    def pca(x):
        return projection @ (x - mean)
    trafo_list += [transforms.Lambda(pca)]

    image_size = 28**2
    scatter_size = mnist_pca.data_shape[1]
    assert projection.shape == (npcs, scatter_size)

    mnist_scaling = MNIST_scaling(trafo_list, device, file_name)
    mins, maxs = mnist_scaling.mins_maxs.to('cpu')
    diams = maxs-mins
    def scale(x):
        assert x.shape == (npcs,)
        return 2 * (x-mins) / diams - 1
    trafo_list += [transforms.Lambda(scale)]

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST(f'{file_name}/data', train=True, download=True, transform=transforms.Compose(trafo_list)),
        batch_size=args.batch_size, shuffle=True, drop_last=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST(f'{file_name}/data', train=False, transform=transforms.Compose(trafo_list)),
        batch_size=args.test_batch_size, shuffle=False, **kwargs)

    epoch = args.initial_epoch-1
    # if args.load_model:
    #     tt = TorchTT(input_dim=npcs, output_dim=10, rank=20, feature_dim=args.initial_degree+1, device=device)
    #     model = PolynomiallyMeasuredTT(tt, args.initial_degree).to(device)
    #     model.load_state_dict(torch.load(f"{experiment_name}/model.pt"))
    #     components = model.tt.components
    # else:
    #     components = None

    summands = []
    assert len(summands) == args.initial_rank

    # terminate after deceeding an estimated increase of 1% (1e-2) accuracy over the measurement interval
    diff_lim = -ln2*1e-2
    for rank in range(args.initial_rank+1, args.maximal_rank+1):
        print(f"""
Image dimension:      {image_size}
Scattering dimension: {scatter_size}
Reduced dimension:    {npcs}

Polynomial degree:    {degree}
Rank:                 {rank}
""")

        tt = TorchTT(input_dim=npcs, output_dim=10, rank=1, feature_dim=degree+1, device=device)
        offset = PolynomiallyMeasuredTT(ModuleSum(summands, input_dim=npcs, output_dim=10, feature_dim=degree+1, device=device), degree).to(device)
        model = PolynomiallyMeasuredTT(tt, degree, offset=offset).to(device)
        optimizer = optim.Adam(model.parameters(), lr=1e-4)

        if epoch == 0:
            test(args, model, device, test_loader, 0)

        test_losses = deque([float("nan")]*10, maxlen=10)  # len(test_losses) enters the termination criterion
        for epoch in range(epoch+1, args.epochs+1):
            train(args, model, device, train_loader, optimizer, epoch)
            test_loss = test(args, model, device, test_loader, epoch * len(train_loader.dataset))[0]
            test_losses.append(test_loss)
            if args.save_model:
                torch.save(model.state_dict(), f"{experiment_name}/model.pt")

            @define
            def diff():
                ls = torch.tensor(test_losses, dtype=torch.float)
                return torch.sum(ls[1:] - ls[:-1])

            print(f"Estimated change in accuracy (ECA): {ln2*diff:.2e}")
            if diff > diff_lim:
                print(f"ECA exceeds limit: {ln2*diff:.2e} > {diff_lim:.2e}")
                print("Stopping optimization")
                break

        summands.append(tt)
        writer.add_scalar('Loss/rank', test_loss, rank)

    #TODO: when terminating print the arguments that are needed to pick up where you left
