import os, argparse
from collections import deque
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_tt import TorchTT
from torchvision import datasets, transforms
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)  # suppress tensorboard FutureWarning
from torch.utils.tensorboard import SummaryWriter
from fMap import reshape, scatter, MNIST_PCA, MNIST_scaling
from H1ONB import coeffs


file_name = os.path.splitext(os.path.basename(__file__))[0]
experiment_name = f"{file_name}"
os.makedirs(experiment_name, exist_ok=True)
torch.manual_seed(0)
writer = SummaryWriter(f"runs/{experiment_name}/")


def define(fnc):
    return fnc()


scattering_levels = 3  # max value: 6
assert scattering_levels == 3  # otherwise the precomputed values are wrong
ln2 = float(np.log(2))
# terminate after deceeding an estimated increase of 1% (1e-2) accuracy over an interval of 5 epochs
diff_len = 5
diff_lim = -ln2*1e-2
validation_split = 0.15


class PolynomiallyMeasured(nn.Module):
    def __init__(self, module, degrees, output_shape):
        super(PolynomiallyMeasured, self).__init__()
        self.module = module
        self.degrees = degrees
        device = self.module.device
        max_degree = max(degrees)
        self.exponents = torch.arange(max_degree+1, dtype=torch.float, device=device)
        self.coefficients = torch.tensor(coeffs(max_degree), dtype=torch.float, device=device)
        self.output_shape = output_shape

        assert isinstance(module, TorchTT)
        assert all(module.input_dimensions-1 == degrees)

    def forward(self, input_data):
        batch_size = input_data.shape[0]
        assert input_data.shape == (batch_size, len(self.degrees)), f"Shape condition failed: {x.shape} == {(batch_size, len(self.degrees))}"
        data = []
        for x, degree in zip(input_data.T, self.degrees):
            assert x.shape == (batch_size,), f"Shape condition failed: {x.shape} == {(batch_size,)}"
            x = x[:, None] ** self.exponents[:degree+1]
            assert x.shape == (batch_size, degree+1), f"Shape condition failed: {x.shape} == {(batch_size, degree+1)}"
            x = x @ self.coefficients[:degree+1,:degree+1].T
            assert x.shape == (batch_size, degree+1), f"Shape condition failed: {x.shape} == {(batch_size, degree+1)}"
            data.append(x)
        ret = self.module(*data)
        assert ret.shape == (batch_size,) + self.output_shape, f"Shape condition failed: {ret.shape} == {(batch_size,) + self.output_shape}"
        return ret

class Classifier(nn.Module):
    def __init__(self, module, input_shape, output_shape):
        super(Classifier, self).__init__()
        self.module = module
        assert len(input_shape) == 1
        self.input_dimension = input_shape[0]
        assert len(input_shape) == 1
        self.output_dimension = output_shape[0]

    def forward(self, x):
        batch_size = x.shape[0]
        assert x.ndim == 2 and x.shape[1] >= self.input_dimension, f"Shape condition failed: {x.shape} >= {(batch_size, self.input_dimension)}"
        x = x[:,:self.input_dimension]
        ret = self.module(x)
        assert ret.shape == (batch_size, self.output_dimension), f"Shape condition failed: {ret.shape} == {(batch_size, self.output_dimension)}"
        return F.log_softmax(ret, dim=1)

def Classifier_from_TorchTT(tt):
    assert tt.output_dimension == 10
    degrees = tt.input_dimensions-1
    assert len(degrees) > 0 and all(degrees >= 0)
    input_shape = (len(tt.input_dimensions),)
    output_shape = (tt.output_dimension,)
    return Classifier(
        PolynomiallyMeasured(tt, degrees=degrees, output_shape=output_shape),
        input_shape=input_shape,
        output_shape=output_shape
    ).to(device)

def train(model, optimizer, epoch, tag):
    model.train()
    data_points_seen = (epoch-1) * training_set_size
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        data_points_seen += len(data)
        if batch_idx % args.log_interval == 0:
            writer.add_scalar(f'{tag}/train/loss', loss.item(), data_points_seen)
            p_seen = batch_idx*len(data)
            p_loss = loss.item()
            p_seen_perc = 100. * p_seen / training_set_size
            print(f"[{tag}] Train Epoch: {epoch} [{p_seen}/{training_set_size} ({p_seen_perc:5.2f}%)]\tLoss: {p_loss:.6f}")

def train_loop(tt, initial_epoch, tag):
    #TODO: train_loop() hat die gleiche Signatur wie State()
    #      Mach lieber eine State().train()? Oder einen neuen Constructor TrainedState()?
    model = Classifier_from_TorchTT(tt)
    optimizer = optim.Adam(model.parameters(), lr=1e-4)

    losses = deque([float("nan")]*diff_len, maxlen=diff_len)
    for epoch in range(initial_epoch, args.epochs[1]+1):
        train(model, optimizer, epoch, tag)

        assert tt is model.module.module
        state = State(tt, epoch, tag)
        state.log_validation()
        losses.append(state.loss)

        @define
        def diff():
            ls = torch.tensor(losses, dtype=torch.float)
            return torch.sum(ls[1:] - ls[:-1])

        print(f"Estimated change in accuracy (ECA): {ln2*diff:.2e}")
        if diff > diff_lim:
            print(f"ECA exceeds limit: {ln2*diff:.2e} > {diff_lim:.2e}")
            print("Stopping optimization")
            break

    writer.add_scalar(f'{tag}/loss', state.loss, state.parameters)
    writer.add_scalar(f'{tag}/accuracy', state.accuracy, state.parameters)
    return state

def test(model, data_loader):
    model.eval()
    loss = 0
    correct = 0
    count = 0
    with torch.no_grad():
        for data, target in data_loader:
            count += len(data)
            data, target = data.to(device), target.to(device)
            output = model(data)
            loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()
    return loss/count, correct/count


class State(object):
    def __init__(self, tt, epoch, tag):
        assert isinstance(tt, TorchTT)  #TODO: allow to pass models?
        assert isinstance(epoch, int) and epoch >= 0
        assert tag in ["Order", "Degree", "Rank", "Best", None]  #TODO: None???
        self.tt = tt
        self.epoch = epoch
        self.tag = tag
        self.model = Classifier_from_TorchTT(self.tt)
        self.loss, self.accuracy = test(self.model, validation_loader)

    @property
    def order(self): return len(self.tt.dimensions)

    @property
    def degrees(self): return self.tt.input_dimensions-1

    @property
    def ranks(self): return self.tt.ranks

    @property
    def parameters(self):
        return self.tt.count_parameters()

    def save(self):
        assert self.tag == "Best"
        torch.save({
            "state_dict": self.tt.state_dict(),
            "epoch": self.epoch,
            "tag": self.tag
        }, f"{experiment_name}/model.pt")

    @classmethod
    def load(cls, path):
        d = torch.load(path)
        components = [d['state_dict'][f'components.{k}'] for k in range(len(d['state_dict']))]
        tt = TorchTT(components=components)
        return cls(tt, d['epoch'], d['tag'])  # State.epoch contains the number of epochs optimized

    def log_validation(self):
        data_points_seen = self.epoch * training_set_size
        writer.add_scalar(f'{self.tag}/validation/loss', self.loss, data_points_seen)
        writer.add_scalar(f'{self.tag}/validation/accuracy', self.accuracy, data_points_seen)
        print(f"\n[{self.tag}] Validation set: Average loss: {self.loss:.4f}, Accuracy: {100*self.accuracy:5.2f}%\n")

    def log_test(self):
        assert self.tag == "Best"
        model = self.model
        test_loss, test_acc = test(model, test_loader)
        writer.add_scalar('Parameters/test/loss', test_loss, self.parameters)
        writer.add_scalar('Parameters/test/accuracy', test_acc, self.parameters)
        print(f"\nTest set: Average loss: {test_loss:.4f}, Accuracy: {100*test_acc:5.2f}%\n")

def update_order(initial_state):
    tt = initial_state.tt
    if len(tt.input_dimensions) < args.orders[1]:  # consistent with initial_state.order
        tt = TorchTT(input_dimensions = tt.input_dimensions.tolist()+[2],
                     output_dimension = tt.output_dimension,
                     ranks            = tt.ranks.tolist()+[1],
                     components       = tt.components,
                     device           = tt.device)
        print(f"Increase order: {len(initial_state.tt.input_dimensions)} --> {len(tt.input_dimensions)}")
        # consistent with initial_state.order
        print(f"Change in parameters: {initial_state.tt.count_parameters()} --> {tt.count_parameters()}")
        return train_loop(tt, initial_state.epoch+1, tag="Order")
    else:
        return initial_state

CDI = 1  # Current Degree Index
def update_degree(initial_state):
    global CDI
    assert 1 <= CDI < len(initial_state.tt.components)  # the first dimension is always 10
    assert all(initial_state.degrees <= args.degrees[1])
    # Da die Relevanz der Komponenten abklingt nehmen wir an, 
    # dass die Sequenzen der optimalen Grade und Ränge monoton fallend sind.

    def test_degree(idx):
        assert 1 <= idx
        if idx >= len(initial_state.tt.components):
            return initial_state
        tt = initial_state.tt
        if tt.dimensions[idx] >= args.degrees[1]+1:
            return initial_state
        #TODO: Don't distinguish between output- and input-dimensions?
        new_input_dimensions = tt.input_dimensions
        new_input_dimensions[idx-1] += 1  # dimensions == [output_dimension] + input_dimensions --> idx-1
        tt = TorchTT(input_dimensions = new_input_dimensions,
                     output_dimension = tt.output_dimension,
                     ranks            = tt.ranks,
                     components       = tt.components,
                     device           = tt.device)
        print(f"Increase degree in Mode {idx}: {initial_state.tt.dimensions[idx]} --> {tt.dimensions[idx]}")
        print(f"Change in parameters: {initial_state.tt.count_parameters()} --> {tt.count_parameters()}")
        return train_loop(tt, initial_state.epoch+1, tag="Degree")

    # Naïver ansatz:
    current = test_degree(CDI)
    right = test_degree(CDI+1)
    if current.accuracy > right.accuracy:
        while CDI > 1 and current.accuracy > right.accuracy:
            CDI -= 1
            current, right = test_degree(CDI), current
    else:
        current, right = right, test_degree(CDI+2)

    if current.accuracy > right.accuracy:
        return current
    else:
        CDI += 1
        return right

CRI = 1  # Current Rank Index
def update_rank(initial_state):
    global CRI
    #TODO: Why do we need state.ranks?
    assert 1 <= CRI < len(initial_state.tt.ranks)-1  # the first and last rank are always 1
    assert all(initial_state.tt.ranks <= args.ranks[1])
    # Da die Relevanz der Komponenten abklingt nehmen wir an, 
    # dass die Sequenzen der optimalen Grade und Ränge monoton fallend sind.

    def test_rank(idx):
        assert 1 <= idx
        if idx >= len(initial_state.tt.ranks)-1:
            return initial_state
        tt = initial_state.tt
        if tt.ranks[idx] >= args.ranks[1]:
            return initial_state
        new_ranks = tt.ranks
        new_ranks[idx] += 1
        tt = TorchTT(input_dimensions = tt.input_dimensions,
                     output_dimension = tt.output_dimension,
                     ranks            = new_ranks,
                     components       = tt.components,
                     device           = tt.device)
        print(f"Increase Rank between Modes {idx-1}--{idx}: {initial_state.tt.ranks[idx]} --> {tt.ranks[idx]}")
        print(f"Change in parameters: {initial_state.tt.count_parameters()} --> {tt.count_parameters()}")
        return train_loop(tt, initial_state.epoch+1, tag="Degree")

    # Naïver ansatz:
    current = test_rank(CRI)
    right = test_rank(CRI+1)
    if current.accuracy > right.accuracy:
        while CRI > 1 and current.accuracy > right.accuracy:
            CRI -= 1
            current, right = test_rank(CRI), current
    else:
        current, right = right, test_rank(CRI+2)

    if current.accuracy > right.accuracy:
        return current
    else:
        CRI += 1
        return right


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example using adaptive Tensor Trains')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', nargs=2, type=int, default=[1,200],
                        help='range of epochs to train (inclusive, default: [1,200])')
    parser.add_argument('--degrees', nargs=2, type=int, default=[0,5],
                        help='range of degrees to train (inclusive, default: [0,5])')
    parser.add_argument('--ranks', nargs=2, type=int, default=[0,40],
                        help='range of ranks to train (inclusive, default: [1,40])')
    parser.add_argument('--orders', nargs=2, type=int, default=[2,81],
                        help='range of orders to train (inclusive, default: [2,81])')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--gpu', type=int, default=0,
                        help='GPU to use (use -1 for CPU)')
    args = parser.parse_args()

    print("Arguments:")
    mlk = max(map(len, args.__dict__.keys()))
    for k,v in args.__dict__.items():
        space = " "*(mlk-len(str(k))+1)
        print(f"    {k}:{space}{v}")

    use_cuda = args.gpu >= 0 and torch.cuda.is_available()
    if args.gpu >= torch.cuda.device_count():
        raise RuntimeError(f"GPU index too large ({args.gpu} > {torch.cuda.device_count()-1})")
    device = torch.device(f"cuda:{args.gpu}" if use_cuda else "cpu")
    print(f"\nRunning on device:    {device}\n")

    trafo_list = [transforms.ToTensor(),  # converts PIL images with shape (H,W,C) and range [0,255] to tensors with shape (C,H,W) and range [0,1]
                  transforms.Lambda(reshape(28,28)),
                  transforms.Lambda(scatter("haar", scattering_levels))]
    trafo_list += [MNIST_PCA(trafo_list, device, file_name).transform]
    trafo_list += [MNIST_scaling(trafo_list, device, file_name).transform]

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    mnist_train = datasets.MNIST('data', train=True, download=True, transform=transforms.Compose(trafo_list))
    indices = np.arange(len(mnist_train))
    np.random.shuffle(indices)
    validation_set_size = int(validation_split*len(mnist_train))
    training_set_size = len(mnist_train) - validation_set_size
    assert len(indices[validation_set_size:]) == training_set_size
    train_sampler = torch.utils.data.SubsetRandomSampler(indices[validation_set_size:])
    train_loader = torch.utils.data.DataLoader(mnist_train, batch_size=args.batch_size, sampler=train_sampler, **kwargs)
    assert len(indices[:validation_set_size]) == validation_set_size
    validation_sampler = torch.utils.data.SubsetRandomSampler(indices[:validation_set_size])
    validation_loader = torch.utils.data.DataLoader(mnist_train, batch_size=args.batch_size, sampler=validation_sampler, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=False, transform=transforms.Compose(trafo_list)),
        batch_size=args.test_batch_size, shuffle=False, **kwargs)
    test_set_size = len(test_loader.dataset)

    print(f"""
Training set size:    {training_set_size}
Validation set size:  {validation_set_size}
Test set size:        {test_set_size}

Image dimension:      {28**2}
Scattering dimension: {next(iter(train_loader))[0].shape[1]}
""")

    assert 1 <= args.epochs[0] <= args.epochs[1]
    if args.epochs[0] == 1:
        @define
        def state():
            tt = TorchTT(input_dimensions=[1], output_dimension=10, ranks=[1,1,1], device=device)
            state = State(tt, 0, None)  # State.epoch contains the number of epochs optimized
            print()
            for tag in "Modes Degree Rank".split():
                state.tag = tag
                state.log_validation()
            print()
            state.tag = "Best"
            state.log_test()
            return state
        assert state.epoch+1 == args.epochs[0]
        state = update_degree(state)
        state.tag = "Best"
        state.save()
        state.log_test()
    else:
        state = State.load(f"{experiment_name}/model.pt")
        assert state.epoch+1 == args.epochs[0]

    assert args.orders[0] <= state.order <= args.orders[1]
    assert min(state.degrees) >= args.degrees[0] and max(state.degrees) <= args.degrees[1]
    assert min(state.ranks) >= args.ranks[0] and max(state.ranks) <= args.ranks[1]

    while state.epoch < args.epochs[1]:
        print(f"""
Order:              {state.order}
Polynomial degrees: {state.degrees.tolist()}
Ranks:              {state.ranks.tolist()}
""")

        def state_diff(st):
            # perf_diff = state.loss - st.loss
            perf_diff = st.accuracy - state.accuracy
            dofs_diff = st.parameters - state.parameters
            return perf_diff / (dofs_diff+1)

        new_state = max(
            update_order(state),
            update_degree(state),
            update_rank(state),
            key=state_diff
        )
        new_state.tag = "Best"

        if new_state.epoch == state.epoch:
            print("Optimization does not yield any improvement")
            print("Stopping adaptivity")
            break

        state = new_state
        state.save()
        state.log_test()
