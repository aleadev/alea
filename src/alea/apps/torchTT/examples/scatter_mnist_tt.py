import os
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_tt import TorchTT
from torchvision import datasets, transforms
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)  # suppress tensorboard FutureWarning
from torch.utils.tensorboard import SummaryWriter


degree = 4
rank = 10
scattering_levels = 3  # max value: 6
npcs = 20
assert scattering_levels == 3 and npcs == 20  # otherwise the precomputed values are wrong


file_name = os.path.splitext(os.path.basename(__file__))[0]
experiment_name = f"{file_name}/rank-{rank}_degree-{degree}"  # {file_name}/... is degree 2 and rank 20; {file_name}/degree_{degree} is rank 20
os.makedirs(experiment_name)


seed = 0
writer = SummaryWriter(f"runs/{experiment_name}")


def compute(fnc):
    try:
        return torch.load(f"{file_name}/{fnc.__name__}.pt")
    except FileNotFoundError:
        print(f"Computing {fnc.__name__}")
        ret = fnc()
        if not os.path.isdir(f"{file_name}"):
            os.mkdir(f"{file_name}")
        torch.save(ret, f"{file_name}/{fnc.__name__}.pt")
        return ret


class PolynomiallyMeasuredTT(nn.Module):
    def __init__(self, tt, degree):
        super(PolynomiallyMeasuredTT, self).__init__()
        assert tt.feature_dim == degree+1, f"{tt.feature_dim} vs {degree+1}"
        self.tt = tt
        self.degree = degree

    def forward(self, x):
        assert x.ndim == 2 and x.shape[1] == self.tt.input_dim  # (batch_size, input_dim)
        x = x[:, :, None] ** torch.arange(self.degree+1, dtype=torch.float, device=self.tt.device)  #TODO: device?
        x = self.tt(x)
        return F.log_softmax(x, dim=1)

def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    data_points_seen = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        data_points_seen += len(data)
        if batch_idx % args.log_interval == 0:
            writer.add_scalar('Loss/train',
                              loss.item(), # running_loss / 1000,
                              epoch * len(train_loader.dataset) + data_points_seen)
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

def test(args, model, device, test_loader, data_points_seen):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    writer.add_scalar('Loss/test',
                      test_loss,
                      data_points_seen)
    writer.add_scalar('Accuracy/test',
                      correct/len(test_loader.dataset),
                      data_points_seen)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the Model')
    parser.add_argument('--load-model', action='store_true', default=False,
                        help='For Loading the Model')
    parser.add_argument('--initial-epoch', type=int, default=1,
                        help='Initial epoch when loading the Model')
    parser.add_argument('--gpu', type=int, default=0,
                        help='GPU to use')
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(seed)

    device = torch.device(f"cuda:{args.gpu}" if use_cuda else "cpu")
    print("Running on device:", device) # (f"cuda:{args.gpu}" if use_cuda else "cpu"))

    def reshape(*shape):
        def reshape(x):
            assert x.shape == (1,28,28)
            return torch.reshape(x, shape)
        return reshape

    def scatter(wave, num_levels):
        import pywt
        assert isinstance(wave, str)
        try: pywt.Wavelet(wave)
        except: raise ValueError("Feature map '%s' is not supported"%wave)
        # assert isinstance(num_levels, int) and num_levels >= 0
        assert num_levels >= 0

        filter = lambda image: (lambda lowpass, others: [lowpass]+list(others))(*pywt.dwt2(image, wave))
        abs_filter = lambda image: map(abs, filter(image))
        lowpass = lambda image: filter(image)[0]

        def concatenate(ls):
            return sum(map(list, ls), [])

        def next_level(level):
            return concatenate(abs_filter(image) for image in level)

        def wrapped(x):
            ret = []
            level = [x]  # the first scattering level contains just the image
            ret.extend(map(lowpass, level))
            # ret.extend(level)
            for level_idx in range(1, num_levels):
                if level[0].size == 1:
                    break
                level = next_level(level)
                ret.extend(map(lowpass, level))
                # ret.extend(level)
            ret = torch.cat([torch.from_numpy(r.reshape(-1)) for r in ret])
            return ret

        wrapped.__name__ = f"scatter_{wave.lower()}_{num_levels}"
        return wrapped

    #TODO: orthonormalization?
    trafo_list = [transforms.ToTensor(),  # converts PIL images with shape (H,W,C) and range [0,255] to tensors with shape (C,H,W) and range [0,1]
                  transforms.Lambda(reshape(28,28)),
                  transforms.Lambda(scatter("haar", scattering_levels))]
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    pca_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=True, download=True,
                    transform=transforms.Compose(trafo_list)),
        batch_size=args.batch_size, shuffle=True, drop_last=True, **kwargs)

    image_size = 28**2
    scatter_size = next(iter(pca_loader))[0].shape[1]

    print("Image dimension:     ", image_size)
    print("Scattering dimension:", scatter_size)
    print("Reduced dimension:   ", npcs)
    print()
    print("Polynomial degree:   ", degree)
    print("Rank:                ", rank)

    @compute
    def mean():
        mean = torch.zeros((scatter_size,), dtype=torch.float, device=device)
        for batch_idx, (data, target) in enumerate(pca_loader):
            assert data.shape == (args.batch_size, scatter_size), f"{data.shape} vs ({args.batch_size}, {scatter_size})"
            data = data.to(device)
            mean += torch.mean(data, dim=0)
            if batch_idx % args.log_interval == 0:
                n_batches = len(pca_loader.dataset)//args.batch_size
                print(f"Computing mean: {batch_idx//args.log_interval+1}/{n_batches//args.log_interval+1}")
        mean /= (batch_idx + 1)
        return mean

    @compute
    def cov():
        cov = torch.zeros((scatter_size, scatter_size), dtype=torch.float, device=device)
        for batch_idx, (data, target) in enumerate(pca_loader):
            assert data.shape == (args.batch_size, scatter_size)
            data = data.to(device) - mean
            cov += (data.T @ data) / args.batch_size
            if batch_idx % args.log_interval == 0:
                n_batches = len(pca_loader.dataset)//args.batch_size
                print(f"Computing cov: {batch_idx//args.log_interval+1}/{n_batches//args.log_interval+1}")
        cov /= (batch_idx + 1)
        return (cov + cov.T)/2

    @compute
    def projection():
        es,vs = torch.eig(cov, eigenvectors=True)
        # assert torch.all(abs(es[:,1]) < 1e-8)
        es = es[:,0]
        # assert torch.norm((vs * es) @ vs.T - cov) < 1e-4
        return vs.T[:20]
    assert projection.shape == (npcs, scatter_size)

    mean = mean.to('cpu')
    projection = projection.to('cpu')
    def pca(x):
        return projection @ (x - mean)

    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=True, download=True, transform=transforms.Compose(trafo_list + [transforms.Lambda(pca)])),
        batch_size=args.batch_size, shuffle=True, drop_last=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=False, transform=transforms.Compose(trafo_list + [transforms.Lambda(pca)])),
        batch_size=args.test_batch_size, shuffle=False, **kwargs)


    tt = TorchTT(input_dim=npcs, output_dim=10, rank=rank, feature_dim=degree+1,
                 components=None, device=device)
    model = PolynomiallyMeasuredTT(tt, degree).to(device)
    # optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)
    if args.load_model:
        model.load_state_dict(torch.load(f"{experiment_name}/model.pt"))
    optimizer = optim.Adam(model.parameters(), lr=1e-4)

    test(args, model, device, test_loader, (args.initial_epoch-1) * len(train_loader.dataset))  #TODO: epoch?
    for epoch in range(args.initial_epoch, args.initial_epoch + args.epochs):
        train(args, model, device, train_loader, optimizer, epoch)
        test(args, model, device, test_loader, epoch * len(train_loader.dataset))  #TODO: epoch?
        if args.save_model:
            torch.save(model.state_dict(), f"{experiment_name}/model.pt")
