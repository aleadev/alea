import os, argparse
from collections import deque
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_tt import TorchTT
from torchvision import datasets, transforms
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)  # suppress tensorboard FutureWarning
from torch.utils.tensorboard import SummaryWriter
from fMap import reshape, scatter, MNIST_PCA


degree = 2
rank = 20
scattering_levels = 3  # max value: 6
npcs = 80
assert scattering_levels == 3  # otherwise the precomputed values are wrong


file_name = os.path.splitext(os.path.basename(__file__))[0]
experiment_name = f"{file_name}/rank-{rank}_degree-{degree}"  # {file_name}/... is degree 2 and rank 20; {file_name}/degree_{degree} is rank 20
os.makedirs(experiment_name, exist_ok=True)


torch.manual_seed(0)
writer = SummaryWriter(f"runs/{experiment_name}")


def define(fnc):
    return fnc()

#TODO: add kw-argument to enable/disable caching --- for this you should use a decorator-decorator
def compute(fnc):
    try:
        return torch.load(f"{file_name}/{fnc.__name__}.pt")
    except FileNotFoundError:
        print(f"Computing {fnc.__name__}")
        ret = fnc()
        if not os.path.isdir(f"{file_name}"):
            os.mkdir(f"{file_name}")
        torch.save(ret, f"{file_name}/{fnc.__name__}.pt")
        return ret

log = lambda x: float(torch.log(torch.tensor(float(x))))
ln2 = log(2)

class PolynomiallyMeasuredTT(nn.Module):
    def __init__(self, tt, degree):
        super(PolynomiallyMeasuredTT, self).__init__()
        assert tt.feature_dim == degree+1, f"{tt.feature_dim} vs {degree+1}"
        self.tt = tt
        self.degree = degree

    def forward(self, x):
        assert x.ndim == 2 and x.shape[1] == self.tt.input_dim  # (batch_size, input_dim)
        x = x[:, :, None] ** torch.arange(self.degree+1, dtype=torch.float, device=self.tt.device)  #TODO: device?
        x = self.tt(x)
        return F.log_softmax(x, dim=1)

def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    data_points_seen = (epoch-1) * len(train_loader.dataset)
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        data_points_seen += len(data)
        if batch_idx % args.log_interval == 0:
            writer.add_scalar('Loss/train', loss.item(), data_points_seen)  #TODO: epoch?
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

def test(args, model, device, test_loader, data_points_seen):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    writer.add_scalar('Loss/test', test_loss, data_points_seen)
    writer.add_scalar('Accuracy/test', correct/len(test_loader.dataset), data_points_seen)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

    return test_loss, correct/len(test_loader.dataset)

if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the Model')
    parser.add_argument('--load-model', action='store_true', default=False,
                        help='For Loading the Model')
    parser.add_argument('--gpu', type=int, default=0,
                        help='GPU to use (use -1 for CPU)')
    args = parser.parse_args()
    use_cuda = args.gpu >= 0 and torch.cuda.is_available()
    if args.gpu >= torch.cuda.device_count():
        raise RuntimeError(f"GPU index too large ({args.gpu} > {torch.cuda.device_count()-1})")

    device = torch.device(f"cuda:{args.gpu}" if use_cuda else "cpu")
    print("Running on device:   ", device)
    print()

    trafo_list = [transforms.ToTensor(),  # converts PIL images with shape (H,W,C) and range [0,255] to tensors with shape (C,H,W) and range [0,1]
                  transforms.Lambda(reshape(28,28)),
                  transforms.Lambda(scatter("haar", scattering_levels))]

    mnist_pca = MNIST_PCA(trafo_list, device, file_name)

    image_size = 28**2
    scatter_size = mnist_pca.data_shape[1]

    mean = mnist_pca.mean.to('cpu')
    eigenvectors = mnist_pca.eigenvectors.to('cpu')

    epoch = 0
    components = None
    # terminate after deceeding an estimated increase of 1% (1e-2) accuracy over the measurement interval
    diff_lim = -ln2*1e-2
    for npcs in range(1, 81):
        projection = eigenvectors[:npcs]
        def pca(x):
            return projection @ (x - mean)

        print(f"""
Image dimension:      {image_size}
Scattering dimension: {scatter_size}
Reduced dimension:    {npcs}

Polynomial degree:    {degree}
Rank:                 {rank}
""")

        kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
        train_loader = torch.utils.data.DataLoader(
            datasets.MNIST('data', train=True, download=True, transform=transforms.Compose(trafo_list + [transforms.Lambda(pca)])),
            batch_size=args.batch_size, shuffle=True, **kwargs)
        test_loader = torch.utils.data.DataLoader(
            datasets.MNIST('data', train=False, transform=transforms.Compose(trafo_list + [transforms.Lambda(pca)])),
            batch_size=args.test_batch_size, shuffle=False, **kwargs)


        tt = TorchTT(input_dim=npcs, output_dim=10, rank=rank, feature_dim=degree+1,
                     components=components, device=device)
        model = PolynomiallyMeasuredTT(tt, degree).to(device)
        # if args.load_model:
        #     model.load_state_dict(torch.load(f"{experiment_name}/model.pt"))
        # optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)
        optimizer = optim.Adam(model.parameters(), lr=1e-4)

        if epoch == 0:
            test(args, model, device, test_loader, 0)

        test_losses = deque([float("nan")]*10, maxlen=10)  # len(test_losses) enters the termination criterion
        for epoch in range(epoch+1, args.epochs+1):
            train(args, model, device, train_loader, optimizer, epoch)
            test_loss = test(args, model, device, test_loader, epoch * len(train_loader.dataset))[0]
            test_losses.append(test_loss)
            if args.save_model:
                torch.save(model.state_dict(), f"{experiment_name}/model.pt")

            @define
            def diff():
                ls = torch.tensor(test_losses, dtype=torch.float)
                return torch.sum(ls[1:] - ls[:-1])

            print(f"Estimated change in accuracy (ECA): {ln2*diff:.2e}")
            if diff > diff_lim:
                print(f"ECA exceeds limit: {ln2*diff:.2e} > {diff_lim:.2e}")
                print("Stopping optimization")
                break

        components = tt.components
        writer.add_scalar('Loss/modes', test_loss, npcs+1)
