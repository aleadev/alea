import os, argparse
from collections import deque
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_tt import TorchTT
from torchvision import datasets, transforms
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)  # suppress tensorboard FutureWarning
from torch.utils.tensorboard import SummaryWriter
from fMap import reshape, scatter, MNIST_PCA, MNIST_scaling
from H1ONB import coeffs


file_name = os.path.splitext(os.path.basename(__file__))[0]
experiment_name = f"{file_name}/"  # {file_name}/... is degree 2 and rank 20; {file_name}/degree_{degree} is rank 20
os.makedirs(experiment_name, exist_ok=True)
torch.manual_seed(0)
writer = SummaryWriter(f"runs/{experiment_name}")


def define(fnc):
    return fnc()


scattering_levels = 3  # max value: 6
assert scattering_levels == 3  # otherwise the precomputed values are wrong
ln2 = float(np.log(2))
# terminate after deceeding an estimated increase of 1% (1e-2) accuracy over an interval of 5 epochs
diff_len = 5
diff_lim = -ln2*1e-2
validation_split = 0.15


class PolynomiallyMeasured(nn.Module):
    def __init__(self, module, degree=0):
        super(PolynomiallyMeasured, self).__init__()
        self.module = module
        self.degree = degree
        device = self.module.device
        self.exponents = torch.arange(degree+1, dtype=torch.float, device=device)
        self.coefficients = torch.tensor(coeffs(degree), dtype=torch.float, device=device)
        self.input_shape = (module.input_dim,)  #TODO: generalize (works only for TorchTT)
        self.output_shape = (module.output_dim,)  #TODO: generalize

    def forward(self, x):
        batch_size = x.shape[0]
        assert x.shape == (batch_size,) + self.input_shape, f"{x.shape} vs {(batch_size,) + self.input_shape}"
        x = x[:, :, None] ** self.exponents
        # x = torch.einsum('oi,bdi -> bdo', self.coefficients, x)
        x = torch.tensordot(x, self.coefficients, dims=([-1], [-1]))
        assert x.shape == (batch_size,) + self.input_shape + (self.degree+1,), f"{x.shape} vs {(batch_size,) + self.input_shape + (self.degree+1,)}"  #TODO: generalize
        ret = self.module(x)
        assert ret.shape == (batch_size,) + self.output_shape, f"{ret.shape} vs {(batch_size,) + self.output_shape}"
        return ret

class Classifier(nn.Module):
    def __init__(self, module):
        super(Classifier, self).__init__()
        self.module = module
        self.input_shape = module.input_shape
        self.output_shape = module.output_shape

    def forward(self, x):
        batch_size = x.shape[0]
        assert x.ndim == 2 and len(self.input_shape) == 1
        input_dim = self.input_shape[0]
        if x.shape[1] > input_dim:
            x = x[:,:input_dim]
        assert x.shape == (batch_size, input_dim), f"{x.shape} vs {(batch_size,) + self.input_shape}"  #TODO: adaptivity in modes ...
        ret = F.log_softmax(self.module(x), dim=1)
        assert ret.shape == (batch_size,) + self.output_shape, f"{ret.shape} vs {(batch_size,) + self.output_shape}"
        return ret


def train(model, optimizer, epoch, tag):
    model.train()
    data_points_seen = (epoch-1) * training_set_size
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        data_points_seen += len(data)
        if batch_idx % args.log_interval == 0:
            writer.add_scalar(f'{tag}/train/loss', loss.item(), data_points_seen)
            p_seen = batch_idx*len(data)
            p_loss = loss.item()
            p_seen_perc = 100. * p_seen / training_set_size
            print(f"[{tag}] Train Epoch: {epoch} [{p_seen}/{training_set_size} ({p_seen_perc:5.2f}%)]\tLoss: {p_loss:.6f}")

def test(model, data_loader):
    model.eval()
    loss = 0
    correct = 0
    count = 0
    with torch.no_grad():
        for data, target in data_loader:
            count += len(data)
            data, target = data.to(device), target.to(device)
            output = model(data)
            loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()
    return loss/count, correct/count

def train_loop(tt, initial_epoch, tag):
    degree = tt.feature_dim-1
    assert degree >= 0
    model = Classifier(PolynomiallyMeasured(tt, degree=degree)).to(device)
    optimizer = optim.Adam(model.parameters(), lr=1e-4)

    test_losses = deque([float("nan")]*diff_len, maxlen=diff_len)
    for epoch in range(initial_epoch, args.epochs[1]+1):
        train(model, optimizer, epoch, tag)
        val_loss, val_acc = test(model, validation_loader)
        data_points_seen = epoch * training_set_size
        writer.add_scalar(f'{tag}/validation/loss', val_loss, data_points_seen)
        writer.add_scalar(f'{tag}/validation/accuracy', val_acc, data_points_seen)
        print(f"\n[{tag}] Validation set: Average loss: {val_loss:.4f}, Accuracy: {100*val_acc:5.2f}%\n")
        test_losses.append(val_loss)

        torch.save(model.state_dict(), f"{experiment_name}/model.pt")

        @define
        def diff():
            ls = torch.tensor(test_losses, dtype=torch.float)
            return torch.sum(ls[1:] - ls[:-1])

        print(f"Estimated change in accuracy (ECA): {ln2*diff:.2e}")
        if diff > diff_lim:
            print(f"ECA exceeds limit: {ln2*diff:.2e} > {diff_lim:.2e}")
            print("Stopping optimization")
            break

    assert tt is model.module.module
    return State(tt, val_loss, epoch)



class State(object):
    def __init__(self, tt, loss, epoch):
        self.tt = tt
        self.loss = loss
        self.epoch = epoch

    @property
    def modes(self): return self.tt.input_dim+1

    @property
    def degree(self): return self.tt.feature_dim-1

    @property
    def rank(self): return self.tt.rank

    @property
    def parameters(self):
        rank = self.tt.rank
        dimension = self.tt.feature_dim
        modes = self.tt.input_dim+1
        return 10*rank + (modes-2)*dimension*rank**2 + rank*dimension

def update_modes(initial_state):
    tt = initial_state.tt
    degree = lambda tt: tt.feature_dim-1
    if tt.input_dim < args.modes[1]:
        old_modes = tt.input_dim+1
        old_degree = degree(tt)
        old_rank = tt.rank
        tt = TorchTT(input_dim   = tt.input_dim+1,
                     output_dim  = tt.output_dim,
                     rank        = tt.rank,
                     feature_dim = tt.feature_dim,
                     components  = tt.components,
                     device      = tt.device)
        print(f"update_modes: [{old_modes} Modes | Degree {old_degree} | Rank {old_rank}] --> [{tt.input_dim+1} Modes | Degree {degree(tt)} | Rank {tt.rank}]")
        final_state = train_loop(tt, initial_state.epoch+1, tag="Modes")
        writer.add_scalar('Modes/loss', final_state.loss, tt.input_dim+1)
        return final_state
    else:
        return initial_state

def update_degree(initial_state):
    tt = initial_state.tt
    degree = lambda tt: tt.feature_dim-1
    if degree(tt) < args.degrees[1]:
        old_modes = tt.input_dim+1
        old_degree = degree(tt)
        old_rank = tt.rank
        tt = TorchTT(input_dim   = tt.input_dim,
                     output_dim  = tt.output_dim,
                     rank        = tt.rank,
                     feature_dim = degree(tt)+2,
                     components  = tt.components,
                     device      = tt.device)
        print(f"update_degree: [{old_modes} Modes | Degree {old_degree} | Rank {old_rank}] --> [{tt.input_dim+1} Modes | Degree {degree(tt)} | Rank {tt.rank}]")
        final_state = train_loop(tt, initial_state.epoch+1, tag="Degree")
        writer.add_scalar('Degree/loss', final_state.loss, degree(tt))
        return final_state
    else:
        return initial_state

def update_rank(initial_state):
    #TODO: This may be unstable since the update_rank.py optimized only the added rank-1 tensor
    #      and not the complete tensor (see examples.update_rank.ModuleSum).
    tt = initial_state.tt
    degree = lambda tt: tt.feature_dim-1
    if tt.rank < args.ranks[1]:
        old_modes = tt.input_dim+1
        old_degree = degree(tt)
        old_rank = tt.rank
        tt = TorchTT(input_dim   = tt.input_dim,
                     output_dim  = tt.output_dim,
                     rank        = tt.rank+1,
                     feature_dim = tt.feature_dim,
                     components  = tt.components,
                     device      = tt.device)
        print(f"update_rank: [{old_modes} Modes | Degree {old_degree} | Rank {old_rank}] --> [{tt.input_dim+1} Modes | Degree {degree(tt)} | Rank {tt.rank}]")
        final_state = train_loop(tt, initial_state.epoch+1, tag="Rank")
        writer.add_scalar('Rank/loss', final_state.loss, tt.rank)
        return final_state
    else:
        return initial_state

def test_state(state):
    degree = state.tt.feature_dim-1
    assert degree >= 0
    model = Classifier(PolynomiallyMeasured(state.tt, degree=degree)).to(device)
    test_loss, test_acc = test(model, test_loader)
    writer.add_scalar('Parameters/test/loss', test_loss, state.parameters)
    writer.add_scalar('Parameters/test/accuracy', test_acc, state.parameters)
    print(f"\nTest set: Average loss: {test_loss:.4f}, Accuracy: {100*test_acc:5.2f}%\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example using adaptive Tensor Trains')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', nargs=2, type=int, default=[1,200],
                        help='range of epochs to train (inclusive, default: [1,200])')
    parser.add_argument('--degrees', nargs=2, type=int, default=[0,5],
                        help='range of degrees to train (inclusive, default: [0,5])')
    parser.add_argument('--ranks', nargs=2, type=int, default=[0,40],
                        help='range of ranks to train (inclusive, default: [1,40])')
    parser.add_argument('--modes', nargs=2, type=int, default=[1,80],
                        help='range of modes to train (inclusive, default: [1,80])')
    # parser.add_argument('--save-model', action='store_true', default=False,
    #                     help='For Saving the Model')
    # parser.add_argument('--load-model', action='store_true', default=False,
    #                     help='For Loading the Model')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--gpu', type=int, default=0,
                        help='GPU to use (use -1 for CPU)')
    args = parser.parse_args()

    print("Arguments:")
    mlk = max(map(len, args.__dict__.keys()))
    for k,v in args.__dict__.items():
        space = " "*(mlk-len(str(k))+1)
        print(f"    {k}:{space}{v}")

    use_cuda = args.gpu >= 0 and torch.cuda.is_available()
    if args.gpu >= torch.cuda.device_count():
        raise RuntimeError(f"GPU index too large ({args.gpu} > {torch.cuda.device_count()-1})")
    device = torch.device(f"cuda:{args.gpu}" if use_cuda else "cpu")
    print(f"\nRunning on device:    {device}\n")

    trafo_list = [transforms.ToTensor(),  # converts PIL images with shape (H,W,C) and range [0,255] to tensors with shape (C,H,W) and range [0,1]
                  transforms.Lambda(reshape(28,28)),
                  transforms.Lambda(scatter("haar", scattering_levels))]
    trafo_list += [MNIST_PCA(trafo_list, device, file_name).transform]
    trafo_list += [MNIST_scaling(trafo_list, device, file_name).transform]

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    mnist_train = datasets.MNIST('data', train=True, download=True, transform=transforms.Compose(trafo_list))
    indices = np.arange(len(mnist_train))
    np.random.shuffle(indices)
    validation_set_size = int(validation_split*len(mnist_train))
    training_set_size = len(mnist_train) - validation_set_size
    assert len(indices[validation_set_size:]) == training_set_size
    train_sampler = torch.utils.data.SubsetRandomSampler(indices[validation_set_size:])
    train_loader = torch.utils.data.DataLoader(mnist_train, batch_size=args.batch_size, sampler=train_sampler, **kwargs)
    assert len(indices[:validation_set_size]) == validation_set_size
    validation_sampler = torch.utils.data.SubsetRandomSampler(indices[:validation_set_size])
    validation_loader = torch.utils.data.DataLoader(mnist_train, batch_size=args.batch_size, sampler=validation_sampler, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=False, transform=transforms.Compose(trafo_list)),
        batch_size=args.test_batch_size, shuffle=False, **kwargs)
    test_set_size = len(test_loader.dataset)

    print(f"""
Training set size:    {training_set_size}
Validation set size:  {validation_set_size}
Test set size:        {test_set_size}

Image dimension:      {28**2}
Scattering dimension: {next(iter(train_loader))[0].shape[1]}
""")

    if args.epochs[0] == 1:
        @define
        def state():
            tt = TorchTT(input_dim=1, output_dim=10, rank=1, feature_dim=1, device=device)
            degree = tt.feature_dim-1
            assert degree >= 0
            model = Classifier(PolynomiallyMeasured(tt, degree=degree)).to(device)

            val_loss, val_acc = test(model, validation_loader)
            print()
            for tag in "Modes Degree Rank".split():
                writer.add_scalar(f'{tag}/validation/loss', val_loss, 0)
                writer.add_scalar(f'{tag}/validation/accuracy', val_acc, 0)
                print(f"[{tag}] Validation set: Average loss: {val_loss:.4f}, Accuracy: {100*val_acc:5.2f}%")
            print()

            state = State(tt, val_loss, 0)  # State.epoch contains the number of epochs optimized
            return state
        test_state(state)
    else:
        @define
        def state():
            state_dict = torch.load(f"{experiment_name}/model.pt")
            ranks = []
            dimensions = []
            comp = state_dict['module.module.components.0']
            ranks.append(comp.shape[0])
            for k in range(len(state_dict)):
                comp = state_dict[f'module.module.components.{k}']
                assert comp.ndim == 3
                dimensions.append(comp.shape[1])
                ranks.append(comp.shape[2])
            output_dim = 10
            assert dimensions.pop(0) == output_dim
            input_dim = len(dimensions)
            feature_dim = dimensions[0]
            assert all(d == feature_dim for d in dimensions)
            assert ranks.pop(0) == 1 == ranks.pop(-1)
            rank = ranks[0]
            assert all(r == rank for r in ranks)
            tt = TorchTT(input_dim=input_dim, output_dim=output_dim, rank=rank, feature_dim=feature_dim, device=device)
            degree = tt.feature_dim-1
            assert degree >= 0
            model = Classifier(PolynomiallyMeasured(tt, degree=degree)).to(device)
            model.load_state_dict(state_dict)

            val_loss, val_acc = test(model, validation_loader)
            state = State(tt, val_loss, args.epochs[0]-1)  # State.epoch contains the number of epochs optimized
            return state

    while state.epoch < args.epochs[1]:
        print(f"""
Modes:             {state.modes}
Polynomial degree: {state.degree}
Rank:              {state.rank}
""")

        print("""
        Du solltest inhomogene Ränge und Polynomgrade erlauben.
        """)

        def state_diff(st):
            loss_diff = state.loss - st.loss
            # dofs_diff = max(st.parameters - state.parameters, 1)
            dofs_diff = st.parameters - state.parameters
            return loss_diff / (dofs_diff+1)  #TODO: accs_diff / dofs_diff?

        new_state = max(
                update_modes(state),
                update_degree(state),
                update_rank(state),
            key = state_diff)

        if new_state.epoch == state.epoch:
            print("Optimization does not yield any improvement")
            print("Stopping adaptivity")
            break

        state = new_state
        test_state(state)

    #TODO: when terminating print the arguments that are needed to pick up where you left
