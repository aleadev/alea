import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch_tt import TorchTT
from torch.utils.tensorboard import SummaryWriter
from torchvision import datasets, transforms


seed = 0
writer = SummaryWriter("runs/mnist_tt")


class PolynomiallyMeasuredTT(nn.Module):
    def __init__(self, tt, degree):
        super(PolynomiallyMeasuredTT, self).__init__()
        assert tt.feature_dim == degree+1, f"{tt.feature_dim} vs {degree+1}"
        self.tt = tt
        self.degree = degree

    def forward(self, x):
        assert x.ndim == 2 and x.shape[1] == self.tt.input_dim  # (batch_size, input_dim)
        x = x[:, :, None] ** torch.arange(self.degree+1, dtype=torch.float, device=self.tt.device)  #TODO: device?
        x = self.tt(x)
        return F.log_softmax(x, dim=1)

def train(args, model, device, train_loader, optimizer, epoch):
    model.train()
    data_points_seen = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        data_points_seen += len(data)
        if batch_idx % args.log_interval == 0:
            writer.add_scalar('Loss/train',
                              loss.item(), # running_loss / 1000,
                              epoch * len(train_loader.dataset) + data_points_seen)
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))

def test(args, model, device, test_loader, data_points_seen):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in test_loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(test_loader.dataset)

    writer.add_scalar('Loss/test',
                      test_loss,
                      data_points_seen)
    writer.add_scalar('Accuracy/test',
                      correct/len(test_loader.dataset),
                      data_points_seen)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(test_loader.dataset),
        100. * correct / len(test_loader.dataset)))

if __name__ == '__main__':
    # Training settings
    parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
    parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                        help='input batch size for training (default: 64)')
    parser.add_argument('--test-batch-size', type=int, default=1000, metavar='N',
                        help='input batch size for testing (default: 1000)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--lr', type=float, default=0.01, metavar='LR',
                        help='learning rate (default: 0.01)')
    parser.add_argument('--momentum', type=float, default=0.5, metavar='M',
                        help='SGD momentum (default: 0.5)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='disables CUDA training')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--save-model', action='store_true', default=False,
                        help='For Saving the Model')
    parser.add_argument('--load-model', action='store_true', default=False,
                        help='For Loading the Model')
    parser.add_argument('--initial-epoch', type=int, default=1,
                        help='Initial epoch when loading the Model')
    args = parser.parse_args()
    use_cuda = not args.no_cuda and torch.cuda.is_available()

    torch.manual_seed(seed)

    device = torch.device("cuda:2" if use_cuda else "cpu")

    def reshape(x):
        assert x.shape == (1,7,7)
        return torch.reshape(x, (-1,))

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=True, download=True,
                       transform=transforms.Compose([
                           transforms.Resize((7,7)),
                           transforms.ToTensor(),
                           # transforms.Normalize((0.1307,), (0.3081,)),
                           transforms.Lambda(reshape)
                       ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST('data', train=False, transform=transforms.Compose([
                           transforms.Resize((7,7)),
                           transforms.ToTensor(),
                           # transforms.Normalize((0.1307,), (0.3081,))
                           transforms.Lambda(reshape)
                       ])),
        batch_size=args.test_batch_size, shuffle=False, **kwargs)


    degree = 1
    tt = TorchTT(input_dim=7**2, output_dim=10, rank=20, feature_dim=degree+1,
                 components=None, device=device)
    model = PolynomiallyMeasuredTT(tt, degree).to(device)
    # optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)
    if args.load_model:
        model.load_state_dict(torch.load("mnist_tt.pt"))
    optimizer = optim.Adam(model.parameters(), lr=1e-4)

    for epoch in range(args.initial_epoch, args.initial_epoch + args.epochs):
        train(args, model, device, train_loader, optimizer, epoch)
        test(args, model, device, test_loader, (epoch+1) * len(train_loader.dataset))
        if args.save_model:
            torch.save(model.state_dict(), "mnist_tt.pt")
