import os
from functools import wraps
import torch
from torchvision import datasets, transforms
import warnings


def reshape(*shape):
    def reshape(x):
        assert x.shape == (1,28,28)
        return torch.reshape(x, shape)
    return reshape

def scatter(wave, num_levels):
    import pywt
    assert isinstance(wave, str)
    try: pywt.Wavelet(wave)
    except: raise ValueError("Feature map '%s' is not supported"%wave)
    assert num_levels >= 0

    filter = lambda image: (lambda lowpass, others: [lowpass]+list(others))(*pywt.dwt2(image, wave))
    abs_filter = lambda image: map(abs, filter(image))
    lowpass = lambda image: filter(image)[0]

    def concatenate(ls):
        return sum(map(list, ls), [])

    def next_level(level):
        return concatenate(abs_filter(image) for image in level)

    def wrapped(x):
        ret = []
        level = [x]  # the first scattering level contains just the image
        ret.extend(map(lowpass, level))
        # ret.extend(level)
        for level_idx in range(1, num_levels):
            if level[0].size == 1:
                break
            level = next_level(level)
            ret.extend(map(lowpass, level))
            # ret.extend(level)
        ret = torch.cat([torch.from_numpy(r.reshape(-1)) for r in ret])
        return ret

    wrapped.__name__ = f"scatter_{wave.lower()}_{num_levels}"
    return wrapped


def cached_method(meth):
    @wraps(meth)
    def new_method(self, *args, **kwargs):
        class_name = self.__class__.__name__
        try:
            return torch.load(f"{self.cache_dir}/{class_name}.{meth.__name__}.pt")
        except FileNotFoundError:
            print(f"Computing {meth.__name__}")
            ret = meth(self, *args, **kwargs)
            os.makedirs(self.cache_dir, exist_ok=True)
            torch.save(ret, f"{self.cache_dir}/{class_name}.{meth.__name__}.pt")
            return ret
    return new_method


class MNIST_scaling(object):
    def __init__(self, trafo_list, device, cache_dir):
        #TODO: take the dataloader directly?
        self.__scaling_loader = None
        self.trafo_list = trafo_list
        self.device = device
        self.cache_dir = cache_dir
        assert len(self.data_shape) == 2

    @property
    def transform(self):
        mins, maxs = self.mins_maxs.to('cpu')
        diams = maxs-mins
        def scale(x):
            return 2 * (x-mins) / diams - 1
        return transforms.Lambda(scale)

    @property
    @cached_method
    def data_shape(self):
        return next(iter(self.scaling_loader))[0].shape

    @property
    def scaling_loader(self):
        if self.__scaling_loader is None:
            use_cuda = self.device.type == 'cuda' and torch.cuda.is_available()
            kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
            self.__scaling_loader = torch.utils.data.DataLoader(
                    datasets.MNIST('data', train=True, download=True,
                                transform=transforms.Compose(self.trafo_list)),
                    batch_size=1000, shuffle=False, drop_last=False, **kwargs)
        return self.__scaling_loader

    @property
    @cached_method
    def mins_maxs(self):
        mins_maxs = torch.empty((2,self.data_shape[1]), dtype=torch.float)
        mins_maxs[0] = float('inf'); mins_maxs[1] = -float('inf')
        n_batches = len(self.scaling_loader.dataset) // self.data_shape[0]
        for batch_idx, (data, target) in enumerate(self.scaling_loader, start=1):
            assert data.shape == self.data_shape, f"{data.shape} vs {self.data_shape}"
            mins_maxs[0] = torch.min(mins_maxs[0], torch.min(data, dim=0).values)
            mins_maxs[1] = torch.max(mins_maxs[1], torch.max(data, dim=0).values)
            print(f"Computing mean: {batch_idx}/{n_batches}")
        return mins_maxs


class MNIST_PCA(object):
    def __init__(self, trafo_list, device, cache_dir):
        #TODO: take the dataloader directly?
        self.__pca_loader = None
        self.trafo_list = trafo_list
        self.device = device
        self.cache_dir = cache_dir
        assert len(self.data_shape) == 2

    @property
    def transform(self):
        mean = self.mean.to('cpu')
        projection = self.eigenvectors.to('cpu')
        def pca(x):
            return projection @ (x - mean)
        return transforms.Lambda(pca)

    @property
    def pca_loader(self):
        if self.__pca_loader is None:
            use_cuda = self.device.type == 'cuda' and torch.cuda.is_available()
            kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
            self.__pca_loader = torch.utils.data.DataLoader(
                    datasets.MNIST('data', train=True, download=True,
                                transform=transforms.Compose(self.trafo_list)),
                    batch_size=1000, shuffle=False, drop_last=False, **kwargs)
        return self.__pca_loader

    @property
    @cached_method
    def data_shape(self):
        return next(iter(self.pca_loader))[0].shape

    @property
    @cached_method
    def mean(self):
        mean = torch.zeros((self.data_shape[1],), dtype=torch.float, device=self.device)
        n_batches = len(self.pca_loader.dataset) // self.data_shape[0]
        for batch_idx, (data, target) in enumerate(self.pca_loader, start=1):
            assert data.shape == self.data_shape
            data = data.to(self.device)
            mean += torch.mean(data, dim=0)
            print(f"Computing mean: {batch_idx}/{n_batches}")
        assert batch_idx == n_batches
        mean /= n_batches
        return mean

    @property
    @cached_method
    def cov(self):
        cov = torch.zeros((self.data_shape[1], self.data_shape[1]), dtype=torch.float, device=self.device)
        n_samples = len(self.pca_loader.dataset)
        n_batches = n_samples // self.data_shape[0]
        for batch_idx, (data, target) in enumerate(self.pca_loader, start=1):
            assert data.shape == self.data_shape
            data = data.to(self.device) - self.mean
            cov += (data.T @ data)
            print(f"Computing cov: {batch_idx}/{n_batches}")
        assert batch_idx == n_batches
        cov /= n_samples - 1  # unbiased estimator ...
        return (cov + cov.T)/2

    @property
    @cached_method
    def eigenvectors(self):
        es,vs = torch.eig(self.cov, eigenvectors=True)
        # assert torch.all(abs(es[:,1]) < 1e-8)
        ss = abs(es[:,0])
        order = torch.argsort(ss, descending=True)
        # assert torch.norm((vs * es[order]) @ vs.T - cov) < 1e-4
        print(f"Computing eigenvectors: 1/1")
        return vs[:, order].T
