from __future__ import (division, print_function)
from alea.math_utils.param_pde.forward_operator.poisson import ParametricPoisson, SolutionCache
from alea.math_utils.param_pde.affine_field import AffineField
from alea.math_utils.tensor.torch_tt import TorchTT
import torch
import json
import time
from dolfin import *

set_log_level(50)
import matplotlib.pyplot as plt

# mesh = UnitSquareMesh(10, 10)
# alternatively we can use alea template meshes and boundary types
from alea.math_utils.param_pde.mesh_util import get_mesh, get_boundary
mesh = get_mesh("square", mesh_nodes=1e2)
boundary = get_boundary("dirichlet")

M = 20
use_gpu = True


path_to_data = "/Home/optimier/marschall/Projects/alea/src/alea/apps/gpu_test.dat"

if use_gpu:
    device = torch.device("cuda:2")
else:
    device = torch.device("cpu")

fs = FunctionSpace(mesh, 'CG', 3)

bc = DirichletBC(fs, Constant(0), boundary)

# plot(mesh)

coef_param = {
    "coef_type": "cos",                   # type: str
    "amptype": "decay-inf",               # type: str
    "decayexp": 2,                        # type: float
    "gamma": 0.9,                         # type: float
    "freqscale": 1.0,                     # type: float
    "freqskip": 0,                        # type: int
    "scale": 1.0,                         # type: float
    "coef_mean": 1.0,                     # type: float
    "rv_type": "uniform"                  # type: str
}

coef = AffineField(**coef_param)

rhs = Constant(1.0)

from torch.utils.data import Dataset
class FemDataset(Dataset):
    """FEM dataset."""

    def __init__(self, coef, rhs, fs, bc, M, n_samples, device=None, progress=False, load_data=None):
        if device is None:
            device = torch.device("cpu")
        self.problem = ParametricPoisson(coef, rhs, fs, bc)
        self.samples = torch.rand(M, n_samples) * 2 - 1
        self.labels = torch.Tensor(fs.dim(), n_samples)
        if load_data is not None:
            try:
                with open(load_data, 'r') as _f:
                    inf = json.load(_f)
                self.samples = torch.Tensor(inf["samples"])
                self.labels = torch.Tensor(inf["labels"])
                self.samples.to(device)
                self.labels.to(device)
                return
            except IOError:
                print("load data path not found")

        cache = SolutionCache()
        if progress:
            from alea.utils.progress.percentage import PercentageBar
            bar = PercentageBar(n_samples)
        for lia in range(n_samples):
            sol = torch.Tensor(self.problem.solve(self.samples[:, lia].detach().numpy(), M, cache=cache).vector())
            self.labels[:, lia] = sol
            bar.next()
        if load_data is not None:
            inf = {"samples": self.samples.numpy().tolist(), "labels": self.labels.numpy().tolist()}
            with open(load_data, 'w') as _f:
                json.dump(inf, _f)
        self.samples.to(device)
        self.labels.to(device)
    def __len__(self):
        return self.samples.size(1)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        sample = {'sample': self.samples[:, idx], 'label': self.labels[:, idx]}
        return sample


# Miscellaneous initialization
torch.manual_seed(0)

# MPS parameters
bond_dim      = 8

# Training parameters
num_train  = 10000
num_test   = 1000
batch_size = 100
num_epochs = 500
learn_rate = 1e-4
l2_reg     = 0.

print("build training set")
train_set = FemDataset(coef, rhs, fs, bc, M, num_train, device=device, progress=True, load_data=path_to_data)
print("build test set")
test_set = FemDataset(coef, rhs, fs, bc, M, num_test, device=device, progress=True)

samplers = {'train': torch.utils.data.SubsetRandomSampler(range(num_train)),
            'test': torch.utils.data.SubsetRandomSampler(range(num_test))}
train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size,
                                           sampler=samplers["train"], drop_last=True)
test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size,
                                           sampler=samplers["test"], drop_last=True)
num_batches = {name: total_num // batch_size for (name, total_num) in
               [('train', num_train), ('test', num_test)]}

print(f"Training on {num_train} FEM data \n"
      f"(testing on {num_test}) for infinitely many epochs")
print(f"Maximum MPS bond dimension = {bond_dim}")
# print(f" * {'Adaptive' if adaptive_mode else 'Fixed'} bond dimensions")
# print(f" * {'Periodic' if periodic_bc else 'Open'} boundary conditions")
print(f"Using Adam w/ learning rate = {learn_rate:.1e}")
if l2_reg > 0:
    print(f" * L2 regularization = {l2_reg:.2e}")

feature_dim = 7


def feature(input_data):
    assert len(input_data.shape) == 2
    retval = torch.empty(list(input_data.shape) + [feature_dim]).to(device)
    if feature_dim == 7:
        retval[:, :, 0] = torch.ones(input_data.shape, device=device)
        retval[:, :, 1] = input_data
        retval[:, :, 2] = 0.5 * (3 * input_data ** 2 - retval[:, :, 0])
        retval[:, :, 3] = 0.5 * (5 * input_data ** 3 - 3 * retval[:, :, 1])
        retval[:, :, 4] = 1 / 8 * (35 * input_data ** 4 - 30 * input_data ** 2 + 3)
        retval[:, :, 5] = 1 / 8 * (63 * input_data ** 5 - 70 * input_data ** 3 + 15 * input_data)
        retval[:, :, 6] = 1 / 16 * (231 * input_data ** 6 - 315 * input_data ** 4 + 105 * input_data ** 2 - 5)
    if feature_dim == 2:
        retval[:, :, 0] = input_data
        retval[:, :, 1] = 1 - input_data
    if feature_dim == 9:
        retval[:, :, 0] = torch.ones(input_data.shape)
        retval[:, :, 1] = input_data
        retval[:, :, 2] = input_data ** 2
        retval[:, :, 3] = input_data ** 3
        retval[:, :, 4] = input_data ** 4
        retval[:, :, 5] = input_data ** 5
        retval[:, :, 6] = input_data ** 6
        retval[:, :, 7] = input_data ** 7
        retval[:, :, 8] = input_data ** 8
    return retval


mps = TorchTT(input_dim=M, output_dim=fs.dim(), rank=bond_dim, feature_dim=feature_dim, feature=feature,
              device=device).to(device)
epoch_num = 0

start_time = time.time()
update_time = time.time()
update_plot = True
# Let's start training!
# for epoch_num in range(1, num_epochs+1):
while True:
    if time.time() - update_time > 1:
        update_plot = True
        update_time = time.time()
    running_loss = 0.
    running_acc = 0.

    # Set our loss function and optimizer

    loss_fun = torch.nn.MSELoss(reduction="sum")
    optimizer = torch.optim.Adam(mps.parameters(), lr=learn_rate,
                                 weight_decay=l2_reg)

    for item in train_loader:
        inputs = item["sample"].to(device=device)
        labels = item["label"].to(device=device)

        scores = mps(inputs)
        # Compute the loss and accuracy, add them to the running totals
        loss = loss_fun(scores, labels)
        with torch.no_grad():
            accuracy = torch.sum(
                torch.tensor([torch.dist(scores[i, :], labels[i, :], p=2) / torch.norm(labels[i, :], p=2)
                              for i in range(scores.size(0))], device=device))
            running_loss += loss
            running_acc += accuracy / batch_size

        # Backpropagate and update parameters
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    if update_plot:
        print(f"Number of parameter = {sum(p.numel() for p in mps.parameters() if p.requires_grad)}")
        print(f"### Epoch {epoch_num} ###")
        print(f"Empirical train loss:     {running_loss / num_batches['train']:.4f}")
        print(f"Empirical train accuracy: {running_acc / num_batches['train']:.4f}")

    # Evaluate accuracy of MPS classifier on the test set
    with torch.no_grad():
        running_acc = 0.
        running_loss = 0.
        for item in test_loader:
            inputs = item["sample"].to(device=device)
            labels = item["label"].to(device=device)

            # Call our MPS to get logit scores and predictions
            scores = mps(inputs)
            # Compute the loss and accuracy, add them to the running totals
            loss = loss_fun(scores, labels)
            accuracy = torch.sum(
                torch.tensor([torch.dist(scores[i, :], labels[i, :], p=2) / torch.norm(labels[i, :], p=2)
                              for i in range(scores.size(0))], device=device))
            running_loss += loss
            running_acc += accuracy / batch_size
    if update_plot:
        print(f"Empirical test loss:     {running_loss / num_batches['test']:.4f}")
        print(f"Empirical test accuracy: {running_acc / num_batches['test']:.4f}")
        print(f"Runtime so far:         {int(time.time()-start_time)} sec\n")
        update_plot = False
    epoch_num += 1
    if running_acc / num_batches['test'] < 1 - 3:
        break