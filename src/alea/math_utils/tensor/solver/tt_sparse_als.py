"""
Implementing a wrapper for the ALS algorithm using a sparse first component.
@author: Manuel Marschall
"""
# region Imports
from alea.math_utils.tensor.solver.les_solver import LesSolver
from alea.math_utils.tensor.smatrix import smatrix
from alea.math_utils.tensor.tt_util import (ttRightOrthogonalize, ttMoveOrthogonality, reshape, leftThreeHalfdot,
                                            rightThreeHalfdot, rankOneProduct, leftThreeMatrixHalfdot,
                                            rightThreeMatrixHalfdot, ravel, ttRandomDeterministicRankOne)
from alea.math_utils.tensor.solver.tt_sgfem_util import ttCompPCG
from alea.math_utils.tensor.extended_fem_tt import ExtendedTT
from alea.utils.tictoc import TicToc
import tt
import numpy as np
import scipy.sparse as sps
# endregion


class TTSparseALS(LesSolver):
    # region init
    def __init__(self,
                 lhs,                               # type: smatrix
                 rhs,                               # type: list or ExtendedTT
                 ranks,                             # type: list
                 als_iterations=1000,               # type: int
                 convergence=1e-10                  # type: float
                 ):
        """
        constructor
        :param lhs: TT-sparse operator
        :param rhs: TT-tensor cores as list
        :param ranks: list of tensor ranks to solve with
        """
        LesSolver.__init__(self, lhs, rhs)
        self.ranks = ranks
        self.rank = max(self.ranks)
        self.als_iterations = als_iterations
        self.convergence = convergence
        self.local_error_list = []
        self.first_comp_convergence_list = []
        self.actual_iterations = -1
    # endregion

    # region def solve
    def solve(self,
              preconditioner=None,                  # type: dict
              start_rank_one=False,                 # type: bool
              init_value=None,                      # type: list
              normalize_init=False                  # type: bool
              ):
        """
        wrapper for the Sparse ALS method creating initial value and preconditioner
        :param preconditioner: dictionary containing information about the used preconditioner
        :param start_rank_one: flag to use a rank one initial tensor
        :param init_value: given initial tensor
        :param normalize_init: flag to normalize the initial tensor
        :return: solution tensor
        """

        if isinstance(self.rhs, list):
            import tt
            self.rhs = tt.vector.from_list(self.rhs)
        if isinstance(self.rhs, ExtendedTT):
            import tt
            self.rhs = tt.vector.from_list(self.rhs.components)
        W = self.create_start_tensor(start_rank_one, init_value, normalize_init, preconditioner)
        # print("start_tensor: {}".format(W))
        P = self.create_precondition(preconditioner)
        i = 0
        while True:
            result = ttALS(self.lhs, self.rhs, W, conv=self.convergence, maxit=self.als_iterations, P=P,
                           return_local_error=True, no_print=False)
            W, self.local_error_list, self.first_comp_convergence_list, self.actual_iterations, restart = result
            if restart is True and i < 10:
                print("  create a rank - 1 update and restart the algorithm {} / {}".format(i, 10))
                W += ttRandomDeterministicRankOne(self.lhs.n)
            else:
                break
        import tt
        if isinstance(W, tt.vector):
            W = tt.vector.to_list(W)
        return W
    # endregion

    # region def create start tensor
    def create_start_tensor(self,
                            start_rank_one=False,   # type: bool
                            _init_value=None,       # type: list or ExtendedTT
                            normalize_init=False,   # type: bool
                            preconditioner=None     # type: dict
                            ):
        """
        function to create a starting tensor according to given flags. if nothing is specified, return a random tensor
        of the current rank
        :param start_rank_one: create a rank one tensor and return it
        :param _init_value: predefined initial value
        :param normalize_init: flag to normalize the initial value
        :param preconditioner: dict containing information about the used preconditioner
        :return: start tensor
        """
        if start_rank_one is True:
            #                                       # create random deterministic rank one tensor
            start_ten = ttRandomDeterministicRankOne(self.lhs.n)
            return start_ten

        if _init_value is not None:
            if isinstance(_init_value, ExtendedTT):
                init_value = _init_value.components
            else:
                init_value = _init_value
            if normalize_init is True:
                # TODO: do not use the tt toolbox for norm calculation and structure
                import tt
                W = tt.vector.from_list(init_value)
                W *= (1 / W.norm())  # normalize
                # W = tt.vector.to_list(W)

                # print("rank of W : {}".format(max(tt.vector.from_list(W).r)))
                return W
            else:
                return _init_value
        #                                           # create tensor with given rank and size
        # print("random tensor")
        last_rank = 0
        W = ttRandomDeterministicRankOne(self.lhs.n)
        P = self.create_precondition(preconditioner)
        while True:
            W, _, restart = ttALS(self.lhs, self.rhs, W, conv=self.convergence, maxit=self.als_iterations, P=P,
                                  no_print=True)
            if max(W.r) >= self.rank-1 and not restart:
                W += ttRandomDeterministicRankOne(self.lhs.n)
                break
            if max(W.r) <= last_rank and not restart:
                print("WARNING: Create start tensor: the rank decreased in the last iteration step")
                break
            last_rank = max(W.r)
            W += ttRandomDeterministicRankOne(self.lhs.n)
            print("start tensor rank increased: \n   {}".format(W))
        return W
    # endregion

    # region create precondition
    def create_precondition(self,
                            preconditioner          # type: dict
                            ):
        """
        implements a wrapper for different kinds of precondition. Currently supports only the field mean method
        :param preconditioner:
        :return:
        """
        retval = dict()
        if preconditioner is None:
            retval["precond"] = np.eye(self.lhs.n[0], self.lhs.n[0])
            retval["method"] = "spsolve"
            return retval
        # print preconditioner
        assert("name" in preconditioner)
        # print(preconditioner["name"].strip().capitalize())
        if preconditioner["name"].strip() == "field mean":
            assert("coef" in preconditioner)
            assert(hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert(callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["method"] = "cg"
            retval["precond"] = extract_mean()
            return retval
        if preconditioner["name"].strip() == "diag field mean":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["method"] = "direct diag"
            mean_mat = extract_mean().diagonal()
            mean_mat_inv = sps.spdiags(mean_mat**(-1), 0, mean_mat.size, mean_mat.size)
            mean_mat = sps.spdiags(mean_mat, 0, mean_mat.size, mean_mat.size)
            retval["precond"] = mean_mat
            retval["precond_inv"] = mean_mat_inv
            return retval
        if preconditioner["name"].strip() == "stored field mean":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            print("    invert mean matrix")
            mat = extract_mean()
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored field trace":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_trace"))
            extract_trace = getattr(preconditioner["coef"], "extract_trace", None)
            assert (callable(extract_trace))
            print("  use precondition: {}".format(preconditioner["name"]))
            print("    invert trace matrix")
            mat = extract_trace()
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored field mean var":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            assert (hasattr(preconditioner["coef"], "extract_variance"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            extract_var = getattr(preconditioner["coef"], "extract_variance", None)
            assert (callable(extract_mean))
            assert (callable(extract_var))
            print("  use precondition: {}".format(preconditioner["name"]))
            print("    invert mean matrix")
            mat_mean = extract_mean()
            mat_var = extract_var()
            mat = mat_mean + mat_var
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored trace":
            assert ("operator" in preconditioner)
            assert (hasattr(preconditioner["operator"], "extract_trace"))
            trace = getattr(preconditioner["operator"], "extract_trace", None)
            assert (callable(trace))
            assert (hasattr(preconditioner["operator"], "extract_stochastic_dimension"))
            dim = getattr(preconditioner["operator"], "extract_stochastic_dimension", None)
            assert (callable(dim))
            print("  use precondition: {}".format(preconditioner["name"]))
            mat = trace() * dim()**(-1)

            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                mat_inv = np.linalg.inv(mat.todense())
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored"
            return retval
        if preconditioner["name"].strip() == "stored rank":
            assert ("operator" in preconditioner)
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            mat = preconditioner["operator"].tt_bilinearform.cores[0]

            # for lia in range(len(mat)):
            #     mat[lia] = sps.eye(mat[lia].shape[0], mat[lia].shape[1])
            mat_inv = []
            with TicToc(sec_key="preconditioner", key="invert mean matrix", do_print=True, active=True):
                for lia in range(len(mat)):
                    mat_inv.append(np.linalg.inv(mat[lia].todense()))
            retval["precond"] = mat
            retval["precond_inv"] = mat_inv
            retval["method"] = "stored rank"
            return retval
        if preconditioner["name"].strip() == "direct":
            assert ("operator" in preconditioner)
            # assert ("coef" in preconditioner)
            # assert (hasattr(preconditioner["coef"], "extract_mean"))
            # extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            # assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["data"] = np.zeros((len(preconditioner["operator"].tt_bilinearform.cores[0]),
                                       preconditioner["operator"].tt_bilinearform.cores[0][0].nnz))
            for lia in range(len(preconditioner["operator"].tt_bilinearform.cores[0])):
                retval["data"][lia, :] = preconditioner["operator"].tt_bilinearform.cores[0][lia].data

            retval["method"] = "direct"
            return retval
        if preconditioner["name"].strip() == "direct gmres":
            assert ("operator" in preconditioner)
            # assert ("coef" in preconditioner)
            # assert (hasattr(preconditioner["coef"], "extract_mean"))
            # extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            # assert (callable(extract_mean))
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["data"] = np.zeros((len(preconditioner["operator"].tt_bilinearform.cores[0]),
                                       preconditioner["operator"].tt_bilinearform.cores[0][0].nnz))
            for lia in range(len(preconditioner["operator"].tt_bilinearform.cores[0])):
                retval["data"][lia, :] = preconditioner["operator"].tt_bilinearform.cores[0][lia].data

            retval["method"] = "direct gmres"
            return retval
        if preconditioner["name"].strip() == "scipy cg":
            assert ("coef" in preconditioner)
            assert (hasattr(preconditioner["coef"], "extract_mean"))
            extract_mean = getattr(preconditioner["coef"], "extract_mean", None)
            assert (callable(extract_mean))
            retval["precond"] = np.linalg.inv(extract_mean().todense())
            retval["method"] = "scipy cg"
            return retval
        if preconditioner["name"].strip() == "LU":
            print("  use precondition: {}".format(preconditioner["name"]))
            retval["method"] = "direct lu"
            return retval
        if preconditioner["name"].strip() == "none":
            retval["precond"] = 1
            retval["method"] = "none"
            return retval
        if preconditioner["name"].strip() == "sylvester glcr":
            # if True:
            #     raise ValueError("Does not converge")
            retval["precond"] = None
            retval["method"] = "sylvester glcr"
            return retval
        print("use no preconditioner")
        retval["precond"] = np.eye(self.lhs.n[0], self.lhs.n[0])
        retval["method"] = "spsolve"
        return retval

    # endregion


def ttALS(A,  # type: smatrix
          B,  # type: tt.vector
          U,  # type: tt.vector
          maxit=1000,  # type: int
          conv=1e-12,  # type: float
          approxtol=1e-20,  # type: float
          P=None,  # type: np.array or dict
          converged_by_ref=None,  # type: bool
          return_local_error=False,  # type: bool
          reference_solution=None,  # type: tt.vector
          no_print=False  # type: bool
          ):
    """
    Computes the solution of the tensor train system | AX - B|_F using alternating least squares
    :param A: operator used in the system as tensor train operator with sparse first component
    :param B: rhs as tensor train
    :param U: initial guess as tensor train
    :param maxit: number of maximal iterations (sweeps)
    :param conv: desired accuracy to reach. can Only be estiamted as difference between two iterations
    :param approxtol: approximation tolerance used for rounding tensors in the svd procedure
    :param P: dictionary containing additional information for the local solver
    :param converged_by_ref: boolean to store the convergence property. Do not ask why I introduced that.
    :param return_local_error: Flag to return more verbose information about the solution process
    :param reference_solution: optional reference solution to look at convergence
    :param no_print: flag to stop printing
    :return: Solution as tensor train
    """
    # region Assertions on convergence by ref object
    if converged_by_ref is not None:
        assert (isinstance(converged_by_ref, list))
        assert (len(converged_by_ref) == 1)
        assert (isinstance(converged_by_ref[0], bool))
    # endregion
    # region initial problem print
    if not no_print:
        print("#" * 40)
        print("Solve AU=B using ALS")
        print("A: {}".format(A))
        print("B: {}".format(B))
        print("init value: {}".format(U))
        print("using maximal {} sweeps and a micro-iteration approximation tolerance of {}".format(maxit, conv))
        print("#" * 40)

    # endregion

    # region def set core
    def set_core(V, k, v):
        cores = tt.tensor.to_list(V)
        cores[k] = v
        return tt.tensor.from_list(cores)

    # endregion

    # region orthogonalize the initial guess
    V = ttRightOrthogonalize(U, approxtol)
    # endregion

    # region some parameters used later
    d = U.d
    n = U.n
    ranks = V.r
    bool1 = True
    local_error_list = []
    first_comp_convergence_list = []
    i = 0

    if False:
        parallel = Parallel(mp.cpu_count(), backend="threading")
    else:
        parallel = None
    # endregion
    for i in range(maxit):
        for it in range(2 * d - 2):
            if bool1:
                k = it % (d - 1)
                with TicToc(sec_key="ALS-solver", key="  **** ALS sweeps ****", active=True, do_print=False):
                    if k == 0:
                        #                           # get list of component tensors of current iterate
                        cores = tt.tensor.to_list(V)
                        vv = ravel(cores[0])  # vectorize the first component
                        #                           # multiply rhs and current iterate
                        b = reshape(ttHalfdot(V, B, 0), [ranks[0] * n[0] * ranks[1]])
                        #                           # get operator list and remainder term
                        a, right = ttHalfdot(V, A, 0)
                        if P is not None:
                            with TicToc(sec_key="ALS-solver", key="    **** ALS first comp PCG ****", active=True,
                                        do_print=False):
                                v, first_comp_convergence = ttCompPCG(a, b, x=vv, right=right, P=P, ret_conv=True,
                                                                      maxit=maxit, para=parallel)
                                first_comp_convergence_list.append(first_comp_convergence)
                                # print("  Micro PCG steps: {}".format(len(first_comp_convergence)))
                                v = reshape(v, [ranks[0], n[0], ranks[1]])
                            V = set_core(V, 0, v)
                            if not isinstance(P, dict):
                                V = ttMoveOrthogonality(V, 0, 1, P=P, approxtol=approxtol)
                            else:
                                if P["method"] == "direct" or P["method"] == "none" or P["method"] == "sylvester glcr" \
                                        or P["method"] == "direct lu":
                                    V = ttMoveOrthogonality(V, 0, 1, approxtol=approxtol)
                                else:
                                    print("you should not be here")
                                    V = ttMoveOrthogonality(V, 0, 1, P=P["precond"], approxtol=approxtol)
                        else:
                            with TicToc(sec_key="ALS-solver", key="    **** ALS first PCG w/o P****", active=True,
                                        do_print=False):
                                v, first_comp_convergence = ttCompPCG(a, b, x=vv, right=right, maxit=maxit,
                                                                      ret_conv=True)
                                first_comp_convergence_list.append(first_comp_convergence)
                                v = reshape(v, [ranks[0], n[0], ranks[1]])
                            V = set_core(V, 0, v)
                            V = ttMoveOrthogonality(V, 0, 1, approxtol=approxtol)
                    else:
                        b = reshape(ttHalfdot(V, B, k), [ranks[k] * n[k] * ranks[k + 1]])
                        a = ttHalfdot(V, A, k)
                        with TicToc(sec_key="ALS-solver", key="    **** ALS direct solve ****", active=True,
                                    do_print=False):
                            v = reshape(ttCompPCG(a, b, maxit=maxit), [ranks[k], n[k], ranks[k + 1]])
                        V = set_core(V, k, v)
                        V = ttMoveOrthogonality(V, k, 1, approxtol=approxtol)

                ranks = V.r
                # logger.debug('ranks after step %i,%i: %s', i, k, ranks)
                if k == d - 2:
                    # print('distance of iteration step {}: {}'.format(k, (U - V).norm()))
                    # logger.debug('distance of iteration steps: %.16f;', (U-V).norm())
                    bool1 = False
            else:
                k = d - 1 - (it % (d - 1))
                with TicToc(sec_key="ALS-solver", key="  **** ALS back sweep ****", active=True, do_print=False):
                    b = reshape(ttHalfdot(V, B, k), [ranks[k] * n[k] * ranks[k + 1]])
                    a = reshape(ttHalfdot(V, A, k), [ranks[k] * n[k] * ranks[k + 1], ranks[k] * n[k] * ranks[k + 1]])
                    with TicToc(sec_key="ALS-solver", key="    **** ALS direct solve back****", active=True,
                                do_print=False):
                        v = reshape(ttCompPCG(a, b, maxit=maxit), [ranks[k], n[k], ranks[k + 1]])
                    V = set_core(V, k, v)
                    V = ttMoveOrthogonality(V, k, -1, approxtol=approxtol)
                    ranks = V.r
                    # logger.debug('ranks after step %i,%i: %s', i, k, ranks)

                    if k == 1:
                        # print('distance of iteration step {}: {}'.format(k, (U - V).norm()))
                        # logger.info('distance of iteration steps: %.16f;', (U-V).norm())
                        bool1 = True

        no_print_local = False
        if return_local_error is True or True:
            # this was used for Max preconditioner test. Here we need a reference solution
            if reference_solution is not None:
                local_error_list.append(tt.vector.norm(reference_solution - V))
            else:
                local_error_list.append((U - V).norm())
            if i > 100 and local_error_list[-1] > 0.01:
                print("local error of step {} is to huge after this long time -> restart with rank 1 update")
                if return_local_error is False:
                    return U, i, True
                return U, local_error_list, first_comp_convergence_list, i, True
            if i > 20:
                history = local_error_list[i - 20:]
                max_h = np.max(history)
                min_h = np.min(history)
                update = min_h * max_h ** (-1)
                print("local error of step {}: {} -> update: {}".format(i, local_error_list[-1], update))
                no_print_local = True
                if update > 0.5 and 1e-7 < min_h <= max_h:
                    print("  no significant increase in acc -> restart with rank 1 update")
                    if return_local_error is False:
                        return U, i, True
                    return U, local_error_list, first_comp_convergence_list, i, True
                if update > 0.5 and min_h < 1e-7:
                    print("  no significant increase in acc  but difference is small -> take current solution")
                    if return_local_error is False:
                        return U, i, False
                    return U, local_error_list, first_comp_convergence_list, i, False
        if not no_print_local:
            print("local error of step {}: {}".format(i, local_error_list[-1]))

        if (U - V).norm() < conv:
            if not no_print:
                print("ALS converged")
            U = V
            if converged_by_ref is not None:
                converged_by_ref[0] = True
            break
        U = V
    if return_local_error is True:
        return U, local_error_list, first_comp_convergence_list, i, False
    else:
        return U, i, False


def ttHalfdot(U, B, k):
    """input has to be orthogonal in the respective component!"""

    # tt_tensor
    if isinstance(B, tt.vector):
        d = U.d
        ucores = tt.tensor.to_list(U)
        bcores = tt.tensor.to_list(B)

        left = np.array([1])
        for i in range(k):
            b = rankOneProduct(bcores[i], left=left)
            left = leftThreeHalfdot(ucores[i], b)

        right = np.array([1])
        for i in range(d - 1, k, -1):
            b = rankOneProduct(bcores[i], right=right)
            right = rightThreeHalfdot(ucores[i], b)

        return rankOneProduct(bcores[k], left=left, right=right)

    # tt_matrix
    elif isinstance(B, tt.matrix):
        d = U.d
        n = U.n
        ur = U.r
        ucores = tt.tensor.to_list(U)
        ar = B.tt.r
        acores = tt.matrix.to_list(B)

        left = np.array([[[1]]])
        for i in range(k):
            left = leftThreeMatrixHalfdot(ucores[i], acores[i], left)

        right = np.array([[[1]]])
        for i in range(d - 1, k, -1):
            right = rightThreeMatrixHalfdot(ucores[i], acores[i], right)

        left = np.transpose(left, axes=(0, 2, 1))
        left = reshape(left, [ur[k] * ur[k], ar[k]])
        right = np.transpose(right, axes=(0, 2, 1))
        right = reshape(right, [ur[k + 1] * ur[k + 1], ar[k + 1]])
        a = reshape(acores[k], [ar[k], n[k] * n[k], ar[k + 1]])
        mid = rankOneProduct(a, left=left, right=right)
        mid = reshape(mid, [ur[k], ur[k], n[k], n[k], ur[k + 1], ur[k + 1]])
        mid = np.transpose(mid, axes=(0, 2, 4, 1, 3, 5))

        return reshape(mid, [ur[k] * n[k] * ur[k + 1], ur[k] * n[k] * ur[k + 1]])

    # tt_sparse_matrix
    else:
        assert isinstance(B, smatrix)
        d = U.d
        n = U.n
        ur = U.r
        ucores = tt.tensor.to_list(U)

        ar = B.r
        acores = B.cores

        if k == 0:
            right = np.array([[[1]]])
            for i in range(d - 1, k, -1):
                right = rightThreeMatrixHalfdot(ucores[i], acores[i], right)

            return acores[0], right

        else:
            u0 = reshape(ucores[0], [n[0], ur[1]])
            left = np.zeros([ur[1], ar[1], ur[1]])
            for k1 in range(ar[1]):
                left[:, k1, :] = u0.T.dot(acores[0][k1].dot(u0))
            for i in range(1, k):
                left = leftThreeMatrixHalfdot(ucores[i], acores[i], left)

            right = np.array([[[1]]])
            for i in range(d - 1, k, -1):
                right = rightThreeMatrixHalfdot(ucores[i], acores[i], right)

            left = np.transpose(left, axes=(0, 2, 1))
            left = reshape(left, [ur[k] * ur[k], ar[k]])
            right = np.transpose(right, axes=(0, 2, 1))
            right = reshape(right, [ur[k + 1] * ur[k + 1], ar[k + 1]])
            a = reshape(acores[k], [ar[k], n[k] * n[k], ar[k + 1]])
            mid = rankOneProduct(a, left=left, right=right)
            mid = reshape(mid, [ur[k], ur[k], n[k], n[k], ur[k + 1], ur[k + 1]])
            mid = np.transpose(mid, axes=(0, 2, 4, 1, 3, 5))

            return reshape(mid, [ur[k] * n[k] * ur[k + 1], ur[k] * n[k] * ur[k + 1]])