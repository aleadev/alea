"""
General interface for a solver of a linear equation system.
 @author: Manuel Marschall
"""


class LesSolver(object):
    # region Init
    def __init__(self, lhs, rhs):
        """
        defines the left and right hand side of a linear system of equations
        :param lhs: left hand side (operator)
        :param rhs: right hand side (vector/operator)
        """
        self.lhs = lhs
        self.rhs = rhs
    # endregion

    # region solve
    def solve(self):
        """
        solver method
        :return: result of Ax=b
        """
        raise NotImplemented("the abstract class does not include the solve method")
    # endregion
