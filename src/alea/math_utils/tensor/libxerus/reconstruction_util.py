from alea.utils.progress.bar import Bar
import xerus as xe
from dolfin import (Function, FunctionSpace, inner, dx, TestFunction, TrialFunction, assemble, solve,
                    as_backend_type)
import numpy as np
import scipy.sparse as sps
from scipy.sparse.linalg.dsolve import factorized
from alea.math_utils.tensor.extended_fem_tt import (ExtendedFEMTT, ExtendedTT, BasisType)


def reconstruct_project(_samples,                   # type: np.array
                        nodes,                      # type: list
                        fs,                         # type: FunctionSpace
                        new_fs=None,                # type: None or FunctionSpace
                        adf_tol=1e-8,               # type: float
                        adf_iter=1000,              # type: int
                        _poly_dim=10,               # type: int or list
                        use_rb=True,                # type: bool
                        ew_tol=1e-16,               # type: None or float
                        poly_sys="L",               # type: str
                        identity_init=False
                        ):
    """ 
    Implements the xerus reconstruction by taking samples of size NxM (N=fs.dim(), M=#of samples) and corresponding
    nodes of size M and build a tensor train fit, using the ADF algorithm.
    Optional: project samples onto another function space first
    Optional: use the reduced basis approach to fit the tensor to a global (reduced) basis instead
    :param _samples: samples of FE functions, array of shape (N, M)
    :param nodes: nodes used to create the samples
    :param fs: FunctionSpace were _samples are defined on N=fs.dim()
    :param new_fs: optional: new FunctionSpace to project to
    :param adf_tol: tolerance for the ADF algorithm
    :param adf_iter: Number of iteration in the ADF algorithm
    :param _poly_dim: stochastic polynomial dimension, either an int or a list of size len(nodes[0])
    :param use_rb: flag to use the reduced basis approach
    :param ew_tol: threshold to truncate the reduced basis at given eigenvalues
    :return: ExtendedFEMTensor
    """
    # TODO: adapt new functionality of xerus 2019 version
    use_cpp_code = False                            # unused flag to call an external c++ file
    use_external_call = False                       # unused flag to call an external python file

    N = _samples.shape[0]                           # finite element space dimension
    M = _samples.shape[1]                           # number of samples

    samples = None                                  # init used samples
    m = None                                        # init rb basis
    if N != fs.dim():
        raise ValueError("The samples must be defined on the current fs.")

    # region Project samples onto new function space
    if new_fs is not None and N != new_fs.dim():    # a new function space is given -> projection
        # ####
        #  We compute the projection by hand, since it involves solving a LES Mx = b for every sample and we
        #  can speed up the solving process by building a Matrix decomposition first M = LR
        #  and use forward-backward substitution.
        # ####
        # region build LR decomposition of mass matrix
        w = TestFunction(new_fs)
        Pv = TrialFunction(new_fs)
        a = inner(w, Pv) * dx
        A = assemble(a)
        mass = sps.csc_matrix(as_backend_type(A).sparray())
        mass.astype("d")
        sp_solve = factorized(mass)
        # endregion
        bar = Bar("project samples onto new fs", max=_samples.shape[1])
        samples = np.zeros((new_fs.dim(), _samples.shape[1]))
        old_fun = Function(fs)
        for k in range(M):
            old_fun.vector().set_local(_samples[:, k])
            b = assemble(inner(w, old_fun) * dx)

            samples[:, k] = sp_solve(b.array())
            # ####
            # # alternatively, use the fenics projection. Probably slower for M >> 1
            # samples[:, k] = project(old_fun, curr_fs).vector().array()
            # # assert np.linalg.norm(new_fun.vector().array() - samples[:, k]) < 1e-10
            # ####
            bar.next()
        bar.finish()
    else:
        samples = _samples
        print("Samples have the same length as the function space dimension. Are they aligned? -> no projection")
    # endregion
    # region compute reduced basis spanned by the samples
    if use_rb:
        m, ew, _ = np.linalg.svd(samples, full_matrices=False, compute_uv=True)
        ew_err = np.sum(ew[ew <= ew_tol])
        ew = ew[ew > ew_tol]

        print("err: {}, {} samples rounded from fs-dim {} to {}".format(ew_err, samples.shape[1], m.shape[0], len(ew)))

        m = m[:, :len(ew)]

        trafo_samples = m.T.dot(samples)
    else:
        trafo_samples = samples
        # trafo_samples = np.loadtxt("obs_example.nd", delimiter=",")
        # nodes = np.loadtxt("sam_example.nd", delimiter=",")
    # endregion

    # region define tensor dimensions
    if isinstance(_poly_dim, list):
        poly_dim = _poly_dim
    else:
        poly_dim = [_poly_dim] * len(nodes[0])
    if use_rb:
        dimension = [m.shape[1]] + poly_dim
    else:
        dimension = [samples.shape[0]] + poly_dim
    # endregion

    # region start tensor reconstruction
    if use_cpp_code:

        bar = Bar("write out samples", max=trafo_samples.shape[1])
        for lia in range(trafo_samples.shape[1]):
            with open("cpp_recon/data{}.dat".format(lia), "wb") as f:
                for lib in range(len(nodes[lia])):
                    f.write(str(nodes[lia][lib]) + " ")
                f.write("\n")
                for lib in range(trafo_samples.shape[0]):
                    f.write(str(trafo_samples[lib, lia]) + " ")
            bar.next()
        bar.finish()

        import subprocess
        subprocess.call("./cpp_recon/a.out", shell=True)
        res = xe.load_from_file("cpp_recon/result.dat")
        import os
        os.remove("cpp_recon/result.dat")
    elif use_external_call and False:
        np.save("xerus_samples.npy", trafo_samples)
        np.save("xerus_nodes.npy", nodes)
        np.save("xerus_dimension.npy", dimension)
        np.save("xerus_config.npy", np.array([adf_iter, adf_tol]))

        import subprocess
        subprocess.check_call("python xerus_reconstruction.py", shell=True)

        res = xe.load_from_file("xerus_result.dat")
    else:
        measurements = xe.UQMeasurementSet()  # create Xerus measurement Set
        for lia in range(trafo_samples.shape[1]):
            y = nodes[lia]
            sample = trafo_samples[:, lia]
            # noinspection PyTypeChecker,PyCallByClass
            measurements.add(y, xe.Tensor.from_ndarray(sample))
        if poly_sys == "L":
            basis = xe.PolynomBasis.Legendre  # define flag of used polynomials in xerus
        elif poly_sys == "H":
            basis = xe.PolynomBasis.Hermite
        else:
            raise ValueError("Unknown basis type: {}".format(poly_sys))
        if not identity_init:
            res = xe.uq_ra_adf(measurements, basis, dimension, adf_tol, adf_iter)
        else:
            iv = []
            for lia in range(len(dimension)):
                if lia == 0:

                    iv.append(np.ones((1, dimension[lia], 1)))
                else:
                    loc_core = np.zeros((1, dimension[lia], 1))
                    loc_core[0, 1, 0] = 1
                    iv.append(loc_core)
            iv = ExtendedTT(iv, basis=[BasisType.points]*len(dimension))
            iv = iv.to_xerus_tt()
            iv.assume_core_position(0)
            print(iv.dimensions)
            res = xe.uq_ra_adf_iv(iv, measurements, basis, adf_tol, adf_iter)
    # endregion
    if poly_sys == "L":
        basis_list = [BasisType.points] + [BasisType.Legendre] * len(nodes[0])
    elif poly_sys == "H":
        basis_list = [BasisType.points] + [BasisType.Hermite] * len(nodes[0])
    else:
        raise ValueError("Unknown basis type: {}".format(poly_sys))
    res = ExtendedTT.from_xerus_tt(res, basis_list)  # Store as extended TT
    if use_rb:
        res.components[0] = np.dot(m, res.components[0][0, :, :]).reshape((1, m.shape[0], -1), order="F")
    if new_fs is not None:
        retval = ExtendedFEMTT(res.components, basis_list, new_fs)
    else:
        retval = ExtendedFEMTT(res.components, basis_list, fs)
    return retval
