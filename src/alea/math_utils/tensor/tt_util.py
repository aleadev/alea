from __future__ import (division, print_function)
from alea.math_utils.param_pde.affine_field import AffineField
from dolfin import (FunctionSpace, project, refine, cells, FiniteElement, interpolate)
import numpy as np
import scipy
import ffc
# import tt
from decimal import *
import math
from scipy.linalg import svd, qr
from alea.math_utils.tensor.extended_fem_tt import (ExtendedFEMTT, BasisType)
from alea.utils.tictoc import TicToc


def get_extendedtt_from_affine_field(af,            # type: AffineField
                                     fs,            # type: FunctionSpace
                                     m              # type: int
                                     ):             # type: (...) -> ExtendedFEMTT
    """
    creates a Legendre representation of the affine sum expansion
    a_0(x) + sum_k^M a_k(x)y_k
    projected onto the given function space.
    The Legendre representation is of the form
    a(x, y) = sum_{k_0, k_M=0}^{[M]} A_0[x, k_0] prod_{m=1}^M sum_{mu_m=0}^1 A_m[k_{m-1},mu_m, k_m] P_{mu_m}(y_m)
    where
    A_0[x, k_0] = a_k(x) projected onto fs
    and
    A_m[:, 0, :] = diag((1, ..., 1)) - e_{m, m}
    A_m[:, 0, :] = e_{m ,m}
    with e_{i, j} is the matrix of all zeros, except for the (i, j) entry, which is equal to 1
    :param af: AffineField
    :param fs: FunctionSpace
    :param m: expansion Length parameter
    :return: ExtendedFEMTT
    """
    assert isinstance(af, AffineField)
    assert isinstance(fs, FunctionSpace)
    first_comp = np.zeros((1, fs.dim(), m+1))
    comp_list = []
    basis_list = [BasisType.points] + [BasisType.Legendre] * m
    for lia in range(m+2):                          # loop through all components of the desired tensor
        if lia == m + 1:
            comp_list[-1] = np.einsum('ijk, k-> ij', comp_list[-1], np.ones(m+1)).reshape((m+1, 2, 1))
            continue
        amp_func = project(af[lia][0][0], fs)       # project current coef function onto fs
        #                                           # add the function vector into the first component
        first_comp[0, :, lia] = amp_func.vector()[:]
        if lia == 0:                                # the first component is only the mean value
            continue
        # TODO: better sparse
        curr_comp = np.zeros((m+1, 2, m+1))         # other components are constructed as described above
        curr_comp[:, 0, :] = np.eye(m+1, m+1)
        curr_comp[lia, 0, lia] = 0
        curr_comp[lia, 1, lia] = 1  # np.sqrt(1/3)       # due to normalisation of the polynomials
        comp_list.append(curr_comp)
    comp_list.insert(0, first_comp)

    #                                               # return an extended finite element tensor
    retval = ExtendedFEMTT(comp_list, basis_list, fs)
    retval.normalise()
    # retval.canonicalize_left()
    return retval


def ravel(A):
    return np.ravel(np.array(A), order='F')


def reshape(vec, shape):
    return np.reshape(vec, shape, order='F')


# Not tested???
def ttNormalize(U, k=None, approxtol=1e-14):
    if k is None:
        k = U.d

    V = ttRightOrthogonalize(U, approxtol)

    vcore = V.core
    n = V.n
    d = V.d
    r = V.r
    pos = V.ps

    for i in range(k - 1):
        v = reshape(vcore[pos[i] - 1:pos[i + 1] - 1], (r[i] * n[i], r[i + 1]))

        a, b, c = np.linalg.svd(v,full_matrices=0)
        # a, b, c = scipy.linalg.svd(v, full_matrices=0)

        x = reshape(vcore[pos[i + 1] - 1:pos[i + 2] - 1], (r[i + 1], n[i + 1] * r[i + 2]))
        if i < d - 2:
            right = vcore[pos[i + 2] - 1:pos[d] - 1]
        r[i + 1] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)
        pos = np.cumsum([1] + (n * r[:d] * r[1:d + 1]).tolist())

        a = a[:, :r[i + 1]]
        b = np.diag(b[:r[i + 1]])
        c = c[:r[i + 1], :]

        x = b.dot(c.dot(x))

        vcore[pos[i] - 1:pos[i + 1] - 1] = ravel(a)
        vcore[pos[i + 1] - 1:pos[i + 2] - 1] = ravel(x)
        if i < d - 2:
            vcore[pos[i + 2] - 1:pos[d] - 1] = ravel(right)

    V.ps = pos
    V.r = r
    V.core = vcore

    return V


def ttRightOrthogonalize(U, approxtol=1e-14):
    if isinstance(U, list):
        U = tt.vector.from_list(U)
    V = U.copy()
    vcore = V.core
    r = V.r
    n = V.n
    d = V.d
    pos = V.ps

    for i in range(d - 1, 0, -1):
        right = vcore[pos[i] - 1:pos[i + 1] - 1]
        right = reshape(right, (r[i], n[i] * r[i + 1]))

        left, mid, right = np.linalg.svd(right,full_matrices=0)
        # left, mid, right = scipy.linalg.svd(right, full_matrices=0)

        u = reshape(vcore[pos[i - 1] - 1:pos[i] - 1], (r[i - 1] * n[i - 1], r[i]))
        if i + 1 < d:
            v = vcore[pos[i + 1] - 1:pos[d] - 1]
        r[i] = max(len(np.nonzero(np.array(mid) > approxtol)[0].tolist()), 1)

        right = right[:r[i], :]
        left = np.dot(u, np.dot(left[:, :r[i]], np.diag(mid[:r[i]])))

        pos = np.cumsum([1] + (n * r[:d] * r[1:d + 1]).tolist())
        vcore[pos[i] - 1:pos[i + 1] - 1] = ravel(right)
        vcore[pos[i - 1] - 1:pos[i] - 1] = ravel(left)
        if i + 1 < d:
            vcore[pos[i + 1] - 1:pos[d] - 1] = v
        vcore = vcore[:pos[d] - 1]

    V.core = vcore
    V.ps = pos
    V.r = r
    return V


def ttMoveOrthogonality(U, k, direction, approxtol=1e-14, P=None):
    d = U.d
    n = U.n
    ranks = U.r
    cores = tt.vector.to_list(U)

    if direction == 1:
        if k == 0 and P is not None:
            u = reshape(cores[0], [n[0], ranks[1]])
            if isinstance(P, list):
                # print("u shape: {}".format(u.shape))
                uau = np.zeros((n[0], ranks[1]))
                for lia in range(ranks[1]):
                    # print("P[{}] shape: {}".format(lia, P[lia].shape))
                    uau[:, lia] = P[lia].dot(u[:, lia])

                    # print("uau shape: {}".format(uau[lia].shape))
                # print("uau shape: {}".format(uau.shape))
                # assert (np.allclose(uau, u))
                uau = np.dot(u.T, uau)
            else:
                uau = np.dot(u.T, (P.dot(u)))
            a,b,c = np.linalg.svd(uau,full_matrices=0)
            # a, b, c = scipy.linalg.svd(uau, full_matrices=0)
            right = reshape(cores[1], [ranks[1], n[1] * ranks[2]])
            ranks[1] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)

            a = a[:, :ranks[1]]
            b = np.diag(b[:ranks[1]])
            s = np.linalg.norm(b)
            c = c[:ranks[1], :]
            c = c / np.sqrt(s)
            d = np.sqrt(b / s)
            dinv = np.linalg.inv(d)

            cores[0] = reshape(np.dot(u.dot(c.T), dinv), [1, n[0], ranks[1]])
            cores[1] = reshape(np.dot(d, (s * c.dot(right))), [ranks[1], n[1], ranks[2]])
            return tt.vector.from_list(cores)
        else:
            u = reshape(cores[k], [ranks[k] * n[k], ranks[k + 1]])
            a,b,c = np.linalg.svd(u,full_matrices=0)
            # a, b, c = scipy.linalg.svd(u, full_matrices=0)
            right = reshape(cores[k + 1], [ranks[k + 1], n[k + 1] * ranks[k + 2]])
            ranks[k + 1] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)

            a = a[:, :ranks[k + 1]]
            b = np.diag(b[:ranks[k + 1]])
            c = c[:ranks[k + 1], :]

            cores[k] = reshape(a, [ranks[k], n[k], ranks[k + 1]])
            cores[k + 1] = reshape(np.dot(np.dot(b, c), right), [ranks[k + 1], n[k + 1], ranks[k + 2]])
            return tt.vector.from_list(cores)
    else:
        assert direction == -1
        u = reshape(cores[k], [ranks[k], n[k] * ranks[k + 1]])
        a,b,c = np.linalg.svd(u,full_matrices=0);
        # a, b, c = scipy.linalg.svd(u, full_matrices=0);
        left = reshape(cores[k - 1], [ranks[k - 1] * n[k - 1], ranks[k]])
        ranks[k] = max(len(np.nonzero(np.array(b) > approxtol)[0].tolist()), 1)

        a = a[:, :ranks[k]]
        b = np.diag(b[:ranks[k]])
        c = c[:ranks[k], :]

        cores[k] = reshape(c, [ranks[k], n[k], ranks[k + 1]])
        cores[k - 1] = reshape(np.dot(np.dot(left, a), b), [ranks[k - 1], n[k - 1], ranks[k]])
        return tt.vector.from_list(cores)

    return 0


def ttTruncate(U, ranks, approxtol=1e-14):
    # from right to left in this version
    if isinstance(ranks, int):
        ranks = ranks * np.ones([U.d, 1])

    V = U.copy()
    vcore = V.core
    r = V.r
    n = V.n
    d = V.d
    pos = V.ps

    for i in range(d - 1, 0, -1):  # d:-1:2
        right = vcore[pos[i] - 1:pos[i + 1] - 1]
        right = reshape(right, (r[i], n[i] * r[i + 1]))

        left, mid, right = np.linalg.svd(right,full_matrices=0)
        # left, mid, right = scipy.linalg.svd(right, full_matrices=0)

        u = reshape(vcore[pos[i - 1] - 1:pos[i] - 1], (r[i - 1] * n[i - 1], r[i]))
        if i + 1 < d:
            v = vcore[pos[i + 1] - 1:pos[d] - 1]
        r[i] = max(len(np.nonzero(np.array(mid) > approxtol)[0].tolist()), 1)
        if ranks[i] < r[i]:
            r[i] = ranks[i]

        right = right[:r[i], :]
        left = np.dot(u, np.dot(left[:, :r[i]], np.diag(mid[:r[i]])))

        pos = np.cumsum([1] + (n * r[:d] * r[1:d + 1]).tolist())
        vcore[pos[i] - 1:pos[i + 1] - 1] = ravel(right)
        vcore[pos[i - 1] - 1:pos[i] - 1] = ravel(left)
        if i + 1 < d:
            vcore[pos[i + 1] - 1:pos[d] - 1] = v
        vcore = vcore[:pos[d] - 1]

    V.core = vcore
    V.ps = pos
    V.r = r
    return V


def ttIsOrthogonalized(U, i):
    # For validation purposes only, very slow
    Cores = tt.vector.to_list(U)
    d = U.d
    n = U.n
    r = U.r

    for k in range(i - 1):
        u = reshape(Cores[k], [r[k] * n[k], r[k + 1]])
        c = u.T.dot(u)
        if np.linalg.norm(c - np.eye(r[k + 1])) > 1e-10:
            return False

    for k in range(i, d):
        u = reshape(Cores[k], [r[k], n[k] * r[k + 1]])
        c = u.dot(u.T)
        if np.linalg.norm(c - np.eye(r[k])) > 1e-10:
            return False

    return True


def ttGetVec(U, vec):
    cores = tt.vector.to_list(U)
    r = U.r
    n = U.n
    d = U.d

    right = cores[d - 1][:, int(vec[d - 2]), 0]
    for i in range(d - 2, 0, -1):
        left = cores[i][:, int(vec[i - 1]), :]
        right = left.dot(right)
    left = reshape(cores[0], [n[0], r[1]])

    right = ravel(left.dot(right))
    return right


def rankOneProduct(comp, left=None, mid=None, right=None):
    l2 = comp.shape[0]
    m2 = comp.shape[1]
    r2 = comp.shape[2]

    if left is not None:
        if len(left.shape) > 1:
            l1 = left.shape[0]
        else:
            l1 = 1
        comp = reshape(comp, [l2, m2 * r2])
        comp = left.dot(comp)
        comp = reshape(comp, [l1, m2, r2])
        l2 = l1

    if mid is not None:
        if len(mid.shape) > 1:
            m1 = mid.shape[0]
        else:
            m1 = 1
        comp = np.transpose(comp, axes=(1, 0, 2))
        comp = reshape(comp, [m2, l2 * r2])
        comp = mid.dot(comp)
        comp = reshape(comp, [m1, l2, r2])
        comp = np.transpose(comp, axes=(1, 0, 2))
        m2 = m1

    if right is not None:
        if len(right.shape) > 1:
            r1 = right.shape[0]
        else:
            r1 = 1
        comp = reshape(comp, [l2 * m2, r2])
        comp = comp.dot(right.T)
        comp = reshape(comp, [l2, m2, r1])

    return comp


def leftThreeHalfdot(U, V):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]

    vr3 = V.shape[2]

    u = reshape(U, [ur1 * ur2, ur3])
    v = reshape(V, [ur1 * ur2, vr3])

    return np.dot(u.T, v)


def rightThreeHalfdot(U, V):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]

    vr1 = V.shape[0]

    u = reshape(U, [ur1, ur2 * ur3])
    v = reshape(V, [vr1, ur2 * ur3])

    return np.dot(u, v.T)


def leftThreeMatrixHalfdot(U, A, left):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]
    ar1 = A.shape[0]
    ar4 = A.shape[3]

    u = reshape(U, [ur1, ur2 * ur3])
    left = rankOneProduct(left, left=u.T, right=u.T)
    left = reshape(left, [ur2, ur3, ar1, ur2, ur3])
    left = np.transpose(left, axes=(1, 4, 0, 2, 3))
    left = reshape(left, [ur3 * ur3, ur2 * ar1 * ur2])
    a = np.transpose(A, axes=(1, 0, 2, 3))
    a = reshape(a, [ur2 * ar1 * ur2, ar4])
    left = np.dot(left, a)
    left = reshape(left, [ur3, ur3, ar4])
    left = np.transpose(left, axes=(0, 2, 1))

    return left


def rightThreeMatrixHalfdot(U, A, right):
    ur1 = U.shape[0]
    ur2 = U.shape[1]
    ur3 = U.shape[2]
    ar1 = A.shape[0]
    ar4 = A.shape[3]

    u = reshape(U, [ur1 * ur2, ur3])
    right = rankOneProduct(right, left=u, right=u)
    right = reshape(right, [ur1, ur2, ar4, ur1, ur2])
    right = np.transpose(right, axes=(0, 3, 1, 2, 4))
    right = reshape(right, [ur1 * ur1, ur2 * ar4 * ur2])
    a = np.transpose(A, axes=(1, 3, 2, 0))
    a = reshape(a, [ur2 * ar4 * ur2, ar1])
    right = np.dot(right, a)
    right = reshape(right, [ur1, ur1, ar1])
    right = np.transpose(right, axes=(0, 2, 1))

    return right


def tt_round(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""
    if not isinstance(_ten, list):
        raise ValueError("tt_round: ten is not a list")

    d = len(_ten)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = tt.vector.from_list(_ten).copy()
    # print(ten)
    ten = tt.vector.to_list(ten)
    # print("ranks= {}".format(ranks))
    err = 0
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0], ten[i].shape[1] * ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition

        left, mid, right = svd(right, full_matrices=False)
        if i > 1:
            u = reshape(ten[i - 1], [ten[i - 1].shape[0] * ten[i - 1].shape[1], ten[i - 1].shape[2]])
        else:
            u0 = ten[0]
            # for lia in mid:
            # print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ranks[i] = np.min([ranks[i], right.shape[0]])
        right = right[:ranks[i], :]
        err += np.sum(np.array(mid[ranks[i] + 1:]) ** 2)
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        ten[i] = reshape(right, [ranks[i], ten[i].shape[1], ranks[i + 1]])
        if i > 1:
            left = np.dot(u, np.dot(left[:, :ranks[i]], np.diag(mid[:ranks[i]])))
            # print(" left matricisation shape= {}".format(left.shape))
            # print("  reshape into = {}".format([ten[i-1].shape[0], ten[i-1].shape[1], ranks[i]]))
            ten[i - 1] = reshape(left, [ten[i - 1].shape[0], ten[i - 1].shape[1], ranks[i]])
        else:
            left = np.dot(left[:, :ranks[1]], np.diag(mid[:ranks[1]]))
            leftlist = []
            # for j in range(ranks[1]):
            #    leftlist.append(sum([u0[k] * left[k, j] for k in range(len(u0))]))
            # print leftlist
            # ten[0] = np.array(leftlist)
            ten[0] = np.array([np.dot(u0[0], left)])
            #     print("round error = {}".format(np.sqrt(err)))
    return ten


def tt_round_qr(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""

    print("test")
    if not isinstance(_ten, list):
        if isinstance(_ten, tt.vector):
            _ten = tt.vector.to_list(_ten)
        else:
            raise ValueError("tt_round: ten is not a list nor a tensor")
    d = len(_ten)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = tt.vector.from_list(_ten).copy()
    # print(ten)
    ten = tt.vector.to_list(ten)
    # print("ranks= {}".format(ranks))
    err = 0
    for i in range(d):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0] * ten[i].shape[1], ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        core, right = qr(right, mode='economic', pivoting=False, overwrite_a=True)
        if i < d - 1:
            right_core = reshape(ten[i + 1], [ten[i + 1].shape[0], ten[i + 1].shape[1] * ten[i + 1].shape[2]])

            # print("  reduced matrix shape= {}".format(core.shape))
            # print("  reshape into = {}".format([core.shape[0], ten[i].shape[1], ten[i].shape[2]]))
            ten[i] = reshape(core, [ten[i].shape[0], ten[i].shape[1], core.shape[1]])
            # print("  multiply R {} with right core {} ".format(right.shape, right_core.shape))
            right = np.dot(right, right_core)
            ten[i + 1] = reshape(right, [right.shape[0], ten[i + 1].shape[1], ten[i + 1].shape[2]])
    err = 0
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0], ten[i].shape[1] * ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        left, mid, right = svd(right, full_matrices=0)
        if i > 1:
            u = reshape(ten[i - 1], [ten[i - 1].shape[0] * ten[i - 1].shape[1], ten[i - 1].shape[2]])
        else:
            u0 = ten[0]
        # for lia in mid:
        #     print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ranks[i] = np.min([ranks[i], right.shape[0]])
        right = right[:ranks[i], :]
        err += np.sum(mid[ranks[i]:] ** 2)
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        ten[i] = reshape(right, [ranks[i], ten[i].shape[1], ranks[i + 1]])

        if i > 1:
            left = np.dot(u, np.dot(left[:, :ranks[i]], np.diag(mid[:ranks[i]])))
            # print(" left matricisation shape= {}".format(left.shape))
            # print("  reshape into = {}".format([ten[i-1].shape[0], ten[i-1].shape[1], ranks[i]]))
            ten[i - 1] = reshape(left, [ten[i - 1].shape[0], ten[i - 1].shape[1], ranks[i]])
        else:
            left = np.dot(left[:, :ranks[1]], np.diag(mid[:ranks[1]]))
            leftlist = []
            # for j in range(ranks[1]):
            #    leftlist.append(sum([u0[k] * left[k, j] for k in range(len(u0))]))
            # print leftlist
            # ten[0] = np.array(leftlist)
            ten[0] = np.array([np.dot(u0[0], left)])
    # print("rounding error QR : {}".format(np.sqrt(err)))
    return ten, np.sqrt(err)


def tt_round_nmf(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""

    if not isinstance(_ten, list):
        if isinstance(_ten, tt.vector):
            _ten = tt.vector.to_list(_ten)
        else:
            raise ValueError("tt_round: ten is not a list nor a tensor")
    d = len(_ten)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = tt.vector.from_list(_ten).copy()
    # print(ten)
    ten = tt.vector.to_list(ten)
    # print("ranks= {}".format(ranks))
    err = 0
    for i in range(d):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0] * ten[i].shape[1], ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        core, right = qr(right, mode='economic', pivoting=False, overwrite_a=True)
        if i < d - 1:
            right_core = reshape(ten[i + 1], [ten[i + 1].shape[0], ten[i + 1].shape[1] * ten[i + 1].shape[2]])

            # print("  reduced matrix shape= {}".format(core.shape))
            # print("  reshape into = {}".format([core.shape[0], ten[i].shape[1], ten[i].shape[2]]))
            ten[i] = reshape(core, [ten[i].shape[0], ten[i].shape[1], core.shape[1]])
            # print("  multiply R {} with right core {} ".format(right.shape, right_core.shape))
            right = np.dot(right, right_core)
            ten[i + 1] = reshape(right, [right.shape[0], ten[i + 1].shape[1], ten[i + 1].shape[2]])
    err = 0
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        right = reshape(ten[i], [ten[i].shape[0], ten[i].shape[1] * ten[i].shape[2]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        left, right = nmf_gradient_method(right, ranks[i])
        if i > 1:
            u = reshape(ten[i - 1], [ten[i - 1].shape[0] * ten[i - 1].shape[1], ten[i - 1].shape[2]])
        else:
            u0 = ten[0]
        # for lia in mid:
        #     print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ranks[i] = np.min([ranks[i], right.shape[0]])
        right = right[:ranks[i], :]
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        ten[i] = reshape(right, [ranks[i], ten[i].shape[1], ranks[i + 1]])

        if i > 1:
            left = np.dot(u, left[:, :ranks[i]])
            # print(" left matricisation shape= {}".format(left.shape))
            # print("  reshape into = {}".format([ten[i-1].shape[0], ten[i-1].shape[1], ranks[i]]))
            ten[i - 1] = reshape(left, [ten[i - 1].shape[0], ten[i - 1].shape[1], ranks[i]])
        else:
            left = left[:, :ranks[1]]
            leftlist = []
            # for j in range(ranks[1]):
            #    leftlist.append(sum([u0[k] * left[k, j] for k in range(len(u0))]))
            # print leftlist
            # ten[0] = np.array(leftlist)
            ten[0] = np.array([np.dot(u0[0], left)])
    # print("rounding error QR : {}".format(np.sqrt(err)))
    return ten

def tt_nmf_from_full(_ten, ranks):
    """ Computes an approximation to a tt.vector with maximal ranks and right orthogonalizes the tensor"""

    shapes = _ten.shape
    d = len(_ten.shape)  # get dimension of the tensor as length of the list of cores
    if isinstance(ranks, int):
        ranks = [1] + [ranks] * (d - 1) + [1]
    ten = []
    curr_core = _ten
    # print("ranks= {}".format(ranks))
    for i in range(d - 1, 0, -1):
        # print("core {} shape= {}".format(i, ten[i].shape))
        #                                           # create last core by reshaping it to a matrix
        if i < d-1:
            reshaped_core = reshape(curr_core, [np.prod(shapes[:i]), shapes[i] * curr_core.shape[-1]])
        else:
            print("shapes:i-1 = {}".format(shapes[:i]))
            reshaped_core = reshape(curr_core, [np.prod(shapes[:i]), shapes[i]])
        # print(" matricisation shape= {}".format(right.shape))
        # left, mid, right = np.linalg.svd(right,full_matrices=0)
        #                                           # calculate singular value decomposition
        norm = np.linalg.norm(reshaped_core, ord='fro')
        curr_core, right = nmf_gradient_method(reshaped_core*(norm)**(-1), ranks[i])
        right *= norm
        #  left, mid, right = svd(reshaped_core, full_matrices=0)
        #  ranks[i] = np.min([ranks[i], right.shape[0]])
        #  right = right[:ranks[i], :]
        #  curr_core = np.dot(left[:, :ranks[1]], np.diag(mid[:ranks[1]]))
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))
        if i < d-1:
            ten.insert(0, reshape(right, [right.shape[0], shapes[i], ranks[i+1]]))
            if i == 1:
                ten.insert(0, reshape(curr_core, [1, shapes[i-1], ranks[i]]))
        else:
            ten.insert(0, reshape(right, [right.shape[0], right.shape[1], 1]))

        # for lia in mid:
        #     print lia
        # c.r[i] = max(len(np.nonzero(np.array(mid) > eps)[0].tolist()),1)
        ## ranks[i] = np.min([ranks[i], right.shape[0]])
        ## right = right[:ranks[i], :]
        # print("  reduced matrix shape= {}".format(right.shape))
        # print("  reshape into = {}".format([ranks[i], ten[i].shape[1], ranks[i+1]]))

    # print("rounding error QR : {}".format(np.sqrt(err)))
    return ten


def nmf_gradient_method(A, rank, tol=1e-10, lambd=1.0, delta=1.0, max_iters=100, d_improve_iters=10, sweeps=100):
    from alea.utils.tictoc import TicToc
    n = A.shape[0]
    m = A.shape[1]
    def gradient_decent_step(alpha, x, gradient):
        with TicToc(key="gradient step", active=False, do_print=False):
            retval = project_to_non_negative(x - alpha * gradient(x))
        return retval

    method_step = gradient_decent_step
    method_string = "gradient_descent"

    f = lambda W, H: (np.linalg.norm(A - np.dot(W, H), ord='fro') ** 2 + lambd * np.trace(np.dot(W.T, W) +
                                                                                          (delta - 1) * np.eye(
                                                                                              W.T.shape[0],
                                                                                              W.shape[1])))
    f_orig = lambda W, H: (np.linalg.norm(A - np.dot(W, H), ord='fro') ** 2 +
                           lambd * np.log(np.linalg.det(np.dot(W.T, W) + delta * np.eye(W.T.shape[0], W.shape[1]))))

    value = lambda W, H: np.linalg.norm(A - np.dot(W, H), ord='fro') / np.linalg.norm(A, ord='fro')

    def project_to_non_negative(mat):
        return mat.clip(min=0)

    def grad_f(W, H, half_sweep, _D_1=None):
        # if half_sweep is true, then H is fixed, otherwise W is fixed
        if _D_1 is None:
            _D_1 = np.eye(rank, rank)
            _key = "gradient evaluation"
        else:
            _key = "gradient evaluation (speed)"
        with TicToc(key=_key, active=True, do_print=False):
            if half_sweep is True:
                buffer_matrix = np.dot(W, H)
                # print("W*H: {}".format(buffer_matrix))
                buffer_matrix = buffer_matrix - A
                # print("W*H-A: {}".format(buffer_matrix))
                buffer_matrix = np.dot(buffer_matrix, H.T)
                # print("(W*H-A)*H.T: {}".format(buffer_matrix))
                buffer_matrix2 = np.dot(W, _D_1)
                # print("W*D: {}".format(buffer_matrix2))
                retval = 2*buffer_matrix + lambd * buffer_matrix2
                # print("retval: {}".format(retval))
                return retval
            else:
                return 2 * np.dot(W.T, (np.dot(W, H) - A))

    def grad_f_log(W, H, half_sweep):
        # if half_sweep is true, then H is fixed, otherwise W is fixed
        with TicToc(key="gradient evaluation (logdet)", active=True, do_print=False):
            if half_sweep is True:
                return 2 * np.dot((np.dot(W, H) - A), H.T) + lambd * np.dot(W, (
                np.dot(W.T, W) + delta * np.eye(rank, rank)))
            else:
                return 2 * np.dot(W.T, (np.dot(W, H) - A))

    print("calculate gradient method for lambda = {}".format(lambd))
    D_1 = np.zeros((rank, rank))
    D_2 = np.zeros((rank, rank))
    fun_speed = []

    functional_speed = []
    log_det_value_speed = []
    w = np.random.rand(n, rank) * 100
    # w = w * np.linalg.norm(np.dot(w.T, w))**(-1)
    h = np.random.rand(rank, m) *200
    # h = h * np.linalg.norm(np.dot(h.T, h))**(-1)
    w_speed = w
    h_speed = h
    #fun_speed.append(value(w_speed, h_speed))
    #functional_speed.append(f(w_speed, h_speed))

    end_all = False

    alpha = 0.01
    for d in range(d_improve_iters):
        if d < 4:
            alpha *= 2
        if end_all is True:
            break
        if d == 0:
            D_1 = np.eye(rank, rank)
            D_2 = np.zeros((rank, rank))
        else:
            if d % 5 == 0:
                TicToc.sortedTimes()
            with TicToc(key="SVD", active=True, do_print=False):
                # _sigma = np.linalg.svd(np.dot(D_1, (np.dot(w_speed.T, w_speed) + delta*np.eye(rank, rank)) + D_2),
                #                                full_matrices=False, compute_uv=False)
                # matrix_to_svd = (np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank)) * np.linalg.norm(
                #     np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank)) ** (-1)
                # print("normalizing: {}".format(np.linalg.norm(
                #     np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank))))
                _sigma = np.linalg.svd(np.dot(w_speed.T, w_speed) + np.eye(rank, rank), full_matrices=False,
                                       compute_uv=False)  # * np.linalg.norm(
                # np.dot(w_speed.T, w_speed) + delta * np.eye(rank, rank))**(0.5)


                print("new eigenvalues to adapt to: {}".format(_sigma))
                if np.min(_sigma) < 1e-5:
                    end_all = True
                    break
                D_1 = np.diag(np.array([mu ** (-2) for mu in _sigma]))
                D_2 = np.diag(np.array([np.log(mu ** 2) - 1 for mu in _sigma]))
        for k in range(max_iters):
            alpha = alpha
            for i in range(sweeps):

                if method_string == "gradient_descent":
                        # with TicToc(key="normalisation (truncated)", active=True, do_print=False):
                        #     w *= np.linalg.norm(w, ord=1) ** (-1)
                        #     h *= np.linalg.norm(h, ord=1) ** (-1)
                    with TicToc(key="gradient step (speed)", active=True, do_print=False):
                        w_speed = method_step(alpha, w_speed, lambda W: grad_f(W, h_speed, True, _D_1=D_1))
                        h_speed = method_step(alpha, h_speed, lambda H: grad_f(w_speed, H, False, _D_1=D_1))
                        # with TicToc(key="normalisation (speed)", active=True, do_print=False):
                        #     w_speed *= np.linalg.norm(w_speed, ord=1) ** (-1)
                        #     h_speed *= np.linalg.norm(h_speed, ord=1) ** (-1)
                        # with TicToc(key="normalisation (logdet)", active=True, do_print=False):
                        #     w_log *= np.linalg.norm(w_log, ord=1) ** (-1)
                        #     h_log *= np.linalg.norm(h_log, ord=1) ** (-1)
                    # with TicToc(key="gradient step (det)", active=True, do_print=False):
                    #     w_log = method_step(alpha, w_log, lambda W: grad_f_det(W, h_det, True))
                    #     h_log = method_step(alpha, h_log, lambda H: grad_f_det(w_det, H, False))

                    # print("w = {}".format(w_speed))
                    # print("h = {}".format(h_speed))
                    if w_speed.max <= 0:
                        print("w_speed is zero in step {}".format((d, k)))

            # fun_speed.append(value(w_speed, h_speed))
            # functional_speed.append(f(w_speed, h_speed))
            # log_det_value_speed.append(f_orig(w_speed, h_speed))
    return w_speed, h_speed


# region def has full rank
def has_full_rank(ten):
    if isinstance(ten, tt.vector):
        ten = tt.vector.to_list(ten)

    n = 1
    for lia in range(len(ten)-1, -1, -1):
        if lia == len(ten)-1:
            if ten[lia].shape[2] != 1:
                raise ValueError("last rank should be == 1")
        if lia == 0:
            if ten[lia].shape[0] != 1:
                raise ValueError("first rank should be == 1")
            break
        n *= ten[lia].shape[1]
        r = ten[lia].shape[0]
        if r < n:
            return False
    return True
# endregion

# region def compute Frobenius norm of a given tensor
def frob_norm(U):
    """
    computes the Frobenius norm of a tensor using a faster algorithm than the Oseledets implementation by contracting
    over the ranks and dimensions in one step.
    :param U: tt.vector or numpy list or list of components
    :return: Frobenius norm of U
    """
    if type(U).__module__ == np.__name__ or isinstance(U, list):
        U = U                                       # U is already a numpy array
    elif isinstance(U, tt.vector):
        U = tt.vector.to_list(U)                    # U is given as tt.vector -> convert to list of components
    elif isinstance(U, float) or isinstance(U, int):
        return U                                    # numbers do not need to be converted
    else:                                           # raise TypeError
        raise TypeError("unknown Type: U:{}".format(type(U)))

    retval = 0
    for lia in range(len(U)-1, -1, -1):
        # print("U[{}].shape = {}".format(lia, U[lia].shape))
        if lia == len(U)-1:
            assert U[lia].shape[2] == 1
            #                                       # retval shape [r_{M-1}, r_{M-1}']
            retval = np.dot(U[lia][:, :, 0], U[lia][:, :, 0].T)
            continue
        # print("retval.shape= {}".format(retval.shape))
        #                                           # reshape U_m -> [r_{m-1}*n_m, r_m]
        core = np.reshape(U[lia], (U[lia].shape[0]*U[lia].shape[1], U[lia].shape[2]))
        retval = np.dot(core, retval)               # dot [r_{m-1}*n_m, r_m] x [r_m, r_m'] -> [r_{m-1}*n_m, r_m']
        # print("  2 retval.shape= {}".format(retval.shape))

        retval = np.reshape(retval, (U[lia].shape[0], U[lia].shape[1], retval.shape[1]))
        #                                           # reshape [r_{m-1}, n_m * r_m']
        retval = np.reshape(retval, (U[lia].shape[0], U[lia].shape[1]*retval.shape[2]))
        # print("  3 retval.shape= {}".format(retval.shape))

        #                                           # reshape core [r_{m-1}', n_m * r_m']
        core = np.reshape(U[lia], (U[lia].shape[0], U[lia].shape[1] * U[lia].shape[2]))
        # print("  core.shape= {}".format(core.shape))

        retval = np.dot(retval, core.T)
    assert len(retval.shape) == 2
    assert retval.shape[0] == 1
    assert retval.shape[1] == 1
    if retval[0, 0] <= 0:
        return 0.0
    retval = retval[0, 0]
    return np.sqrt(retval)
# endregion

def ttRandomDeterministicRankOne(n):
    U = tt.rand(n,len(n),1)
    pos = U.ps
    ucore = U.core
    for i in range(1,len(n)):
        ucore[pos[i]-1:pos[i+1]-1] = ravel(np.array(np.eye(n[i],1)))

    U.core = ucore
    return U


def own_tensordot(a, b, axes=2):
    """
    Compute tensor dot product along specified axes for arrays >= 1-D.

    Given two tensors (arrays of dimension greater than or equal to one),
    `a` and `b`, and an array_like object containing two array_like
    objects, ``(a_axes, b_axes)``, sum the products of `a`'s and `b`'s
    elements (components) over the axes specified by ``a_axes`` and
    ``b_axes``. The third argument can be a single non-negative
    integer_like scalar, ``N``; if it is such, then the last ``N``
    dimensions of `a` and the first ``N`` dimensions of `b` are summed
    over.

    Parameters
    ----------
    a, b : array_like, len(shape) >= 1
        Tensors to "dot".

    axes : int or (2,) array_like
        * integer_like
          If an int N, sum over the last N axes of `a` and the first N axes
          of `b` in order. The sizes of the corresponding axes must match.
        * (2,) array_like
          Or, a list of axes to be summed over, first sequence applying to `a`,
          second to `b`. Both elements array_like must be of the same length.

    See Also
    --------
    dot, einsum

    Notes
    -----
    Three common use cases are:
        * ``axes = 0`` : tensor product :math:`a\\otimes b`
        * ``axes = 1`` : tensor dot product :math:`a\\cdot b`
        * ``axes = 2`` : (default) tensor double contraction :math:`a:b`

    When `axes` is integer_like, the sequence for evaluation will be: first
    the -Nth axis in `a` and 0th axis in `b`, and the -1th axis in `a` and
    Nth axis in `b` last.

    When there is more than one axis to sum over - and they are not the last
    (first) axes of `a` (`b`) - the argument `axes` should consist of
    two sequences of the same length, with the first axis to sum over given
    first in both sequences, the second axis second, and so forth.
    """
    try:
        iter(axes)
    except:
        axes_a = list(range(-axes, 0))
        axes_b = list(range(0, axes))
    else:
        axes_a, axes_b = axes
    try:
        na = len(axes_a)
        axes_a = list(axes_a)
    except TypeError:
        axes_a = [axes_a]
        na = 1
    try:
        nb = len(axes_b)
        axes_b = list(axes_b)
    except TypeError:
        axes_b = [axes_b]
        nb = 1

    a, b = np.asarray(a), np.asarray(b)
    as_ = a.shape
    nda = len(a.shape)
    bs = b.shape
    ndb = len(b.shape)
    equal = True
    if na != nb:
        equal = False
    else:
        for k in range(na):
            if as_[axes_a[k]] != bs[axes_b[k]]:
                equal = False
                break
            if axes_a[k] < 0:
                axes_a[k] += nda
            if axes_b[k] < 0:
                axes_b[k] += ndb
    if not equal:
        raise ValueError("shape-mismatch for sum")

    # Move the axes to sum over to the end of "a"
    # and to the front of "b"
    notin = [k for k in range(nda) if k not in axes_a]
    newaxes_a = notin + axes_a
    N2 = 1
    for axis in axes_a:
        N2 *= as_[axis]
    newshape_a = (-1, N2)
    olda = [as_[axis] for axis in notin]

    notin = [k for k in range(ndb) if k not in axes_b]
    newaxes_b = axes_b + notin
    N2 = 1
    for axis in axes_b:
        N2 *= bs[axis]
    newshape_b = (N2, -1)
    oldb = [bs[axis] for axis in notin]

    at = a.transpose(newaxes_a).reshape(newshape_a)
    bt = b.transpose(newaxes_b).reshape(newshape_b)
    out = np.empty((at.shape[0], bt.shape[-1]))
    np.dot(at, bt, out=out)
    return out.reshape(olda + oldb)

def generate_lognormal_tt(B, hdeg, ranks, bxmax, theta=0, rho=1, acc=1e-7):

    d = B.shape[1]
    n = [B.shape[0]] + (d-1)*[hdeg]
    cores = d*[[]]

    right = np.ones([B.shape[0],1])
    for iexp in range(d-1,0,-1):
        coeffs = np.zeros([B.shape[0],hdeg,ranks[iexp+1]])
        tau = np.exp(theta*rho*bxmax[iexp-1])
        fac = np.exp(np.power(B[:,iexp]*tau,2)/2)
        for ig in range(hdeg):
            precoeff = (np.power(B[:,iexp]*tau,ig)/np.sqrt(float(np.math.factorial(ig))))*fac
            for k in range(ranks[iexp+1]):
                coeffs[:,ig,k] = precoeff*right[:,k]

        coeffs = np.reshape(coeffs, (B.shape[0], hdeg*ranks[iexp+1]))
        a, b, c = np.linalg.svd(coeffs, full_matrices=0)
        rr = min([ranks[iexp],a.shape[1]])
        rr = min(rr,max(len(np.nonzero(np.array(b) > acc)[0].tolist()),1))
        a = a[:, :rr]
        b = np.diag(b[:rr])
        c = c[:rr, :]
        right = a.dot(b)
        cores[iexp] = np.reshape(c,(rr,hdeg,ranks[iexp+1]))
        ranks[iexp] = rr

    cores[0] = np.zeros([1,B.shape[0],ranks[1]])
    for idet in range(B.shape[0]):
        for k in range(ranks[1]):
            cores[0][0,idet,k] = np.exp(B[idet,0])*right[idet,k]

    U = tt.tensor.from_list(cores)
    return U


def get_coeff_upper_bound(B):

    bxmax = np.zeros(B.shape[1]-1)

    for i in range(B.shape[1]-1):
        bxmax[i] = max(abs(B[:,i+1]))

    return bxmax

def sample_lognormal_tt(U, sample, bmax, theta=0, rho=1, all_cores_stochastic=False):
    from alea.math_utils.polynomials import polynomials as polys
    # if isinstance(U, tt.vector):
    #     d = U.d
    #     n = U.n
    #     ranks = U.r
    #     cores = tt.tensor.to_list(U)
    # else:
    d = len(U)
    n = [core.shape[1] for core in U]
    ranks = [core.shape[2] for core in U]
    ranks.insert(0, U[0].shape[0])
    cores = U
    # print("samples: {}".format(sample))
    herms = polys.StochasticHermitePolynomials(0,1,normalised=True)
    right = [1]
    if all_cores_stochastic is True:
        right = cores[-1]
        assert (len(right.shape) == 3)
        assert (right.shape[2] == 1)
        right = right[:, :, 0]
        tau = np.exp(-theta * rho * bmax[-1])

        right = np.dot(right, np.array(herms.eval(right.shape[1]-1, tau*sample[-1], all_degrees=True)))
        for i in range(d - 2, -1, -1):
            tau = np.exp(-theta * rho * bmax[i])
            # print("{} rank[{}]={}, n[{}]={}".format(i, i, ranks[i], i, n[i]))
            core = cores[i]
            if len(core.shape) == 5:
                if len(right.shape) == 1:
                    assert (core.shape[3] == 1)
                    core = core[:, :, :, 0, :]
                    right = np.einsum('ijal, l->ija', core, right)
                    right = np.einsum('ija, a->ij', right, np.array(herms.eval(right.shape[2]-1, tau*sample[i],
                                                                               all_degrees=True)))
                elif len(right.shape) == 2:
                    right = np.einsum('ijakl, lk->ija', core, right)
                    right = np.einsum('ija, a->ij', right, np.array(herms.eval(right.shape[2]-1, tau * sample[i],
                                                                               all_degrees=True)))
                else:
                    print("this should not have happen. len(right.shape) = {}".format(len(right.shape)))
                    exit()
            elif len(core.shape) == 3:
                assert (len(right.shape) == 1)
                right = np.einsum('ial, l->ia', core, right)
                right = np.dot(right, np.array(herms.eval(right.shape[1]-1, tau*sample[i], all_degrees=True)))
            else:
                print("this should not have happen. len(core.shape) = {}".format(len(core.shape)))
                exit()

        return right
    else:
        for i in range(d-1,0,-1):
            left = np.reshape(cores[i],(ranks[i]*n[i],ranks[i+1]))
            left = np.reshape(left.dot(right),(ranks[i],n[i]))
            right = []
            tau = np.exp(-theta*rho*bmax[i-1])
            for k in range(ranks[i]):
                loc_core = np.sum([left[k,c]*herms.eval(c,tau*sample[i-1],all_degrees=False) for c in range(left.shape[1])], axis=0)
                right.append(loc_core)

        right = np.array(right)
        left = np.reshape(cores[0],(n[0],ranks[1]))
        left = np.array(left.dot(right))
        return left


def generate_operator_tt(U, ranks, hdegs, M):
    if isinstance(U,tt.vector):
        d = U.d
        cores = tt.tensor.to_list(U)

        opcores = [None]
        for iexp in range(M):
            opcorei = np.zeros([ranks[iexp+1],hdegs[iexp],hdegs[iexp],ranks[iexp+2]])
            for nu1 in range(hdegs[iexp]):
                for nu2 in range(hdegs[iexp]):
                    opcorei[:,nu1,nu2,:] = sum([cores[iexp+1][:,mu,:]*hermite_triprod(mu,nu1,nu2) for mu in range(cores[iexp+1].shape[1])])

            opcores.append(opcorei)

        if M <= d:
            for lia in range(M, d+1):
                opcores[M] = np.einsum('lnmk, kt->lnmt', opcores[M], cores[lia][:, 0, :])

    elif isinstance(U,list):
        cores = U
        d = len(cores) - 1
        # print(ranks)
        opcores = [None]
        for iexp in range(M):
            # print("  {}".format(cores[iexp].shape))
            opcorei = np.zeros([ranks[iexp+1],hdegs[iexp],hdegs[iexp],ranks[iexp+2]])
            for nu1 in range(hdegs[iexp]):
                for nu2 in range(hdegs[iexp]):
                    # print("mu={}, nu1={}, nu2={}".format(cores[iexp].shape[1], nu1, nu2))
                    opcorei[:,nu1,nu2,:] = sum([cores[iexp][:,mu,:]*hermite_triprod(mu,nu1,nu2) for mu in range(cores[iexp].shape[1])])

            opcores.append(opcorei)

        if 0 < M <= d:
            # buffer_core = np.zeros((opcores[M].shape[0],opcores[M].shape[1], opcores[M].shape[2], 1))
            for lia in range(M, d+1):
                opcores[-1] = np.einsum('lnmk, kt->lnmt', opcores[-1], cores[lia][:, 0, :])
            # print opcores[M].shape
            # buffer_core[:, :, :, 0] = opcores[M]
            # opcores[M] = buffer_core

    return opcores


def hermite_triprod(i, j, k):
    # region def triple hermite coefficient

    def triple_hermite_coef(nu, mu, xi):
        """
        calculates the coefficient of Hermite polynomial expansion when used as triple product
        :param nu: coefficient index
        :param mu: solution index
        :param xi: contraction index
        :return: triple hermite coefficient
        """
        if (nu + mu - xi) % 2 != 0:
            return 0  # hermite indices form an odd sum
        if np.abs(nu - mu) > xi:
            return 0  # hermite indices do not couple
        if xi > nu + mu:
            return 0  # again no coupling
        # if xi > min(nu, mu):                            # new: check for correct deterministic estimator
        #     return 0
        eta = (nu + mu - xi) * 0.5  # return coefficient as in
        # nodes, weights = np.polynomial.hermite_e.hermegauss(30)
        # assert (len(nodes) == len(weights))
        # quadrature = np.sum([w * transformed_herms(nu, x) * transformed_herms(mu, x) * transformed_herms(xi, x)
        #                      for (x, w) in zip(nodes, weights)])
        # quadrature *= (np.sqrt(2 * np.pi)) ** (-1)
        # return quadrature
        # print("compute coef for nu={}, mu={}, xi={}".format(nu, mu, xi))
        if nu + mu > 12:
            nu_fac_sqrt = Decimal.sqrt(Decimal(np.math.factorial(nu)))
            mu_fac_sqrt = Decimal.sqrt(Decimal(np.math.factorial(mu)))
            xi_fac_sqrt = Decimal.sqrt(Decimal(np.math.factorial(xi)))
            eta_fac = Decimal(np.math.factorial(eta))
            nu_eta_fac = Decimal(np.math.factorial(nu - eta))
            mu_eta_fac = Decimal(np.math.factorial(mu - eta))
            retval = nu_fac_sqrt / eta_fac
            retval *= mu_fac_sqrt / nu_eta_fac
            retval *= xi_fac_sqrt / mu_eta_fac
            return float(retval)
        return (math.sqrt(np.math.factorial(nu) * np.math.factorial(mu) * np.math.factorial(xi))
                * (np.math.factorial(eta) * np.math.factorial(nu - eta) * np.math.factorial(mu - eta)) ** (-1))

    # endregion
    if True:
        return triple_hermite_coef(i, j, k)
    if bool((i+j-k) % 2) or k < abs(i-j) or k > i+j:
        return 0
    s = (i+j-k)/2
    fac = np.math.factorial
    return np.sqrt(float(fac(i)*fac(j)*fac(k)))/(fac(s)*fac(i-s)*fac(j-s))
    # return np.sqrt(float(fac(i)*fac(j)*fac(i+j-2*k)/fac(k)*fac(k-i)*fac(k-j)))


def extract_mean(U):

    d = U.d
    n = U.n
    ranks = U.r
    cores = tt.tensor.to_list(U)

    right = [1]
    for m in range(d-1,0,-1):
        right = np.dot(cores[m][:,0,:],right)

    return np.dot(np.reshape(cores[0],[n[0],ranks[1]]),right)


def mult_diag(d, mtx, left=True):
    """Multiply a full matrix by a diagonal matrix.
    This function should always be faster than dot.

    Input:
      d -- 1D (N,) array (contains the diagonal elements)
      mtx -- 2D (N,N) array

    Output:
      mult_diag(d, mts, left=True) == dot(diag(d), mtx)
      mult_diag(d, mts, left=False) == dot(mtx, diag(d))
    """
    if left:
        return (d*mtx.T).T
    else:
        return d*mtx


def tt_cont_coeff(af, mesh, M, ranks, hdegs, bmax, theta=0, rho=1, acc=1e-10, mesh_dofs=500, quad_degree=2,
                  coef_mesh_cache=None, domain="undefined", scale=1.0):
    # TODO: make a new mesh (with fe_h)
    # prepare quadrature points and weights as well as the mesh
    from alea.utils.tictoc import TicToc
    with TicToc(sec_key="Create coefficient", key="1. Create mesh and FunctionSpace", active=True, do_print=False):
        if coef_mesh_cache is not None:
            mesh = coef_mesh_cache
        import FIAT
        quad_rule = FIAT.create_quadrature(FIAT.ufc_cell('triangle'), quad_degree, "default")
        qw = np.asarray(quad_rule.get_weights())
        # _, qw = ffc.quadratureelement.create_quadrature('triangle', quad_degree)
        qw *= 2.0                                       # not quite sure why...
        if mesh_dofs > 0:
            fe = FiniteElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree, quad_scheme="default")
            while FunctionSpace(mesh, fe).dim() < mesh_dofs:
                mesh = refine(mesh)  # uniformly refine mesh
                fe = FiniteElement(family="Quadrature", cell=mesh.ufl_cell(), degree=quad_degree,
                                   quad_scheme="default")
        Q = FunctionSpace(mesh, fe)
        # if coef_mesh_cache is None:
        #     from dolfin import File
        #     File("{}-{}-mesh.xml".format(domain, scale)) << mesh
    with TicToc(sec_key="Create coefficient", key="2. Get quadrature nodes and weights", active=True, do_print=False):
        gdim = mesh.geometry().dim()
        qpoints = Q.tabulate_dof_coordinates()
        qpoints = qpoints.reshape((-1, gdim))
        N = qpoints.shape[0]
        vcells = [c.volume() for c in cells(mesh)]
        qweights = np.hstack([qw * cv for cv in vcells])

    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in qpoints])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        fun_Q = interpolate(fun, Q)
        # retval_old = [(np.power(fun(p)*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) *
        #                np.exp(np.power(fun(p)*sigmam, 2) / 2)
        #               for p in qpoints]
        retval = (np.power(scale*fun_Q.vector()[:]*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * np.exp(np.power(
            scale*fun_Q.vector()[:]*sigmam, 2) / 2)
        # print("err={}".format(np.linalg.norm(retval - retval_old)))
        return retval

    cores = M*[[]]
    red_coeffs = np.ones([1, N])

    with TicToc(sec_key="Create coefficient", key="3. Create Cores", active=True, do_print=False):
        for m in range(M, 0, -1):
            # print("compute coef core {}/{}".format(M-m, M))
            with TicToc(sec_key="Create coefficient", key="3. compute correlation matrix", active=True, do_print=False):
                # Compute correlation matrix
                # EB = np.zeros([hdegs[m-1],ranks[m+1],N])
                # print("red_coef shape {}".format(red_coeffs.shape))
                with TicToc(sec_key="Create coefficient", key="3.0 create EB", active=True, do_print=False):
                    EB = np.array([eval_bfun(m, nu) * red_coeffs for nu in range(hdegs[m-1])])

                # print("EB 1 {}".format(EB.shape))
                with TicToc(sec_key="Create coefficient", key="3.1 reshape EB", active=True, do_print=False):
                    EB = np.reshape(EB, [hdegs[m-1]*ranks[m+1], N])
                # print("EB 2 {}".format(EB.shape))

                with TicToc(sec_key="Create coefficient", key="3.2 multiply column with weights".format(m), active=True,
                            do_print=False):
                    C = EB*qweights
                # print("C 1 {}".format(C.shape))
                with TicToc(sec_key="Create coefficient", key="3.4 numpy dot".format(m), active=True, do_print=False):
                    C = C.dot(EB.T)

                # print("C 2 {}".format(C.shape))
            with TicToc(sec_key="Create coefficient", key="3.5 compute SVD", active=True, do_print=False):
                # Eigenvalue decomposition
                vecs, vals, _ = scipy.linalg.svd(C,full_matrices=False, check_finite=False)
                ranks[m] = min(ranks[m],max(len(np.nonzero(np.array(np.sqrt(vals)) > acc)[0].tolist()),1))
                vecs = vecs[:,:ranks[m]]
                vecs = vecs.T
            with TicToc(sec_key="Create coefficient", key="3.6 compute reduced basis", active=True, do_print=False):
                # Compute reduced basis functions
                EB = np.reshape(EB,[hdegs[m-1],ranks[m+1],N])
                vecs = np.reshape(vecs,[ranks[m],hdegs[m-1],ranks[m+1]])
                red_coeffs = np.tensordot(vecs,EB,axes=([1,2],[0,1]))
                cores[m-1] = vecs
        TicToc.sortedTimes()
        TicToc.clear()
    return cores


def sample_cont_coeff(tt_coeff, af, points, sample, ranks, hdegs, bmax, theta=0, rho=1, dg_mesh=None, fs=None,
                      af_cache=None):
    from alea.math_utils.polynomials import polynomials as polys
    M = len(tt_coeff)

    dg_fs = None
    if dg_mesh is not None:
        dg_fs = FunctionSpace(dg_mesh, 'DG', 0)
        N = dg_fs.dim()
    elif fs is not None:
        N = fs.dim()
    else:
        assert points is not None
        assert len(points) > 0
        N = len(points)

    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def tau(m):
        return np.exp(-theta*rho*bmax[m-1])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        if dg_mesh is not None:
            dg_fun = interpolate(fun, dg_fs)
            return (np.power(dg_fun.vector()[:]*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                   np.exp(np.power(dg_fun.vector()[:]*sigmam, 2) / 2)
        elif fs is not None:
            if af_cache is not None:
                func = af_cache.af[m]
            else:
                func = interpolate(fun, fs)
                func = func.vector()[:]
            with TicToc(sec_key="sample cont coef", key="  bfun retval", active=True, do_print=False):
                _retval = (np.power(func * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                    np.exp(np.power(func * sigmam, 2) / 2)
            return _retval
        else:
            return [(np.power(fun(p) * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                   np.exp(np.power(fun(p) * sigmam, 2) / 2) for p in points]

    herms = polys.StochasticHermitePolynomials(0,1,normalised=True)
    right = [1]
    left = np.ones([1, N])
    for m in range(M, 0, -1):
        # EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        if af_cache is not None:
            with TicToc(sec_key="sample cont coef", key="  EB", active=True, do_print=False):
                EB = np.array([af_cache.b_fun[m][nu] * left for nu in range(hdegs[m-1])])
            # with TicToc(sec_key="sample cont coef", key="  left2", active=True, do_print=False):
            #     EB2 = af_cache.b_fun[m][:hdegs[m-1], :]
            #      core = np.reshape(tt_coeff[m-1], (tt_coeff[m-1].shape[0]*tt_coeff[m-1].shape[1], tt_coeff[m-1].shape[2]), order="F")
            #     core = np.dot(core, left)
            #     core = np.reshape(core, (tt_coeff[m-1].shape[0], tt_coeff[m-1].shape[1], core.shape[1]), order="F")
            #     left2 = np.sum(core * EB2, axis=1)
            #     # left_stretch = np.tile(left, (hdegs[m-1], 1, 1)).transpose((1, 0, 2))
            #     # EB2 = (EB2*left_stretch).transpose((1, 0, 2))
        else:
            with TicToc(sec_key="sample cont coef", key="  EB", active=True, do_print=False):
                EB = np.array([eval_bfun(m, nu) * left for nu in range(hdegs[m-1])])
        # print("sample {}/{}".format(m-1, len(sample)))
        # print("hdegs: {}".format(hdegs))
        with TicToc(sec_key="sample cont coef", key="  herms", active=True, do_print=False):
            hb = [herms.eval(nu, tau(m)*sample[m-1], all_degrees=False) for nu in range(hdegs[m-1])]
        with TicToc(sec_key="sample cont coef", key="  left", active=True, do_print=False):
            left = np.tensordot(tt_coeff[m-1], EB, axes=([1, 2], [0, 1]))
        # assert np.allclose(left, left2)
        with TicToc(sec_key="sample cont coef", key="  right 1", active=True, do_print=False):
            right = np.tensordot(tt_coeff[m-1], right, axes=(2, 0))
        with TicToc(sec_key="sample cont coef", key="  right 2", active=True, do_print=False):
            right = right.dot(hb)
    with TicToc(sec_key="sample cont coef", key="  final b0", active=True, do_print=False):
        if dg_mesh is not None:
            dg_fun = interpolate(af[0][0][0], dg_fs)
            b0 = np.exp(dg_fun.vector()[:])
            # b0_old = [np.exp(af[0][0][0](p)) for p in points]
            # print("err={}".format(np.linalg.norm(b0-b0_old)))
        elif fs is not None:
            if af_cache is not None:
                dg_fun = af_cache.af[0]
                # print("use cache at end")
            else:
                dg_fun = interpolate(af[0][0][0], fs)
                dg_fun = dg_fun.vector()[:]
            b0 = np.exp(dg_fun)
        else:
            b0 = [np.exp(af[0][0][0](p)) for p in points]
    # newleft = np.array(left[k, :]*b0 for k in range(ranks[1]))
    with TicToc(sec_key="sample cont coef", key="  newleft", active=True, do_print=False):
        # newleft = np.zeros([ranks[1], N])
        # for k in range(ranks[1]):
        #     newleft[k, :] = left[k, :]*b0
        newleft = left * b0
        # assert np.allclose(newleft, newleft2)
    with TicToc(sec_key="sample cont coef", key="  retval", active=True, do_print=False):
        retval = newleft.T.dot(right)
    # TicToc.sortedTimes(sec_sorted=True)
    return retval


def sample_cont_coeff_2(tt_coeff, af, points, sample, ranks, hdegs, bmax, theta=0, rho=1):
    from alea.math_utils.polynomials import polynomials as polys
    M = len(tt_coeff)
    N = len(points)

    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def tau(m):
        return np.exp(-theta*rho*bmax[m-1])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        return [(np.power(fun(p)*sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * np.exp(np.power(fun(p)*sigmam, 2) / 2) for p in points]

    herms = polys.StochasticHermitePolynomials(0,1,normalised=True)
    right = [1]
    left = np.ones([1,N])
    for m in range(M,0,-1):
        EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        hb = np.zeros(hdegs[m-1])
        for nu in range(hdegs[m-1]):
            eb = eval_bfun(m,nu)
            hb[nu] = herms.eval(nu,tau(m)*sample[m-1],all_degrees=False)
            for k in range(ranks[m+1]):
                EB[nu,k,:] = eb*left[k,:]
        left = np.tensordot(tt_coeff[m-1],EB,axes=([1,2],[0,1]))
        right = np.tensordot(tt_coeff[m-1],right,axes=(2,0))
        right = right.dot(hb)

    b0 = [np.exp(af[0][0][0](p)) for p in points]
    newleft = np.zeros([ranks[1],N])
    for k in range(ranks[1]):
        newleft[k,:] = left[k,:]*b0

    return newleft.T.dot(right)


def fully_disc_first_core(tt_coeff, af, points, ranks, hdegs, bmax, theta=0, rho=1, dg_mesh=None, cg_mesh=None,
                          femdegree=1, scale=1.0):
    M = len(tt_coeff)
    N = len(points)
    from dolfin import interpolate
    fs = None
    if dg_mesh is not None:
        fs = FunctionSpace(dg_mesh, 'DG', femdegree-1)
    if cg_mesh is not None:
        fs = FunctionSpace(cg_mesh, 'CG', femdegree)
    # bmax = np.zeros(M)
    # for m in range(M):
    #     bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        with TicToc(sec_key="create discrete first core", key="  sigma", active=False, do_print=False):
            sigmam = sigma(m)
        with TicToc(sec_key="create discrete first core", key="fun", active=False, do_print=False):
            fun = af[m][0][0]
        with TicToc(sec_key="create discrete first core", key="retval", active=True, do_print=False):
            if dg_mesh is not None or cg_mesh is not None:
                # retval_old = [(np.power(fun(p)*sigmam, nu) / np.sqrt(np.math.factorial(nu))) *
                #           np.exp(np.power(fun(p)*sigmam, 2) / 2) for p in points]
                discrete_fun = interpolate(fun, fs)
                retval = (np.power(discrete_fun.vector()[:] * scale * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) * \
                          np.exp(np.power(discrete_fun.vector()[:] * scale * sigmam, 2) / 2)
                # print("err={}".format(np.linalg.norm(retval_old - retval)))
            else:
                retval = [(np.power(fun(p) * scale * sigmam, nu) / np.sqrt(float(np.math.factorial(nu)))) *
                          np.exp(np.power(fun(p) * scale * sigmam, 2) / 2) for p in points]
        return retval

    from alea.utils.tictoc import TicToc
    left = np.ones([1,N])
    # print("go for M={}".format(M))
    for m in range(M,0,-1):
        # EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        with TicToc(sec_key="create discrete first core", key="create EB", active=True, do_print=False):
            EB = np.array([eval_bfun(m, nu) * left for nu in range(hdegs[m-1])])
        # for nu in range(hdegs[m-1]):
        #     eb = eval_bfun(m, nu)
        #     EB[nu, :, :] = eb*left
        #     # for k in range(ranks[m+1]):
        #     #    EB[nu,k,:] = eb*left[k,:]
        # with TicToc(key="  tensordot", active=True, do_print=False):
        #     left = np.tensordot(tt_coeff[m-1],EB,axes=([1,2],[0,1]))
        # print("tt_coef:{} x {} :EB".format(tt_coeff[m-1].shape, EB.shape))
        with TicToc(sec_key="create discrete first core", key="own tensordot", active=True, do_print=False):
            left = own_tensordot(tt_coeff[m - 1], EB, axes=([1, 2], [0, 1]))
        # print(" left = {}".format(left.shape))
        # einsum is much much slower. think about that for the estimators
        # with TicToc(key="  einsum", active=True, do_print=False):
        #     _left = np.einsum('ijk, jkn->in', tt_coeff[m-1], EB)
        # print("err={}".format(np.linalg.norm(left - _left)))
        # print("tt {} * EB {} = left {}".format(tt_coeff[m-1].shape, EB.shape, left.shape))
    with TicToc(sec_key="create discrete first core", key="first core", active=True, do_print=False):
        if dg_mesh is not None or cg_mesh is not None:
            discrete_fun = interpolate(af[0][0][0], fs)
            b0 = np.exp(discrete_fun.vector()[:])
            # b0_old = [np.exp(af[0][0][0](p)) for p in points]
            # print("err={}".format(np.linalg.norm(b0-b0_old)))
        else:
            b0 = [np.exp(af[0][0][0](p)) for p in points]
        # newleft = np.array(left[k, :]*b0 for k in range(ranks[1]))
        newleft = np.zeros([left.shape[0],N])
        # print newleft.shape
        # print left.shape
        # print ranks
        for k in range(left.shape[0]):
            newleft[k,:] = left[k,:]*b0

    return np.reshape(newleft.T,[1,N,left.shape[0]])
'''


def fully_disc_first_core(tt_coeff, af, points, ranks, hdegs, theta=0, rho=1):

    M = len(tt_coeff)
    N = len(points)

    bmax = np.zeros(M)
    for m in range(M):
        bmax[m] = max([abs(af[m+1][0][0](p)) for p in points])

    def tau(m):
        return np.exp(-theta*rho*bmax[m-1])

    def sigma(m):
        return np.exp(theta*rho*bmax[m-1])

    def eval_bfun(m,nu):
        sigmam = sigma(m)
        fun = af[m][0][0]
        return [(np.power(fun(p)*sigmam, nu) / np.sqrt(np.math.factorial(nu))) * np.exp(np.power(fun(p)*sigmam, 2) / 2) for p in points]

    left = np.ones([1,N])
    for m in range(M,0,-1):
        EB = np.zeros([hdegs[m-1],ranks[m+1],N])
        for nu in range(hdegs[m-1]):
            eb = eval_bfun(m,nu)
            for k in range(ranks[m+1]):
                EB[nu,k,:] = eb*left[k,:]
        left = np.tensordot(tt_coeff[m-1],EB,axes=([1,2],[0,1]))

    b0 = [np.exp(af[0][0][0](p)) for p in points]
    newleft = np.zeros([ranks[1],N])
    for k in range(ranks[1]):
        newleft[k,:] = left[k,:]*b0

    return newleft.T
'''
