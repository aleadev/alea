import math
import time
import torch
import torch.nn as nn
import xerus as xe
import numpy as np

PI = torch.Tensor([math.pi])


class TorchComponent(nn.Module):
    """
    A single tensor component that will be registered to torch
    """
    def __init__(self, _ten):
        super(TorchComponent, self).__init__()
        self.register_parameter(name='tensor',
                                param=nn.Parameter(_ten.contiguous()))

    def forward(self, input_data):
        pass


class TorchTT(nn.Module):
    def __init__(self,
                 input_dim,                         # type: int
                 output_dim,                        # type: int
                 rank,                              # type: int
                 feature_dim,                       # type: list[int] or int
                 feature=None,                      # type: list[callable] or callable or None
                 modules=None,
                 device=None
                 ):
        super(TorchTT, self).__init__()

        if device is None:
            device = torch.device("cpu")
        # if isinstance(feature_dim, list):
        #     raise NotImplementedError("list of feature dimensions is not supported yet."
        #                               "Homogeneous features are achieved by feature = callable")
        if isinstance(feature_dim, list):
            assert len(feature_dim) == input_dim
            # raise NotImplementedError("list of different features is not supported yet")


        if modules is not None:
            self.module_list = nn.ModuleList(modules)
        else:
            module_list = []
            dim_list = [output_dim]
            dim_list += [feature_dim]*input_dim if isinstance(feature_dim, int) else feature_dim
            # get appropriate ranks
            # TODO: make this more educated
            # init_tt = xe.TTTensor.random(dim_list, [1]*input_dim)
            # while min(init_tt.ranks()[input_dim//4:-input_dim//4]) < rank:
            #     init_tt += xe.TTTensor.random(dim_list, [1]*input_dim)
            # self.ranks = [1] + init_tt.ranks() + [1]
            self.ranks = [1] + [rank]*input_dim + [1]
            batch_size = 100 #TODO:!!!
            for lia in range(input_dim+1):
                if True:
                    xetensor = torch.tensor(np.ones((self.ranks[lia], dim_list[lia], self.ranks[lia+1])),
                                            dtype=torch.float, requires_grad=True)
                    for _r in range(self.ranks[lia]):
                        for r_ in range(self.ranks[lia+1]):
                            if _r == r_: continue
                            xetensor[_r, :, r_] = torch.randn(dim_list[lia]) * 1e-9
                elif False:
                    xetensor = xe.Tensor([self.ranks[lia], dim_list[lia], self.ranks[lia+1]])
                    for _lia in range(dim_list[lia]):
                        xetensor += xe.Tensor.dirac([self.ranks[lia], dim_list[lia], self.ranks[lia + 1]], [0, _lia, 0])
                    xetensor = torch.tensor(
                            np.array(xetensor), dtype=torch.float, requires_grad=True
                    )

                    xetensor = xetensor + torch.randn(xetensor.shape) * 1e-9
                else:
                    xetensor = np.zeros([self.ranks[lia], dim_list[lia], self.ranks[lia + 1]])
                    xetensor[0, :, 0] += np.ones(dim_list[lia])
                    xetensor = torch.tensor(
                            np.array(xetensor), dtype=torch.float, requires_grad=True
                    )

                    xetensor = xetensor + torch.randn(xetensor.shape) * 1e-9
                module_list.append(TorchComponent(xetensor.to(device)))
            self.module_list = nn.ModuleList(module_list)

        self.input_dim = input_dim
        self.output_dim = output_dim
        self.feature_dim = feature_dim
        self.feature = feature
        self.device = device

    def contract_half_clever(self, ten_list, timing=False):
        size = int(len(ten_list) - 1)
        if timing: start = time.time()
        # mats.size() = BatchSize x N-2 x r x r
        # N-2 due to first core beeing the output label
        #     and since the last core is only a matrix and can be contracted faster
        mats = torch.zeros((ten_list[0].size(1), len(ten_list) - 1, ten_list[0].size(2), ten_list[0].size(2))).to(self.device)
        for lia in range(len(ten_list) - 1):
            mats[:, lia, :ten_list[lia].size(0), :ten_list[lia].size(2)] = ten_list[lia].permute(1, 0, 2)
        if timing: print("  build mats: {}".format(time.time() - start))
        if timing: start = time.time()
        while size > 1:
            odd_size = (size % 2 == 1)
            half_size = size // 2
            nice_size = 2 * half_size

            even_mats = mats[:, 0:nice_size:2]
            odd_mats = mats[:, 1:nice_size:2]
            # For odd sizes, set aside one batch of matrices for the next round
            # print("size", size, "halfsize", half_size, "nice_size", nice_size, "odd size: {}".format(odd_size))

            leftover = mats[:, nice_size:]
            # Multiply together all pairs of matrices (except leftovers)
            #mats = torch.einsum('bslu,bsur->bslr', [even_mats, odd_mats]).to(self.device)
            mats = torch.matmul(even_mats, odd_mats).to(self.device)
            mats = torch.cat([mats, leftover], 1).to(self.device)

            size = half_size + int(odd_size)
        if timing: print("  contract while: {}".format(time.time() - start))
        if timing: start = time.time()
        retval = mats.squeeze(1).to(self.device)
        # print(retval.size())
        # print(self.module_list[0].tensor.size())
        retval = torch.einsum('ik, bkl->ibl', self.module_list[0].tensor[0, :, :], retval).to(self.device)
        retval = torch.einsum('kb, ibk->ib', ten_list[-1][:, :, 0], retval).to(self.device)
        if timing: print("  contract first and last: {}".format(time.time() - start))
        return retval.t()

    def contract_classical(self, ten_list, timing=False):
        if timing: start = time.time()
        retval = torch.ones(1, device=self.device)
        for lia in range(len(ten_list)-1, -1, -1):
            if lia == len(ten_list)-1:
                retval = ten_list[lia][:, :, 0]
            else:
                retval = torch.einsum('ibk, kb-> ib', ten_list[lia], retval)

        retval = torch.matmul(self.module_list[0].tensor[0, :, :], retval)
        if timing: print("  contract cores: {}".format(time.time() - start))
        return retval.t()

    def contract_input(self, input, timing=False):
        if timing: all_start = time.time()
        ten_list = []
        bs = input.size(0)
        for lia in range(1, len(self.module_list)):
            fd = self.feature_dim[lia-1] if isinstance(self.feature_dim, list) else self.feature_dim
            rl, rr = self.module_list[lia].tensor.size(0), self.module_list[lia].tensor.size(2)
            curr_ten = torch.matmul(self.module_list[lia].tensor.permute(0, 2, 1).reshape(rl * rr, fd),
                                    input[:, lia - 1, :fd].t()).reshape(rl, bs, rr)
            # curr_ten = torch.einsum('idk, bd->ibk', self.module_list[lia].tensor, input[:, lia-1, :self.feature_dim])
            ten_list.append(curr_ten)
        if timing: print("  contract input: {}".format(time.time() - all_start))
        return ten_list

    def forward(self, input_data):
        timing = False

        #                                           # create ord.3 tensor (Bs, Pixel, d)
        input_data = self.embed_input(input_data)
        if timing: start = time.time()
        ten_list = self.contract_input(input_data, timing=timing)
        if timing: print("contract input: {}".format(time.time() - start))
        assert input_data.size(1) == len(self.module_list)-1
        # print("TT contraction")
        if timing: start = time.time()
        inp_first = self.contract_classical(ten_list, timing=timing)
        if timing: print("contract TT: {}".format(time.time() - start))
        # print("TF contraction")
        # if timing: start = time.time()
        # clever = self.contract_half_clever(ten_list, timing=timing)
        # if timing: print("contract clever: {}".format(time.time() - start))
        # print("contraction diff:", torch.norm(clever - inp_first).item()/torch.norm(inp_first).item())
        return inp_first



    def embed_input(self, input_data):
        # TODO: this can probably be done faster
        assert len(input_data.shape) in [2, 3]
        # input_data.shape(0) == Batch
        # input_data.shape(1) == Pixel or sample
        assert input_data.size(1) == self.input_dim

        # If input already has a feature dimension, return it as is
        if len(input_data.shape) == 3:
            # if input_data.size(2) != self.feature_dim:
            #     raise ValueError(f"input_data has wrong shape to be unembedded "
            #                      "or pre-embedded data (input_data.shape = "
            #                      f"{list(input_data.shape)}, feature_dim = {self.feature_dim})")
            # print("!!!!!!!!!!!!!!!")
            return input_data

        embedded_shape = list(input_data.shape) + [self.feature_dim]


        if self.feature is None:
            if self.feature_dim != 2:
                    raise RuntimeError(f"self.feature_dim = {self.feature_dim}, "
                          "but default feature_map requires self.feature_dim = 2")
            embedded_data = torch.empty(embedded_shape).to(self.device)
            embedded_data[:,:,0] = input_data # torch.sin(0.5*input_data*PI)
            embedded_data[:,:,1] = 1 - input_data # torch.cos(0.5*input_data*PI)
            return embedded_data

        # TODO: list[callable]
        # assert hasattr(self.feature, "callable")
        embedded_data = self.feature(input_data)

        assert embedded_data.size(2) == self.feature_dim

        return embedded_data


    def core_len(self):
        """
        Returns the dimension of the feature space
        """
        return self.feature_dim

    def __len__(self):
        """
        Returns the number of tensor components
        """
        return self.input_dim

    def canonicalize(self, left=True):
        xeTT = xe.TTTensor(len(self.module_list))
        norm_list = []
        for lia in range(len(self.module_list)):
            curr_core = self.module_list[lia].tensor
            xecore = xe.Tensor.from_buffer(np.array(curr_core.detach().numpy(), dtype=np.float))
            curr_norm = xe.frob_norm(xecore)
            xeTT.set_component(lia, xecore)
            norm_list.append(curr_norm)
        # xeTT = xeTT / xe.frob_norm(xeTT)
        # print("dim: {}".format(xeTT.dimensions))
        print("ranks: {}".format(xeTT.ranks()))
        if left:
            # xeTT.move_core(0, keepRank=False)
            xeTT.round(1e-7)
            # xeTT = xeTT + xe.TTTensor.random(xeTT.dimensions, [1]*(xeTT.order()-1))
            # xeTT = xeTT + xe.TTTensor.random(xeTT.dimensions, [1]*(xeTT.order()-1))
        else:
            xeTT.canonicalize_right()
        print("ranks: {}".format(xeTT.ranks()))
        module_list = []
        for lia in range(len(self.module_list)):
            xetensor = np.array(xeTT.get_component(lia))
            xetensor = torch.tensor(xetensor, dtype=torch.float, requires_grad=True)
            module_list.append(TorchComponent(xetensor))
        return module_list
        # self.module_list = nn.ModuleList(module_list)