from __future__ import (division, print_function, absolute_import)
from alea.utils.progress.percentage import PercentageBar
import numpy as np
import xerus as xe
from alea.math_utils.tensor.extended_tt import ExtendedTT, BasisType
from alea.math_utils.tensor.extended_fem_tt import ExtendedFEMTT
import time

class Reconstruction(object):
    def __init__(self,
                 iteration,
                 tolerance,
                 samples,
                 basis,
                 func,
                 ):
        self.iteration = iteration
        self.tolerance = tolerance
        self.samples = samples
        self.basis = basis
        self.func = func
        self.ten_dim = [len(base) for base in self.basis]

    def run(self, *args):
        pass

    def _eval_samples(self,
                      measurements=False,           # type: bool
                      progress=False,               # type: bool
                      sqrt_approx=False             # type: bool
                      ):                            # type: (...) -> xe.RankOneMeasurementSet or tuple
        bar = None
        meas = None
        if progress:
            print("Evaluate {} samples".format(len(self.samples)))
            bar = PercentageBar(len(self.samples))
        m = len(self.samples[0])
        positions = []
        solutions = []
        if measurements:
            # meas = xe.UQMeasurementSet()
            meas = xe.RankOneMeasurementSet()
        for sample in self.samples:
            assert len(sample) == m
            _val = np.array([f(sample[0]) for f in self.basis[0]])
            # print("value: {}".format(_val))
            pos = [xe.Tensor.from_buffer(_val)]
            for lib in range(1, m):
                _val = np.array([f(sample[lib]) for f in self.basis[lib]])
                # print("ph{} = {}".format(lib, _val))
                pos.append(xe.Tensor.from_buffer(_val))
            if sqrt_approx:
                val = np.sqrt(self.func(sample))
            else:
                val = self.func(sample)
            # print("  func eval at {} ={}".format(sample, val))
            if measurements:
                meas.add(pos, val)
            else:
                positions.append(pos)
                # previously: 
                # solutions.append(xe.Tensor.from_buffer(np.array([val])))
                # the update works with a physical dimension since val is now a vector
                # Otherwise you have to force the list as above. TODO
                solutions.append(xe.Tensor.from_buffer(np.array(val)))
            if progress:
                bar.next()
        if measurements:
            return meas
        return positions, solutions


class ADFReconstruction(Reconstruction):

    def run(self,
            iv,
            maxrank,
            verbose=False,
            init_rank=5,
            min_resnorm_decrease=0.9,
            sqrt_approx=False):

        def build_iv():
            _iv = xe.TTTensor.random(self.ten_dim, [init_rank] * (len(self.ten_dim) - 1))
            for k in range(1, len(self.ten_dim) - 1):
                _iv.set_component(k, xe.Tensor.dirac([init_rank, self.ten_dim[k], init_rank], 0))
            _iv.set_component(len(self.ten_dim) - 1, xe.Tensor.dirac([init_rank, self.ten_dim[-1], 1], 0))
            mean = np.zeros((self.ten_dim[0], init_rank))
            for lia in range(measurements.size()):
                mean[0, 0] += measurements.get_measuredValue(lia)
            mean /= measurements.size()
            mean = xe.Tensor.from_buffer(np.array(mean))
            mean.reinterpret_dimensions([1, self.ten_dim[0], init_rank])
            _iv.set_component(0, mean)
            return _iv
        adf = xe.ADFVariant(self.iteration, self.tolerance, min_resnorm_decrease)
        pd = xe.PerformanceData(verbose)  # print = True
        max_r = [maxrank] * (len(self.ten_dim) - 1)

        measurements = self._eval_samples(measurements=True, progress=verbose, sqrt_approx=sqrt_approx)
        if iv is None:
            iv = build_iv()

        try:
            adf(iv, measurements, max_r, pd)
        except:
            print("Xerus has thrown an exception")
            return None
        retval = ExtendedTT.from_xerus_tt(iv, basis=self.basis)
        if sqrt_approx:
            raise NotImplementedError("Current basis type has no multiplication defined")
            retval = retval.multiply_with_extendedTT(retval)
        return retval


class UQADFReconstruction(Reconstruction):

    def run(self,
            verbose=False,
            non_uq=True,
            fs=None):

        pos, sol = self._eval_samples(measurements=False, progress=verbose)
        # print(pos)
        # print(sol)
        if non_uq:
            dimensions = [1] + [n for n in self.ten_dim]
            print(dimensions)
            try:
                result = xe.uq_ra_adf(pos, sol, dimensions, self.tolerance, self.iteration)
            except:
                print("Xerus has thrown an exception")
                return None
            result = ExtendedTT.from_xerus_tt(result, basis=[BasisType.points] + self.basis)
            first_comp = np.einsum('i, ijk->jk', result.components[0][0, 0, :], result.components[1])
            first_comp = first_comp.reshape((1, self.ten_dim[0], -1))
            retval = ExtendedTT([first_comp] + result.components[2:], basis=self.basis)
        else:
            # raise NotImplementedError("first dimension treated as physical dim not implemented atm")
            assert fs is not None
            dimensions = [fs.dim()] + [n for n in self.ten_dim]
            result = xe.uq_ra_adf(pos, sol, dimensions, self.tolerance, self.iteration)
            result = ExtendedTT.from_xerus_tt(result, basis=[BasisType.points] + self.basis)
            retval = ExtendedFEMTT(result.components, basis=[BasisType.points] + self.basis, fs=fs)

        return retval

try:
    import torch
    from torch.utils.data import Dataset
    from alea.math_utils.tensor.torch_tt import TorchTT
    class TorchTTReconstruction(Reconstruction):

        def __init__(self, iteration, tolerance, samples, basis, func):
            assert PYTORCH
            super(TorchTTReconstruction, self).__init__(iteration, tolerance, samples, basis, func)

        def run(self,
                max_local_dim,
                num_test=10,
                batch_size=5,
                l2_reg=0.0,
                max_rank=10,
                learn_rate=1e-4,
                max_epoch=1000,
                verbose=False,
                device=None):

            num_train = len(self.samples) - num_test

            pos, sol = self._eval_samples(measurements=False, progress=verbose)

            print("build training set")
            train_set = SampleDataset(pos[:num_train], sol[:num_train], max_local_dim, device)

            samplers = {'train': torch.utils.data.SubsetRandomSampler(range(num_train)),
                        'test': torch.utils.data.SubsetRandomSampler(range(num_test))}
            train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size,
                                                    sampler=samplers["train"], drop_last=True)
            num_batches = {name: total_num // batch_size for (name, total_num) in
                        [('train', num_train), ('test', num_test)]}
            print("max rank: {}".format(max_rank))
            mps = TorchTT(input_dim=len(self.basis), output_dim=1, rank=max_rank, feature_dim=self.ten_dim,
                        feature=None,
                        device=device).to(device)
            epoch_num = 0

            update_time = time.time()
            # Let's start training!
            # for epoch_num in range(1, num_epochs+1):
            while True:
                if time.time() - update_time > 1:
                    update_time = time.time()
                running_loss = 0.
                running_acc = 0.

                # Set our loss function and optimizer

                loss_fun = torch.nn.MSELoss(reduction="sum")
                # optimizer = torch.optim.Adam(mps.parameters(), lr=learn_rate, weight_decay=l2_reg)
                optimizer = torch.optim.SGD(mps.parameters(), lr=learn_rate, weight_decay=l2_reg)

                for item in train_loader:
                    inputs = item["sample"].to(device=device)
                    labels = item["label"].to(device=device)
                    # print("in: {}, label: {}".format(inputs.size(), labels.size()))
                    scores = mps(inputs)
                    # Compute the loss and accuracy, add them to the running totals
                    loss = loss_fun(scores, labels)
                    running_loss += loss
                    # with torch.no_grad():
                        # accuracy = torch.sum(
                        #     torch.tensor([torch.dist(scores[i, :], labels[i, :], p=2) / torch.norm(labels[i, :], p=2)
                        #                   for i in range(scores.size(0))], device=device))

                        # running_acc += accuracy / batch_size

                    # Backpropagate and update parameters
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()
                if epoch_num % 10 == 0:
                    print("Number of parameter = {}".format(sum(p.numel() for p in mps.parameters() if p.requires_grad)))
                    print("### Epoch {} ###".format(epoch_num))
                    print("Empirical train loss:     {:.8f}".format(running_loss / num_batches['train']))
                    # print("Empirical rel error :     {:.8f}".format(running_acc / num_batches['train']))
                epoch_num += 1

                # if (running_acc / num_batches['train']) < self.tolerance:
                if running_loss < self.tolerance:
                    break
                if epoch_num > max_epoch:
                    break
            xe_tt = xe.TTTensor(len(mps.module_list))
            for lia in range(len(mps.module_list)):
                curr_core = mps.module_list[lia].tensor
                xecore = xe.Tensor.from_buffer(np.array(curr_core.detach().numpy(), dtype=np.float))
                # curr_norm = xe.frob_norm(xecore)
                xe_tt.set_component(lia, xecore)
                # norm_list.append(curr_norm)
            result = ExtendedTT.from_xerus_tt(xe_tt, basis=[BasisType.points] + self.basis)
            first_comp = np.einsum('i, ijk->jk', result.components[0][0, 0, :], result.components[1])
            first_comp = first_comp.reshape((1, self.ten_dim[0], -1))
            retval = ExtendedTT([first_comp] + result.components[2:], basis=self.basis)
            return retval


        class SampleDataset(Dataset):
            """Sample dataset."""

            def __init__(self, samples, labels, max_local_dim, device=None):
                if device is None:
                    device = torch.device("cpu")
                assert len(samples) == len(labels)
                n = len(samples)
                m = len(samples[0])
                d = max_local_dim
                self.samples = torch.zeros(m, n, d)
                self.labels = torch.Tensor(m, n)

                for lia, (sample, label) in enumerate(zip(samples, labels)):
                    assert len(sample) == m
                    for lib in range(len(sample)):
                        curr_ten = np.array(sample[lib])
                        self.samples[lib, lia, :curr_ten.shape[0]] = torch.Tensor(curr_ten)
                    self.labels[:, lia] = torch.Tensor(np.array(label))
                self.samples.to(device)
                self.labels.to(device)

            def __len__(self):
                return self.samples.size(1)

            def __getitem__(self, idx):
                if torch.is_tensor(idx):
                    idx = idx.tolist()
                sample = {'sample': self.samples[:, idx], 'label': self.labels[:, idx]}
                return sample

except ImportError:
    print("WARNING: torch not found -> no PYTORCH backend possible")
    
    