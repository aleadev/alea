from __future__ import division, absolute_import, print_function
import unittest
from alea.utils.testing import *
from alea.math_utils.basis import FourierCosModes, FourierSinModes, ConstantFourierMode, PickableBase, NumpyPolynomial
import time
import numpy as np
np.random.seed(80190)


class TestBasis(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()
        self.coef_list = [1.2, 3.5, -2, 0.1]
        self.coef_list2 = [10, 20, 40]
        self.cosBasis = PickableBase([FourierCosModes(2), FourierCosModes(4), FourierCosModes(-2)])
        self.sinBasis = PickableBase([FourierSinModes(2), FourierSinModes(4), FourierSinModes(-2)])
        self.constBasis = PickableBase([ConstantFourierMode()])
        self.numpyBasis = PickableBase([NumpyPolynomial(self.coef_list), NumpyPolynomial(self.coef_list2)])
        self.test_grid = np.linspace(-1, 1, num=100)

    def tearDown(self):
        t = time.time() - self.startTime
        print("{}: {}".format(self.id(), t))

    def test_cos_hashing(self):
        _hash = self.cosBasis.to_hash()
        basis = PickableBase.from_hash(_hash)
        left = np.sum([base(self.test_grid) for base in self.cosBasis])
        right = np.sum([base(self.test_grid) for base in basis])
        self.assertLessEqual(np.linalg.norm(left - right)/np.linalg.norm(left), 1e-10)

    def test_sin_hashing(self):
        _hash = self.sinBasis.to_hash()
        basis = PickableBase.from_hash(_hash)
        left = np.sum([base(self.test_grid) for base in self.sinBasis])
        right = np.sum([base(self.test_grid) for base in basis])
        self.assertLessEqual(np.linalg.norm(left - right)/np.linalg.norm(left), 1e-10)

    def test_const_hashing(self):
        _hash = self.constBasis.to_hash()
        basis = PickableBase.from_hash(_hash)
        left = np.sum([base(self.test_grid) for base in self.constBasis])
        right = np.sum([base(self.test_grid) for base in basis])
        self.assertLessEqual(np.linalg.norm(left - right)/np.linalg.norm(left), 1e-10)

    def test_nppoly_hashing(self):
        _hash = self.numpyBasis.to_hash()
        basis = PickableBase.from_hash(_hash)
        left = np.sum([base(self.test_grid) for base in self.numpyBasis])
        right = np.sum([base(self.test_grid) for base in basis])
        self.assertLessEqual(np.linalg.norm(left - right)/np.linalg.norm(left), 1e-10)


if __name__ == '__main__':
    print("#"*20)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestBasis)
    unittest.TextTestRunner(verbosity=0).run(suite)

