from __future__ import (division, print_function)
from dolfin import (FunctionSpace, Expression, Function, TrialFunction, TestFunction, inner, nabla_grad, dx,
                    DirichletBC, solve, parameters, assemble, as_backend_type)
from alea.math_utils.param_pde.affine_field import AffineField
from alea.math_utils.param_pde.lognormal_field import LognormalField
import numpy as np
import scipy.sparse as sps
import sys
new_dolfin = False
if sys.version_info >= (3, 0):
    new_dolfin = True


class SolutionCache:
    def __init__(self):
        self.str = None


class ParametricPoisson(object):
    def __init__(self,
                 coef,                              # type: AffineField or Expression or Function or LognormalField
                 rhs,                               # type: AffineField or Expression or Function
                 fs,                                # type: FunctionSpace
                 bc                                 # type: DirichletBC
                 ):                                 # type: (...) -> None
        """
        Simple poisson type PDE problem with
            # bilinear form a(u, v; y) = \int_D a(x, y) \nabla u(x, y) \nabla v(x, y) dx
            # linear form \ell(u; y') = \int_D f(x, y') \nabla v(x, y') dx
            # + boundary conditions

        :param coef: (parametric) coefficient field.
        :param rhs: (parametric) right hand side
        :param fs: fenics FunctionSpace on which the problem should be solved
        :param bc: boundary conditions to apply
        """
        self.coef = coef
        self.rhs = rhs
        self.fs = fs
        self.bc = bc

    def solve(self,
              y,                                    # type: list or np.ndarray
              reference_m,                          # type: int
              cache=None,                           # type: SolutionCache
              y_f=None,                             # type: list or np.ndarray
              cache_f=None,                         # type: SolutionCache
              expression_degree=10                  # type: int
              ):                                    # type: (...) -> Function
        """
        Solve method for the parametric poisson problem.
        If the coefficient or the rhs are parametric, the corresponding parameters y and y_f must be provided.
        :param y: parameter for the coefficient
        :param reference_m: number of modes to include. must be < len(y)
        :param cache: SolutionCache to cache the expression string -> speeds up the next evaluation
        :param y_f: parameter for the rhs
        :param cache_f: SolutionCache for the rhs expression string
        :param expression_degree: Fenics interpolation degree
        :return: Fenics Function as the solution of the Poisson problem
        """

        u, v = TrialFunction(self.fs), TestFunction(self.fs)
        if isinstance(self.coef, AffineField):
            exp_a = self.create_expression(y, self.coef, reference_m, cache, expression_degree)
        elif isinstance(self.coef, LognormalField):
            exp_a = self.create_expression(y, self.coef.affine_field, reference_m, cache, expression_degree,
                                           expfield=True)
        else:
            exp_a = self.coef

        if isinstance(self.rhs, AffineField):
            if y_f is None:
                y_f = y
            exp_f = self.create_expression(y_f, self.rhs, reference_m, cache_f, expression_degree)
        else:
            exp_f = self.rhs

        bf = inner(exp_a*nabla_grad(u), nabla_grad(v))*dx
        lf = inner(exp_f, v)*dx

        retval = Function(self.fs)
        solve(bf == lf, retval, self.bc)

        return retval

    @staticmethod
    def create_expression(y,                        # type: list or np.ndarray
                          field,                    # type: AffineField
                          reference_m,              # type: int
                          cache=None,               # type: SolutionCache
                          expression_degree=10,     # type: int
                          expfield=False            # type: bool
                          ):                        # type: (...) -> Expression
        """
        helper method to create a fenics expression by string manipulation.
        Much faster than iterative projection and evaluation.
        But, this implementation is limited to the series expansion case
        :param y: parameter
        :param field: AffineField
        :param reference_m: number of modes to include. must be < len(y)
        :param cache: SolutionCache to cache the expression string -> speeds up the next evaluation
        :param expression_degree: Fenics interpolation degree
        :param expfield: flag to take the exponential of the field expression
        :return: Fenics Expression
        """
        a_0 = field.coef_field.mean_func

        if cache is None or cache.str is None:
            def get_cpp(coeff_exp):
                if new_dolfin:
                    cpp = coeff_exp._cppcode
                else:
                    cpp = coeff_exp.cppcode
                return cpp.replace("A", str(coeff_exp.A)).replace("B", str(coeff_exp.B)).replace("freq", str(
                        coeff_exp.freq)).replace("M", str(coeff_exp.M)).replace("N", str(coeff_exp.N))
                # or a_0.B as before? In log normal case a_0.A fits much better, since it is the mean

            a_ex_m = "+".join([str(a_0.A)] + ["A" + str(m) + "*" +
                                              get_cpp(field[m + 1][0][0]) for m in
                                              range(reference_m)])
            a_a_m = ",".join(["A%i=0" % m for m in range(reference_m)])
            if expfield:
                a_ex_m = "exp(" + a_ex_m + ")"

            cell_str = ',' if reference_m > 0 else ''
            cell_str += " cell='triangle', degree={}".format(expression_degree)

            eval_m = "Expression('%s', %s)" % (a_ex_m, a_a_m + cell_str)
            # print("=" * 80)
            # print(a_ex_m)
            # print("+" * 80)
            # print(a_A_m)
            # print("+" * 80)
            # print(eval_m)
            # print("=" * 80)
            if cache is not None:
                cache.str = eval_m

            exp_a = eval(eval_m)
        else:
            # print("string cache found")
            exp_a = eval(cache.str)
        for m in range(reference_m):
            if y[m] is None:
                raise ValueError("y[{}] is None".format(m))
            am = "A%i" % m
            setattr(exp_a, am, y[m])
        return exp_a

    def get_mass_matrix(self, 
                        fs,                         # type: FunctionSpace
                        apply_bc=False              # type: bool
                        ):                          # type: #TODO
        """
        computes mass matrix on function space
        :param fs: Fenics FunctionSpace
        :param apply_bc: flag to apply the boundary conditions
        :return: mass matrix as fenics object
        """
        mesh = fs.mesh()
        u = TrialFunction(fs)
        v = TestFunction(fs)
        mass = inner(u, v) * dx(mesh)
        mass = assemble(mass)
        if apply_bc:
            self.bc.apply(mass)
        return mass
            
    def get_mass_matrix_csr(self,
                            fs,                     # type: FunctionSpace 
                            apply_bc=False          # type: bool
                            ):                      # type: (...) -> sps.csr_matrix
        """
        computes mass matrix on function space and converts it to a scipy sparse csr_matrix
        :param fs: Fenics FunctionSpace
        :param apply_bc: flag to apply the boundary conditions
        :return: mass matrix as csr_matrix
        """
        backend = parameters.linear_algebra_backend
        parameters.linear_algebra_backend = "Eigen"
        mesh = fs.mesh()
        u = TrialFunction(fs)
        v = TestFunction(fs)
        mass = inner(u, v) * dx(mesh)
        mass = assemble(mass)
        if apply_bc:
            self.bc.apply(mass)
        sps_mat = sps.csr_matrix(as_backend_type(mass).sparray())

        parameters.linear_algebra_backend = backend
        return sps_mat

    def get_stiff_matrix(self,
                         fs,                        # type: FunctionSpace
                         apply_bc=False             # type: bool
                         ):                         # type: # TODO
        """
        computes stiff matrix on function space
        :param fs: Fenics FunctionSpace
        :param apply_bc: flag to apply the boundary conditions
        :return: stiff matrix as fenics object
        """
        mesh = fs.mesh()
        u = TrialFunction(fs)
        v = TestFunction(fs)
        stiff = inner(u, v) * dx(mesh)
        stiff = assemble(stiff)
        if apply_bc:
            self.bc.apply(stiff)
        return stiff

    def get_stiff_matrix_csr(self,
                             fs,                    # type: FunctionSpace
                             apply_bc=False         # type: bool
                             ):                     # type: (...) -> sps.csr_matrix
        """
        computes stiff matrix on function space and converts it to a scipy sparse csr_matrix
        :param fs: Fenics FunctionSpace
        :param apply_bc: flag to apply the boundary conditions
        :return: stiff matrix as csr_matrix
        """
        backend = parameters.linear_algebra_backend
        parameters.linear_algebra_backend = "Eigen"
        mesh = fs.mesh()
        u = TrialFunction(fs)
        v = TestFunction(fs)
        stiff = inner(u, v) * dx(mesh)
        stiff = assemble(stiff)
        if apply_bc:
            self.bc.apply(stiff)
        sps_mat = sps.csr_matrix(as_backend_type(stiff).sparray())

        parameters.linear_algebra_backend = backend
        return sps_mat
