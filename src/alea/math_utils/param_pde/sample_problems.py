from __future__ import (division, print_function)
from dolfin import (Constant, Expression)
from alea.math_utils.stochastic.random_variable import (UniformRV, NormalRV)
from alea.math_utils.multiindex_set import MultiindexSet
from collections import namedtuple
from scipy.special import zeta
from alea.math_utils.param_pde.coefficient_field import ParametricCoefficientField
import numpy as np


class SampleProblem(object):

    # Definition of PDE types
    POISSON = "poisson"
    NAVIER_LAME = "navier_lame"
    CONVECTION_DIFFUSION = "convection_diffusion"
    pde_types = [POISSON, NAVIER_LAME, CONVECTION_DIFFUSION]

    # Definitions for coefficient fields, min/max values and max gradient in 1D and 2D
    func_defs = dict()
    func_defs[("cos", 1)] = "A*B*cos(freq*pi*M*x[0])", "-A*B", "A*B", "A*B*M*freq*pi"
    func_defs[("cos", 2)] = "A*B*cos(freq*pi*M*x[0])*cos(freq*pi*N*x[1])", "-A*B", "A*B", "A*B*freq*pi*(M+N)"
    func_defs[("sin", 1)] = "A*B*sin(freq*pi*(M+1)*x[0])", "-A*B", "A*B", "A*B*(M+1)*freq*pi"
    func_defs[("sin", 2)] = "A*B*sin(freq*pi*(M+1)*x[0])*sin(freq*pi*(N+1)*x[1])", "-A*B", "A*B", \
                            "A*B*freq*pi*(M+N+2)"
    func_defs[("monomials", 1)] = "A*B*pow(x[0],freq*M)", None, None, None
    func_defs[("monomials", 2)] = "A*B*pow(x[0],freq*M)*pow(x[1],freq*N)", None, None, None
    func_defs[("constant", 1)] = "A*B*1.0", "A*B", "A*B", "0.0"
    func_defs[("constant", 1)] = "1.0+A-A+B-B", "1.0", "1.0", "0.0"
    func_defs[("constant", 2)] = func_defs[("constant", 1)]
    func_defs[("deterministic", 1)] = "0.0+A-A+B-B", "0.0", "0.0", "0.0"
    func_defs[("deterministic", 2)] = func_defs[("deterministic", 1)]

    # Definitions for right hand side functions
    defaults = dict()
    defaults[(NAVIER_LAME, "rhs")] = "zero"
    defaults[(POISSON, "rhs")] = "constant"
    defaults[(CONVECTION_DIFFUSION, "rhs")] = "constant"

    rhs_defs = dict()
    rhs_defs[(NAVIER_LAME, "zero")] = Constant((0.0, 0.0))
    rhs_defs[(POISSON, "zero")] = Constant(0.0)
    rhs_defs[(POISSON, "constant")] = Constant(1.0)
    rhs_defs[(CONVECTION_DIFFUSION, "constant")] = Constant(1.0)

    # Definitions for boundary conditions
    DIRICHLET = "dirichlet"
    NEUMANN = "neumann"

    LEFT = "left"
    RIGHT = "right"
    TOP = "top"
    BOTTOM = "bottom"

    BoundaryDef = namedtuple("BoundaryDef", "type where func")

    boundary_defs = dict()
    boundary_defs[(NAVIER_LAME, "dirichlet_normal_stress")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant((0.0, 0.0))),
                                                              BoundaryDef(type=DIRICHLET, where=RIGHT, func=Constant((0.3, 0.0)))]
    boundary_defs[(NAVIER_LAME, 0)] = boundary_defs[(NAVIER_LAME, "dirichlet_normal_stress")]

    boundary_defs[(NAVIER_LAME, "dirichlet_shear_stress")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant((0.0, 0.0))),
                                                             BoundaryDef(type=DIRICHLET, where=RIGHT, func=Constant((0.0, 0.3)))]
    boundary_defs[(NAVIER_LAME, 1)] = boundary_defs[(NAVIER_LAME, "dirichlet_shear_stress")]

    boundary_defs[(NAVIER_LAME, "dirichlet_normal_shear")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant((0.0, 0.0))),
                                                             BoundaryDef(type=DIRICHLET, where=RIGHT, func=Constant((1.0, 1.0)))]
    boundary_defs[(NAVIER_LAME, 2)] = boundary_defs[(NAVIER_LAME, "dirichlet_normal_shear")]

    boundary_defs[(NAVIER_LAME, "neumann_shear")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant((0.0, 0.0))),
                                                      BoundaryDef(type=NEUMANN, where=RIGHT, func=Constant((0.0, 1.0)))]
    boundary_defs[(NAVIER_LAME, 3)] = boundary_defs[(NAVIER_LAME, "neumann_shear")]

    boundary_defs[(NAVIER_LAME, "dirichlet_top_neumann_normal")] = [BoundaryDef(type=DIRICHLET, where=TOP, func=Constant((0.0, 0.0))),
                                                                    BoundaryDef(type=NEUMANN, where=RIGHT, func=Constant((1.0, 0.0)))]
    boundary_defs[(NAVIER_LAME, 4)] = boundary_defs[(NAVIER_LAME, "dirichlet_top_neumann_normal")]

    boundary_defs[(POISSON, "dirichlet_zero_left")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant(0.0))]
    boundary_defs[(POISSON, 0)] = boundary_defs[(POISSON, "dirichlet_zero_left")]

    boundary_defs[(POISSON, "dirichlet_inhomogeneous")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant(0.0)),
                                                           BoundaryDef(type=DIRICHLET, where=RIGHT, func=Constant(1.0))]
    boundary_defs[(POISSON, 1)] = boundary_defs[(POISSON, "dirichlet_inhomogeneous")]

    boundary_defs[(POISSON, "dirichlet_zero_all")] = [BoundaryDef(type=DIRICHLET, where="all", func=Constant(0.0))]
    boundary_defs[(POISSON, 2)] = boundary_defs[(POISSON, "dirichlet_zero_all")]

    boundary_defs[(POISSON, "dirichlet_neumann1")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant(0.0)),
                                                      BoundaryDef(type=NEUMANN, where=RIGHT, func=Constant(1.0))]
    boundary_defs[(POISSON, 3)] = boundary_defs[(POISSON, "dirichlet_neumann1")]

    boundary_defs[(POISSON, "dirichlet_neumann2")] = [BoundaryDef(type=DIRICHLET, where=TOP, func=Constant(1.0)),
                                                      BoundaryDef(type=NEUMANN, where=RIGHT, func=Constant(1.0))]
    boundary_defs[(POISSON, 4)] = boundary_defs[(POISSON, "dirichlet_neumann2")]

    boundary_defs[(CONVECTION_DIFFUSION, "dirichlet_zero_left")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant(0.0))]
    boundary_defs[(CONVECTION_DIFFUSION, 0)] = boundary_defs[(CONVECTION_DIFFUSION, "dirichlet_zero_left")]

    boundary_defs[(CONVECTION_DIFFUSION, "dirichlet_inhomogeneous")] = [BoundaryDef(type=DIRICHLET, where=LEFT, func=Constant(0.0)),
                                                           BoundaryDef(type=DIRICHLET, where=RIGHT, func=Constant(1.0))]
    boundary_defs[(CONVECTION_DIFFUSION, 1)] = boundary_defs[(CONVECTION_DIFFUSION, "dirichlet_inhomogeneous")]

    boundary_defs[(CONVECTION_DIFFUSION, "dirichlet_zero_all")] = [BoundaryDef(type=DIRICHLET, where="all", func=Constant(0.0))]
    boundary_defs[(CONVECTION_DIFFUSION, 2)] = boundary_defs[(CONVECTION_DIFFUSION, "dirichlet_zero_all")]

    # definitions for random variables
    UNIFORM = "uniform"
    NORMAL = "normal"

    rv_defs = dict()
    rv_defs[UNIFORM] = lambda i: UniformRV(a=-1, b=1)
    rv_defs[NORMAL] = lambda i: NormalRV(mu=0, sigma=1)


    @staticmethod
    def get_decay_start(exp, gamma=1):
        start = 1
        while zeta(exp, start) >= gamma:
            start += 1
        return start

    @staticmethod
    def get_ampfunc(amptype, decayexp, gamma):
        if amptype == "decay-inf":
            start = SampleProblem.get_decay_start(decayexp, gamma)
            amp = gamma / zeta(decayexp, start)
            ampfunc = lambda i: amp / (float(i) + start) ** decayexp
            # logger.info("type is decay_inf with start = " + str(start) + " and amp = " + str(amp))
        elif amptype == "decay-algebraic":
            ampfunc = lambda i: gamma / (float(i) + 1) ** decayexp
            # logger.info("type is decay_algebraic with gamma = " + str(gamma) + " and decayexp = " + str(decayexp))
        elif amptype == "decay-exponential":
            ampfunc = lambda i: np.exp(-decayexp * (i + 1))
            # logger.info("type is decay_exponential decayexp = " + str(decayexp))
        elif amptype == "constant":
            ampfunc = lambda i: gamma
        else:
            raise ValueError("Unknown amplitude type {}".format(amptype))
        return ampfunc

    @classmethod
    def setup_coef_field(cls,
                         functype,
                         amptype,
                         rvtype='uniform',
                         gamma=0.9,
                         decayexp=2,
                         freqscale=1,
                         freqskip=0,
                         scale=1,
                         dim=2,
                         secondparam=None,
                         coef_mean=1.0
                         ):
        try:
            rvs = cls.rv_defs[rvtype]
        except KeyError:
            raise ValueError("Unknown RV type {}".format(rvtype))

        try:
            func = cls.func_defs[(functype, dim)]
        except KeyError:
            raise ValueError("Unknown function type {} for dim {}".format(functype, dim))

        # get scaling factor function
        ampfunc = SampleProblem.get_ampfunc(amptype, decayexp, gamma)

        # logger.info("amp function: %s", str([ampfunc(i) for i in range(10)]))

        mis = MultiindexSet.createCompleteOrderSet(dim)
        for i in range(freqskip + 1):
            next(mis)
            # mis.next()

        def generate_expression(func, freq, A, B, m, n, data_degree=10):
            func, min_val, max_val, max_grad = func
            if n is None:
                ex = Expression(func, freq=freq, A=A, B=B, M=m, degree=data_degree, cell='triangle')
            else:
                ex = Expression(func, freq=freq, A=A, B=B, M=m, N=n, degree=data_degree, cell='triangle')
            try:
                for k, v in {"freq": freq, "A": A, "B": B, "M": m, "N": n, "pi": "np.pi"}.iteritems():
                    v = str(v)
                    min_val = min_val.replace(k, v)
                    max_val = max_val.replace(k, v)
                    max_grad = max_grad.replace(k, v)
                ex.min_val = eval(min_val)
                ex.max_val = eval(max_val)
                ex.max_grad = eval(max_grad)
            except:
                # print("no min/max info available for coefficients")
                # logger.info("no min/max info available for coefficients")
                pass
            return ex

        a0 = generate_expression(("A*B", "A*B", "A*B", "0.0"), freq=freqscale, A=coef_mean, B=scale, m=0, n=None)
        if dim == 1:
            a = (generate_expression(func=func, freq=freqscale, A=ampfunc(i), B=scale,
                                     m=int(mu[0]), n=None) for i, mu in enumerate(mis))
        else:
            a = (generate_expression(func=func, freq=freqscale, A=ampfunc(i), B=scale,
                                     m=int(mu[0]), n=int(mu[1])) for i, mu in enumerate(mis))

        if secondparam is not None:
            try:
                from itertools import izip as zip
            except ImportError:  # will be 3.x series
                pass
            a0 = (a0, secondparam[0])
            a = ((am, bm) for am, bm in zip(a, secondparam[1]))
        return ParametricCoefficientField(a0, a, rvs)
