from __future__ import (division, print_function)
import numpy as np
from dolfin import (FunctionSpace, project, interpolate, Function)
from alea.math_utils.param_pde.sample_problems import SampleProblem
from alea.utils.parametric_array import ParametricArray


class AffineFieldCache(object):
    def __init__(self,
                 fs                                 # type: FunctionSpace
                 ):                                 # type: (...) -> None
        self.fs = fs
        self.func_list = []


class AffineField(object):
    def __init__(self,
                 coef_type="cos",                   # type: str
                 amptype="decay-inf",               # type: str
                 decayexp=2,                        # type: float
                 gamma=0.9,                         # type: float
                 freqscale=1.0,                     # type: float
                 freqskip=0,                        # type: int
                 scale=1.0,                         # type: float
                 coef_mean=0.0,                     # type: float
                 rv_type="uniform",                 # type: str
                 discretisation="project"           # type: str
                 ):                                 # type: (...) -> None

        # region build option dictionary
        coef_options = {
            "decayexp": decayexp,
            "gamma": gamma,
            "freqscale": freqscale,
            "freqskip": freqskip,
            "rvtype": rv_type,
            "scale": scale,
            "coef_mean": coef_mean
        }
        # endregion

        if discretisation == "project":
            self.discretisation_fun = project
        elif discretisation == "interpolate":
            self.discretisation_fun = interpolate
        else:
            raise ValueError("unknown discretisation function. use 'project' or 'interpolate'")

        # setup coeff field
        self.coef_field = SampleProblem.setup_coef_field(coef_type, amptype, **coef_options)
        # decay coefficient function
        self.ampfunc = SampleProblem.get_ampfunc(amptype, decayexp, gamma)
        self.coeff_type = coef_type
        self.amptype = amptype
        self.decayexp = decayexp
        self.gamma = gamma
        self.scale = scale
        self.freqscale = freqscale
        self.freqskip = freqskip
        self.coef_mean = coef_mean
        self.rv_type = rv_type

    def __getitem__(self,
                    i                               # type: int
                    ):                              # type: (...) -> [ParametricArray, ParametricArray, function]
        if i > 0:
            return self.coef_field[i - 1], self.ampfunc(i)
        else:
            return (self.coef_field.mean_func, None), 1

    def evaluate_basis(self, x, _m):
        assert _m > 0
        _n = x.shape[0]
        v = np.zeros((_n, _m + 1))
        # evaluate all basis functions
        for i, p in enumerate(x):
            v[i, :] = [self[m][0][0](p) for m in range(_m + 1)]
        return v

    def __call__(self, y, fs, cache=None, project_result=False):
        if isinstance(y, list):
            y = [1] + y
            _m = len(y)
        elif isinstance(y, np.ndarray):
            y = np.insert(y, 0, 1)
            _m = y.shape[0]
        else:
            raise ValueError("unknown type for y: {}".format(y))

        if cache is None:
            cache = AffineFieldCache(fs)
        assert(cache.func_list is not None)
        if len(cache.func_list) < _m:
            assert cache.fs is not None
            cache.func_list = []
            for m in range(_m):
                fun = self.discretisation_fun(self[m][0][0], cache.fs)
                cache.func_list.append(fun.vector()[:])
            cache.func_mat = np.array(cache.func_list)
        y_ = np.array(y)
        retval = np.dot(y_, cache.func_mat)
        if project_result:
            ret = Function(fs)
            ret.vector().set_local(retval)
            return ret
        return retval

    def sample_rvs(self, _m):
        return [self.rvs(m).sample(1)[0] for m in range(_m)]

    @property
    def rvs(self):
        return self.coef_field.rvs

    def get_infty_norm(self, _m):
        if (self.coeff_type == 'cos' or self.coeff_type == "sin") and self.amptype == "decay-inf":
            # using a decay that defines a convergent series in the affine part
            from scipy.special import zeta  # get zeta function
            #                                           # get decay start value from affine field
            start = SampleProblem.get_decay_start(self.decayexp, self.gamma)
            amp = self.gamma / zeta(self.decayexp, start)  # get corresponding amplification
            #                                           # \|a_m\|_\infty = scale*gamma/zeta * (m+start)^{-sigma}
            bxmax_m = [self.scale * amp * (i + start) ** (-self.decayexp) for i in range(_m)]
            bxmax = np.array(bxmax_m)  # use np.arrays
        elif (self.coeff_type == 'cos' or self.coeff_type == "sin") and self.amptype == "decay-algebraic":
            #                                           # special case using algebraic decay
            # start = SampleProblem.get_decay_start(decayexp, gamma)
            amp = self.gamma
            start = 1
            bxmax_m = [self.scale * amp * (i + start) ** (-self.decayexp) for i in range(_m)]
            # print bxmax_M
            bxmax = np.array(bxmax_m)
        else:
            raise NotImplementedError("other coefficient field types are not implemented")
        return bxmax

    def get_filename(self):
        filename = "AffineField"
        filename += "coeff_type:{}".format(self.coeff_type)
        filename += "amptype:{}".format(self.amptype)
        filename += "decayexp:{}".format(self.decayexp)
        filename += "gamma:{}".format(self.gamma)
        filename += "scale:{}".format(self.scale)
        filename += "freqscale:{}".format(self.freqscale)
        filename += "freqskip:{}".format(self.freqskip)
        filename += "coef_mean:{}".format(self.coef_mean)
        filename += "rv_type:{}".format(self.rv_type)
        return filename

    def get_hash(self):
        return hash(self.get_filename())

    def get_ainfty(self, m, fs):
        a0_f = self.coef_field.mean_func
        if isinstance(a0_f, tuple):
            a0_f = a0_f[0]
        # determine min \overline{a} on D (approximately)

        f1 = project(a0_f, fs)
        am_f, _ = self.coef_field[m]
        if isinstance(am_f, tuple):
            am_f = am_f[0]

        # determine ||a_m/\overline{a}||_{L\infty(D)} (approximately)
        f2 = project(am_f, fs)
        f3 = np.divide(np.ascontiguousarray(f2.vector()), np.ascontiguousarray(f1.vector()))      # TODO: revert...
        # print f1.array,f2.array
        ainftym = max(f3)
        # print ">>>>>>>>>>", max_am, min_a0, ainftym
        assert isinstance(ainftym, float)  # and ainftym > 0
        return ainftym
