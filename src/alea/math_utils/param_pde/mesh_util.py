import sys
if sys.version_info >= (3, 0):
    from dolfin import (UnitSquareMesh, UnitDiscMesh, MPI, refine, FunctionSpace, Mesh, DOLFIN_EPS,
                        DirichletBC, Constant, parameters)
    mpi_comm_world = MPI.comm_world
else:
    from dolfin import (UnitSquareMesh, UnitDiscMesh, mpi_comm_world, refine, FunctionSpace, Mesh, DOLFIN_EPS,
                        DirichletBC, Constant, parameters)
import numpy as np


def get_mesh(mesh_type="square", mesh_refine=-1, mesh_nodes=1e2):

    import os
    parameters['reorder_dofs_serial'] = False  # set default behaviour of fenics dof reordering
    if mesh_type == "lshape":
        lshape_xml = os.path.join(os.path.dirname(__file__), 'lshape.xml')
        mesh = Mesh(lshape_xml)

    elif mesh_type == "circle":
        mesh = UnitDiscMesh.create(MPI.comm_world, 3, 1, 2)
        # old fenics version. TODO: check version
        # mesh = UnitDiscMesh(mpi_comm_world(), 3, 1, 2)  # generate unit square mesh with n rings, order 1 and geo dim 2
    elif mesh_type == "square":
        mesh = UnitSquareMesh(3, 3)
    elif mesh_type == "circle_hole":
        _xml = os.path.join(os.path.dirname(__file__), 'circle_hole.xml')
        mesh = Mesh(_xml)
    elif mesh_type == "circle_square":
        _xml = os.path.join(os.path.dirname(__file__), 'circle_square.xml')
        mesh = Mesh(_xml)
    else:
        raise ValueError("unknown mesh type: {}".format(mesh_type))

    if mesh_refine > -1:
        for lia in range(mesh_refine):
            old_cells = FunctionSpace(mesh, "DG", 0).dim()
            mesh = refine(mesh)
            # region project refined boundary points back on unitcircle
            if mesh_type == "circle":
                def u0_boundary(x, on_boundary):
                    return on_boundary
                fs = FunctionSpace(mesh, 'CG', 1)
                bc = DirichletBC(fs, Constant(0), u0_boundary)
                dof2val = bc.get_boundary_values()
                bc_dofs = dof2val.keys()
                for lib, x in enumerate(mesh.coordinates()):
                    if lib in bc_dofs:
                        if -1e-12 < x[0] < 1e-12:
                            if x[1] > 0:
                                x[1] = np.sqrt(1 - x[0] ** 2)
                            else:
                                x[1] = -np.sqrt(1 - x[0] ** 2)
                        elif x[0] >= 0 and x[1] > 0:
                            x[0] = np.sqrt(1 - x[1] ** 2)
                        elif x[1] <= 0 <= x[0]:
                            x[0] = np.sqrt(1 - x[1] ** 2)
                        elif x[0] < 0 < x[1]:
                            x[0] = -np.sqrt(1 - x[1] ** 2)
                        elif x[0] < 0 and x[1] <= 0:
                            x[0] = -np.sqrt(1 - x[1] ** 2)
            # endregion
            print("refined from {} to {} cells".format(old_cells, FunctionSpace(mesh, "DG", 0).dim()))
    elif mesh_nodes > 0:
        while FunctionSpace(mesh, "CG", 1).dim() < mesh_nodes:
            old_cells = FunctionSpace(mesh, "DG", 0).dim()
            mesh = refine(mesh)  # refine mesh until minimal number of nodes is reached
            # region project refined boundary points back on unitcircle
            if mesh_type == "circle":
                def u0_boundary(x, on_boundary):
                    return on_boundary

                fs = FunctionSpace(mesh, 'CG', 1)
                bc = DirichletBC(fs, Constant(0), u0_boundary)
                dof2val = bc.get_boundary_values()
                bc_dofs = dof2val.keys()
                for lib, x in enumerate(mesh.coordinates()):
                    if lib in bc_dofs:
                        if -1e-12 < x[0] < 1e-12:
                            if x[1] > 0:
                                x[1] = np.sqrt(1 - x[0] ** 2)
                            else:
                                x[1] = -np.sqrt(1 - x[0] ** 2)
                        elif x[0] >= 0 and x[1] > 0:
                            x[0] = np.sqrt(1 - x[1] ** 2)
                        elif x[0] >= 0 and x[1] <= 0:
                            x[0] = np.sqrt(1 - x[1] ** 2)
                        elif x[0] < 0 and x[1] > 0:
                            x[0] = -np.sqrt(1 - x[1] ** 2)
                        elif x[0] < 0 and x[1] <= 0:
                            x[0] = -np.sqrt(1 - x[1] ** 2)
            # endregion
            print("refined from {} to {} cells".format(old_cells, FunctionSpace(mesh, "DG", 0).dim()))
    parameters['reorder_dofs_serial'] = True  # set default behaviour of fenics dof reordering
    return mesh


def get_boundary(boundary_type="dirichlet"):
    boundary = None
    if boundary_type == "dirichlet":
        def boundary(x, on_boundary):
            return on_boundary
    elif boundary_type == "inner_neumann":
        def boundary(x, on_boundary):
            return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS
    elif boundary_type == "south_neumann":
        def boundary(x, on_boundary):
            if x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS:  # South site open
                return on_boundary
            return False
    elif boundary_type == "inner_dirichlet_south_neumann":
        def boundary(x, on_boundary):
            if x[1] > 0 + DOLFIN_EPS:
                return on_boundary
            else:
                return False  # Dirichlet + south open
    elif boundary_type == "circle_dirichlet":
        def boundary(x, on_boundary):
            return on_boundary and abs(x[0]*x[0] + x[1]*x[1] - 1 < DOLFIN_EPS)

    else:
        print("unknown boundary condition type: {} -> exit()".format(boundary_type))
        exit()
    return boundary