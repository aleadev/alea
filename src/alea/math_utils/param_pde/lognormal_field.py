"""
This class implements the analytic representation of the lognormal coefficient field
@author: M. Marschall
"""
# region imports
from __future__ import (division, print_function)
import numpy as np
from dolfin import (FunctionSpace, Function)
from alea.math_utils.tensor.tt_util import (tt_cont_coeff, sample_cont_coeff, fully_disc_first_core,
                                            sample_lognormal_tt)
from alea.math_utils.stochastic.random_variable import NormalRV
from alea.math_utils.param_pde.affine_field import AffineField

import os
# endregion

FILE_PATH = os.path.dirname(__file__) + '/tmp/'


class LognormalField(object):
    # region def init
    def __init__(self,
                 _af                                # type: AffineField
                 ):
        """
        Constructor
        :param _af: Affine field to construct the log-normal field from
        """
        self.affine_field = _af
        assert(isinstance(self.affine_field.coef_field.rvs(0), NormalRV))
    # endregion

    # region property mean_func
    @property
    def mean_func(self):
        return self.affine_field.coef_field.mean_func
    # endregion

    # region property rvs
    @property
    def rvs(self):
        return self.affine_field.coef_field.rvs
    # endregion

    # region property funcs
    @property
    def funcs(self):
        return self.affine_field.coef_field.funcs
    # endregion

    # region def sample_rvs
    def sample_rvs(self, m):
        return self.affine_field.sample_rvs(m)
    # endregion

    # region def call
    def __call__(self,
                 y,                                 # type: list or np.ndarray
                 fs,                                # type: FunctionSpace
                 cache=None,                        # type: object
                 project_result=False               # type: bool
                 ):                                 # type: (...) -> np.ndarray or Function
        """
        calls the affine field and evaluates the exponential of it at given sample points y and physical mesh of the fs
        :param y: sample node in stochastic space
        :param fs: Fenics FunctionSpace to project and evaluate the field at
        :param cache: class instance that contains the object func_list
        :param project_result: flag to create a fenics function object
        :return: value of exp(a(x,y))
        """
        if project_result:
            vec = np.exp(self.affine_field(y, fs, cache=cache, project_result=False))
            retval = Function(fs)
            retval.vector().set_local(vec)
            return retval
        return np.exp(self.affine_field(y, fs, cache=cache, project_result=False))
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "LognormalField"
        filename += "/af:" + self.affine_field.get_filename()
        return filename
    # endregion

    # region def get_hash
    def get_hash(self):
        return hash(self.get_filename())
    # endregion


"""
This class implements the semi discrete representation of the lognormal coefficient field in tensor train format.
Creation of the field is described in M. Pfeffer dissertation or briefly in EMPS1.
Fully-discrete refers to a discrete stochastic space in the sense of an underlying, finite dimensional polynomial basis
in each stochastic dimension.
@author: M. Marschall
"""
# region imports
# TODO # from alea.application.bayes.alea_util.configuration.asgfem_config import AsgfemConfig
# endregion


class TTLognormalSemidiscreteField(LognormalField):
    # region def init
    def __init__(self,
                 _af,                               # type: AffineField
                 hdegs,                             # type: list
                 ranks,                             # type: list
                 fs,                                # type: FunctionSpace
                 mesh_dofs=100000,                  # type: int
                 quad_degree=7,                     # type: int
                 theta=0.5,                         # type: float
                 rho=1,                             # type: float
                 acc=1e-10,                         # type: float
                 scale=1.0,                         # type: float
                 domain='square'                    # type: str
                 ):
        """
        Constructor to create a semi-discrete coefficient field in tensor train format
        :param hdegs: list of hermite degrees
        :param ranks: list of ranks
        :param fs: FunctionSpace to define the coef on
        :param mesh_dofs: number of mesh dofs to minimal reach while initial refinement
        :param quad_degree: quadrature degree of the rule to integrate the occuring quadruple integrals
        :param theta: scale 1 from GS (acta)
        :param rho: scale 2 from GS (acta)
        :param acc: accuracy to reach while tensor eigenvalue truncation
        """
        LognormalField.__init__(self, _af)
        self.hermite_degree = hdegs
        self.ranks = ranks
        self.fs = fs
        self.mesh_dofs = mesh_dofs
        self.quad_degree = quad_degree
        self.theta = theta
        self.rho = rho
        self.acc = acc
        self.scale = scale
        self.domain = domain
        self.cont_coef_cores = []
        self.max_norms = self.affine_field.get_infty_norm(len(self.hermite_degree))
        self.computed = False
        # if self.load() is False or True:
        self._create_cont_coeff()                   # start mesh calculation
        # else:
        #     self.computed = True

    # endregion

    # region def private create continuous cores
    def _create_cont_coeff(self):
        from copy import deepcopy

        cont_coeff_cores = tt_cont_coeff(self.affine_field, self.fs.mesh(), len(self.hermite_degree),
                                         deepcopy(self.ranks),
                                         self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                         acc=self.acc, mesh_dofs=self.mesh_dofs, quad_degree=self.quad_degree,
                                         coef_mesh_cache=None, domain=self.domain, scale=self.scale)
        self.cont_coef_cores = cont_coeff_cores
        # TODO
        # self.save()
        self.computed = True
    # endregion

    # region def sample semi-discrete field
    def __call__(self,
                 y,                                 # type: list or np.ndarray
                 fs,                                # type: FunctionSpace
                 cache=None,
                 project_result=False
                 ):
        """
        sample the continuous coefficient for the sample nodes on the function space
        :param y:
        :param fs:
        :param cache: Not used ATM
        :param project_result:
        :return:
        """
        vec = sample_cont_coeff(self.cont_coef_cores, self.affine_field, [], y, self.ranks,
                                self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho, fs=fs)
        if project_result:
            retval = Function(fs)
            retval.vector().set_local(vec)
            return retval
        return vec
    # endregion

    # region def get continuous coefficient cores
    def get_cont_cores(self):
        if self.computed is True:
            return self.cont_coef_cores
        else:
            raise ValueError("Cont-cores are not computed yet")
    # endregion

    # region def sample continuous field
    def sample_continuous_field(self,
                                y,  # type: list or np.ndarray
                                fs,  # type: FunctionSpace
                                cache=None,
                                project_result=False
                                ):
        return LognormalField.__call__(self, y, fs, cache, project_result)
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "TTLognormalSemiDiscreteField"
        filename += "lognormal_af:{}".format(super(TTLognormalSemidiscreteField, self).get_filename())
        filename += "hermite_degree={}".format(self.hermite_degree)
        filename += "ranks={}".format(self.ranks)
        filename += "max_norms={}".format(self.max_norms)
        filename += "mesh_dofs={}".format(self.mesh_dofs)
        filename += "quad_degree={}".format(self.quad_degree)
        filename += "theta={}".format(self.theta)
        filename += "rho={}".format(self.rho)
        filename += "acc={}".format(self.acc)
        filename += "scale={}".format(self.scale)
        filename += "domain={}".format(self.domain)
        return filename
    # endregion

    # region load
    def load(self):
        # TODO
        raise NotImplemented
        """
        loads a pickled file that contains the continuous tensor cores, as well as the mesh
        :return: boolean
        """
        try:
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.cont_coef_cores = tmp_dict['coef_cores']
            self.mesh = Mesh(FILE_PATH + str(self.get_hash()) + '-mesh.xml')
        except Exception as ex:
            print("exception: {}".format(ex.message))
            return False
        return True
    # endregion

    # region def save
    def save(self):
        # TODO
        raise NotImplemented
        """
        pickles the cores of the continuous coefficient tensor and the mesh into a file
        :return: boolean
        """
        try:
            File(FILE_PATH + str(self.get_hash()) + "-mesh.xml") << self.mesh
            retval = dict()
            retval['coef_cores'] = self.cont_coef_cores
            f = open(FILE_PATH + str(self.get_hash()) + ".dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
        except Exception as ex:
            print("exception: {}".format(ex.message))
            return False
        return True
    # endregion

    # region def read coef from config
    @staticmethod
    def read_coef_from_config(
                              config                # type: AsgfemConfig
                              ):
        """
        trys to read a complete coefficient from a given AsgfemConfig file
        :param config: configuration file containing all necessary information
        :return: TTLognormalSemidiscreteField
        """
        # TODO:
        raise NotImplemented
        assert(isinstance(config, AsgfemConfig))
        affine_field = AffineField(config.coeffield_type, "decay-algebraic", config.decay_exp_rate,
                                   config.sgfem_gamma, config.freq_scale, config.freq_skip, config.scale,
                                   config.field_mean)
        hermite_degree = [config.hermite_degree]*config.num_coeff_in_coeff
        ranks = [1] + [config.max_rank]*config.num_coeff_in_coeff + [1]
        max_norms = affine_field.get_infty_norm(config.num_coeff_in_coeff)
        retval = TTLognormalSemidiscreteField(affine_field, hermite_degree, ranks, max_norms,
                                              mesh_dofs=config.coef_mesh_dofs, quad_degree=config.coef_quad_degree,
                                              theta=config.theta, rho=config.rho, acc=config.coef_acc, scale=config.scale,
                                              domain=config.domain)
        return retval
    # endregion


class TTLognormalFullyDiscreteField(TTLognormalSemidiscreteField):
    # region def init
    def __init__(self,
                 semi_cont_field,                   # type: TTLognormalSemidiscreteField
                 ):
        """
        create a fully discrete coefficient field in TT format using the given continuous cores and the physical
        fenics function space
        :param semi_cont_field: semi discrete coefficient field
        :param fs: function space of physical description
        """
        TTLognormalSemidiscreteField.__init__(self, semi_cont_field.affine_field, semi_cont_field.hermite_degree,
                                              semi_cont_field.ranks, semi_cont_field.fs,
                                              semi_cont_field.mesh_dofs, semi_cont_field.quad_degree,
                                              semi_cont_field.theta, semi_cont_field.rho, semi_cont_field.acc,
                                              semi_cont_field.scale, semi_cont_field.domain)
        self.femdegree = self.fs.ufl_element().degree()
        if self.fs.ufl_element().family() == "Lagrange":
            self.family = "CG"
        elif self.fs.ufl_element().family() == "Discontinuous Lagrange":
            self.family = "DG"
        else:
            raise NotImplemented("Other spaces than CG or DG are not implemented")
        if self.family == "DG" and not self.femdegree == 0:
            raise NotImplemented("For DG spaces we need to have constant elements -> go over to CG")
        self.dofs = self.fs.dim()
        self.computed = False
        self.scale = semi_cont_field.scale
        self._create_discrete_coeff()

    # endregion

    # region def private create discrete cores
    def _create_discrete_coeff(self):
        from copy import deepcopy
        if self.family == "CG":
            mp = np.array(self.fs.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, self.fs.mesh().geometry().dim()))

            first_core = fully_disc_first_core(self.cont_coef_cores, self.affine_field, mp, self.ranks,
                                               self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                               cg_mesh=self.fs.mesh(), femdegree=self.femdegree, scale=self.scale)
        elif self.family == "DG":
            mp = np.array(self.fs.tabulate_dof_coordinates())
            #                                       # reshape dof vector according to physical dimension
            mp = mp.reshape((-1, self.fs.mesh().geometry().dim()))

            first_core = fully_disc_first_core(self.cont_coef_cores, self.affine_field, mp, self.ranks,
                                               self.hermite_degree, self.max_norms, theta=self.theta, rho=self.rho,
                                               cg_mesh=self.fs.mesh(), femdegree=1, scale=self.scale)
        else:
            raise NotImplemented("other types of meshes then CG and DG are not implemented")
        self.discrete_coef_cores = deepcopy(self.cont_coef_cores)
        self.discrete_coef_cores.insert(0, first_core)
        self.computed = True
    # endregion

    # region def sample fully-discrete field
    def __call__(self,
                 y,
                 fs,
                 cache=None,
                 project_result=False
                 ):
        """

        :param y:
        :param fs:
        :param cache:
        :param project_result:
        :return:
        """
        if self.computed is False:
            raise ValueError("Discrete Field is not computed at the moment")
        vec = sample_lognormal_tt(self.discrete_coef_cores, y, self.max_norms, theta=self.theta, rho=self.rho)
        if project_result:
            retval = Function(self.fs)
            retval.vector()[:] = vec
            return retval
        return vec
    # endregion

    # region def get continuous coefficient cores
    def get_cont_cores(self):
        if self.computed is True:
            return self.cont_coef_cores
        else:
            raise ValueError("Cont-cores are not computed yet")
    # endregion

    # region def sample semi-discrete field
    def sample_semidiscrete_field(self,
                                  x,                # type: np.array
                                  y                 # type: list
                                  ):                # return: np.array
        return TTLognormalSemidiscreteField.__call__(self, x, y)
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "TTLognormalSemiDiscreteField"
        filename += "lognormal_af:{}".format(super(TTLognormalFullyDiscreteField, self).get_filename())
        filename += "family:{}".format(self.family)
        filename += "degree:{}".format(self.femdegree)
        filename += "dim:{}".format(self.dofs)
        return filename
    # endregion

    # region def __getitem__
    def __getitem__(self, i):
        """
        enables the use of the [] operator to get access to the cores more quickly
        :param i: index
        :return: core at index
        """
        if self.computed is not True:
            raise ValueError("discrete cores are not computed yet")
        if 0 < i < len(self.discrete_coef_cores):
            return self.discrete_coef_cores[i]
    # endregion

    # region def __len__
    def __len__(self):
        """
        computes and returns the len of discrete coefficient cores
        :return: number of cores (incl the first deterministic one)
        """
        if self.computed is not True:
            raise ValueError("discrete cores are not computed yet")
        return len(self.discrete_coef_cores)
    # endregion

    # region extract mean
    def extract_mean(self):
        """
        computes the mean of the current coefficient field as a function dependent on the space variable
        :return: list containing the mean value at the mesh node
        """
        # TODO
        raise NotImplemented
        import tt
        Pvec = extract_mean(tt.vector.from_list(self.discrete_coef_cores))
        P = compute_FE_matrix_coeff(Pvec, self.mesh, exp_a=True, degree=self.femdegree)
        return P
    # endregion
