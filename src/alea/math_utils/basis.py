from __future__ import division, print_function, absolute_import
import numpy as np
from decimal import Decimal as Dec
from alea.application.bayes.paper_radialtrafo.polynomial import ArbitraryPrecision_Poly
# from typing import List

SIN = lambda k: lambda t: (1. / np.sqrt(np.pi)) * np.sin(k * t)
COS = lambda k: lambda t: (1. / np.sqrt(np.pi)) * np.cos(k * t)
CONSTANTFOURIERMODE = lambda t: t**0 / np.sqrt(2 * np.pi)
BASESEPARATOR = "#-#"


class PickableFunction(object):

    def __call__(self, x):
        pass

    def to_hash(self):                              # type: (...) -> str
        pass

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                 ):                                 # type: (...) -> PickableFunction or None
        pass


class FourierSinModes(PickableFunction):
    def __init__(self, order):
        self.order = order

    def __call__(self, x):
        return SIN(self.order)(x)

    def to_hash(self):                              # type: (...) -> str
        return "FourierSinMode{}".format(self.order)

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> FourierSinModes or None
        try:
            order = int(hash_str.strip("FourierSinModes"))
        except ValueError as ex:
            print("ValueError: Can not convert {} to int: {}".format(hash_str.strip("FourierSinModes"), ex.message))
            return None
        except TypeError as ex:
            print("TypeError: Can not convert {} to int: {}".format(hash_str.strip("FourierSinModes"), ex.message))
            return None
        return FourierSinModes(order)


class FourierCosModes(PickableFunction):
    def __init__(self, order):
        self.order = order

    def __call__(self, x):
        return COS(self.order)(x)

    def to_hash(self):                              # type: (...) -> str
        return "FourierCosMode{}".format(self.order)

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> FourierCosModes or None
        try:
            order = int(hash_str.strip("FourierCosModes"))
        except ValueError as ex:
            print("ValueError: Can not convert {} to int: {}".format(hash_str.strip("FourierCosModes"), ex.message))
            return None
        except TypeError as ex:
            print("TypeError: Can not convert {} to int: {}".format(hash_str.strip("FourierCosModes"), ex.message))
            return None
        return FourierCosModes(order)


class ConstantFourierMode(PickableFunction):
    def __init__(self):
        pass

    def __call__(self, x):
        return CONSTANTFOURIERMODE(x)

    def to_hash(self):
        return "ConstantFourierMode"

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                 ):
        return ConstantFourierMode()


class NumpyPolynomial(PickableFunction):
    def __init__(self,
                 coef_list                          # type: list
                 ):
        self.coef_list = coef_list
        self.poly = np.polynomial.Polynomial(coef_list)

    def __call__(self, x):
        return self.poly(x)

    def to_hash(self):                              # type: (...) -> str
        coef_str = ", ".join([str(v) for v in self.coef_list])
        return "NumpyPolynomial{}".format(coef_str).replace("[", "").replace("]", "")

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> NumpyPolynomial
        coef_list = [float(v) for v in hash_str.strip("NumpyPolynomial").split(",")]
        return NumpyPolynomial(coef_list)

class NumpyLegendrePolynomial(PickableFunction):
    def __init__(self,
                 coef_list,                         # type: list
                 domain=None                        # type: list or None
                 ):
        self.coef_list = coef_list
        self.domain = [-1, 1] if domain is None else domain
        self.poly = np.polynomial.legendre.Legendre(coef_list, domain=self.domain)

    def __call__(self, x):
        return self.poly(x)

    def to_hash(self):                              # type: (...) -> str
        coef_str = ", ".join([str(v) for v in self.coef_list])
        return "NumpyLegendrePolynomial{}".format(coef_str).replace("[", "").replace("]", "")

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> NumpyPolynomial
        coef_list = [float(v) for v in hash_str.strip("NumpyLegendrePolynomial").split(",")]
        return NumpyLegendrePolynomial(coef_list)

class NumpyHermitePolynomial(PickableFunction):
    def __init__(self,
                 coef_list                          # type: list
                 ):
        self.coef_list = coef_list
        self.poly = np.polynomial.hermite_e.HermiteE(coef_list)

    def __call__(self, x):
        return self.poly(x)

    def to_hash(self):                              # type: (...) -> str
        coef_str = ", ".join([str(v) for v in self.coef_list])
        return "NumpyHermiteEPolynomial{}".format(coef_str).replace("[", "").replace("]", "")

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> NumpyHermitePolynomial
        coef_list = [float(v) for v in hash_str.strip("NumpyHermiteEPolynomial").split(",")]
        return NumpyHermitePolynomial(coef_list)

class ArbitraryPrecisionPolynomial(PickableFunction):
    def __init__(self,
                 coef_list                          # type: list
                 ):
        self.coef_list = coef_list
        self.poly = ArbitraryPrecision_Poly(coef_list)

    def __call__(self, x):
        return self.poly(x)

    def __getitem__(self, item):
        return self.poly.coef[item]

    def __len__(self):
        return len(self.poly.coef)

    @staticmethod
    def polymul(c1, c2):
        return ArbitraryPrecision_Poly.polymul(c1, c2)

    @staticmethod
    def set_precision(prec):
        ArbitraryPrecision_Poly.set_precision(prec)

    @staticmethod
    def skp(f, g, w, subdomain):
        ArbitraryPrecision_Poly.skp(f, g, w, subdomain)

    def to_hash(self):                              # type: (...) -> str
        coef_str = ", ".join([str(v) for v in self.coef_list])
        return "ArbitraryPrecisionPolynomial{}".format(coef_str).replace("[", "").replace("]", "")

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> NumpyPolynomial
        coef_list = [Dec(v) for v in hash_str.strip("ArbitraryPrecisionPolynomial").split(",")]
        return ArbitraryPrecisionPolynomial(coef_list)


class PickableBase(object):
    def __init__(self,
                 base_list                          # type: List[PickableFunction]
                 ):
        self.base_list = base_list

    def to_hash(self):                              # type: (...) -> str
        retval = ""
        for lia, item in enumerate(self.base_list):
            retval += item.to_hash()
            if lia < len(self.base_list)-1:
                retval += BASESEPARATOR
        return retval

    def __getitem__(self,
                    item                            # type: int
                    ):                              # type: (...) -> PickableFunction
        return self.base_list[item]

    @staticmethod
    def from_hash(
                  hash_str                          # type: str
                  ):                                # type: (...) -> PickableBase
        print(hash_str)
        hash_list = hash_str.split(BASESEPARATOR)
        base_list = []
        for item in hash_list:

            if item.startswith("NumpyPolynomial"):
                stripped_item = item.strip("NumpyPolynomial")
                print(stripped_item)
                base_list.append(NumpyPolynomial.from_hash(stripped_item))
            elif item.startswith("FourierCosMode"):
                base_list.append(FourierCosModes.from_hash(item))
            elif item.startswith("FourierSinMode"):
                base_list.append(FourierSinModes.from_hash(item))
            elif item.startswith("ConstantFourierMode"):
                base_list.append(ConstantFourierMode.from_hash(item))
            elif item.startswith("ArbitraryPrecisionPolynomial"):
                base_list.append(ArbitraryPrecisionPolynomial.from_hash(item))
            else:
                raise ValueError("unknown basis type: {}".format(item))
        return PickableBase(base_list)

    def __len__(self):
        return len(self.base_list)

    def __repr__(self):
        return str([base for base in self.base_list])

    def __str__(self):
        return self.__repr__()