from types import GeneratorType


class ParametricArray(object):
    """Dynamically growing array based on a generator or a callable."""
    Empty = object()

    def __init__(self, func):
        self._vals = []
        self._func = func

    def __len__(self):
        return len(self._vals)

    def __call__(self, i):
        return self.__getitem__(i)

    def __getitem__(self, i):
        if i >= len(self._vals):
            self._grow(i)
        val = self._vals[i]
        if val is ParametricArray.Empty:
            assert not isinstance(self._func, GeneratorType)
            val = self._func(i)
            self._vals[i] = val
        return val

    def _grow(self, i):
        ell = len(self._vals)
        if i >= ell:
            if callable(self._func):
                self._vals += [ParametricArray.Empty] * (i - ell + 1)
            else:
                self._vals += [next(self._func) for _ in range(i - ell + 1)]

    def __str__(self):
        return str(self._vals)
