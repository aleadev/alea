from __future__ import print_function, division
import timeit
import numpy as np


class PercentageBar:

    def __init__(self,
                 n_iter,                            # type: int
                 notebook=False                     # type: bool
                 ):
        """
        Simple percentage and expected remaining time counter for long loops
        :param n_iter: maximal number of iteration
        :param notebook: Flag to use the notebook display function, instead of print
        """
        assert n_iter > 0
        self.start = timeit.default_timer()
        self.n_iter = n_iter
        self.lia = 0
        self.notebook = notebook

    def next(self):
        """
        activate printing the current progress and iterates the counter
        :return: None
        """
        stop = timeit.default_timer()
        if (self.lia / self.n_iter * 100) < 5:
            expected_time = "calculating"
            suffix = ""
        else:
            time_perc = timeit.default_timer()
            expected_time = np.round((time_perc - self.start) / (self.lia / self.n_iter) / 60, 2)
            suffix = " minutes"
        if expected_time == "calculating":
            print(
                "  Current progress:{:.2f}% run-time: {:.2f}/{}".format(np.round((self.lia + 1) / self.n_iter * 100, 2),
                                                                        np.round((stop - self.start) / 60, 2),
                                                                        expected_time) + suffix,
                end='\r')
        else:
            print("  Current progress:{:.2f}% run-time: {:.2f}/{:.2f}".format(np.round((self.lia+1) / self.n_iter * 100, 2),
                                                              np.round((stop - self.start) / 60, 2),
                                                              expected_time) + suffix,
                  end='\r')
        self.lia += 1
