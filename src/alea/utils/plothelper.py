from dolfin import plot, interactive, UnitSquareMesh, Expression, FunctionSpace, interpolate, Function, Mesh, File,\
    MeshFunctionSizet
import numpy as np
from mpl_toolkits.mplot3d import Axes3D


class PlotProxy(object):
    def __init__(self, name):
        self.name = name
        self.handle = None
    def plot(self, f, *args, **kwargs):
        if self.handle is None:
            if not "title" in kwargs.keys():
                kwargs["title"] = self.name
            self.handle = plot(f, *args, **kwargs)
        else:
            self.handle.plot(f)

class PlotHelper(object):
    def __init__(self):
        self.handles = {}
    def __getitem__(self, name):
        try:
            ph = self.handles[name]
        except:
            ph = PlotProxy(name)
            self.handles[name] = ph
        return ph

def test_plotting():
    mesh = UnitSquareMesh(10,10)
    V = FunctionSpace(mesh, 'CG', 1)
    f1 = Expression("sin(A*(x[0]+x[1]))", A=1)
    f2 = Expression("cos(A*x[0]*x[1])", A=1)
    ph = PlotHelper()
    for A in range(5):
        f1.A = A+1
        f2.A = A+1
        g1 = interpolate(f1, V)
        g2 = interpolate(f2, V)
        ph["f1"].plot(g1)
        ph["f2"].plot(g2, interactive=True)


# taken from
# http://fenicsproject.org/qa/5795/plotting-dolfin-solutions-in-matplotlib

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.colors as col

def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def plot_mpl(obj):
    ''' plot FEniCS object (use with plt.figure() ... plt.show()) '''
    plt.gca().set_aspect('equal')
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')
    elif isinstance(obj, MeshFunctionSizet):
        mesh, C = obj.mesh(), obj.array()
        vmin, vmax = min(C), max(C)
        plt.tripcolor(mesh2triang(mesh), facecolors=C, edgecolors=['none','k'][0], cmap=plt.cm.coolwarm, vmin=vmin, vmax=vmax)  # coolwarm, brg, seismic, winter, jet, hot, spectral, prism
    else:
        print "unsupported plotting type", type(obj)
        assert TypeError


def write_pvd_file(_path, _obj):
    f2 = File(_path + ".pvd")
    f2 << _obj

def write_2d_png(_path, _obj):
    plt.figure()
    plt.gca().set_aspect('equal')
    if isinstance(_obj, Function):
        _mesh = _obj.function_space().mesh()
        if _mesh.geometry().dim() != 2:
            raise AttributeError
        if _obj.vector().size() == _mesh.num_cells():
            C = _obj.vector().array()
            plt.tripcolor(mesh2triang(_mesh), C, edgecolors="k")
        else:
            C = _obj.compute_vertex_values(_mesh)
            plt.tripcolor(mesh2triang(_mesh), C, shading='gouraud', edgecolors="k")
    elif isinstance(_obj, Mesh):
        if _obj.geometry().dim() != 2:
            raise AttributeError
        plt.triplot(mesh2triang(_obj), color='k')
    # TODO: make a 3D plot
    plt.colorbar()
    plt.title("min value of array: {}".format(np.min(_obj.vector().array())))
    plt.savefig(_path + ".png")
    plt.clf()


def write_3d_png(_path, _obj):
    plt.figure()
    ax = plt.gca(projection='3d')
    ax.set_aspect('equal')
    if isinstance(_obj, Function):
        _mesh = _obj.function_space().mesh()
        if _mesh.geometry().dim() != 2:
            raise AttributeError
        if _obj.vector().size() == _mesh.num_cells():
            C = _obj.vector().array()
            # plt.tripcolor(mesh2triang(_mesh), C, edgecolors="k")
            ax.plot_trisurf(mesh2triang(_mesh), C, cmap=plt.cm.CMRmap)
        else:
            C = _obj.compute_vertex_values(_mesh)
            ax.plot_trisurf(mesh2triang(_mesh), C, cmap=plt.cm.CMRmap)
    else:
        raise ValueError("con not plot unknown file type: {}".format(type(_obj)))
    plt.title("min value of array: {}".format(np.min(_obj.vector().array())))
    plt.savefig(_path + ".png")
    plt.clf()

if __name__ == '__main__': 
    test_plotting()


def draw_figure(canvas, figure, loc=(0, 0)):
    import matplotlib.backends.tkagg as tkagg
    from matplotlib.backends.backend_agg import FigureCanvasAgg
    from Tkinter import PhotoImage
    """ Draw a matplotlib figure onto a Tk canvas

    loc: location of top-left corner of figure on canvas in pixels.
    Inspired by matplotlib source: lib/matplotlib/backends/backend_tkagg.py
    """
    figure_canvas_agg = FigureCanvasAgg(figure)
    figure_canvas_agg.draw()
    figure_x, figure_y, figure_w, figure_h = figure.bbox.bounds
    figure_w, figure_h = int(figure_w), int(figure_h)
    photo = PhotoImage(master=canvas, width=figure_w, height=figure_h)

    # Position: convert from top-left anchor to center anchor
    canvas.create_image(loc[0] + figure_w/2, loc[1] + figure_h/2, image=photo)

    # Unfortunately, there's no accessor for the pointer to the native renderer
    tkagg.blit(photo, figure_canvas_agg.get_renderer()._renderer, colormode=2)

    # Return a handle which contains a reference to the photo object
    # which must be kept live or else the picture disappears
    return photo
