from __future__ import (division, print_function)
import numpy as np

from dolfin import *
from alea.math_utils.param_pde.affine_field import AffineField
from alea.math_utils.param_pde.lognormal_field import LognormalField
from alea.math_utils.param_pde.forward_operator.poisson import ParametricPoisson, SolutionCache
from alea.math_utils.param_pde.mesh_util import (get_mesh, get_boundary)
from alea.math_utils.tensor.extended_fem_tt import (ExtendedFEMTT, ExtendedTT, BasisType)

from alea.utils.progress.percentage import PercentageBar
from alea.application.bayes.tempered_posterior import TemperedPosterior
np.random.seed(8011990)

exp_field_options = {
    "coef_type": "cos",
    "amptype": "decay-inf",
    "decayexp": 2,
    "gamma": 0.9,
    "freqscale": 1.0,
    "freqskip": 0,
    "scale": 1.0,
    "coef_mean": 0.0,
    "rv_type": "normal"
}

# create the affine field for the exponent
exp_field_af = AffineField(**exp_field_options)
# wrap the affine field with a log-normal description
exp_field = LognormalField(exp_field_af)

# given a Fenics FunctionSpace, boundary condition, a rhs and the coefficient above,
# we define the forward problem as
# problem = ParametricPoisson(exp_field, rhs, FunctionSpace, boundaryCondition)

ref_mesh = get_mesh("square", mesh_nodes=1000)
boundary = get_boundary("dirichlet")
ref_fs = FunctionSpace(ref_mesh, 'CG', 2)
ref_bc = DirichletBC(ref_fs, Constant(0.0), boundary)
rhs = Constant(1.0)

ref_problem = ParametricPoisson(exp_field, rhs, ref_fs, ref_bc)
print("dimension of reference space: {}".format(ref_problem.fs.dim()))

K = 25
points = []
for x in np.arange(0+1/sqrt(K), 1, 1/sqrt(K)):
    for y in np.arange(0+1/sqrt(K), 1, 1/sqrt(K)):
        points.append([x, y])

M = 2

sigma = 20
n_samples = 1000

true_para = np.random.randn(M)
true_coef = exp_field(true_para, ref_fs, project_result=True)
true_sol = ref_problem.solve(true_para, reference_m=M)


noise = 1e-2
delta = []
for lia in range(len(points)):
    loc_delta = true_sol(points[lia]) + np.random.randn()*noise
    delta.append(loc_delta)
obs = {"x": points,
       "delta": delta}


def Phi(_model, _xi, _obs, _sigma, *args, **kwargs):
    _sol = _model.solve(_xi, *args, **kwargs)
    return 0.5/_sigma*np.sum([(_sol(_obs["x"][_lia]) - _obs["delta"][_lia])**2 for _lia in range(len(_obs["x"]))],
                             axis=0)

def prior(_xi):
    return 1/np.sqrt(2*np.pi)*np.exp(-0.5*np.linalg.norm(_xi)**2)

solver_cache = SolutionCache()

mesh = get_mesh("square", mesh_nodes=100)
fs = FunctionSpace(mesh, 'CG', 1)
bc = DirichletBC(fs, Constant(0.0), boundary)

pw_problem = ParametricPoisson(exp_field, rhs, fs, bc)

_phi = lambda _xi: Phi(_model=pw_problem, _xi=_xi, _obs=obs, _sigma=noise, reference_m=2, cache=solver_cache)

if False:
    from scipy import integrate
    from numpy.polynomial.hermite_e import hermegauss

    nodes, weights = hermegauss(99)

    Z_true = np.sum([[w1*w2*np.exp(-_phi([n1, n2])) for (w1, n1) in zip(weights, nodes)]
                     for (w2, n2) in zip(weights, nodes)])

    print(Z_true)
    exit()

    Z_true, int_err = integrate.dblquad(lambda _xi1, _xi2: np.exp(-_phi([_xi1, _xi2]))*prior([_xi1, _xi2]),
                                        -np.inf, np.inf, lambda _x: -np.inf, lambda _x: np.inf)

    print(Z_true)
    print(int_err)
    # ###### results in Z = 2.1139
    exit()

print("create samples")
n_Z_samples = 10000

Z_samples = np.array(map(np.random.randn, [2 for _ in range(n_Z_samples)]))

print("sample Z")
Z_true = np.sum(map(lambda _xi: np.exp(-_phi(_xi)), [Z_samples[lia, :] for lia in range(n_Z_samples)]))/n_Z_samples
print(Z_true)
exit()



recon_dict = {
    "new_fs": None,
    "adf_tol": 1e-8,
    "adf_iter": 10000,
    "_poly_dim": 5,
    "use_rb": False,
    "ew_tol": 1e-16,
    "poly_sys": "H"
}


beta = 1/(sigma-1)
tp = TemperedPosterior(_phi, beta)
hashfile = hash(str(n_samples) + str(M) + str(beta))
hashfile += hash(frozenset(recon_dict.items()))

tmp_file = "tmp/" + str(hashfile) + ".dat"
tempered_tt = ExtendedTT.load(tmp_file)
if tempered_tt is None:
    tempered_tt = tp.reconstruct_tt(n_samples, M, np.random.randn, **recon_dict)
    tempered_tt.save(tmp_file)

print(tempered_tt)

exp_tt_half_hash = hashfile + hash(str(sigma))
tmp_file = "tmp/" + str(exp_tt_half_hash) + ".dat"
exp_tt_half = ExtendedTT.load(tmp_file)
if exp_tt_half is None or True:
    bar = PercentageBar(sigma)
    exp_tt_half = tempered_tt.copy()
    for lia in range(int(sigma/2)-1):
        exp_tt_half = exp_tt_half.multiply_with_extendedTT(tempered_tt)
        exp_tt_half.round()
        bar.next()
    # exp_tt_half = exp_tt_half.canonicalize_left()
    exp_tt_half = exp_tt_half.cut(maxdim=20)
    exp_tt_half.save(tmp_file)
print(exp_tt_half)

print(exp_tt_half.components[0])
Z = np.sum(np.dot(exp_tt_half.components[0][0, :, :], exp_tt_half.components[0][0, :, :].T))
# base_change = np.eye(exp_tt_half.n[0], exp_tt_half.n[1])
# base_change = np.reshape(base_change, (base_change.shape[0], 1, base_change.shape[1]))

# Z = exp_tt_half.multandeval(exp_tt_half, [0, 0], base_change=base_change, progress=True)
print(Z)