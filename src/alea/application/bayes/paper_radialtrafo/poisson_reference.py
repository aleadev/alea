from __future__ import (division, print_function, absolute_import)
import sys
import numpy as np
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
from alea.application.bayes.paper_radialtrafo.config import RadialTrafoConfig as Config
import time
import matplotlib.pyplot as plt
import json

import os

if sys.version_info < (3, 0):
    from dolfin import (set_log_level, WARNING)

    set_log_level(WARNING)
else:
    from dolfin import (set_log_level, LogLevel)

    set_log_level(LogLevel.ERROR)

np.random.seed(8011990)
problem_type = "gaussian"

config_file_path = "{}_conf.json".format(problem_type)
d = 3
var = 1e-7
mean = 1
mc_ref = 10000

config = Config(config_file_path)
config["dim"] = d
config["problem"]["M"] = d
config["problem"]["n_points"] = 144
config["problem"]["noise"] = var

config["optimizer"]["use_difftool"] = True
config["optimizer"]["log_optimizer"] = True
config["optimizer"]["init_value"] = np.zeros(d)
config["recon"]["n_samples"] = 100
config["recon"]["adf_iteration"] = 10000
config["sample_path"] = "{}_sample_d{}.dat".format(problem_type, d)
config["sample_path_tt"] = "{}_sample_d{}_tt.dat".format(problem_type, d, len(config["recon"]["radi"]))
config["sample_path_prior"] = "numerical_examples_radi/{}_sample_prior_d{}.dat".format(problem_type, d)

if not config.load():
    raise ValueError("something went wrong in the configuration")
problem = config["model"]

config["optimizer"]["init_value"] = problem.true_para
print("#" * 20)
print("compute MAP")
trafo = problem.compute_map(**config["optimizer"])
print("#" * 20)
print("log: {}, difftool: {}".format(config["optimizer"]["log_optimizer"], config["optimizer"]["use_difftool"]))
print("  MAP = {}".format(trafo.map))
print("  hessian: \n{}".format(trafo.hess))
optimizer_calls = config["model"].post.num_calls


def perturbed_prior(x):
    """
    \tilde f_0 = f \circ \tilde T \|det J_T\|
    where \tilde T is the approximated transport, here in the affine case
    \tilde T = H^\frac{1}{2} \cdot + M
    :param x: sample
    :return: transported sample
    """
    f = config["recon"]["func"]
    tilde_T = trafo.transport_trafo
    return f(tilde_T(x)) * trafo.det_hessian()


def posterior(x):
    return (config["recon"]["func"](x))

config["recon"]["radi"] = np.linspace(0, 10, num=30)
config["recon"]["maxdegs"] = [[7] * len(config["recon"]["radi"])] + [5] * (d - 1)
config["recon"]["force_recalc"] = False

print("Start reconstruction")
ten_list = trafo.reconstruct_tt_uq(**config["recon"])
print("Tensor reconstruction result")
print(" Dimensions: {}".format([t.n for t in ten_list]))
print(" Ranks: {}".format([t.r for t in ten_list]))

rad_tt = RadialTT(ten_list, trafo)

print("Compute Z with TT")
Z_list, Z_tt = trafo.compute_Z()
rad_tt.set_normalisation(Z_tt)

def _tt(r, theta):
    return rad_tt([r, *theta], normalised=False)

def _true(r, theta):
    return perturbed_prior(trafo.radial_trafo([r, *theta]))

'''
r = np.linspace(0, 10, num=100)
for lia in range(100):
    theta = [np.random.rand()*2*np.pi] + (np.random.rand(trafo.dim-2)*np.pi).tolist()
    fig = plt.figure()
    plt.subplot(121)
    plt.plot(r, [_true(r[lib], theta) for lib in range(len(r))], label="perturbed")
    plt.plot(r, [_tt(r[lib], theta) for lib in range(len(r))], label="approx")
    plt.legend()
    plt.subplot(122)
    plt.semilogy(r, [np.abs(_true(r[lib], theta)-_tt(r[lib], theta))/_true(r[lib], theta)
                 for lib in range(len(r))], label="rel error")
    plt.legend()
    fig.savefig("tmp/perturbed_{}.png".format(lia))

r = np.linspace(0, 10, num=100)
fig = plt.figure()
for lia in range(10):
    theta = [np.random.rand()*2*np.pi] + (np.random.rand(trafo.dim-2)*np.pi).tolist()

    plt.semilogy(r, [_true(r[lib], theta) for lib in range(len(r))])
    # plt.plot(r, [_tt(r[lib], theta) for lib in range(len(r))], label="approx")
    # plt.legend()
fig.savefig("tmp/perturbed_many_log.png")
'''

posterior_sample_path = "poisson_reference_samples_d{}.dat".format(d)
if not os.path.exists(posterior_sample_path):
    print("create MCMC samples for error sampling later from true posterior")

    trafo.create_trafo_samples(mc_ref, posterior_sample_path, posterior)


from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler

with open(posterior_sample_path, "r") as f:
    inf = json.load(f)
sampler = MCMC_Sampler(log_prob=None,
                       N=None,
                       burn_in=None,
                       ndim=None,
                       nwalkers=None,
                       p0=None,
                       samples=inf
                       )

from alea.utils.corner import corner
fig = corner(sampler.get(mc_ref))
fig.savefig("poisson_reference_d{}.pdf".format(d))

samples = np.array(sampler.get(mc_ref))
mean_true = np.mean(samples, axis=0)
mean_approx = rad_tt.mean()
print(mean_true)
print(mean_approx)
print(np.linalg.norm(mean_true - mean_approx)/np.linalg.norm(mean_true))

cov_true = np.cov(samples.T, bias=True)
cov_approx = rad_tt.covariance(useshift=False)
print("cov_true:")
print(cov_true)
print("cov_approx:")
print(cov_approx)
print("cov_true / cov_approx:")
print(cov_true / cov_approx)

print("relative error:")
print(np.linalg.norm(cov_true - cov_approx)/np.linalg.norm(cov_true))