import numpy as np
from scipy.optimize import minimize


class testinv():
    def __init__(self, dim):
        self.dim = dim

        self.map = np.ones(dim)
        self.hess = np.eye(dim, dim)
        self.u, self.sigma, self.v = np.linalg.svd(self.hess, full_matrices=False)

    def fun(self, hatx, _x = None, trafo=False):
        theta, r = hatx[1:], hatx[0]
        sins = np.sin(theta)
        coss = np.cos(theta)

        mat = np.zeros((self.dim, self.dim - 1))
        if self.dim == 2:
            mat[0, :] = coss[0]
            mat[1, :] = sins[0]
        else:  # dim > 2
            mat[0, :] = np.concatenate([np.array([coss[0]]), sins[1:]])
            mat[1, :] = sins
            for d in range(2, self.dim):
                mat[d, :] =  np.array( [1.] * (d - 1) + [coss[d-1]] + sins[d:].tolist())

        res = r * np.prod(mat, axis = 1)
        if trafo:
            h12 = self.u.dot(np.diag(self.sigma ** (-0.5)).dot(self.v))
            res = h12.dot(res) + self.map
        if _x is not None:
            return np.linalg.norm(res - _x)
        else:
            return res


def main():

    dim = 2
    T = testinv(dim)

    for i in range(100):
        x = 5*np.random.rand(dim)

        # x = [3.31099, 0.0466]

        iv = np.array([1.]*dim)

        tol = 1e-16
        bnds = [(0, np.inf)]
        if dim > 1:
            bnds.append((0, 2 * np.pi))
        if dim > 2:
            for _ in range(2, dim):
                bnds.append((0, np.pi))
        sol = minimize(T.fun, iv, args=(x, True), method="SLSQP", tol=tol, bounds=bnds)

        x0 = sol.x
        if np.linalg.norm(T.fun(x0) - x) > 1e-6:
            print(sol)
            print("x = \n", x)

            print("apprx :\n", T.fun(x0))

            print("error", np.linalg.norm(T.fun(x0) - x))


main()