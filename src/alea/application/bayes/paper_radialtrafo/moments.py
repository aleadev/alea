# @author Robert Gruhlke, Wias Berlin, 2019
# log:

from itertools import chain,product
import numpy as np
from sympy import binomial
from sympy.utilities.iterables import multiset_permutations
from sympy.ntheory import multinomial_coefficients
from scipy.integrate import quadrature
from numpy.polynomial import polynomial as Poly

from alea.application.bayes.paper_radialtrafo.polynomial import ArbitraryPrecision_Poly
from alea.math_utils.basis import ArbitraryPrecisionPolynomial
from decimal import Decimal as Dec



# accelerated number partition algorithm
# basis code without truncation provided by
# http://jeromekelleher.net/generating-integer-partitions.html
def truncated_accel_asc(n,dim):
    a = [0 for i in range(n + 1)]
    k = 1
    y = n - 1
    while k != 0:
        x = a[k - 1] + 1
        k -= 1
        while 2 * x <= y:
            a[k] = x
            y -= x
            k += 1
        l = k + 1
        while x <= y:
            a[k] = x
            a[l] = y
            if len(a[:k + 2]) <= dim:
                yield a[:k + 2]
            x += 1
            y -= 1
        a[k] = x + y
        y = x + y - 1
        if len(a[:k + 1]) <= dim:
            yield a[:k + 1]

def get_beta_k_indexset(j_k, dim):
    iterables = [multiset_permutations(p + [0] * (dim - len(p))) for p in truncated_accel_asc(j_k, dim)]
    return chain(*iterables)

def get_j_multiindex(alpha):
    return product(*[list(range(alpha[i] + 1)) for i in range(len(alpha))])

def total_summands(alpha):
    """
    usage :

        for idx, j in zip(total_idx, get_j_multiindex(alpha)):
            # use j information if needed
            for beta_j in idx:
                # beta_j = [beta_1^j, ..., beta_k^j, ..., beta_dim^j]
                # where each beta_k is a list of multiindices with
                # |beta_k| = j_k
                for k, beta_k in enumerate(beta_j):
                #    # use  (k, beta_k) as needed
    """
    dim = len(alpha)
    J = get_j_multiindex(alpha)
    total_idx = [product(*[get_beta_k_indexset(j_k, dim) for j_k in j]) for j in J]

    return total_idx

def c_k_jk_ak(dim, alpha_max, M, H):
    """
    @param dim:
    @param alpha_max : the maximal entry for alpha of interest,e.g.
                        alpha \in [ (1,1,1), (0,0,1), (2,0,1)]
                        then alpha_max = 2
    """
    c = M  # H.dot(M)

    C = np.zeros((dim, alpha_max + 1, alpha_max + 1))
    for alpha_k in range(alpha_max + 1):
        for j_k in range(alpha_k + 1):
            # print("a = {a}, j = {j}".format(a=alpha_k,j=j_k))
            C[:, alpha_k, j_k] = binomial(alpha_k, j_k) * c ** (alpha_k - j_k)

    # C[k,:,:] is a triangular matrix
    return C

def precompute_radial_integral(order, dim, r_domains, basis_r):
    """
       Computes the vector valued tensor

       I[L,n] = integral_{R_L}   r^n basis_r(r)r^{dim-1} dr

       where R_L denotes the L-th layer in r_domains for all n <= order

       @param r_domains:  list of layer domains
                           e.g. r_domains = [(0,0.5),(0.5,1)]

       @param basis_r  : list  of length r_domains, each containing a list of
                         callables representing basis functions on each layer

    """
    # the layers must match
    for i in range(len(r_domains) - 1):
        assert r_domains[i][1] == r_domains[i + 1][0]

    assert(len(r_domains)==len(basis_r))

    I = {}

    # w = lambda r: r ** (dim - 1)
    # w_coeff = [0.] * (dim - 1) + [1] # coeff representation of polynomial w
    w_c_a = [Dec(0.)] * (dim - 1) + [Dec(1.)]

    # p[i] represents r^i polynomial only used in the isinstance case
    p = {}

    for L, R_L in enumerate(r_domains):
        for n in range(0, order + 1):

            I_L_n = np.zeros((len(basis_r[L])))

            # if basis are np.polynomial.Polynomials apply exact algebraic quadrature
            # else apply numeric quadrature

            for n_0, basis_n_0 in enumerate(basis_r[L]):

                if isinstance(basis_n_0, ArbitraryPrecision_Poly) or isinstance(basis_n_0, ArbitraryPrecisionPolynomial):
                    if not n in p:
                        # p[i] represents r -> r^n as coefficient
                        p[n] = [Dec(0.)] * (n) + [Dec(1.)]

                    I_L_n[n_0] = ArbitraryPrecision_Poly.skp(p[n], basis_n_0, w_c_a, subdomain = R_L)

                else:
                    raise NotImplementedError("Other basis functions in radial direction not supported")

                    #if isinstance(basis_n_0, np.polynomial.polynomial.Polynomial):
                    #if not i in p:
                    #        # p[i] represents r -> r^n as coefficient
                    #        p[n] = [0.] * (n + 1)
                    #        p[n][n] = 1.0
                    #
                    #    int_coeff = Poly.polymul(Poly.polymul(p[n], basis_n_0), w_coeff)
                    #    # primitive integral function F
                    #    Int = np.polynomial.Polynomial(Poly.polyint(int_coeff, m=1))
                    #    # HDI :  integral = F(b) - F(a)
                    #    I_L_n[n_0] = Int(R_L[1]) - Int(R_L[0])

                    #else:
                    #    f = lambda r: r ** n * basis_n_0(r) * w(r)
                    #    val, err = quadrature(f, R_L[0], R_L[1])
                    #    assert err < 1e-7
                    #    I_L_n[n_0] = val
            I[(L, n)] = I_L_n

    return I

def precompute_angular_integrals(order, dim, basis):
    """
    Computes the vector valued tensor


      I[i,m,n] =  integral_{W_i}    sin^m(theta_i)*cos^n(theta_i) * basis[i](theta_i) dV_i(theta_i)

    for all n,m <= order.

    Let  N be the maximal approximation order of the basis components:  N = max_i len(basis[i])

    Then this needs a storage of :

                        dim * order^2 * N

    @param basis: list of length dim
                  basis = [basis_i, i = 1,...,dim-1]

                  with basis_i being a list of callable functions


    """
    w = None  # weight function
    W = None  # integration domain

    I = {}

    for i, basis_i in enumerate(basis):
        if i == 0:
            w = lambda t: np.array([1.] * len(t))
            W = [0, 2 * np.pi]
        else:
            w = lambda t: np.sin(t) ** i
            W = [0, np.pi]

        # TODO write as product to speed up
        for n in range(order + 1):
            for m in range(order + 1):

                I_i_m_n = np.zeros((len(basis_i)))
                for n_i, basis_i_n_i in enumerate(basis_i):
                    f = lambda t: np.sin(t) ** m * np.cos(t) ** n * basis_i_n_i(t) * w(t)
                    val, err = quadrature(f, W[0], W[1])
                    assert err < 1e-7
                    I_i_m_n[n_i] = val

                I[(i, m, n)] = I_i_m_n

    return I

def moment_indices(order, dim):
    a = np.empty([dim]*order)
    it = np.ndenumerate(a)
    for idx,_ in it:
        if all(idx[i] <= idx[i+1] for i in range(len(idx)-1)):
            alpha = [0]*dim
            for i in range(order):
                alpha[idx[i]]+=1
            yield alpha

def moment_generator(max_order, M, H, tensor_basis, contract, r_layers = [(0,10.)]):

    """
    Construct a generator for moments of maximal total order of max_order.

    :param max_order:
    :param M:
    :param H:
    :param tensor_basis: callable functions inside [ [r basis], [theta_1 basis], ...]

                         radial part:
                         -----------

                          [r basis] itself is a collection of lists representing basis functions
                          per each layer

                          [r_basis] = [ r_basis_0, r_basis_1, ..., r_basis_L]
                          with r_basis_l being a list of callables

                          angular part:
                          -------------

                          [theta_i_basis] is a list of callables only

    :param contract: contract: contract function takes parameters: l, v

                    with l being a layer number  in {0,1,...}
                    and  v being a list of vectors of len(tensor_basis) = dim of the underlying problem

                    The function returns a real value.

                    application:

                        Given a tensor train tensor it can be contracted with a given list of vectors.
                        Let
                             TT_list = [ tensor_train_l, for l = 0,...,L]
                        be a list of tensor trains, then
                             contract = lambda l,v : TT_list[l].contract(v)

    :param r_layers: list of r layer domains
                           e.g. r_layers = [(0,0.5),(0.5,1)]
    :return:
    """


    dim = len(M)

    # jacobian of the hessian
    c_H = np.abs(np.linalg.det(H))

    # represents the mapping
    #             j,beta  -> = j_beta[j][beta]
    # with multinomial coefficient j_beta[j][beta] defined as
    #
    #      (  j  )     (             j              )
    #      (     )  =  (                            )
    #      ( beta)     (beta_1; beta_2, ..., beta_d )
    #
    # usage:  j_beta[j][beta]
    #
    j_beta = [multinomial_coefficients(dim, j) for j in range(max_order + 1)]

    # C represents the data
    #                        ( alpha_k )
    #  C[k, alpha_k, j_k] =  (         ) * c_k^{alpha_k-j_k}
    #                        (   j_k   )
    CC = c_k_jk_ak(dim, max_order, M, H)

    # print(CC)

    # represents for given list of  B = matrix([beta_k, k=1,...d]) in IR^d,d
    # the pointwise product:
    #
    #                       H^B
    #
    H_beta = None

    # precomputed integrals used for evaluation of v_0(j)  and v_i( (beta_k)_k )
    # TODO respect layer and tensor basis
    L = len(r_layers)
    I_r = precompute_radial_integral(max_order, dim, r_layers, tensor_basis[0])
    I_t = precompute_angular_integrals(max_order, dim, tensor_basis[1:])

    # print("I_r \n {v}".format(v=I_r))
    # print("I_t \n {v}".format(v=I_t))


    def get_i_m_n(i, sum_beta_k, verbose=False):
        # angular component vectors
        m, n = 0, 0
        if i == 0:
            m = sum_beta_k[1]
            n = sum_beta_k[0]
        else:
            # todo check correct summation
            # TODO i+1 was changed
            m = sum(sum_beta_k[:i + 1])
            n = sum_beta_k[i + 1]

        if verbose:
            print(" (i,m,n) = ({i},{m},{n})".format(i=i, m=m, n=n))
        return (i, m, n)

    def compute_moment(alpha, verbose=False):
        total_idx = total_summands(alpha)
        J = get_j_multiindex(alpha)

        result = 0.

        for idx, j in zip(total_idx, J):
            if verbose:
                print("on j = {v}".format(v=j))
            sum_j = sum(j)

            # use j information if needed
            for beta_j in idx:
                # beta_j = [beta_1^j, ..., beta_k^j, ..., beta_dim^j]
                # where each beta_k is a list of multiindices with
                # |beta_k| = j_k
                if verbose:
                    print(" on beta_j = \n {}".format(list(beta_j)))

                sum_beta_k = np.sum(np.array(beta_k) for beta_k in list(beta_j))

                # perform computation of cal_C(vi, i = 0,...,d-1), representing the contraction operator
                # list of vector valued integrals v = [v0^r, v_1,...,v_{d-1}] e.q. (20)-(21)


                cal_C = sum(contract(l, v=[I_r[(l, sum_j)]] + [I_t[get_i_m_n(i, sum_beta_k, verbose)]
                                                               for i in range(dim - 1)]) for l in range(L))
                # for l in range(L):
                #     print("Ir[(l={}, sum_j={})] = {}".format(l, sum_j, I_r[(l, sum_j)]))

                # TODO write as a onliner with np.prod
                # prod over  C_k^H[j_k,alpha_k, beta_k]
                C_H = []
                for k, beta_k in enumerate(beta_j):

                    a1 = CC[k, alpha[k], j[k]]
                    a2 = j_beta[j[k]][tuple(beta_k)]
                    a3 = h_k_beta_k = np.prod(H[k, :] ** np.array(beta_k))

                    if False and verbose:
                        # if j[k] == 2:
                        print("   ----------")
                        print("   k = {k}, beta_k = {b}".format(k=k, b=beta_k))
                        print("   alph_k, j_k  = {a}, {b}".format(a=alpha[k], b=j[k]))
                        print("     c[k,a_k, j_k] = {}".format(a1))
                        print("     j over beta = {}".format(a2))
                        print("     h_k^beta = {}".format(a3))

                    C_k_H = a1 * a2 * a3
                    C_H.append(C_k_H)
                prod_C_H_k = np.prod(np.array(C_H))

                if verbose:
                    print("  prod_C_H_k = {}".format(prod_C_H_k))
                    print("  cal_C      = {}".format(cal_C))

                add = prod_C_H_k * cal_C
                if verbose:
                    print("curr result : {}".format(result))
                    print("add         : {}".format(add))
                result += add
                if verbose:
                    print("total       : {}".format(result))
        # print("c_H: {}".format(c_H))
        return c_H * result  ## ??

    return compute_moment






def moment_generator_quadratic_transport(max_order, T2, tensor_basis, contract, r_layers = [(0,10.)]):
    """
    Computes

         \sum_\ell int_{X_\ell}        T(x)^alpha  \tilde{f}_0(x)  dx
      =  \sum_\ell int_{\hat{X_\ell}}  T(Phi(\hat{x}))^alpha  \tilde{f}_0^TT(\hat{x})  |det J_\Phi (\hat{x})| d\hat x


      where \Phi : \hat{X}_\ell -> X_\ell (see attention notes)


      T2 = (T2_k)_k  colon IR^d -> IR^d is a quadratic  transport, where T2_k(x) is of the form

                T2_k(x) = 0.5*x^T A x + b^Tx + c


      with A in IR^{d,d}, b in IR^d, c in IR.


      ATTENTION:
                Here only d = 2 supported for the moment.
                Phi is assumed to be the polar transformation


    :param alpha:
    :param T2:
    :param tensor_basis:
    :param contract:
    :param r_layers:
    :return:
    """

    if not T2.dim == 2:
        raise NotImplementedError("Other dimensions than 2 currently not implemented")

    if max_order > 2 :
        raise NotImplementedError("Higher moments currently not implemented")


    # for d = 2
    #
    #   Phi( r, t )  = r   [cos(t) , sin(t)]^T

    # T2_k(Phi(r,theta))^alpha
    #
    #
    #   (ax^2 + bx + cy^2 + dy  + exy + f)^alpha
    #
    #  a * r^2 cos(t)^2   +  b r cos(t) + c r^2 sin(t)^2 + d r sin(t) + e r^2 sin(t)cos(t) + f
    #
    #
    #  r^2 * (a cos(t)^2 + c sin(t)^2 + e sin(t)*cos(t) ) +  r * (b cos(t) + d sin(t)) + f

    L = len(r_layers)
    I_r = precompute_radial_integral(2*max_order, T2.dim, r_layers, tensor_basis[0])
    I_t = precompute_angular_integrals(2*max_order, T2.dim, tensor_basis[1:])


    # in vectornotation:
    a = 0.5*T2.A[:,0,0]
    b =     T2.H[:,0]
    c = 0.5*T2.A[:,1,1]
    d =     T2.H[:,1]
    e = 0.5*(T2.A[:, 0, 1] + T2.A[:, 1, 0])
    f =     T2.b[:]


    # TODO generalize to arbitrary dimension
    def compute_moment(alpha, verbose=False):


        def contraction( r_deg , sin_deg, cos_deg ):
            return sum(contract(l, v=[I_r[(l, r_deg)]] + [I_t[(i, sin_deg, cos_deg)]
                                               for i in range(T2.dim - 1)]) for l in range(L))


        if alpha[0] + alpha[1] == 0:

            res = contraction(0,0,0)
            return res

        elif alpha[0] + alpha[1] == 1:
            # note this rule holds for general dimension, replace with sum(alpha) == 1 and k is the position of entry 1

            k = 0 if alpha[0] == 1 else 1
            #  r^2 * (a cos(t)^2 + c sin(t)^2 + e sin(t)*cos(t) ) +  r * (b cos(t) + d sin(t)) + f

            # r^2 * (a cos(t)^2 + c sin(t)^2 + e sin(t)*cos(t) )
            res =  a[k] * contraction(2,0,2) + c[k] * contraction(2,2,0) + e[k] * contraction(2,1,1)

            # affine contribution r * (b cos(t) + d sin(t))
            res += b[k] * contraction(1,0,1) + d[k] * contraction(1,1,0)

            # f
            res += f[k] * contraction(0,0,0)


            return res



        elif alpha[0] == 2 or alpha[1] == 2:

            k = 0 if alpha[0] == 2 else 1
            #  (r^2 * (a cos(t)^2 + c sin(t)^2 + e sin(t)*cos(t) ) +  r * (b cos(t) + d sin(t)) + f ) ^2

            # order r^4 :
            #     a^2cos^4(t)  + c^2 sin^4 (t) + (e^2+2ac) sin^2(t)cos^2(t) +  2ae sin(t)cos^3(t) + 2ce sin^3(t)cos(t)
            res_4 = a[k]**2 * contraction(4,0,4) +  c[k]**2 * contraction(4,4,0)
            res_4 += (e[k]**2 + 2*a[k]*c[k]) * contraction(4,2,2)
            res_4 += 2*a[k]*e[k] * contraction(4,1,3)
            res_4 += 2*c[k]*e[k] * contraction(4,3,1)

            # order r^3 :
            #  2* ( ab cos^3(t) + (ad +be) sin(t) cos^2(t) +  (bc + de) sin^2(t) cos(t) + cd sin^3(t) )
            res_3 = a[k]*b[k] * contraction(3,0,3)
            res_3 += (a[k]*d[k]+b[k]*e[k]) *contraction(3,1,2)
            res_3 += (b[k]*c[k]+d[k]*e[k]) *contraction(3,2,1)
            res_3 += c[k]*d[k] * contraction(3,3,0)
            res_3 *= 2.

            # order r^2:
            #  (b^2 + 2af)cos^2(t) + (d^2 + 2 cf) sin^2(t) + 2*(bd + ef) sin(t) cos(t)
            res_2 = (b[k]**2 + 2*a[k]*f[k]) *contraction(2,0,2)
            res_2 += (d[k]**2 + 2*c[k]*f[k]) *contraction(2,2,0)
            res_2 += 2*(b[k]*d[k] + e[k]*f[k]) *contraction(2,1,1)


            # order r^1 :
            #  2*(bf cos(t) + df sin(t))
            res_1 = 2* (b[k]*f[k] *contraction(1,0,1) + d[k]*f[k] *contraction(1,1,0))

            # order r^0
            res_0 = f[k]**2  *contraction(0,0,0)

            return res_4 + res_3 + res_2 + res_1 + res_0

        elif alpha[0] == 1 and alpha[1] == 1 :
            #   (r^2 * (a1 cos(t)^2 + c1 sin(t)^2 + e1 sin(t)*cos(t) ) +  r * (b1 cos(t) + d1 sin(t)) + f1 )
            # * (r^2 * (a2 cos(t)^2 + c2 sin(t)^2 + e2 sin(t)*cos(t) ) +  r * (b2 cos(t) + d2 sin(t)) + f2 )


            #order 4 :
            #
            #   a1 * a2 cos^4(t)  + c1*c2 sin^4(t)
            # + (e1e2 + a1c2 + a2c1) sin^2(t) cos^2(t)
            # + (a1e2+a2e1) * sin(t) cos^3(t)
            # + (c1e2 + c2e1) * sin^3(t) cos(t)

            res_4  = a[0] * a[1]  *contraction(4,0,4)
            res_4 += c[0] * c[1]  *contraction(4,4,0)
            res_4 += (e[0]*e[1] + a[0]*c[1] + a[1]*c[0]) *contraction(4,2,2)
            res_4 += (a[0]*e[1] + a[1]*e[0]) *contraction(4,1,3)
            res_4 += (c[0]*e[1] + c[1]*e[0]) *contraction(4,3,1)


            # order 3 :
            #   (a1b2 + a2b1) cos^3(t) + (c1d2+c2d1) sin^3(t)
            # + (a1d2 +a2d1 + b2e1 + b1e2) sin(t) cos^2(t)
            # + (b2c1 + b1c2 + d2e1 + d1e2) sin^2(t) cos(t)
            res_3 =  ( a[0] * b[1] + a[1] * b[0] ) * contraction(3, 0, 3)
            res_3 += ( c[0] * d[1] + c[1] * d[0] ) * contraction(3, 3, 0)
            res_3 += (a[0]*d[1]+a[1]*d[0] + b[1]*e[0] + b[0]*e[1] )  * contraction(3, 1,2)
            res_4 += (b[1]*c[0]  + b[0]*c[1] + d[1]*e[0] + d[0]*e[1] )  * contraction(3, 2,1)


            # order 2 :
            #
            # (a1f2 + a2f1 + b1b2) cos^2(t)  + (c1f2+c2f1+d1d2) sin^2(t) + (e1f2+e2f1+b1d2+b2d1)sin(t)cos(t)
            res_2 =  (a[0]*f[1] + a[1]*f[0] + b[0]*b[1])  * contraction(2, 0, 2)
            res_2 += (c[0]*f[1] + c[1]*f[0] + d[0]*d[1])  * contraction(2, 2, 0)
            res_2 += (e[0]*f[1] + e[1]*f[0] + b[0]*d[1] + b[1]*d[0] ) * contraction(2, 1, 1)


            # order 1 :
            # ( b1f2 + b2f1) cos(t)  + (d0f1 + d1f0) sin(t)
            res_1 = (b[0]*f[1] + b[1]*f[0]) * contraction(1, 0, 1)
            res_1 += (d[0]*f[1] + d[1]*f[0]) * contraction(1, 1, 0)

            # order 0 :
            #  f1 * f0
            res_0 = f[0]*f[1] * contraction(0, 0, 0)


            return res_4 + res_3 + res_2 + res_1 + res_0

        else :
            raise NotImplementedError("other moments not supported")


    return compute_moment

def testcase_1():
    dim = 3

    # M = np.random.rand(dim)  # np.array([1.,1.])
    # A = np.random.rand(dim, dim)
    # H = A.transpose().dot(A)
    M = np.ones(dim)
    H = 1e-4*np.eye(dim, dim)
    u, s, v = np.linalg.svd(H)
    H12 = (u.dot(np.diag(s) ** (0.5))).dot(v)

    normalize = (2 * np.pi) ** (-0.5 * dim) * np.linalg.det(H) ** (-0.5)
    tt_cores = [normalize] + [1.] * (dim - 1)
    CONST = lambda default: lambda t: t ** 0
    f = lambda r: np.exp(-0.5 * r ** 2)
    tbasis = [ [[f]] ] + [[CONST(default)] for default in range(dim - 1)]

    print(H12)
    def contract(l, v):
        # TODO This is just for testing purposes
        # this have to be replaced with a correct contraction
        assert l == 0
        # print("constract with : \n {}".format(v))
        res = 1
        for pos in range(dim - 1, -1, -1):
            res *= tt_cores[pos] * v[pos]

        return res

    gen = moment_generator(max_order=2, M=M, H=H12, tensor_basis=tbasis,
                           contract=contract, r_layers=[(0, 10.)])

    # test second moments
    print("===========================================================================")
    print("=========================== Testcase 0 : Gaussian Distribution ============")
    print("===========================================================================")
    print("exact second moment:")
    M = np.array([[M[i]] for i in range(dim)])
    print(M.dot(M.transpose()) + H)

    RES = np.zeros((dim, dim))
    for i in range(0, dim):
        for j in range(0, dim):
            alpha = np.zeros(dim, dtype=np.int)
            alpha[i] += 1
            alpha[j] += 1

            res = gen(alpha, verbose=False)
            RES[i, j] = res
            # print("gen ({i},{j}) = {r}".format(i=i,j=j,r=res))
    print("generator")
    print(np.round(RES, 8))



def testcase_2():
    dim = 5
    max_order = 2

    M = np.random.rand(dim)  # np.array([1.,1.])
    A = np.random.rand(dim, dim)
    H = A.transpose().dot(A)
    u, s, v = np.linalg.svd(H)
    H12 = (u.dot(np.diag(s) ** (0.5))).dot(v)

    normalize = (2 * np.pi) ** (-0.5 * dim) * np.linalg.det(H) ** (-0.5)
    tt_cores = [normalize] + [1.] * (dim - 1)
    CONST = lambda default: lambda t: t ** 0
    f = lambda r: np.exp(-0.5 * r ** 2)
    tbasis = [[[f]]] + [[CONST(default)] for default in range(dim - 1)]

    def contract(l, v):
        # TODO This is just for testing purposes
        # this have to be replaced with a correct contraction
        assert l == 0
        # print("constract with : \n {}".format(v))
        res = 1
        for pos in range(dim - 1, -1, -1):
            res *= tt_cores[pos] * v[pos]

        return res

    gen = moment_generator(max_order=max_order, M=M, H=H12, tensor_basis=tbasis,
                           contract=contract, r_layers=[(0, 10.)])

    moments = {}
    for order in range(max_order+1):
        alphas = moment_indices(order, dim)
        moments[order] = {}
        for alpha in alphas:
            moments[order][tuple(alpha)] = gen(alpha, verbose=False)

    print("===========================================================================")
    print("=========================== Testcase 1 : Application Moment computation ===")
    print("===========================================================================")

    for order in moments.keys():
        print("============= order = {} =============".format(order))
        print("=== number of moments : {} ===".format(len(moments[order])))
        for alpha, moment in moments[order].items():
            print("{a} -> {v}".format(a=alpha, v=moment[0]))



if __name__ == "__main__":
    #test()
    testcase_1()
    # testcase_2()