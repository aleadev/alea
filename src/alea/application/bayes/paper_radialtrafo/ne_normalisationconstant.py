from __future__ import (division, print_function, absolute_import)
import sys
import numpy as np
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
from alea.application.bayes.paper_radialtrafo.config import RadialTrafoConfig as Config
import time
import json
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler

import os
if sys.version_info < (3, 0):
    from dolfin import (set_log_level, WARNING)
    set_log_level(WARNING)
else:
    from dolfin import (set_log_level, LogLevel)
    set_log_level(LogLevel.ERROR)

np.random.seed(8011990)

config_file_path = "gaussian_conf.json"
if not os.path.isfile(config_file_path):
    # Config.write_dummy_config_gauss(config_file_path)
    Config.write_dummy_config_poisson(config_file_path)
dims = [2, 4, 6, 8, 10]
vars = [1e-2, 1e-4, 1e-6, 1e-8]
n_recon_samples = [1000]
means = [1]
# Z_mc_samples = [100, 1000, 10000]
Z_mc_samples = [100, 1000, 10000]

result_path = "gauss_z_examples.dat"

if os.path.exists(result_path):
    with open(result_path, "r") as f:
        info = json.load(f)

    mean = 1
    n_recon_sample = 1000
    Z_mc_sample = 100
    with open("Z_err_dvar.dat", "w") as out:
        out.write("d,1e-2,1e-4,1e-6,1e-8\n")
        for d in dims:
            out.write("{},".format(d))
            Z_err_list = []
            for lia, var in enumerate(vars):
                curr_path = "numerical_examples_Z/d{}_v{}_m{}_n{}_Zmc{}_gauss.dat".format(d, var, mean, n_recon_sample,
                                                                                          Z_mc_sample)
                with open(curr_path, "r") as f:
                    inf = json.load(f)
                curr_z_err = np.abs(inf["Z_true"] - inf["Z_tt"])/np.abs(inf["Z_true"])
                out.write("{}".format(curr_z_err))
                if lia < len(vars)-1:
                    out.write(",")
            out.write("\n")

    with open("Z_dur_dvar.dat", "w") as out:
        out.write("d,1e-2,1e-4,1e-6,1e-8\n")
        for d in dims:
            out.write("{},".format(d))
            Z_err_list = []
            for lia, var in enumerate(vars):
                curr_path = "numerical_examples_Z/d{}_v{}_m{}_n{}_Zmc{}_gauss.dat".format(d, var, mean,
                                                                                          n_recon_sample,
                                                                                          Z_mc_sample)
                with open(curr_path, "r") as f:
                    inf = json.load(f)
                curr_z_err = inf["duration"]
                out.write("{}".format(curr_z_err))
                if lia < len(vars) - 1:
                    out.write(",")
            out.write("\n")

    exit()



info = []



config = Config(config_file_path)
for d in dims:
    for var in vars:
        for n_recon_sample in n_recon_samples:
            for mean in means:
                for Z_mc_sample in Z_mc_samples:
                    curr_path = "numerical_examples_Z/d{}_v{}_m{}_n{}_Zmc{}_gauss.dat".format(d, var, mean, n_recon_sample, Z_mc_sample)
                    if os.path.exists(curr_path):
                        with open(curr_path, "r") as f:
                            inf = json.load(f)
                        info.append(inf)
                        continue
                    config["dim"] = d
                    config["problem"]["mean"] = np.ones(d) * mean
                    config["problem"]["var"] = np.eye(d, d) * var
                    config["recon"]["radi"] = np.linspace(0, 10, num=20)
                    config["recon"]["n_samples"] = n_recon_sample
                    config["recon"]["maxdegs"] = [[7] * len(config["recon"]["radi"])] + [1] * (d - 1)
                    config["sample_path"] = "gauss_sample_d{}_m{}_v{}_r{}.dat".format(d, var, mean, len(config["recon"]["radi"]))
                    config["sample_path_tt"] = "gauss_sample_d{}_m{}_v{}_r{}_tt.dat".format(d, var, mean, len(config["recon"]["radi"]))
                    config["recon"]["force_recalc"] = True
                    # #######

                    if not config.load():
                        raise ValueError("something went wrong in the configuration")
                    problem = config["model"]

                    # TODO: use heuristical a priori adaptivity to chose radi
                    start = time.time()
                    print("#"*20)
                    print("compute MAP")
                    trafo = problem.compute_map(**config["optimizer"])
                    print("#"*20)
                    print("log: {}, difftool: {}".format(config["optimizer"]["log_optimizer"], config["optimizer"]["use_difftool"]))
                    print("  MAP = {}".format(trafo.map))
                    print("  hessian: \n{}".format(trafo.hess))
                    optimizer_calls = config["model"].post.num_calls
                    ### ###
                    if config_file_path == "gaussian_conf.json":
                        Z_true = 1
                        if Z_mc_sample != Z_mc_samples[0]:
                            continue
                        Z_mc_sample = 0

                    else:
                        n_z_samples = 10000

                        print("create MCMC samples for error sampling later from true posterior")


                        def prior(x):
                            """
                            :param x: sample
                            :return: transported sample
                            """
                            _f = config["model"].prior
                            return _f(x)


                        trafo.create_trafo_samples(n_z_samples, config["sample_path_prior"], prior)

                        print("compute Z approx")
                        with open(config["sample_path_prior"], "r") as f:
                            inf = json.load(f)
                        sampler = MCMC_Sampler(log_prob=None,
                                               N=None,
                                               burn_in=None,
                                               ndim=None,
                                               nwalkers=None,
                                               p0=None,
                                               samples=inf
                                               )
                        Z_true = 1/n_z_samples*np.sum([np.exp(-config["model"].Phi(s)) for s in sampler.get(n_z_samples)], axis=0)

                    ten_list = trafo.reconstruct_tt_uq(**config["recon"])
                    print("Tensor reconstruction result")
                    print(" Dimensions: {}".format([t.n for t in ten_list]))
                    print(" Ranks: {}".format([t.r for t in ten_list]))

                    rad_tt = RadialTT(ten_list, trafo)

                    print("Compute Z with TT")
                    Z_list, Z_tt = trafo.compute_Z()
                    rad_tt.set_normalisation(Z_tt)
                    duration = time.time() - start
                    calls = config["model"].post.num_calls
                    for lia, Z in enumerate(Z_list):
                        print("circular disc [{}, {}] Z_TT = {} / {}".format(config["recon"]["radi"][lia], config["recon"]["radi"][lia + 1],
                                                                             Z, Z_tt))

                    print("Z_MC:", Z_true, "Z_tt:", rad_tt.Z_tt, "Z_tail:", rad_tt.Z_tail, "Z_approx:", rad_tt.Z_tail + rad_tt.Z_tt)
                    inf ={
                        "duration": duration,
                        "calls": calls,
                        "Z_true": Z_true,
                        "Z_tt": rad_tt.Z_tt,
                        "Z_tail": rad_tt.Z_tail,
                        "Z_approx": rad_tt.Z_tt + rad_tt.Z_tail,
                        "dim": d,
                        "var": var,
                        "mean": mean,
                        "n_recon_samples": n_recon_sample,
                        "Z_mc_sample": Z_mc_sample,
                        "optimizer_calls": optimizer_calls,
                        "dofs": rad_tt.dofs(),
                        "average_r": rad_tt.average_r(),
                    }
                    with open(curr_path, "w") as f:
                        json.dump(inf, f, indent=4)
                    info.append(inf)

with open(result_path, "w") as f:
    json.dump(info, f, indent=4)
