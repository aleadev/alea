import numpy as np
from scipy.stats import multivariate_normal
from scipy.optimize import minimize
from numpy.polynomial.hermite import hermgauss
from numpy.polynomial.legendre import leggauss
from scipy.integrate import dblquad
from alea.utils.decorators import count_calls
#numpy.polynomial.hermite.hermgauss
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

#import TransportMaps as TM

import TransportMaps.Distributions as DIST

import numpy.linalg as npla
import TransportMaps as TM
import TransportMaps.Maps as MAPS

from scipy.optimize import basinhopping, differential_evolution, shgo, dual_annealing

from scipy.linalg import expm



class triangle_quadratic_transport(object):
    def __init__(self, dim):
        self.dim = dim
        # number of degrees of freedom in a dim x dim symmetrix
        # matrix
        self.sndof = int(dim * (dim + 1) / 2)

        # represents identity map
        # self.A = np.zeros((self.dim,self.dim,self.dim))
        # self.H = np.eye(self.dim,self.dim)
        # self.b = np.zeros((self.dim))



        self.A = np.zeros((self.dim, self.dim, self.dim))
        self.H = np.zeros((self.dim, self.dim))
        self.b = np.zeros((self.dim))

        # indices of lower triangular matrix access
        self.idx = np.tril_indices(dim, k=0, m=dim)


    def get(self):

        p = []
        p.append(self.A[0,0,0])

        p += self.A[1,:,:][self.idx].tolist()

        p.append(self.H[0,0])

        p.append(self.H[1,0])
        p.append(self.H[1,1])

        p += (self.b[:]).tolist()

        return np.array(p)

    def set(self, p):


        #print("p = ", p )

        self.A[0,0,0] = p[0]

        self.A[1,:,:][self.idx] = p[1:self.sndof+1]
        self.A[1, :, :] = np.tril(self.A[1, :, :]) + np.triu(self.A[1, :, :].T, 1)

        self.H[0,0] = p[self.sndof + 1]
        self.H[0,1] = p[self.sndof + 2]

        self.H[1,0] = p[self.sndof+2 + 1]
        self.H[1,1] = p[self.sndof+2 + 2]

        self.b[0] = p[self.sndof + 2 + 3]
        self.b[1] = p[self.sndof + 2 + 4]

        #print(" A1 \n", np.round(self.A[0, :, :], 4))
        #print(" A2 \n", np.round(self.A[1, :, :], 4))
        #print("  H \n", np.round(self.H, 4))
        #print("  b \n", np.round(self.b, 4))

    def T(self, x):
        """
        Returns T(x)

          0.5 * Axx + Hx + b

        :param x:
        :return:
        """

        Axx = np.array([x.transpose().dot((self.A[k, :, :]).dot(x)) for k in range(self.dim)])
        Hx = self.H.dot(x)

        return ( 0.5*Axx + Hx + self.b).reshape(1, 2)

    def jac(self, x):
        """
        computes  |det jacobian(T)[x]|
        :param x:
        :return:
        """
        J_Ax = np.zeros((self.dim, self.dim))

        for k in range(self.dim):
            J_Ax[k, :] = self.A[k].dot(x)

        res = np.abs(np.linalg.det(J_Ax + self.H))
        #print("jac = ", res)
        return res

class convex_comb_transport(object):
    def __init__(self,T1,T2,t):
        assert T1.dim == T2.dim
        self.dim = T1.dim
        self.A = t * T1.A + (1.-t) * T2.A
        self.H = t * T1.H + (1.-t) * T2.H
        self.b = t * T1.b + (1.-t) * T2.b

    def T(self, x):
        """
        Returns T(x)

          0.5 * Axx + Hx + b

        :param x:
        :return:
        """

        Axx = np.array([x.transpose().dot((self.A[k, :, :]).dot(x)) for k in range(self.dim)])
        Hx = self.H.dot(x)

        return (0.5 * Axx + Hx + self.b).reshape(1, 2)

    def jac(self, x):
        """
        computes  |det jacobian(T)[x]|
        :param x:
        :return:
        """
        J_Ax = np.zeros((self.dim, self.dim))

        for k in range(self.dim):
            J_Ax[k, :] = self.A[k].dot(x)

        res = np.abs(np.linalg.det(J_Ax + self.H))
        # print("jac = ", res)
        return res




    def visualize(self):

        for k in range(2):
            print("{a:>7}x^2 + {b:>7}x + {c:>7}y^2 +"
                  " {d:>7}y + {e:>7}xy + {f:>7}".format(a=round(0.5*self.A[k, 0, 0], 5),
                                                        b=round(self.H[k, 0], 5),
                                                        c=round(0.5*self.A[k, 1, 1], 5),
                                                        d=round(self.H[k, 1], 5),
                                                        e=round(0.5*(self.A[k, 0, 1] + self.A[k, 1, 0]), 5),
                                                        f=round(self.b[k], 5)))



class quadratic_transport(object):

    def __init__(self, dim):

        self.dim = dim
        # number of degrees of freedom in a dim x dim symmetrix
        # matrix
        self.sndof = int(dim*(dim+1)/2)

        # represents identity map
        #self.A = np.zeros((self.dim,self.dim,self.dim))
        #self.H = np.eye(self.dim,self.dim)
        #self.b = np.zeros((self.dim))



        self.A = np.random.randn(self.dim, self.dim, self.dim)
        self.H = np.random.randn(self.dim, self.dim)
        self.b = np.random.randn(self.dim)


        #indices of lower triangular matrix access
        self.idx = np.tril_indices(dim, k=0, m=dim)

    def get(self):
        """
        extracts d.o.f. to a single vector
            sndof :=  (dim+1)*dim/2

        with length:
           ndofs(A) + ndofs(H) + ndofs(b)
        dim * sndof + sndof    + dim
        :return:
        """

        p = []
        for k in range(self.dim):
            p += (self.A[k,:,:][self.idx]).tolist()
        p += (self.H[self.idx]).tolist()
        p += (self.b[:]).tolist()

        print(p)

        return np.array(p)

    def set(self, p):
        """
        sets p into the d.o.f. of the transport

        p must have length

           ndofs(A) + ndofs(H) + ndofs(b)
        dim * sndof + sndof    + dim

        :return:
        """

        for k in range(self.dim):
            self.A[k,:,:][self.idx] = p[k*self.sndof: (k+1)*self.sndof]
            self.A[k,:,:] = np.tril(self.A[k,:,:]) + np.triu(self.A[k,:,:].T, 1)

            #self.A[k,:,:] = expm(self.A[k,:,:])

        self.H[self.idx] = p[self.dim * self.sndof: (self.dim+1) * self.sndof]
        self.H = np.tril(self.H) + np.triu(self.H.T, 1)

        #self.H = expm(self.H)

        self.b = p[(self.dim+1) * self.sndof :]

        print("quadratic form ")
        print(" A1 \n", np.round(self.A[0, :, :], 4))
        print(" A2 \n", np.round(self.A[1, :, :], 4))
        print("  H \n", np.round(self.H, 4))
        print("  b \n", np.round(self.b, 4))

    def T(self, x):
        """
        Returns T(x)

          0.5 * Axx + Hx + b

        :param x:
        :return:
        """

        Axx = np.array([x.transpose().dot((self.A[k, :, :]).dot(x)) for k in range(self.dim)])
        Hx = self.H.dot(x)

        return ( 0.5*Axx + Hx + self.b).reshape(1, 2)

    def jac(self, x):
        """
        computes  |det jacobian(T)[x]|
        :param x:
        :return:
        """
        J_Ax = np.zeros((self.dim, self.dim))

        for k in range(self.dim):
            J_Ax[k, :] = self.A[k].dot(x)

        res = np.abs(np.linalg.det(J_Ax + self.H))
        #print("jac = ", res)
        return res

    def extract_and_set(self, T):
        """
        @pre T must be a second order transport for dim = 2
        :param T:
        :return:
        """

        x = np.array([0., 0.]).reshape(1, 2)
        T00 = T(x)

        x = np.array([1., 0.]).reshape(1, 2)
        T10 = T(x)

        x = np.array([-1., 0.]).reshape(1, 2)
        Tm10 = T(x)

        x = np.array([0., 1.]).reshape(1, 2)
        T01 = T(x)

        x = np.array([0., -1.]).reshape(1, 2)
        T0m1 = T(x)

        x = np.array([1., 1.]).reshape(1, 2)
        T11 = T(x)

        f = T00
        b = (T10 - Tm10) / 2.
        d = (T01 - T0m1) / 2.

        c = (T01 + T0m1) / 2. - f
        a = (T10 + Tm10) / 2. - f

        e = T11 - (a + b + c + d + f)

        print(a, b, c, d, e, f)
        print(a.shape)

        for k in range(2):
            print("{a:>7}x^2 + {b:>7}x + {c:>7}y^2 +"
                  " {d:>7}y + {e:>7}xy + {f:>7}".format(a=round(a[0, k], 5), b=round(b[0, k], 5),
                                                        c=round(c[0, k], 5), d=round(d[0, k], 5),
                                                        e=round(e[0, k], 5), f=round(f[0, k], 5)))

        self.b[:] = f[:]

        self.H[:, 0] = b[:]
        self.H[:, 1] = d[:]

        for k in range(2):
            self.A[k, 0, 0] = a[0, k]
            self.A[k, 0, 1] = self.A[k, 1, 0] = 0.5 * e[0, k]
            self.A[k, 1, 1] = c[0, k]
            self.A[k, :, :] *= 2.

        print(" A1 \n", np.round(self.A[0, :, :],4))
        print(" A2 \n", np.round(self.A[1, :, :],4))
        print("  H \n", np.round(self.H,4))
        print("  b \n", np.round(self.b,4))

class affine_transport(object):

    def __init__(self, dim):

        self.dim = dim
        # number of degrees of freedom in a dim x dim symmetrix
        # matrix
        self.sndof = int(dim*(dim+1)/2)

        #self.A = np.zeros((self.dim,self.dim,self.dim))
        #self.H = np.zeros((self.dim,self.dim))
        #self.b = np.zeros((self.dim))

        self.H = np.random.randn(self.dim, self.dim)
        self.b = np.random.randn(self.dim)


        #indices of lower triangular matrix access
        self.idx = np.tril_indices(dim, k=0, m=dim)

    def get(self):
        """
        extracts d.o.f. to a single vector
            sndof :=  (dim+1)*dim/2

        with length:
           ndofs(A) + ndofs(H) + ndofs(b)
        dim * sndof + sndof    + dim
        :return:
        """

        p = []
        p += (self.H[self.idx]).tolist()
        p += (self.b[:]).tolist()
        return np.array(p)

    def set(self, p):
        """
        sets p into the d.o.f. of the transport

        p must have length

           ndofs(A) + ndofs(H) + ndofs(b)
        dim * sndof + sndof    + dim

        :return:
        """


        self.H[self.idx] = p[: self.sndof]
        self.H = np.tril(self.H) + np.triu(self.H.T, 1)

        self.b = p[self.sndof :]


    def T(self, x):
        """
        Returns T(x)

          0.5 * Axx + Hx + b

        :param x:
        :return:
        """

        return self.H.dot(x) + self.b

    def jac(self, x):
        """
        computes  |det jacobian(T)[x]|
        :param x:
        :return:
        """

        return np.abs(np.linalg.det(self.H))


def banana(xx):
    # by phillip and modified by robert (scale parameter)

    if isinstance(xx, list):

        x = xx[0]
        y = xx[1]
    else:
        if xx.shape == (1,2):
            x = xx[0,0]
            y = xx[0,1]
        else:
            x = xx[0]
            y = xx[1]

    s = 1.


    # parameters

    a = 1

    b = 1*3 # 1* !

    # normalization constants for fixed parameters

    d = 5.5 / 2

    e = 6 - d

    # rescaled variables

    xt = (2 * x + 1)

    yt = (e * y + d)

    r = (a - xt) ** 2 + b * (yt - xt ** 2) ** 2
    r *= s

    # r is the Rosenbrock function

    return np.exp(-r)


def test2():
    # b = 0.5 * 3
    #[-2.87431661  0.98369122  0.5596401   2.9811599  -1.00599224 -0.53049316
    # -0.02810091 -0.10127464 -0.36436355  0.69296581  0.73570936]

    pass

def  test1():
    # leggauss with deg = 30
    # b = 0.2 * 3

    # CG
    #
    # P = [ 0.07137585  0.06624797  0.07635815 -0.01542108  0.01561595 -0.09043043
    #   0.0176388   0.31902313  0.39910617  0.02200122 -0.51148373]

    # COBLYA :
    # [ 0.06752524  0.06801149  0.07104104 -0.02278282  0.01434201 -0.08838415
    #   0.0169868   0.31886472  0.38652338  0.01512184 -0.52138973]



    # adapt quad 1e-4 auf [-2.5,2.5]′ 2
    #
    #  b = 0.5 * 3
    # CG method
    #
    # [ 0.08638181  0.06250029  0.02643948 -0.01177345  0.00614744 -0.08080108
    #  0.07834151  0.28225141  0.27697595 -0.02007987 -0.55808826]

    #p = [ 0.08638181,  0.06250029,  0.02643948, -0.01177345,  0.00614744, -0.08080108,
    #      0.07834151,  0.28225141,  0.27697595, -0.02007987, -0.55808826]


    # 0.5  with relative maximum optimisation

    #p = [0.08521213,  0.06021724,  0.02600653, -0.01941457,  0.00975642, - 0.07573718,
    # 0.07671436,  0.28273096,  0.27057598, - 0.02125662, - 0.56181764]

    #p =[0.08837726,  0.04537802,  0.04301565, - 0.01470322, - 0.01058483, - 0.07707013,
    #    0.07232551,  0.25728305,  0.27589458, - 0.02005682, - 0.56239111]


    #p = [ 0.05867513,  0.03845016, -0.03089703  ,0.0095847  , 0.04551352, -0.02966106,
    #         0.07806086 , 0.28945834 , 0.27361034, -0.0286752,  -0.5616332 ]

    #p = np.array(p)


    # dual annealing
    #p = [ 0.03375873,  0.03740477, -0.02736914, -0.00708058,  0.05455976,  0.02051612,
    #      0.00132314,  0.31162474,  0.36452723, -0.01826526, -0.53757881]
    #p = np.array(p)


    def f(xx):
        # banana by phillip and modified by robert (scale parameter)

        x = xx[0]
        y = xx[1]

        s = 1.

        # parameters

        a = 1

        b = 0.5 * 3  # 1* !

        # normalization constants for fixed parameters

        d = 5.5 / 2

        e = 6 - d

        # rescaled variables

        xt = (2 * x + 1)

        yt = (e * y + d)

        r = (a - xt) ** 2 + b * (yt - xt ** 2) ** 2
        r *= s

        # r is the Rosenbrock function

        return np.exp(-r)

    dim = 2

    gauss_0 = multivariate_normal(mean=np.array([0]*dim), cov=np.eye(dim,dim))
    f0 = lambda x: gauss_0.pdf(x)

    value, err = dblquad(lambda x, y: f([x, y]), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)
    print("banana Z = ", value)
    print("err = ", err)

    if err > 5e-8:
        print(err)
        raise Exception("Z not good")

    Z_banane = value

    f_inf = lambda x: 1. / Z_banane * f(x)

    qT = quadratic_transport(dim)
    qT.set(p)

    x = [-3, 3]
    y = [-3, 3]  # Define the borders
    deltaX = (max(x) - min(x)) / 10
    deltaY = (max(y) - min(y)) / 10
    xmin = min(x) - deltaX
    xmax = max(x) + deltaX
    ymin = min(y) - deltaY
    ymax = max(y) + deltaY


    N = 400j

    # print(xmin, xmax, ymin, ymax)  # Create meshgrid
    xx, yy = np.mgrid[xmin:xmax:N, ymin:ymax:N]



    def polar(r,t):
        return  r* np.array([np.cos(t), np.sin(t)])

    R = 5.


    def pert_prior(x):
        return f_inf(qT.T(x)) * qT.jac(x)

    if False:

        RN = 8
        rr = np.linspace(0, R, RN)

        SVD_N = 400

        tt = np.linspace(0, 2 * np.pi, SVD_N)

        fig, axes = plt.subplots(1, RN-1, sharey='row')

        for k in range(len(rr)-1):

            r0, r1 = rr[k], rr[k+1]

            rr_local = np.linspace(r0,r1, SVD_N)

            A = np.zeros((len(rr_local), len(tt)))

            for j in range(len(tt)):
                for i in range(len(rr_local)):

                    r, t = rr_local[i], tt[j]
                    x = polar(r, t)

                    A[i,j] = pert_prior(x)

            _, s, _ = np.linalg.svd(A, full_matrices=True)


            #fig, ax = plt.subplots(1, RN-1, k+1)
            axes[k].set_title('['+str(round(r0,5)) + ',' + str(round(r1,5))+']')

            axes[k].semilogy(list(range(30)), s[:30])

            # Turn on the minor TICKS, which are required for the minor GRID
            axes[k].minorticks_on()

            axes[k].grid(b=True, which='minor', color='k', linestyle='--')
            axes[k].grid(b=True, which='major', color='k', linestyle='-')

        # Note that this is the same as
        plt.show()




        plt.figure()
        rr = np.linspace(0, R, 20)
        tt = np.linspace(0,2*np.pi, N)
        ax = plt.subplot(1, 2, 1)
        for r in rr:
            v = []
            for t in tt:
                x = polar(r,t)
                v.append(pert_prior(x))

            ax.plot(tt, v, label = str(round(r,5)))
        ax.set_xlabel(r'$\theta$')
        ax.legend()

        rr = np.linspace(0, R, N)
        tt = np.linspace(0, 2 * np.pi, 25)
        ax = plt.subplot(1, 2, 2)
        for t in tt:
            v = []
            for r in rr:
                x = polar(r, t)
                v.append(pert_prior(x))

            ax.plot(rr, v, label=str(round(t, 5)))

        ax.set_xlabel(r'$r$')
        ax.legend()

        #plt.legend()
        plt.show()


    rr, tt = np.mgrid[0:R:N, 0:2*np.pi:N]

    f0_perturbed_rt = np.zeros(rr.shape)

    for l in range(len(rr)):
        for k in range(len(tt)):
            x = polar(rr[k,l], tt[k,l])

            f0_perturbed_rt[k,l] = pert_prior(x)



    print("xx.shape : ", xx.shape)
    f0_vals = np.zeros(xx.shape)
    ftranps = np.zeros(xx.shape)
    banana_vals = np.zeros(xx.shape)
    for l in range(len(xx)):
        for k in range(len(xx)):
            x = np.array([xx[k, l], yy[k, l]])
            f0_vals[k, l] = f0(x)
            ftranps[k, l] = f_inf(qT.T(x)) * qT.jac(x)
            banana_vals[k, l] = f_inf(x)

    fig = plt.figure(figsize=(20, 20))
    ax = plt.subplot(2, 2, 1)  # plt.axes()#plt.axes(projection='3d')
    surf = ax.pcolormesh(xx, yy, banana_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('posterior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 2)

    surf = ax.pcolormesh(xx, yy, f0_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('Gaussian (target) prior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 3)
    surf = ax.pcolormesh(xx, yy, ftranps, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('perturbed prior in ' + r'$x$' + ' coordinates')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    #ax = plt.subplot(3, 2, 4)
    #surf = ax.pcolormesh(xx, yy, np.abs(f0_vals - ftranps), cmap='coolwarm', edgecolor='none')
    #ax.set_xlabel('x')
    #ax.set_ylabel('y')
    #ax.set_title('abs(diff)')

    #fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 4)  # plt.axes()#plt.axes(projection='3d')
    surf = ax.pcolormesh(rr, tt, f0_perturbed_rt, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel(r'$r$')
    ax.set_ylabel(r'$\theta$')
    ax.set_title('perturbed prior in ' + r'$(r,\theta)$' + ' coordinates')
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()

def yard():
    deg = 20

    quadrule = "leggauss"  # "legendre"

    if quadrule == "hermite":
        x, w = hermgauss(deg)
    elif quadrule == "leggauss":
        x, w = leggauss(deg)  #
        x = 2 * x
        w = 1. / 4 ** 2 * w


    def quad(I):

        res = 0.
        for i in range(len(x)):
            for j in range(len(x)):
                res += w[i] * w[j] * I(x[i], x[j])

        return res



    def J_quad(p):
        qT.set(p)
        value = quad(lambda x, y: np.abs(perturbed_prior(np.array([x,y])) - f0(np.array([x, y])))**2 )
        print(value, "   ", np.round(p, 3))
        return value

    def polar(r,t):
        return  r* np.array([np.cos(t), np.sin(t)])

    rr = np.linspace(0,3, 15)
    tt = np.linspace(0,2*np.pi, 30)

    X = []
    for r in rr:
        for t in tt:
            X.append(polar(r,t))
    def J_polarpoints(p):
        qT.set(p)
        value = sum( np.log(1./Z * perturbed_prior(x)/ f0(x))   for x in X)
        print(value, "   ", np.round(p,3))
        return value



class Banana_perturbed_prior(object):
    """
    Represents the perturbed prior defined
    via the convexcombination of transports T1 and T2

        T = t * T1 + (1-t) * T2

    where T2 is a affine transport (computed internally)
    and T1 is the exact quadratic transport for the underlying banana distribution.

    With that t controls a type of perturbation of the exact transport.
    """

    def __init__(self, t_convex):
        a = 1.  # 2.5#1.
        b = 1.
        mu = np.zeros(2)
        sigma2 = np.array([[1., 0.9], [0.9, 1.]])
        pi = DIST.BananaDistribution(a, b, mu, sigma2)

        self.post = pi.pdf
        rho = DIST.StandardNormalDistribution(2)

        # Construct the quadractic exact transport
        lin_tm = MAPS.LinearTransportMap(mu, npla.cholesky(sigma2))
        ban_tm = MAPS.FrozenBananaMap(a, b)
        Tstar = MAPS.CompositeTransportMap(ban_tm, lin_tm)
        myTT_star = quadratic_transport(2)
        myTT_star.extract_and_set(Tstar)
        self.trafo = myTT_star
        # Compute a affine transport
        order = 1
        T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(2, order, 'full')

        push_rho = DIST.PushForwardTransportMapDistribution(T, rho)

        qtype = 3  # Gauss quadrature
        qparams = [10] * 2  # Quadrature order
        reg = None  # No regularization
        tol = 1e-5  # Optimization tolerance
        ders = 2  # Use gradient and Hessian
        log = push_rho.minimize_kl_divergence(
            pi, qtype=qtype, qparams=qparams, regularization=reg,
            tol=tol, ders=ders)
        T_affine = quadratic_transport(2)
        T_affine.extract_and_set(T)

        convex_T = convex_comb_transport(myTT_star, T_affine, t=t_convex)


        self._convex_T = convex_T
        @count_calls
        def pert_prior(z):
            Tx = convex_T.T(z)
            res = pi.pdf(Tx) * convex_T.jac(z)
            return res


        self.pprior = pert_prior

    def __call__(self,z):
        return self.pprior(z)


    def compute_posterior_moments(self, order):


        def quad(i,j):
            """
            computs  x^i y^j posterior(x,y) d(x,y)
            :param i:
            :param j:
            :return:
            """
            value, err = dblquad(lambda x, y: (x**i) * (y**j) *self.post(np.array([x, y]).reshape(1, 2)), -np.inf, np.inf,
                                 lambda x: -np.inf,
                                 lambda x: np.inf)

            print(value, err)
            #if err > 1e-6:
            #    print(err)
            #    raise Exception("Moment ({i},{j}) not accurate computed via quadrature".format(i=i,j=j))

            return value




        if order == 0:
            # compute Z
            Z = 2.7175467088793495 # err : 9.651452348435896e-08
            return Z #quad(0,0)

        if order == 1 :
            # compute mean
            mean = np.zeros(2)

            #mean[0] = quad(1,0)
            #mean[1] = quad(0,1)

            mean[0] = 0.00226580745561404   # err : 4.580669448803528e-06
            mean[1] = -5.426027310791309    # err :  2.5298246729728148e-05
            return mean

        if order == 2 :
            second_moments = np.zeros((2,2))

            second_moments[0,0] = 2.7102468600431107 #err  8.519905743378977e-06    quad(2,0)
            second_moments[1,1] = 18.87469671728102  # err 0.00042904265709964307   quad(0,2)
            second_moments[0,1] = second_moments[1,0] = 2.4135811937454212  # err 0.0002821701128429852 quad(1,1)

            return second_moments






def test_banana_perturbed_prior():
    print("start test_banana perturbed prior")

    pert_prior = Banana_perturbed_prior(0.5)

    print(pert_prior.compute_posterior_moments(2))

    exit()

    maxR = 8.

    K = 5
    ss = maxR / K #0.25
    fig, ax = plt.subplots(2,K, sharey = 'row')#figure()

    vmax = 1.

    plt.xticks(rotation=90)
    for k in range(K):

        ndiscr = 150
        r = np.linspace(ss*k, ss*(k+1), ndiscr)  # np.linspace(-2,2,ndiscr) #
        t = np.linspace(0, 2 * np.pi, ndiscr)  # np.linspace(-2,2, ndiscr) #
        rr, tt = np.meshgrid(r, t)

        def polar(r, t):
            return r * np.array([np.cos(t), np.sin(t)])

        vals_polar = []

        for t_val in t:
            for r_val in r:
                z = polar(r_val, t_val)

                vals_polar.append(pert_prior(z))

        vals_polar = np.array(vals_polar)

        vals_polar = vals_polar.reshape(rr.shape)


        for tick in ax[0,k].get_xticklabels():
            tick.set_rotation(90)

        ax[0, k].set_xticks([ss * k, ss * (k + 1)], minor=False)
        if k == 0 :
            ax[0, k].set_xticklabels([str(round(ss * (k ), 2))] + [str(round(ss * (k + 1), 2))])
        if k > 0 :

            ax[0,k].set_xticklabels( [''] +[str(round(ss*(k+1),2))])


        if k == 0: vmax = np.max(vals_polar)

        fig.savefig("banana_test.png")


        #plt.subplot(2, K, 1 + k)


        for tick in ax[0,k].get_xticklabels():
            print(tick)
        print("=====")

        surf =  ax[0,k].pcolormesh(rr, tt, vals_polar, cmap='coolwarm', edgecolor='none', vmin=0., vmax=vmax)
        #fig.colorbar(surf, shrink=0.5, aspect=5)



        _,s,_ = npla.svd(vals_polar)
        #plt.subplot(2, K,  1 + k + K)
        ax[1, k].plot(np.log(s[:8]))
        ax[1, k].grid(b=True, which='major', color='k', linestyle='-')
        ax[1, k].grid(b=True, which='minor', color='r', linestyle='--')
        ax[1, k].minorticks_on()



    plt.show()



def linear_to_quadratic_marzuk():
    a = 1.  # 2.5#1.
    b = 1.
    mu = np.zeros(2)
    sigma2 = np.array([[1., 0.9], [0.9, 1.]])
    pi = DIST.BananaDistribution(a, b, mu, sigma2)

    rho = DIST.StandardNormalDistribution(2)

    ndiscr = 200
    x = np.linspace(-5, 5, ndiscr)  # np.linspace(-2,2,ndiscr) #
    y = np.linspace(-10, 5, ndiscr)  # np.linspace(-2,2, ndiscr) #
    xx, yy = np.meshgrid(x, y)
    X2d = np.vstack((xx.flatten(), yy.flatten())).T

    # print(X2d.shape)
    pdf2d = pi.pdf(X2d).reshape(xx.shape)
    if False:
        plt.figure()
        # plt.contour(xx, yy, pdf2d, levels=levels_pdf2d);
        plt.pcolormesh(xx, yy, pdf2d, cmap='coolwarm', edgecolor='none')
        plt.show()
        exit()

    lin_tm = MAPS.LinearTransportMap(mu, npla.cholesky(sigma2))
    ban_tm = MAPS.FrozenBananaMap(a, b)
    Tstar = MAPS.CompositeTransportMap(ban_tm, lin_tm)

    myTT_star = quadratic_transport(2)
    myTT_star.extract_and_set(Tstar)


    x1 = np.linspace(-7, 7, ndiscr)
    X1d = np.vstack((x1, np.zeros(ndiscr))).T

    order = 1
    T = TM.Default_IsotropicIntegratedSquaredTriangularTransportMap(2, order, 'full')

    push_rho = DIST.PushForwardTransportMapDistribution(T, rho)

    print("start optimization")
    qtype = 3  # Gauss quadrature
    qparams = [10] * 2  # Quadrature order
    reg = None  # No regularization
    tol = 1e-5  # Optimization tolerance
    ders = 2  # Use gradient and Hessian
    log = push_rho.minimize_kl_divergence(
        pi, qtype=qtype, qparams=qparams, regularization=reg,
        tol=tol, ders=ders)


    T_affine = quadratic_transport(2)
    T_affine.extract_and_set(T)

    #myTT = quadratic_transport(2)
    #myTT.extract_and_set(T)


    convex_params = [0.,0.25, 0.5, 0.75, 1.]
    fig = plt.figure()

    for k, t_convex in enumerate(convex_params):

        convex_T = convex_comb_transport(myTT_star,T_affine, t = t_convex)

        target = rho.pdf(X2d).reshape(xx.shape)

        def pert_prior(z):
            Tx = convex_T.T(z)
            #print("Tx ", Tx)
            res = pi.pdf(Tx) * convex_T.jac(z)
            #print("perturbed(0,0) ", res)
            return res


        ax = plt.subplot(len(convex_params), 4, 1 + k*4)
        if k == 0: plt.title("posterior banana")

        Z = pert_prior(np.array([0., 0.])) / rho.pdf(np.array([0., 0.]).reshape(1, 2))

        print("Z = ", Z)

        # exit()


        surf = plt.pcolormesh(xx, yy, 1. / Z * pdf2d, cmap='coolwarm', edgecolor='none')
        fig.colorbar(surf, shrink=0.5, aspect=5)

        ax = plt.subplot(len(convex_params), 4, 2+ k*4)
        if k == 0: plt.title("gaussian target prior")
        surf = plt.pcolormesh(xx, yy, target, cmap='coolwarm', edgecolor='none')
        fig.colorbar(surf, shrink=0.5, aspect=5)

        vals = []
        for yy1 in y:
            for xx1 in x:
                z = np.array([xx1, yy1])  # .reshape(1, 2)
                vals.append(pert_prior(z))

        # print(len(vals))
        # print(xx.shape)
        vals = np.array(vals)
        # print(vals.shape)

        vals = vals.reshape(xx.shape)

        ax = plt.subplot(len(convex_params), 4, 3+ k*4)
        if k == 0: plt.title("pertubated prior")
        surf = plt.pcolormesh(xx, yy, 1. / Z * vals, cmap='coolwarm', edgecolor='none')
        fig.colorbar(surf, shrink=0.5, aspect=5)

        ax = plt.subplot(len(convex_params), 4, 4+ k*4)
        r = np.linspace(0, 8, ndiscr)  # np.linspace(-2,2,ndiscr) #
        t = np.linspace(0, 2*np.pi, ndiscr)  # np.linspace(-2,2, ndiscr) #
        rr, tt = np.meshgrid(r, t)

        def polar(r,t):
            return r* np.array([np.cos(t),np.sin(t)])

        vals_polar = []

        for t_val in t:
            for r_val in r:
                z = polar(r_val,t_val)

                vals_polar.append(pert_prior(z))

        vals_polar = np.array(vals_polar)
        vals_polar = vals_polar.reshape(rr.shape)

        if k == 0:plt.title("polar coordinate view")
        surf = plt.pcolormesh(rr, tt,vals_polar, cmap='coolwarm', edgecolor='none')
        fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()



def test_marzuk_new():

    a = 1.  # 2.5#1.
    b = 1.
    mu = np.zeros(2)
    sigma2 = np.array([[1., 0.9], [0.9, 1.]])  # np.array([[1., 0.9],[0.9, 1.]])
    # sigma2 = np.array([[1.,0.],[0.,1.]])
    pi = DIST.BananaDistribution(a, b, mu, sigma2)

    rho = DIST.StandardNormalDistribution(2)

    dim = 2

    gauss_0 = multivariate_normal(mean=np.array([0] * dim), cov=np.eye(dim, dim))
    f0 = lambda x: rho.pdf(x)

    #foo = pi.pdf
    foo = banana

    compute_Z = True
    if compute_Z :

        value, err = dblquad(lambda x, y: foo(np.array([x, y]).reshape(1,2)), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)

        #banana
        print("banana Z = ", value)
        print("err = ", err)

        if err > 1e-6:
            print(err)
            raise Exception("Z not good")

        Z = value
    else:
        Z = 2.7175467088793495



    f = lambda x: 1./Z * foo(x)  # non scaled "density"

    N = 20
    rr = np.linspace(0, 1, 8)
    tt = np.linspace(0, 2 * np.pi, 20)
    X = []
    def polar(r, t):
        return r * np.array([np.cos(t), np.sin(t)])
    for r in rr:
        for t in tt:
            X.append(polar(r, t))
    X = np.random.randn(N,2)
    tri_qT = triangle_quadratic_transport(dim)

    def perturbed_prior(x):

        res = f(tri_qT.T(x)) * np.abs(tri_qT.jac(x))
        #tx = tri_qT.T(x)
        # print("tri tx : ", tx)
        #print("res = ", res)
        return res

    def tri_J_gauss(p):
        tri_qT.set(p)
        #value = sum(np.abs(1 - np.log(perturbed_prior(x)) / np.log(f0(x))) ** 2 for x in X)
        value = 1./len(X)*sum(np.abs(np.log(perturbed_prior(x)/f0(x))) ** 2 for x in X)
        print(value, "   ", np.round(p, 3))
        return value

    # p0 = qT.get()
    # p0 = [0,0,0,-2, 0,0,1,0,1,0,-1]

    # tri_p0 = [ 0., -2, 0., 0., 1., 0., 1., 0., -1]

    tri_p0 = [0., -2., 0., 0., 1, 0., 0.9, 0.43589, 0, -1]
    tri_p0 = np.array(tri_p0)
    tri_J_gauss(tri_p0)

    #exit()
    # tri_qT.set(tri_p0)


    # exit()
    # p0 = np.array(tri_p0)

    p0 = [0., 0., 0., 0., 1., 0., 0., 1., 0, 0.]
    p0 = np.array(p0)
    p0 = np.random.randn(len(p0))

    #p0 = [-2.28078808e-04,  3.45709555e-02, -9.76637838e-02, 2.73039362e-01,
    #      -1.19167245e-01,  3.32913009e-01, -2.64885657e-01, 3.67327285e-01,
    #      -2.32773154e-04, -5.38845627e-01]
    #p0 = np.array(p0)


    # b = 1:
    p0 =  [0. ,    0.222,    0.138,    0.086,    0.299,    0.188,    0.437,    0.125,    0.002, - 0.536]

    # best fit for b  = 1
    #p =
    #[5.93482910e-07  2.21171781e-01  1.38326528e-01  8.65231717e-02
    # 2.99751349e-01  1.87480414e-01  4.35542579e-01  1.24251001e-01
    # 5.24073009e-06 - 5.38450855e-01

    p0 = [5.93482910e-07, 2.21171781e-01,  1.38326528e-01,  8.65231717e-02, 2.99751349e-01,
         1.87480414e-01 , 4.35542579e-01 , 1.24251001e-01, 5.24073009e-06,  - 5.38450855e-01]

    #[0.004, 0.007, 0.024, 0.308, -0.026, -0.353, 0.093, -0.438, -0.003, -0.542]

    optimize = False
    if optimize:
        bounds = [(-1., 1.) for _ in range(len(p0))]
        minimizer_kwargs = {"method": "CG"}
        #res = basinhopping(tri_J_gauss, p0, minimizer_kwargs=minimizer_kwargs, niter = 20)

        #res = dual_annealing(tri_J_gauss, bounds=bounds, seed=1234, maxiter=200)

        #res = shgo(tri_J_gauss,bounds)

        #res = differential_evolution(tri_J_gauss,bounds)
        res = minimize(tri_J_gauss, p0, method='CG')  # ''COBYLA') #'COBYLA')  CG
        p = res.x
        # p = tri_p0
        print("-------------------------------")
        print("p = \n", p)
        print("-------------------------------")

        tri_qT.set(p)

    else:
        tri_qT.set(p0)

        #tri_qT.set(tri_p0)


    # exit()

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    x = [-2.5, 2.5]
    y = [-2.5, 2.5]  # Define the borders
    deltaX = (max(x) - min(x)) / 10
    deltaY = (max(y) - min(y)) / 10
    xmin = min(x) - deltaX
    xmax = max(x) + deltaX
    ymin = min(y) - deltaY
    ymax = max(y) + deltaY
    # print(xmin, xmax, ymin, ymax)  # Create meshgrid
    xx, yy = np.mgrid[xmin:xmax:800j, ymin:ymax:800j]

    print("xx.shape : ", xx.shape)
    f0_vals = np.zeros(xx.shape)
    ftranps = np.zeros(xx.shape)
    banana_vals = np.zeros(xx.shape)
    for l in range(len(xx)):
        for k in range(len(xx)):
            x = np.array([xx[k, l], yy[k, l]])
            f0_vals[k, l] = f0(x)
            ftranps[k, l] = perturbed_prior(x)

            banana_vals[k, l] = f(x.reshape(1, 2))

    fig = plt.figure(figsize=(20, 20))
    ax = plt.subplot(2, 2, 1)  # plt.axes()#plt.axes(projection='3d')
    surf = ax.pcolormesh(xx, yy, banana_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('posterior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 2)
    surf = ax.pcolormesh(xx, yy, f0_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('Correct prior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 3)
    surf = ax.pcolormesh(xx, yy, ftranps, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('approximated perturbed prior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    # fig3 = plt.figure(figsize=(13, 7))
    ax = plt.subplot(2, 2, 4)
    surf = ax.pcolormesh(xx, yy, np.abs(f0_vals - ftranps), cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('abs(diff)')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


def test_marzuk():
    import TransportMaps.Distributions as DIST


    a = 1.  # 2.5#1.
    b = 1.
    mu = np.zeros(2)
    sigma2 = np.array([[1., 0.9], [0.9, 1.]])  # np.array([[1., 0.9],[0.9, 1.]])
    #sigma2 = np.array([[1.,0.],[0.,1.]])
    pi = DIST.BananaDistribution(a, b, mu, sigma2)

    rho = DIST.StandardNormalDistribution(2)

    dim = 2

    gauss_0 = multivariate_normal(mean=np.array([0] * dim), cov=np.eye(dim, dim))
    f0 = lambda x: rho.pdf(x)

    if False:
        value, err = dblquad(lambda x, y: pi.pdf(np.array([x, y]).reshape(1,2)), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)
        print("banana Z = ", value)
        print("err = ", err)

        if err > 1e-6:
            print(err)
            raise Exception("Z not good")



        Z_banane = value

    Z_banane = 2.718279119654171

    f = lambda x:  pi.pdf(x)  # non scaled "density"

    qT = quadratic_transport(dim)
    p0 = [0, 0, 0, -2, 0, 0, 1, 0, 1, 0, -1]

    p0 = [ 0.,0.,0., -2., 0., 0., 1, 0.9,  0.43589, 0, -1]
    p0 = np.array(p0)
    qT.set(p0)


    exit()



    def perturbed_prior(x):
        #res = f(qT.T(x)) * qT.jac(x)
        tx = qT.T(x)

        print(" tx = ", tx)
        return f(qT.T(x)) * qT.jac(x)


    Z = perturbed_prior(np.array([0,0.])) / f0(np.array([0.,0.]).reshape(1,2))

    print(Z)


    N = 100
    rr = np.linspace(0, 2, 5)
    tt = np.linspace(0, 2 * np.pi, 20)

    X = []

    def polar(r, t):
        return r * np.array([np.cos(t), np.sin(t)])
    for r in rr:
        for t in tt:
            X.append(polar(r, t))
    #X = np.random.randn(N,2)


    tri_qT = triangle_quadratic_transport(dim)

    def J_gauss(p):
        qT.set(p)
        #exit()
        print("test" , sum([np.log(1./Z *perturbed_prior(x)/ f0(x)) for x in X]))

        value = sum(np.abs(1-1./Z * perturbed_prior(x)/ f0(x))**2  for x in X)
        #print("value ", value)
        print(value, "   ", np.round(p, 3))
        return value
    #exit()



    def tri_perturbed_prior(x):

        res = f(tri_qT.T(x)) * tri_qT.jac(x)
        tx = tri_qT.T(x)
        #print("tri tx : ", tx)
        return res

    def tri_J_gauss(p):
        tri_qT.set(p)
        #exit()
        #print("test" , sum([np.log(1./Z *tri_perturbed_prior(x)/ f0(x)) for x in X]))

        value = sum(np.abs(1-1./Z * tri_perturbed_prior(x)/ f0(x))**2  for x in X)
        #print("value ", value)
        print(value, "   ", np.round(p, 3))
        return value


    #p0 = qT.get()
    #p0 = [0,0,0,-2, 0,0,1,0,1,0,-1]

    #tri_p0 = [ 0., -2, 0., 0., 1., 0., 1., 0., -1]

    tri_p0 = [ 0., -2., 0., 0., 1, 0.9,  0.43589, 0, -1]
    tri_p0 = np.array(tri_p0)
    tri_J_gauss(tri_p0)

    exit()
    #tri_qT.set(tri_p0)


    #exit()
    #p0 = np.array(tri_p0)

    p0 = [0., 0.,0.,0., 1., 0., 1., 0, 0.]
    p0 = np.array(p0)

    #J_gauss(p0)

    #tri_J_gauss(tri_p0)


    #exit()
    #p0 = np.random.randn(len(p0))
    optimize = True
    if optimize:
        bounds = [(-5, 5) for i in range(len(p0))]
        minimizer_kwargs = {"method": "CG"}
        #res = basinhopping(tri_J_gauss, p0, minimizer_kwargs=minimizer_kwargs, niter = 20)

        res = dual_annealing(tri_J_gauss,bounds = bounds, seed = 1234, maxiter = 50)


        #res = differential_evolution(tri_J_gauss,bounds)
        #res = minimize(tri_J_gauss, p0, method='COBYLA')  # ''COBYLA') #'COBYLA')  CG
        p = res.x
        #p = tri_p0
        print("-------------------------------")
        print("p = \n", p)
        print("-------------------------------")

        tri_qT.set(p)


    def perturbed_prior(x):
        return 1./Z * f(tri_qT.T(x)) * tri_qT.jac(x)
    #exit()

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    x = [-5, 5]
    y = [-5, 5]  # Define the borders
    deltaX = (max(x) - min(x)) / 10
    deltaY = (max(y) - min(y)) / 10
    xmin = min(x) - deltaX
    xmax = max(x) + deltaX
    ymin = min(y) - deltaY
    ymax = max(y) + deltaY
    # print(xmin, xmax, ymin, ymax)  # Create meshgrid
    xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]

    print("xx.shape : ", xx.shape)
    f0_vals = np.zeros(xx.shape)
    ftranps = np.zeros(xx.shape)
    banana_vals = np.zeros(xx.shape)
    for l in range(len(xx)):
        for k in range(len(xx)):
            x = np.array([xx[k, l], yy[k, l]])
            f0_vals[k, l] = f0(x)
            ftranps[k, l] = perturbed_prior(x)

            banana_vals[k, l] = f(x.reshape(1,2))

    fig = plt.figure(figsize=(20, 20))
    ax = plt.subplot(2, 2, 1)  # plt.axes()#plt.axes(projection='3d')
    surf = ax.pcolormesh(xx, yy, banana_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('posterior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 2)
    surf = ax.pcolormesh(xx, yy, f0_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('Correct prior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    ax = plt.subplot(2, 2, 3)
    surf = ax.pcolormesh(xx, yy, ftranps, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('approximated perturbed prior')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    # fig3 = plt.figure(figsize=(13, 7))
    ax = plt.subplot(2, 2, 4)
    surf = ax.pcolormesh(xx, yy, np.abs(f0_vals - ftranps), cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_title('abs(diff)')
    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()


def test_transport_gauss():
    dim = 2

    gauss_0 = multivariate_normal(mean=np.array([0] * dim), cov=np.eye(dim, dim))
    f0 = lambda x: gauss_0.pdf(x)

    #gauss = multivariate_normal(mean=[0, 0], cov=[[0.1, 0], [0, 1]])

    value, err = dblquad(lambda x,y : banana([x,y]), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)
    print("banana Z = ", value)
    print("err = ", err)

    if err > 1e-6 :
        print(err)
        raise Exception("Z not good")

    Z_banane = value



    f = lambda x : 1./Z_banane * banana(x)#gauss.pdf(x)

    qT = quadratic_transport(dim)

    #qT = affine_transport(dim)


    deg = 30

    quadrule = "hermite" # "legendre"

    if quadrule == "hermite":
        x,w = hermgauss(deg)
    elif quadrule == "leggauss":
        x,w = leggauss(deg) #
        x = 2.5*x
        w = 1./2.5**2 * w

    def quad(I):

        res = 0.
        for i in range(len(x)):
            for j in range(len(x)):

                res += w[i]*w[j]*I(x[i],x[j])

        return res

    def perturbed_prior(x):
        return f(qT.T(x))*qT.jac(x)


    def J_quad(p):
        qT.set(p)

        #value, err = dblquad(lambda x, y: np.abs(perturbed_prior(np.array([x,y])) - f0(np.array([x, y])))**2, -2.5, 2.5, lambda x: -2.5, lambda x: 2.5, epsrel = 1e-4, epsabs = 1e-4)
        #print("err :", value)

        value = quad(lambda x, y: np.abs(perturbed_prior(np.array([x,y])) - f0(np.array([x, y])))**2 )

        print("")
        print(value)

        return value


    def polar(r,t):
        return  r* np.array([np.cos(t), np.sin(t)])

    rr = np.linspace(0,3, 20)
    tt = np.linspace(0,2*np.pi, 40)

    X = []

    for r in rr:
        for t in tt:
            X.append(polar(r,t))


    def J_polarpoints(p):

        qT.set(p)

        #value = sum( np.abs(f0(x) - perturbed_prior(x))**2  for x in X)


        value = sum( np.abs( perturbed_prior(x) - f0(x) )**2  for x in X)

        print(value, "   ", np.round(p,3))

        return value



    p0 = qT.get()

    #p0 = np.random.randn(len(p0))

    # 0.2
    #p = [ 0.03375873,  0.03740477, -0.02736914, -0.00708058,  0.05455976,  0.02051612,
    #      0.00132314,  0.31162474,  0.36452723, -0.01826526, -0.53757881]
    #p0 = np.array(p)

    # 0.5
    p = [0.08638181, 0.06250029, 0.02643948, -0.01177345, 0.00614744, -0.08080108,
         0.07834151, 0.28225141, 0.27697595, -0.02007987, -0.55808826]
    #p0 = np.array(p)

    #p0 = [0.07137585,  0.06624797,  0.07635815, -0.01542108,  0.01561595, -0.09043043,
    #      0.0176388,   0.31902313,  0.39910617,  0.02200122, -0.51148373]
    #p0 = np.array(p0)


    #p0 = [0.03375873, 0.03740477, -0.02736914, -0.00708058, 0.05455976, 0.02051612,
    #     0.00132314, 0.31162474, 0.36452723, -0.01826526, -0.53757881]
    #p0 = np.array(p0)

    #p0[3*dim:] = np.array([-0.05187077,  0.33910262,  0.48153182, -0.00301009, -0.50669664])
    #print("p0 :\n", p0)

    bounds = [(-10, 10) for i in range(len(p0))]
    minimizer_kwargs = {"method": "CG"}
    #res = basinhopping(J_polarpoints, p0, minimizer_kwargs=minimizer_kwargs, niter = 20)




    #res = differential_evolution(J_polarpoints,bounds)
    #res = shgo(J_polarpoints, bounds,n=60, iters=20, sampling_method='sobol')

    #res = dual_annealing(J_polarpoints, bounds)

    print("================================")

    #print("res candidates : \n", res.xl)

    #print("res.results : \n ", res.funl)
    #res = dual_annealing(J_quad,bounds = bounds, seed = 1234)



    res = minimize(J_quad, p0, method='SLSQP') #''SLSQP') #'COBYLA')  CG
    p = res.x

    print("-------------------------------")
    print("p = \n", p)
    print("-------------------------------")


    qT.set(p)

    #print("A :")
    #print(qT.A)
    print("H :")
    print(qT.H)
    print("b :")
    print(qT.b)

    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    x = [-2,2]
    y = [-2,2]  # Define the borders
    deltaX = (max(x) - min(x)) / 10
    deltaY = (max(y) - min(y)) / 10
    xmin = min(x) - deltaX
    xmax = max(x) + deltaX
    ymin = min(y) - deltaY
    ymax = max(y) + deltaY
    #print(xmin, xmax, ymin, ymax)  # Create meshgrid
    xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]

    print("xx.shape : ", xx.shape)
    f0_vals = np.zeros(xx.shape)
    ftranps = np.zeros(xx.shape)
    banana_vals = np.zeros(xx.shape)
    for l in range(len(xx)):
        for k in range(len(xx)):
            x = np.array([xx[k,l],yy[k,l]])
            f0_vals[k,l] = f0(x)
            ftranps[k, l] = f(qT.T(x))*qT.jac(x)

            banana_vals[k,l] = 1./Z_banane * banana(x)

    fig = plt.figure(figsize=(20,20))
    ax = plt.subplot(2,2,1) #plt.axes()#plt.axes(projection='3d')
    surf = ax.pcolormesh(xx, yy, banana_vals, cmap='coolwarm', edgecolor='none')
    #surf = ax.pcolormesh(xx, yy, banana_vals, rstride=1, cstride=1, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    #ax.set_zlabel('PDF')
    ax.set_title('posterior')
      # add color bar indicating the PDF
    #ax.view_init(60, 35)
    fig.colorbar(surf, shrink=0.5, aspect=5)



    #fig = plt.figure(figsize=(20, 20))
    ax = plt.subplot(2, 2, 2)
    #ax = plt.axes()#plt.axes(projection='3d')
    #surf = ax.pcolormesh(xx, yy, f0_vals, rstride=1, cstride=1, cmap='coolwarm', edgecolor='none')
    surf = ax.pcolormesh(xx, yy, f0_vals, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    #ax.set_zlabel('PDF')
    ax.set_title('Correct prior')
    #fig.colorbar(surf, shrink=0.5, aspect=5)  # add color bar indicating the PDF
    #ax.view_init(60, 35)
    fig.colorbar(surf, shrink=0.5, aspect=5)

    #fig2 = plt.figure(figsize=(13, 7))
    ax = plt.subplot(2, 2, 3)
    #ax = plt.axes()#projection='3d')
    #surf = ax.pcolormesh(xx, yy, ftranps, rstride=1, cstride=1, cmap='coolwarm', edgecolor='none')
    surf = ax.pcolormesh(xx, yy, ftranps, cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    #ax.set_zlabel('PDF')
    ax.set_title('approximated perturbed prior')
    #fig2.colorbar(surf, shrink=0.5, aspect=5)  # add color bar indicating the PDF
    #ax.view_init(60, 35)
    fig.colorbar(surf, shrink=0.5, aspect=5)

    #fig3 = plt.figure(figsize=(13, 7))
    ax = plt.subplot(2, 2, 4)
    #ax = plt.axes()# plt.axes(projection='3d')
    #surf = ax.pcolormesh(xx, yy, np.abs(f0_vals-ftranps), rstride=1, cstride=1, cmap='coolwarm', edgecolor='none')
    surf = ax.pcolormesh(xx, yy, np.abs(f0_vals - ftranps), cmap='coolwarm', edgecolor='none')
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    #ax.set_zlabel('PDF')
    ax.set_title('abs(diff)')
    #fig3.colorbar(surf, shrink=0.5, aspect=5)  # add color bar indicating the PDF
    #ax.view_init(60, 35)

    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()


def standard_gauss(dim):

    f = lambda x: 1./np.sqrt((2*np.pi)**dim)*np.exp(- x.dot(x))
    return f





def main():
    test_banana_perturbed_prior()


    exit()

    linear_to_quadratic_marzuk()

    exit()

    #test_marzuk_new()

    exit()
    test_marzuk()

    exit()
    #TM.tests.run_all()


    #exit()
    #test1()
    #exit()

    test_transport_gauss()


    exit()

    dim = 3

    transport = quadratic_transport(dim)

    p = transport.get()

    p += 1

    transport.set(p)

    print(transport.A)
    print(transport.H)
    print(transport.b)


    x = np.random.rand(3)

    print(transport.T(x))
    print(transport.jac(x))


    exit()

if __name__ == "__main__":
    #pass
    main()