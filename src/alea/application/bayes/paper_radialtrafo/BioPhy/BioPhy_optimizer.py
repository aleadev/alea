from __future__ import division, print_function
import numpy as np
from scipy.optimize import basinhopping, differential_evolution, dual_annealing, minimize, least_squares
from alea.application.bayes.paper_radialtrafo.BioPhy.BioPhy_model import LossModel
from functools import partial


class BioPhyOptimizer(object):
    def __init__(self,
                 x,                                 # type: list
                 y,                                 # type: list
                 model,                             # type: LossModel
                 noise=None,                        # type: None or list
                 *args,
                 **kwargs
                 ):
        self.x = x
        self.y = y
        self.model = model
        self.noise = noise
        self.p = None
        self.loss_dict = {
            "qr": self.x,
            "inten": self.y,
            "err": self.noise
        }
        if "p" in kwargs:
            self.loss_dict["p"] = kwargs["p"]
            self.p = kwargs["p"]
        self.optimizer = None

    def prepare_optimizer(self,
                          func,                     # type: callable
                          bnds,                     # type: list[tuple]
                          n_iter=100,               # type: int
                          tolerance=1e-6,           # type: float
                          verbose=False,            # type: bool
                          *solver_args,
                          **solver_kwargs
                          ):
        pass

    def run(self):
        pass


class BashinHopping(BioPhyOptimizer):
    def __init__(self,
                 x,                                 # type: list
                 y,                                 # type: list
                 model,                             # type: LossModel
                 noise=None,                        # type: None or list
                 *args,
                 **kwargs
                 ):
        super(BashinHopping, self).__init__(x, y, model, noise)
        self.iv = None

    def prepare_optimizer(self,
                          bnds=None,                # type: list[tuple] or None
                          n_iter=100,               # type: int
                          tolerance=1e-6,           # type: float
                          verbose=False,            # type: bool
                          *solver_args,
                          **solver_kwargs
                          ):

        if "method" in solver_kwargs:
            method = solver_kwargs["method"]
            solver_kwargs.pop("method")
        else:
            method = "BFGS"

        if "iv" in solver_kwargs:
            self.iv = solver_kwargs["iv"]
            solver_kwargs.pop("iv")
        else:
            self.iv = np.ones(self.model.params)
        if bnds is not None:
            class MyBounds(object):
                def __init__(self, xmax, xmin):
                    self.xmax = np.array(xmax)
                    self.xmin = np.array(xmin)

                def __call__(self, **kwargs):
                    x = kwargs["x_new"]
                    tmax = bool(np.all(x <= self.xmax))
                    tmin = bool(np.all(x >= self.xmin))
                    return tmax and tmin
            mybounds = MyBounds([b[0] for b in bnds], [b[1] for b in bnds])
        else:
            mybounds = None

        self.optimizer = partial(basinhopping,
                                 niter=n_iter,
                                 minimizer_kwargs={"args": self.loss_dict, "method": method},
                                 accept_test=mybounds,
                                 disp=verbose
                                 )

    def run(self):
        return self.optimizer(func=self.model.loss_functional,
                              x0=self.iv)


class DualAnnealing(BioPhyOptimizer):
    def __init__(self,
                 x,                                 # type: list
                 y,                                 # type: list
                 model,                             # type: LossModel
                 noise=None,                        # type: None or list
                 *args,
                 **kwargs
                 ):
        super(DualAnnealing, self).__init__(x, y, model, noise)
        self.bnds = None
        self.solver_kwargs = None

    def prepare_optimizer(self,
                          bnds=None,                # type: list[tuple] or None
                          n_iter=100,               # type: int
                          tolerance=1e-6,           # type: float
                          verbose=False,            # type: bool
                          *solver_args,
                          **solver_kwargs
                          ):
        assert bnds is not None
        self.bnds = bnds

        if solver_kwargs is not None:
            self.solver_kwargs = solver_kwargs
        self.optimizer = partial(dual_annealing,
                                 maxiter=n_iter,
                                 args=(self.x, self.y, self.noise, self.p)
                                 )

    def run(self):
        return self.optimizer(func=self.model.loss_functional_explicit,
                              bounds=self.bnds,
                              **self.solver_kwargs)


class DifferentialEvolution(BioPhyOptimizer):
    def __init__(self,
                 x,                                 # type: list
                 y,                                 # type: list
                 model,                             # type: LossModel
                 noise=None,                        # type: None or list
                 *args,
                 **kwargs
                 ):
        super(DifferentialEvolution, self).__init__(x, y, model, noise)
        self.solver_kwargs = dict()

    def prepare_optimizer(self,
                          bnds=None,                # type: list[tuple] or None
                          n_iter=100,               # type: int
                          tolerance=1e-6,           # type: float
                          verbose=False,            # type: bool
                          *solver_args,
                          **solver_kwargs
                          ):
        # res = differential_evolution(curr_model.loss_functional_explicit, bnds,
        #                              args=(loss_dict["qr"], loss_dict["inten"], loss_dict["err"], loss_dict["p"]),
        #                              strategy="randtobest1bin",
        #                              maxiter=10000, tol=1e-6, disp=True)
        if solver_kwargs is not None:
            self.solver_kwargs = solver_kwargs
        self.optimizer = partial(differential_evolution,
                                 bounds=bnds,
                                 maxiter=n_iter,
                                 args=(self.x, self.y, self.noise, self.p),
                                 disp=verbose,
                                 tol=tolerance
                                 )

    def run(self):
        return self.optimizer(func=self.model.loss_functional_explicit, **self.solver_kwargs)


class Minimizer(BioPhyOptimizer):
    def __init__(self,
                 x,                                 # type: list
                 y,                                 # type: list
                 model,                             # type: LossModel
                 noise=None,                        # type: None or list
                 *args,
                 **kwargs
                 ):
        super(Minimizer, self).__init__(x, y, model, noise)
        self.iv = None

    def prepare_optimizer(self,
                          bnds=None,                # type: list[tuple] or None
                          n_iter=100,               # type: int
                          tolerance=1e-6,           # type: float
                          verbose=False,            # type: bool
                          *solver_args,
                          **solver_kwargs
                          ):
        # res = minimize(curr_model.loss_functional, iv, args=loss_dict, method=method,
        #                options={"disp": verbose, "gtol": 1e-10, "norm": 2, "maxiter": 10000})

        if "iv" in solver_kwargs:
            self.iv = solver_kwargs["iv"]
        else:
            self.iv = np.ones(self.model.params)

        if "method" in solver_kwargs:
            method = solver_kwargs["method"]
        else:
            method = "BFGS"
        if bnds is not None:
            method = "L-BFGS-B"

        self.optimizer = partial(minimize,
                                 args=self.loss_dict,
                                 method=method,
                                 bounds=bnds,
                                 tol=tolerance,
                                 options={"maxiter": n_iter, "disp": 2 if verbose else 0}
                                 )

    def run(self):
        return self.optimizer(fun=self.model.loss_functional,
                              x0=self.iv)


class LeastSquares(BioPhyOptimizer):
    def __init__(self,
                 x,                                 # type: list
                 y,                                 # type: list
                 model,                             # type: LossModel
                 noise=None,                        # type: None or list
                 *args,
                 **kwargs
                 ):
        super(LeastSquares, self).__init__(x, y, model, noise)
        self.iv = None
        self.optimizer_options = None

    def prepare_optimizer(self,
                          bnds=None,                # type: list[tuple] or None
                          n_iter=100,               # type: int
                          tolerance=1e-6,           # type: float
                          verbose=False,            # type: bool
                          *solver_args,
                          **solver_kwargs
                          ):
        # res = minimize(curr_model.loss_functional, iv, args=loss_dict, method=method,
        #                options={"disp": verbose, "gtol": 1e-10, "norm": 2, "maxiter": 10000})

        if "iv" in solver_kwargs:
            self.iv = solver_kwargs["iv"]
        else:
            self.iv = np.ones(self.model.params)

        if bnds is None:
            bounds = None
        else:
            bounds =([b[0] for b in bnds], [b[1] for b in bnds])
        self.optimizer_options = {
            "jac"     : "3-point",  # x-point rule for jacobian. 2-point or 3-point
            "method"  : "trf",  # optimizer method "trf" or "dogbox" for bounded optimization
            "ftol"    : tolerance,  # function change tolerance
            "xtol"    : tolerance,  # tolerance for variable change
            "max_nfev": 10000,  # maximal number of function evaluations
            "verbose" : 2 if verbose else 0,  # level of output: 0 none, 1 only final, 2 every iteration
            "loss"    : "soft_l1"  # "linear", "soft_l1", "huber", "cauchy", "arctan"
        }

        self.optimizer = partial(least_squares,
                                 kwargs=self.loss_dict,
                                 bounds=bounds,
                                 )

    def run(self):
        return self.optimizer(self.model.loss,
                              self.iv,
                              **self.optimizer_options
                              )
