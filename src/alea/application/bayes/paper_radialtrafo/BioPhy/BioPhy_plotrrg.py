# region import
import json
import matplotlib
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit
import numpy as np
# endregion

# region read important json files containing all the data
with open("all_info_rRg.json", "r") as f:
    all_inf = json.load(f)
# endregion

''' That was part of my manipulation. CoCl only some values and recalculations
    Same for Sp4
    Just ignore this

print(all_inf["CoCl3"]["concentration"])
all_inf["CoCl3"]["concentration"] = all_inf["CoCl3"]["concentration"][:8]
all_inf["CoCl3"]["R"] = all_inf["CoCl3"]["R"][:8]
all_inf["CoCl3"]["Rg"] = all_inf["CoCl3"]["Rg"][:8]
all_inf["CoCl3"]["gamma"] = all_inf["CoCl3"]["gamma"][:8]
all_inf["CoCl3"]["beta"] = all_inf["CoCl3"]["beta"][:8]
all_inf["CoCl3"]["t"] = all_inf["CoCl3"]["t"][:8]
print(all_inf["CoCl3"]["concentration"])
# TODO watch out for sorting
assert all_inf["CoCl3"]["concentration"][0] == 0.1
all_inf["CoCl3"]["R"][0] = 9.31038169515322
all_inf["CoCl3"]["Rg"][0] = 8.157859554419463
all_inf["CoCl3"]["gamma"][0] = 0.027619601748942263
all_inf["CoCl3"]["beta"][0] = 0.8893925924772558
all_inf["CoCl3"]["t"][0] =  1.0644067229313887e-22

assert all_inf["CoCl3"]["concentration"][5] == 0.5
all_inf["CoCl3"]["R"][5] = 16.552751566837593
all_inf["CoCl3"]["Rg"][5] = 17.999359683624885
all_inf["CoCl3"]["gamma"][5] = 0.013453931901000065
all_inf["CoCl3"]["beta"][5] = 2.6102048378585
all_inf["CoCl3"]["t"][5] =  2.2690211391769847e-22

# print(all_inf["Sp4"]["concentration"])
used_idx = [0, 1, 3, 5, 7, 8, 11, 12]
conc_list = []
R_list = []
Rg_list = []
gamma_list = []
beta_list = []
t_list = []
for lia in range(len(all_inf["Sp4"]["concentration"])):
    print(lia)
    print(lia in used_idx)
    if lia in used_idx:
        conc_list.append(all_inf["Sp4"]["concentration"][lia])
        R_list.append(all_inf["Sp4"]["R"][lia])
        Rg_list.append(all_inf["Sp4"]["Rg"][lia])
        gamma_list.append(all_inf["Sp4"]["gamma"][lia])
        beta_list.append(all_inf["Sp4"]["beta"][lia])
        t_list.append(all_inf["Sp4"]["t"][lia])
all_inf["Sp4"]["concentration"] = conc_list
all_inf["Sp4"]["R"] = R_list
all_inf["Sp4"]["Rg"] = Rg_list
all_inf["Sp4"]["gamma"] = gamma_list
all_inf["Sp4"]["beta"] = beta_list
all_inf["Sp4"]["t"] = t_list
# print(all_inf["Sp4"]["concentration"])
with open("all_info_rRg.json", "w") as f:
    json.dump(all_inf, f, indent=4)
exit()
'''

# region some parameter
resolution = 600                                    # dpi
image_format = "png"                                # e.g. png, pdf
plot_path = ""                                      # path to put the images. e.g "" for code directory or "/home/bla/"
                                                    # for absolute path. final / is needed when path is provided
figsize_t = (6, 6)                                  # image size (width, height)
figsize_R = (6, 6)                                  # image size (width, height)
figsize_Rg = (6, 6)                                 # image size (width, height)
# endregion
from BioPhy_model import (BrennichModelTrimmed, get_tetramer_spline, BrennichModelTrimmedLambda)
tet_path = "/home/fenics/shared/alea/src/alea/application/bayes/paper_radialtrafo/BioPhy/tetramer/1mg_modified/spline_data.json"
tetramer = get_tetramer_spline(tet_path)

m = BrennichModelTrimmed(tetramer)
file_type = "pdf"

if False:
    # (NaCl und KCL) + (CaCl und MgCl)
    for salt1, salt2 in zip(["NaCl", "CaCl2"], ["KCl", "MgCl2"]):
        conc_list_1 = all_inf[salt1]["concentration"]
        R_list_1 = all_inf[salt1]["R"]
        Rg_list_1 = all_inf[salt1]["Rg"]
        t_list_1 = all_inf[salt1]["t"]
        arg_idx_1 = np.argsort(conc_list_1)

        conc_list_2 = all_inf[salt2]["concentration"]
        R_list_2 = all_inf[salt2]["R"]
        Rg_list_2 = all_inf[salt2]["Rg"]
        t_list_2 = all_inf[salt2]["t"]
        arg_idx_2 = np.argsort(conc_list_1)


        # R in one plot
        ylim = [1, 20]

        fig, ax1 = plt.subplots(dpi=resolution, figsize=figsize_R)
        fig.suptitle("Inferred radi for {} and {}".format(salt1, salt2))

        ax1.plot(conc_list_1, R_list_1, 'ro')
        ax1.set_xlabel('concentration')
        ax1.set_ylabel('{}'.format(salt1), color='r')
        ax1.tick_params('y', colors='r')
        ax1.set_ylim(*ylim)

        ax2 = ax1.twinx()
        ax2.plot(conc_list_2, R_list_2, 'b*')
        ax2.set_ylabel('{}'.format(salt2), color='b')
        ax2.tick_params('y', colors='b')
        ax2.set_ylim(*ylim)

        # fig.tight_layout()
        fig.savefig(plot_path + "{}{}_R.{}".format(salt1, salt2, image_format), format=image_format)
        fig.clf()
        plt.close(fig)

        # Rg in another plot
        fig, ax1 = plt.subplots(dpi=resolution, figsize=figsize_Rg)
        fig.suptitle("Inferred gyration radi for {} and {}".format(salt1, salt2))

        ax1.plot(conc_list_1, Rg_list_1, 'ro')
        ax1.set_xlabel('concentration')
        ax1.set_ylabel('{}'.format(salt1), color='r')
        ax1.tick_params('y', colors='r')
        ax1.set_ylim(*ylim)

        ax2 = ax1.twinx()
        ax2.plot(conc_list_2, Rg_list_2, 'b*')
        ax2.set_ylabel('{}'.format(salt2), color='b')
        ax2.tick_params('y', colors='b')
        ax2.set_ylim(*ylim)

        # fig.tight_layout()
        fig.savefig(plot_path + "{}{}_Rg.{}".format(salt1, salt2, image_format), format=image_format)
        fig.clf()
        plt.close(fig)

        # t in one plot
        ylim = [0, 1]

        fig, ax1 = plt.subplots(dpi=resolution, figsize=figsize_t)
        fig.suptitle("Inferred Tetramer proportion for {} and {}".format(salt1, salt2))

        ax1.semilogy(conc_list_1, t_list_1, 'ro')
        ax1.set_xlabel('concentration')
        ax1.set_ylabel('{}'.format(salt1), color='r')
        ax1.tick_params('y', colors='r')
        ax1.set_ylim(*ylim)

        ax2 = ax1.twinx()
        ax2.semilogy(conc_list_2, t_list_2, 'b*')

        ax2.set_ylabel('{}'.format(salt2), color='b')
        ax2.tick_params('y', colors='b')
        ax2.set_ylim(*ylim)

        # fig.tight_layout()
        fig.savefig(plot_path + "{}{}_t.{}".format(salt1, salt2, image_format), format=image_format)
        fig.clf()
        plt.close(fig)

    # CoCl3 and Sp4 alone
    for salt in ["CoCl3", "Sp4"]:
        conc_list = all_inf[salt]["concentration"]
        R_list = all_inf[salt]["R"]
        Rg_list = all_inf[salt]["Rg"]
        t_list = all_inf[salt]["t"]

        ylim =[1, 20]
        # R solo
        fig = plt.figure(dpi=resolution, figsize=figsize_R)
        fig.suptitle("Inferred radi for {}".format(salt))
        plt.plot(conc_list, R_list, 'ro')
        plt.xlabel("concentration")
        plt.ylabel("Radi", color="r")
        plt.tick_params("y", colors="r")
        plt.ylim(*ylim)
        fig.savefig(plot_path + "{}_R.{}".format(salt, image_format), format=image_format)
        fig.clf()
        plt.close(fig)

        # Rg solo
        ylim = [1, 20]
        fig = plt.figure(dpi=resolution, figsize=figsize_Rg)
        fig.suptitle("Inferred gyration radi for {}".format(salt))
        plt.plot(conc_list, Rg_list, 'ro')
        plt.xlabel("concentration")
        plt.ylabel("gy. Radi")
        plt.tick_params("y")
        plt.ylim(*ylim)
        fig.savefig(plot_path + "{}_Rg.{}".format(salt, image_format), format=image_format)
        fig.clf()
        plt.close(fig)

        # t solo
        ylim = [0, 1]
        fig = plt.figure(dpi=resolution, figsize=figsize_t)
        fig.suptitle("Inferred Tetramer proportion for {}".format(salt))
        plt.plot(conc_list, t_list, 'ro')
        plt.xlabel("concentration")
        plt.ylabel("tetramer %", color="black")
        plt.tick_params("y", colors="black")
        plt.ylim(*ylim)
        fig.savefig(plot_path + "{}_t.{}".format(salt, image_format), format=image_format)
        fig.clf()
        plt.close(fig)
    exit()

color_list = {
    "0": "red",
    "1": "blue",
    "2": "cyan",
    "3": "orange",
    "4": "green",
    "all": "brown"
}

salt_str = {
    "Sp4": "Spermine",
    "KCl": "KCl",
    "NaCl": "NaCl",
    "CaCl2": r"$\mathrm{CaCl}_2$",
    "MgCl2": r"$\mathrm{MgCl}_2$",
    "CoCl3": "Hexammine-cobalt(III) chloride"
}

# matplotlib.rc('text', usetex = True)

fontsize_label= 15
fontsize_title = 20
fontsize_suptitle = 22

for salt in ["CaCl2"]: # "["Sp4", "KCl", "NaCl", "CaCl2", "MgCl2", "CoCl3"]:
    salt_dict = all_inf[salt]

    import os
    directory = "result_plots/" + salt + "/"
    if not os.path.exists(directory):
        os.makedirs(directory)
    curr_idx = -1
    for lia, inf in enumerate(salt_dict["measurement"]):
        print(inf)
        if inf != "2_0":
            continue
        if salt == "Sp4":
            if inf not in ["0_1", "0_01", "0_02", "0_03", "0_04", "0_05", "0_08", "0_09"]:
                continue
        if salt == "CoCl3":
            if inf not in ["0_1", "0_01", "0_2", "0_02", "0_03", "0_5", "0_05", "0_08"]:
                continue
        curr_idx += 1
        print("salt: {} concentration: {}".format(salt, inf))
        fig = plt.figure(figsize=(16, 8))
        plt.suptitle("Salt: {} concentration: {}mM".format(salt_str[salt], inf.replace("_", ".")), fontsize=fontsize_suptitle)
        if salt == "CoCl3" and inf == "0_5":
            q_min = salt_dict["measurement"][inf]["1"]["q_min"]
            q_max = salt_dict["measurement"][inf]["1"]["q_max"]
            curr_qr = np.array(salt_dict["measurement"][inf]["1"]["qr"])
        else:
            q_min = salt_dict["measurement"][inf]["0"]["q_min"]
            q_max = salt_dict["measurement"][inf]["0"]["q_max"]
            curr_qr = np.array(salt_dict["measurement"][inf]["0"]["qr"])
        curr_qr = curr_qr[np.where((q_min < curr_qr) & (curr_qr < q_max))]
        # ####################################################################
        # ONLY Once for CaCl2 2.0 mM
        if True:
            m = BrennichModelTrimmedLambda(tetramer, _lambda=0.34)
            # with tetramer
            # all_inf[salt]["t"][curr_idx] = 0.15712381942042145
            # all_inf[salt]["R"][curr_idx] = 7.668846526264715
            # all_inf[salt]["Rg"][curr_idx] = 5.505003749811742
            # all_inf[salt]["gamma"][curr_idx] = 0.018910402868493898
            # all_inf[salt]["beta"][curr_idx] = 1.0920590121695473

            # x = [all_inf[salt]["t"][curr_idx], all_inf[salt]["R"][curr_idx], all_inf[salt]["Rg"][curr_idx],
            #      all_inf[salt]["gamma"][curr_idx], all_inf[salt]["beta"][curr_idx]]
            # model_output = m.eval_model(curr_qr, *x[1:])
            # model_output["result"] = (1 - x[0]) * model_output["result"] + (x[0] * tetramer(curr_qr))



            all_inf[salt]["R"][curr_idx] = 6.4169466876088785
            all_inf[salt]["Rg"][curr_idx] = 4.91186280725773
            all_inf[salt]["gamma"][curr_idx] = 0.004412076433672696
            all_inf[salt]["beta"][curr_idx] = 2.941515666939605

            x = [all_inf[salt]["R"][curr_idx], all_inf[salt]["Rg"][curr_idx],
                 all_inf[salt]["gamma"][curr_idx], all_inf[salt]["beta"][curr_idx]]
            model_output = m.eval_model(curr_qr, *x)

        else:
            x = [all_inf[salt]["t"][curr_idx], all_inf[salt]["R"][curr_idx], all_inf[salt]["Rg"][curr_idx],
                 all_inf[salt]["gamma"][curr_idx], all_inf[salt]["beta"][curr_idx]]
            model_output = m.eval_model(curr_qr, *x[1:])
            model_output["result"] = (1 - x[0]) * model_output["result"] + (x[0] * tetramer(curr_qr))

        ######################################################################

        ax = plt.subplot(2, 1, 1)
        xlabel = r"$\mathit{q}$ ($\mathrm{nm}^{-1}$)"
        ax.set_xlabel(xlabel, fontsize=fontsize_label)
        ax.set_ylabel(r"$\mathit{I/c}$ ($\mathrm{cm}^{-1} \mathrm{g}^{-1} \mathrm{L}$)", fontsize=fontsize_label)

        plt.ylim([1e-5, 10])
        plt.xlim([0.08, 2])
        plt.plot(curr_qr, model_output["result"], 'X', c="brown", label="Model fit", linewidth=1, markersize=3,
                 alpha=0.8)

        plt.axvline(q_min, linestyle="dashed", color="black")
        plt.axvline(q_max, linestyle="dashed", color="black")
        ax.set_xscale('log')
        ax.set_yscale('log')

        ax = plt.subplot(2, 1, 2)
        plt.xlabel(xlabel, fontsize=fontsize_label)
        plt.ylim([1e-9, 10])
        plt.xlim([0.08, 2])
        plt.loglog(curr_qr, model_output["a1"] * model_output["Fs"], 'X', label=r"$a_1$Fs", linewidth=1,
                   markersize=3)
        plt.loglog(curr_qr, model_output["a2"] * model_output["Fc"], 'X', label=r"$a_2$Fc", linewidth=1,
                   markersize=3)
        plt.loglog(curr_qr, np.abs(model_output["a3"] * model_output["Ssc"]), 'X', label=r"$a_3$Ssc",
                   linewidth=1,
                   markersize=3)
        plt.loglog(curr_qr, model_output["a4"] * model_output["Scc"], 'X', label=r"$a_4$Scc", linewidth=1,
                   markersize=3)
        plt.axvline(q_min, linestyle="dashed", color="black")
        plt.axvline(q_max, linestyle="dashed", color="black")
        title_str = "Individual contributions"
        plt.title(title_str, fontsize=fontsize_title)

        for meas in salt_dict["measurement"][inf]:
            if inf == "0_5" and meas == "0":
                continue
            if inf == "0_1" and meas == "1":
                continue
            print("measurement: {}".format(meas))
            qr = salt_dict["measurement"][inf][meas]["qr"]
            intensity = salt_dict["measurement"][inf][meas]["intensity"]
            error = salt_dict["measurement"][inf][meas]["error"]
            if meas == "all":
                continue
            ax = plt.subplot(2, 1, 1)
            ax.scatter(qr, intensity, marker='x', s=4, alpha=0.9,
                       facecolor=color_list[meas], edgecolor=color_list[meas],
                       label="Experiment {}".format(str(int(meas)+1))
                       )
            err_qr = np.array(qr)[np.where(np.min(error) < intensity)]
            err_int = np.array(intensity)[np.where(np.min(error) < intensity)]
            curr_err = np.array(error)[np.where(np.min(error) < intensity)]
            # ax.errorbar(err_qr, err_int, yerr=curr_err, color=color_list[meas], label="error {}".format(meas), fmt="none")
            # ax.loglog(qr, error)
        plt.subplot(211)
        plt.legend()
        plt.subplot(212)
        plt.legend()
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        fig.savefig(directory + "{}_{}_lambda0_34.{}".format(salt, inf, file_type), format=file_type, dpi=600)
