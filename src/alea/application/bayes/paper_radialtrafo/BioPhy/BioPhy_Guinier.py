from __future__ import division, print_function
import numpy as np
from scipy.optimize import minimize
from BioPhy_helper_guinier import (loss_functional, get_files_from_directory, read_lists_from_data, plot_guinier,
                                   smooth)
import json

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Model parameter to modify
directory_list = ["KCl/10mM/"]
# directory_list = ["KCl/30mM/"]
q_min_list = [0.08]                                 # Minimal Value
max_Rgq = 1.0                                       # optimizer takes interval where Rg*q <= max_Rgq
safety_offset = 0.2
verbose = 2                                         # verbosity level. 0 None, 2 plots every iteration step
use_first_node_after_max_Rgq = True

smoothing_window = 5                                # if smoothing_window > 1 do smoothing
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

method = "BFGS"

marker = "v"*len(directory_list)
marker_size = 4
marker_edge_color = "m"*len(directory_list)
intensity_mult_factor = 2.64

# region read files
qr_list, qr_short_list = [], []
error_list, error_short_list = [], []
intensity_list, intensity_short_list = [], []
file_name_list = []
for lia, filament_directory in enumerate(directory_list):
    f_used_files, f_file_names = get_files_from_directory(filament_directory)
    assert len(f_used_files) == 1
    assert len(f_file_names) == 1
    f_file_dict = read_lists_from_data(f_used_files, q_min_list[lia], 1e10, intensity_mult_factor=intensity_mult_factor)
    for key, value in f_file_dict.items():
        assert len(value) == 1

    file_name_list.append(f_file_names[0])
    qr_list.append(f_file_dict["qr_trun_left"][0])
    intensity_list.append(f_file_dict["intensity_trun_left"][0])
    error_list.append(f_file_dict["error_trun_left"][0])
    qr_short_list.append(f_file_dict["qr_short"][0])
    intensity_short_list.append(f_file_dict["intensity_short"][0])
    error_short_list.append(f_file_dict["error_short"][0])
# endregion

# region Optimize Parameter
opt_parameter = []
opt_q_max = []
res_list = []
for lia in range(len(directory_list)):
    iv = np.ones(2)
    curr_opt_para = np.ones(2)
    max_idx = 2
    smoothed = False
    while True:
        if smoothing_window > 1 and not smoothed:
            assert smoothing_window % 2 == 1
            intensity_list[lia] = smooth(intensity_list[lia], smoothing_window)
            smoothed = True
        loss_dict = {
                "_qr": qr_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx],
                "_intensity": intensity_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx],
                "_error": np.ones(len(qr_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx])),
                # "_error": error_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx],
                "p": 2
                }
        # bounds = [(l, b) for l, b in zip([i0_lb[lia], rg_lb[lia]], [i0_ub[lia], rg_ub[lia]])]

        res = minimize(loss_functional, iv, args=loss_dict, method=method,
                       options={"disp": verbose, "gtol": 1e-7, "norm": 2})
        curr_opt_para = res.x.tolist()
        curr_opt_para[1] = np.abs(curr_opt_para[1])
        if verbose == 2:
            print("  max_idx: {}".format(max_idx))
            print("  Rg*q: {} vs bound: {}".format(qr_list[lia][q_min_list[lia] <= qr_list[lia]][max_idx-1] *
                                                   curr_opt_para[1], max_Rgq))
            print("  I0: {}, Rg: {}".format(*curr_opt_para))
            q_idx = max_idx

            qr = qr_list[lia][:q_idx]
            error = error_list[lia][:q_idx]
            intensity = intensity_list[lia][:q_idx]

            plot_guinier(qr, intensity, np.linspace(qr[0], qr[-1], num=100), curr_opt_para,
                         directory_list[lia] + "verbosity_{}_guinier_fit".format(max_idx-1), suffix="png")
        if qr_list[lia][q_min_list[lia] <= qr_list[lia]][max_idx-1] * np.abs(curr_opt_para[1]) >= max_Rgq:
            break
        max_idx += 1
        iv = res.x.tolist()
    if use_first_node_after_max_Rgq:
        opt_parameter.append(curr_opt_para)
        opt_q_max.append(max_idx)
    else:
        if (qr_list[lia][q_min_list[lia] <= qr_list[lia]][max_idx - 1] * np.abs(iv[1]) < max_Rgq - safety_offset) or (qr_list[lia][q_min_list[lia] <= qr_list[lia]][max_idx - 1] * np.abs(iv[1]) > max_Rgq + safety_offset):
            opt_parameter.append(curr_opt_para)
            opt_q_max.append(max_idx)
            print("!"*10 + "WARNING" + "!"*10)
            print("  can not return parameter for Rg*q_max <= {}".format(max_Rgq))
            print("  previous optimization did not yield a result in [{0} - {1}, {0} + {1}]".format(max_Rgq,
                                                                                                    safety_offset))
            print("  Return current iterate instead")
            continue

        opt_parameter.append(iv)
        opt_q_max.append(max_idx - 1)
    res_list.append(res)
# endregion

# region plot Guinier Fit in q^2 - q I(q) Plot
for lia, curr_dir in enumerate(directory_list):
    q_idx = opt_q_max[lia]

    qr = qr_list[lia][:q_idx]
    error = error_list[lia][:q_idx]
    intensity = intensity_list[lia][:q_idx]

    print("Guinier Fit of {} ".format(file_name_list[lia]))
    print("  I0 = {}, Rg={}, q_max*Rg={}".format(opt_parameter[lia][0], opt_parameter[lia][1],
                                                 qr[-1]*opt_parameter[lia][1]))
    print("  bounds: ({}, {})".format(qr[0]**2, qr[-1]**2))
    print("  number of q-nodes: {}".format(len(qr)))

    plot_guinier(qr, intensity, np.linspace(qr[0], qr[-1], num=100), opt_parameter[lia], curr_dir + "guinier_fit")
# endregion

for lia, curr_dir in enumerate(directory_list):
    inf = {
        "opt_parameter_I0": opt_parameter[lia][0],
        "opt_parameter_Rg": opt_parameter[lia][1],
        "file_name": file_name_list[lia],
        # "opt_success": int(res_list[lia].success),
        # "message": res_list[lia].message.decode("utf-8")
    }
    with open(curr_dir + "guinier_info.json", "w") as outfile:
        json.dump(inf, outfile, indent=4, sort_keys=True)
