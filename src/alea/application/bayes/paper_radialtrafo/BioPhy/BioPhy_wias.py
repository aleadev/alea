from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
# from BioPhy_model import exp_func as model
from alea.utils.corner import corner
from dolfin import *
from scipy.optimize import least_squares
import emcee
import cPickle as pickle
from functools import partial
from multiprocessing import Pool, cpu_count
from BioPhy_model import wiasModel as model
from BioPhy_model import read_lists_from_data, get_files_from_directory
from BioPhy_model import read_lists_from_data, gaussian, uniform, lognormal

# region parallel configuration
from contextlib import contextmanager
@contextmanager
def terminating(thing):
    try:
        yield thing
    finally:
        thing.terminate()

# endregion

# TODO This needs to be modified to fit your directory structure

directory = "KCl/10mM/"
used_files, file_names = get_files_from_directory(directory)

optimizer_options = {
    "jac": "3-point",                               # x-point rule for jacobian. 2-point or 3-point
    "method": "trf",                                # optimizer method "trf" or "dogbox" for bounded optimization
    "ftol": 1e-10,                                  # function change tolerance
    "xtol": 1e-10,                                  # tolerance for variable change
    "max_nfev": 10000,                              # maximal number of function evaluations
    "verbose": 2,                                   # level of output: 0 none, 1 only final, 2 every iteration
}

save_chain = True
read_chain_path = None #directory + "chain_result_wias.pick"

compute_det_result = True

q_min = 0.08
q_max = 0.8

#
# grid = np.linspace(-1, 5, num=1000)
# prior1 = lambda x: lognormal(x, mu=0.5, sigma=0.1)
# prior2 = lambda x: lognormal(x, mu=0.5, sigma=0.2)
# prior3 = lambda x: lognormal(x, mu=0.5, sigma=0.3)
# prior01 = lambda x: lognormal(x, mu=1, sigma=0.1)
# prior02 = lambda x: lognormal(x, mu=1, sigma=0.2)
# prior03 = lambda x: lognormal(x, mu=1, sigma=0.3)
#
# fig = plt.figure(figsize=(10, 10))
# plt.plot(grid, prior1(grid), label="1")
# plt.plot(grid, prior2(grid), label="2")
# plt.plot(grid, prior3(grid), label="3")
# plt.plot(grid, prior01(grid), label="01")
# plt.plot(grid, prior02(grid), label="02")
# plt.plot(grid, prior03(grid), label="03")
# plt.legend()
# fig.savefig(directory + "prior.png")
# exit()

use_additional_noise = False
start_observation_noise_at = 0.1
observation_noise_slope = -4

use_extrapolation = False
extrapolation_grid = np.logspace(-2.2, 1, num=100)

unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

# initial_guess = [2.54640574, 3.14490887, 5.78039958e-05, 6.32113428e+00]
# R, Rg, beta_s, beta_c, sigma, N_ulf, n, ell, dr, c
initial_guess = [6, 3.84490887, 0.02, 1e-5, 8, 4, 43, 0, 0]
lower_bound =   [0.5, 1e-8, 1e-8, 1e-8, 1, 1, 1, 0, -1]
upper_bound =   [30, 30, 10, 10, 100, 100, 100, 1, 1]

prior = [
    partial(lognormal, mu=0.1, sigma=0.1, logprior=False),
    partial(uniform, a=lower_bound[1], b=upper_bound[1], logprior=True),
    partial(uniform, a=lower_bound[2], b=upper_bound[2], logprior=True),
    partial(uniform, a=lower_bound[3], b=upper_bound[3], logprior=True),
    partial(uniform, a=lower_bound[4], b=upper_bound[4], logprior=True),
    partial(uniform, a=lower_bound[5], b=upper_bound[5], logprior=True),
    partial(uniform, a=lower_bound[6], b=upper_bound[6], logprior=True),
    partial(uniform, a=lower_bound[7], b=upper_bound[7], logprior=True),
    partial(uniform, a=lower_bound[8], b=upper_bound[8], logprior=True),
]
labels = ["R", "Rg", "beta_s", "log(beta_c)", "N_ulf", "n", "ell", "log(dr)", "c"]
marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5


burn_in = 1000
mc_steps = 3000

para_dimension = len(initial_guess)
chain_walker_factor = 6
chain_walker = para_dimension * chain_walker_factor


# region find initial positions for MCMC runs. need to be different
position = []
for lia in range(chain_walker):
    loc_pos = []
    for lib in range(para_dimension):
        if lib <= 7:                                 # R, Rg, beta, gamma must be acceptable initial guesses
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-3
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
        elif lib == 8:                              # dr = 0
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-8
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
        elif lib == 9:
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-3
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
    position.append(loc_pos)
# endregion

assert len(used_files) <= len(marker)

# region Read given data files
file_dict = read_lists_from_data(used_files, q_min, q_max)
qr, qr_short = file_dict["qr"], file_dict["qr_short"]
intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
error, error_short = file_dict["error"], file_dict["error_short"]
# endregion

# region additional noise
additional_noise = []
for lia, _err in enumerate(error_short):
    additional_noise.append(np.zeros(len(_err)))
    if use_additional_noise:
        last_qr = qr_short[0]
        init = True
        for lib, _qr in enumerate(qr_short[lia]):
            if _qr < start_observation_noise_at:
                init_noise = intensity_short[lia][lib]
                last_qr = _qr
                continue
            if init:
                additional_noise[lia][lib] = init_noise
                init = False
                continue
            additional_noise[lia][lib] = additional_noise[lia][lib-1] * np.exp(observation_noise_slope * (_qr - last_qr))
            last_qr = _qr

# endregion

assert len(qr_short) > 0

# region loss function
def loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
    _model = model(_qr_short[0], *theta)
    _retval = (1/(_error_short[0]+_add_noise[0]))*(_intensity_short[0] - _model["result"])
    # _retval = (_intensity_short[0] - _model["result"])
    if len(_qr_short) > 1:
        for _qr, _int, _err, _noise in zip(_qr_short[1:], _intensity_short[1:], _error_short[1:], _add_noise[1:]):
            _model = model(_qr, *theta)
            _retval = np.append(_retval, (1/(_err + _noise))*(_int - _model["result"]))
            # _retval = np.append(_retval, (_int - _model["result"]))
    return _retval


def phi(_xi):
    return 0.5*(np.linalg.norm(loss(_xi, **loss_dict), ord=2)**2)


def lnprob(loc_xi):
    ln_prior = 0
    for lnprob_lia in range(len(loc_xi)):
        if lnprob_lia == 0:
            if loc_xi[lnprob_lia] <= 0:
                return -np.inf
        ln_prior += prior[lnprob_lia](loc_xi[lnprob_lia])
    # print("para={} -> prior: {}".format(loc_xi, ln_prior))
    if ln_prior < -1e8:
        return ln_prior
    _potential = -phi(loc_xi)
    # print("para={} -> potential: {}".format(loc_xi, _potential))
    return ln_prior + _potential
# endregion


for i in range(len(qr_short) + 1):
    if len(qr_short) == 1 and i == 1:
        continue
    if i == len(qr_short):
        loss_dict = {
            "_qr_short"       : [qr_short],
            "_intensity_short": [intensity_short],
            "_error_short"    : [error_short],
            "_add_noise"      : [additional_noise]
        }
    else:
        loss_dict = {
            "_qr_short": [qr_short[i]],
            "_intensity_short": [intensity_short[i]],
            "_error_short": [error_short[i]],
            "_add_noise": [additional_noise[i]]
        }

    if compute_det_result:
        res = least_squares(loss, initial_guess, bounds=(lower_bound, upper_bound), kwargs=loss_dict,
                            **optimizer_options)

        print("final parameters: {}".format(res.x))
        print("optimality condition: {}".format(res.optimality))
        print("function evaluations: {}".format(res.nfev))
        print("gradient evaluations: {}".format(res.njev))
        print("reason for termination: {}".format(res.message))

        det_model_output = model(unified_grid, *res.x)
    # TODO: adapt read chain path for multiple measurements
    if read_chain_path is not None:
        with open(read_chain_path, "rb") as f:
            result = pickle.load(f)

        initial_guess = result["initial_guess"]
        lower_bound = result["lb"]
        upper_bound = result["ub"]
        mc_steps = result["mc_steps"]
        print(result["chain"].shape)
        para_dimension = result["dimension"]
        # burn_in = result["burn_in"]


        samples = result["chain"][:, burn_in:, :].reshape((-1, para_dimension))
        full_samples = result["chain"]
        chain_walker = para_dimension * chain_walker_factor
        # infer_variables = result["label"]

    else:
        with terminating(Pool(processes=cpu_count())) as pool:
            sampler = emcee.EnsembleSampler(chain_walker, para_dimension, lnprob, pool=pool)
            print("run {} burn-in samples".format(burn_in))
            position, prob, state = sampler.run_mcmc(position, burn_in)
            print("run {} MCMC samples".format(mc_steps))
            sampler.run_mcmc(position, mc_steps)

        if save_chain:
            with open(directory + "chain_result_wias.pick", "wb") as f:
                retval = {
                    "chain": sampler.chain,
                    "mc_steps": mc_steps,
                    "dimension": para_dimension,
                    "burn_in": burn_in,
                    "initial_guess": initial_guess,
                    "lb": lower_bound,
                    "ub": upper_bound,
                }
                pickle.dump(retval, f)

        samples = sampler.chain[:, burn_in:, :].reshape((-1, para_dimension))
        full_samples = sampler.chain

    samples[:, 3] = np.log(samples[:, 3])       # beta_c
    samples[:, 7] = np.log(samples[:, 7])       # dr

    # This is the empirical mean of the sample:
    mc_mean = np.mean(samples, axis=0)
    mc_q15 = np.percentile(samples, 15, axis=0)
    mc_q85 = np.percentile(samples, 85, axis=0)
    fig = corner(samples, labels=labels,
                 smooth=1.0, smooth1d=None,
                 quantiles=[0.16, 0.5, 0.84],  # only if you like full plots
                 show_titles=True, title_kwargs={"fontsize": 12},
                 # hist_kwargs={"log": True}
                 )
    # Extract the axes
    axes = np.array(fig.axes).reshape((para_dimension, para_dimension))
    # Loop over the diagonal
    for i in range(para_dimension):
        ax = axes[i, i]
        ax.axvline(mc_mean[i], color="r")

    # Loop over the histograms
    for yi in range(para_dimension):
        for xi in range(yi):
            if yi == xi == 2:
                continue
            ax = axes[yi, xi]

            ax.axvline(mc_mean[xi], color="r")
            ax.axhline(mc_mean[yi], color="r")
            ax.plot(mc_mean[xi], mc_mean[yi], "sr")

    fig.savefig(directory + "chain_result_wias.pdf")

    # for lia, _label in enumerate(infer_variables):
    #     if _label == "gamma" or _label == "c" or _label == "dr":
    #         mc_mean[lia] = np.exp(mc_mean[lia])
    #         mc_q15[lia] = np.exp(mc_q15[lia])
    #         mc_q85[lia] = np.exp(mc_q85[lia])
    mc_mean[3] = np.exp(mc_mean[3])
    mc_q15[3] = np.exp(mc_q15[3])
    mc_q85[3] = np.exp(mc_q85[3])

    mc_mean[7] = np.exp(mc_mean[7])
    mc_q15[7] = np.exp(mc_q15[7])
    mc_q85[7] = np.exp(mc_q85[7])

    print("mc mean: {}".format(mc_mean))
    print("mc 15 quantile: {}".format(mc_q15))
    print("mc 85 quantile: {}".format(mc_q85))

    model_output = model(qr_short[0], *mc_mean)
    model_output_q15 = model(qr_short[0], *mc_q15)
    model_output_q85 = model(qr_short[0], *mc_q85)
    # if det_optimization_result is not None:
    #     det_model_output = model(qr_short[0], *det_optimization_result)

    fig = plt.figure(figsize=(16, 8))

    ax = plt.subplot(1, 1, 1)
    for lia, (_qr, _int, _err) in enumerate(zip(qr, intensity, error)):
        ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                   facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                   label="int {}".format(lia + 1))
        ax.loglog(_qr, _err, '--', label="error {}".format(lia + 1))

    plt.plot(qr_short[0], model_output["result"], 'X', label="MCMC mean", linewidth=1, markersize=3)
    if compute_det_result:
        plt.plot(unified_grid, det_model_output["result"], 'x', label="Optimization", linewidth=1, markersize=3)

    plt.fill_between(qr_short[0], model_output_q15["result"], model_output_q85["result"], alpha=0.2)

    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    plt.legend()
    title_str = "MCMC Parameter: "
    # if det_optimization_result is not None:
    #     title_str += "\n Optimization yields: R={}, Rg={}, $gamma$={}, " \
    #                  "$beta$={}, loss={a}".format(*det_optimization_result[:4],
    #                                               a=np.linalg.norm(loss(det_optimization_result)))
    plt.title(title_str)
    fig.savefig(directory + "mcmc_model_result_wias.pdf")

    fig = plt.figure(figsize=(16, 8))

    for i in range(para_dimension):
        plt.subplot(para_dimension, 1, i + 1)
        for m in range(chain_walker):
            if i == 3 or i == 7:
                plt.semilogy(range(full_samples[m, :, i].shape[0]), full_samples[m, :, i], 'k')
            else:
                plt.plot(range(full_samples[m, :, i].shape[0]), full_samples[m, :, i], 'k')
            plt.plot(range(full_samples[m, :, i].shape[0]), [mc_mean[i]] * (full_samples[m, :, i].shape[0]), '-r')
            plt.ylabel(labels[i])
        plt.axvline(burn_in)
    fig.savefig(directory + "mcmc_chain_runner_wias.pdf")