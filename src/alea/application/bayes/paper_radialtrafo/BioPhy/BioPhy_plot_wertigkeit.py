import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

linewidth = 7
linestyle = "solid"


def func(x, a, b, c):
  return a * np.exp(-b * x) + c


def main():
    # the data you want to plot
    x = np.array([2, 2, 3, 4])
    fig = plt.figure()
    plt.vlines(x[0], 5, 10, colors="blue", linestyles=linestyle, linewidth=linewidth, label="Mg")
    plt.vlines(x[1], 2.5, 4.0, colors="dodgerblue", linestyles=linestyle, linewidth=linewidth, label="Ca")
    plt.vlines(x[2], 0.1, 0.2, colors="green", linestyles=linestyle, linewidth=linewidth, label="Co")
    plt.vlines(x[3], 0.05, 0.08, colors="purple", linestyles=linestyle, linewidth=linewidth, label="Sp")
    # add bar labels
    plt.xticks(x,["II", "II", "III", "IV"])
    plt.xlim(1.5, 4.5)

    plt.yticks([1e1, 1, 1e-1, 1e-2], [1e1, 1, 1e-1, 1e-2])
    plt.ylim(1e-2, 2*1e1)
    plt.yscale("log")

    if False:
        xn = np.array([2, 3, 4])
        yn = np.array([2.25, 0.15, 0.045])

        popt, pcov = curve_fit(func, xn, yn)
        print(popt)
        xh = np.linspace(2, 4, num=100)
        plt.plot(xh, func(xh, *popt), 'r-', label="Fitted Curve")
        plt.title("{:.2f}*exp(-{:.2f}*x) + {:.2f}".format(*popt))
    plt.legend()

    fig.savefig("wertigkeit.pdf", dpi=600)

main()
