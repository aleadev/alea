from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
# from BioPhy_model import exp_func as model
from functools import partial
from BioPhy_model import model as model
from BioPhy_model import read_lists_from_data, get_files_from_directory
from mpltools import annotation

from BioPhy_model import (BrennichModelTrimmed, BrennichModelPaper, BrennichModelFull, BrennichModelMeanTrimmed,
                          get_tetramer_spline)
from BioPhy_optimizer import BashinHopping, DualAnnealing, DifferentialEvolution, Minimizer, LeastSquares

from alea.application.bayes.paper_radialtrafo.BioPhy.BioPhy_helper_guinier import smooth
import json
from itertools import product as iter_product
import argparse

parser = argparse.ArgumentParser(description="BioPhy model optimizer")
parser.add_argument("dir", metavar="dir", type=str)
args = parser.parse_args()

directory = args.dir
tet_path = "/home/fenics/shared/alea/src/alea/application/bayes/paper_radialtrafo/BioPhy/tetramer/1mg_modified/spline_data.json"
used_files, file_names = get_files_from_directory(directory)

tetramer = get_tetramer_spline(tet_path)

q_min = 0.08
q_max = 1

use_extrapolation = False
extrapolation_grid = np.logspace(-2.2, 1, num=100)

optimizer = [LeastSquares] #, DifferentialEvolution]

# models = [BrennichModelTrimmed(), BrennichModelFull(), BrennichModelTrimmed(tetramer), BrennichModelFull(tetramer)]
models = [BrennichModelTrimmed(tetramer)] #, BrennichModelMeanTrimmed(tetramer)]

unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)


marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5
smoothing_window = 0
intensity_mult_factor = 2.64

assert len(used_files) <= len(marker)

# region Read given data files
file_dict = read_lists_from_data(used_files, q_min, q_max, intensity_mult_factor)
qr, qr_short = file_dict["qr"], file_dict["qr_short"]
intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
if smoothing_window > 0:
    for lia in range(len(intensity)):
        intensity[lia] = smooth(intensity[lia], smoothing_window)
        intensity_short[lia] = smooth(intensity_short[lia], smoothing_window)
error, error_short = file_dict["error"], file_dict["error_short"]
# endregion

assert len(qr_short) > 0
problem_list = [(item[0], item[1], item[2]) for item in iter_product(*[models, optimizer, range(len(qr_short) + 1)])]

num_iter = 0
for item in problem_list:
    num_iter += 1
    m, o, i = item[0], item[1], item[2]
    print("Directory: {} Run {}/{}".format(directory, num_iter, len(problem_list)))
    if len(qr_short) == 1 and i == 1:
        print("  All run left out")
        continue
    if i == len(qr_short):
        curr_qr = np.array([_y for _x in qr_short for _y in _x])
        curr_inten = np.array([_y for _x in intensity_short for _y in _x])
        curr_err = np.array([_y for _x in error_short for _y in _x])
        print("  All run with length {}".format(len(curr_qr)))
    else:
        curr_qr = qr_short[i]
        curr_inten = intensity_short[i]
        curr_err = error_short[i]
        print("  Dataset {} with length {}".format(file_names[i], len(curr_qr)))

    print("  optimizer: {}".format(o.__name__))
    print("  model: {}".format(m.model_name))

    bnds = list(zip(m.lower_bound, m.upper_bound))
    # TODO: estimate correct weights
    #       - do an ordinary regression
    #       - obtain the residuals to the estimator
    #       - compute c_j = \sigma_j^2 / \sigma^2 = r^2 / r^2.sum()
    if True:
        opt = o(curr_qr, curr_inten, m, noise=curr_err, p=np.inf)

        opt.prepare_optimizer(bnds, 1000, 1e-10, False)
        res = opt.run()
    else:

        weights = np.ones(len(curr_qr))
        curr_res = np.inf
        curr_i = 0
        iv = np.ones(5)
        while curr_res > 1e-5:
            print("run optimization {} with res:{}".format(curr_i, curr_res))
            opt = o(curr_qr, curr_inten, m, noise=weights, p=2)

            opt.prepare_optimizer(bnds, 10000, 1e-15, False)
            res = opt.run()

            unified_grid = curr_qr
            model_output = m.eval_model(curr_qr, *res.x[1:])
            model_output["result"] = (1 - res.x[0]) * model_output["result"] + (res.x[0] * tetramer(curr_qr))
            residuals = (np.array(model_output["result"] - curr_inten)) * (curr_err/np.sqrt(curr_inten))
            curr_res = np.linalg.norm(residuals)

            fig = plt.figure()
            plt.plot(curr_qr, residuals)
            residuals = smooth(residuals, window_size=9)
            plt.plot(curr_qr, residuals)
            plt.xlabel("q")
            plt.ylabel("r")
            plt.title("residuals")
            if i == len(qr_short):
                fig.savefig(directory + "all_residuals{}_{}_{}.png".format(curr_i, m.model_name, o.__name__), format="png")
            else:
                fig.savefig(directory + file_names[i] + "_residuals{}_{}_{}.png".format(curr_i, m.model_name, o.__name__),
                        format="png")

            weights = residuals**2 / (residuals**2).sum()

            fig = plt.figure()
            plt.plot(curr_qr, weights)
            plt.xlabel("q")
            plt.ylabel("w")
            plt.title("weights")
            if i == len(qr_short):
                fig.savefig(directory + "all_weights{}_{}_{}.png".format(curr_i, m.model_name, o.__name__), format="png")
            else:
                fig.savefig(directory + file_names[i] + "_weights{}_{}_{}.png".format(curr_i, m.model_name, o.__name__),
                        format="png")

            fig = plt.figure()
            plt.plot(curr_qr, weights*residuals/ np.sqrt(curr_err))
            plt.xlabel("q")
            plt.ylabel("r")
            plt.title("weighted residuals")
            if i == len(qr_short):
                fig.savefig(directory + "weightedinten{}_{}_{}.png".format(curr_i, m.model_name, o.__name__),
                            format="png")
            else:
                fig.savefig(
                    directory + file_names[i] + "_weightedinten{}_{}_{}.png".format(curr_i, m.model_name, o.__name__),
                    format="png")

            fig = plt.figure(figsize=(16, 8))
            if i == len(qr_short):
                plt.suptitle(directory + " all measurements")
            else:
                plt.suptitle(directory + file_names[i])

            ax = plt.subplot(2, 1, 1)
            if i == len(qr_short):
                for lia, (_qr, _int, _err) in enumerate(zip(qr, intensity, error)):
                    ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                               facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                               label="int {}".format(lia + 1))
                    ax.loglog(_qr, _err, '--', label="error {}".format(lia + 1))

            else:
                ax.scatter(qr[i], intensity[i], marker=marker[i], s=marker_size,
                           facecolor='lightgrey', edgecolor=marker_edge_color[i],
                           label="intensity")
                ax.loglog(qr[i], error[i], '--', label="error")
                ax.loglog(curr_qr, np.abs(residuals), '-x', label="residuals")

            plt.plot(unified_grid, model_output["result"], 'X', label="Interpolated Curve", linewidth=1, markersize=3,
                     alpha=0.3)
            if use_extrapolation:
                plt.plot(extrapolation_grid, model(extrapolation_grid, *res.x)["result"], 'X',
                         label="extrapolated Curve",
                         linewidth=1, markersize=3)

                _f = lambda _x: model(np.array([_x]), *res.x)["result"][0]
                _x1 = q_min / 10
                _x2 = q_min
                true_slope = np.round(np.log10((_f(_x2) / _f(_x1))) / (np.log10(_x2 / _x1)), 2)
                print("plot {} slope triangle at {}".format(true_slope,
                                                            (q_min, model(np.array([q_min]), *res.x)["result"][0])))
                annotation.slope_marker((q_min / 5, _f(q_min / 5) * 6), (true_slope, 1), ax=ax)
            plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
            plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
            if True:
                # title_str = "Optimization yields: R={}, Rg={}, g={}, b={}, lambda={}, sigma={}".format(model_output["R"],
                #                                                                                        model_output["Rg"],
                #                                                                                        model_output["gamma"],
                #                                                                                        model_output["bc2bs"],
                #                                                                                        model_output["lambda"],
                #                                                                                        None)
                # plt.title(title_str)
                plt.legend()

                ax = plt.subplot(2, 1, 2, sharex=ax)

                plt.loglog(unified_grid, model_output["a1"] * model_output["Fs"], 'X', label="a1*F2", linewidth=1,
                           markersize=3)
                plt.loglog(unified_grid, model_output["a2"] * model_output["Fc"], 'X', label="a2*Fc", linewidth=1,
                           markersize=3)
                plt.loglog(unified_grid, np.abs(model_output["a3"] * model_output["Ssc"]), 'X', label="a3*Ssc",
                           linewidth=1,
                           markersize=3)
                plt.loglog(unified_grid, model_output["a4"] * model_output["Scc"], 'X', label="a4*Scc", linewidth=1,
                           markersize=3)
                plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
                plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
                title_str = "Individual contributions"
                plt.title(title_str)
                plt.legend()

            if i == len(qr_short):
                fig.savefig(directory + "{}_all_optim_model_result_{}_{}.png".format(curr_i, m.model_name, o.__name__),
                            format="png")
            else:
                fig.savefig(
                    directory + file_names[i] + "{}_optim_model_result_{}_{}.png".format(curr_i, m.model_name, o.__name__),
                    format="png")

            curr_i += 1
            iv = res.x
    if m.tetramer is not None:
        model_output = m.eval_model(unified_grid, *res.x[1:])
        model_output["result"] = (1-res.x[0])*model_output["result"] + res.x[0]*tetramer(unified_grid)
    else:
        model_output = m.eval_model(unified_grid, *res.x)

    fig = plt.figure(figsize=(16, 8))
    if i == len(qr_short):
        plt.suptitle(directory + " all measurements")
    else:
        plt.suptitle(directory + file_names[i])

    ax = plt.subplot(2, 1, 1)
    if i == len(qr_short):
        for lia, (_qr, _int, _err) in enumerate(zip(qr, intensity, error)):
            ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                       facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                       label="int {}".format(lia+1))
            ax.loglog(_qr, _err, '--', label="error {}".format(lia+1))

    else:
        ax.scatter(qr[i], intensity[i], marker=marker[i], s=marker_size,
                   facecolor='lightgrey', edgecolor=marker_edge_color[i],
                   label="intensity")
        ax.loglog(qr[i], error[i], '--', label="error")
        # ax.loglog(curr_qr, np.abs(residuals), '-x', label="residuals")

    plt.plot(unified_grid, model_output["result"], 'X', label="Interpolated Curve", linewidth=1, markersize=3,
             alpha=0.3)
    if use_extrapolation:
        plt.plot(extrapolation_grid, model(extrapolation_grid, *res.x)["result"], 'X', label="extrapolated Curve",
                 linewidth=1, markersize=3)

        _f = lambda _x: model(np.array([_x]), *res.x)["result"][0]
        _x1 = q_min/10
        _x2 = q_min
        true_slope = np.round(np.log10((_f(_x2)/_f(_x1)))/(np.log10(_x2 / _x1)), 2)
        print("plot {} slope triangle at {}".format(true_slope, (q_min, model(np.array([q_min]), *res.x)["result"][0])))
        annotation.slope_marker((q_min/5, _f(q_min/5)*6), (true_slope, 1), ax=ax)
    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    if True:
        # title_str = "Optimization yields: R={}, Rg={}, g={}, b={}, lambda={}, sigma={}".format(model_output["R"],
        #                                                                                        model_output["Rg"],
        #                                                                                        model_output["gamma"],
        #                                                                                        model_output["bc2bs"],
        #                                                                                        model_output["lambda"],
        #                                                                                        None)
        # plt.title(title_str)
        plt.legend()

        ax = plt.subplot(2, 1, 2, sharex=ax)

        plt.loglog(unified_grid, model_output["a1"]*model_output["Fs"], 'X', label="a1*F2", linewidth=1,
                 markersize=3)
        plt.loglog(unified_grid, model_output["a2"]*model_output["Fc"], 'X', label="a2*Fc", linewidth=1,
                 markersize=3)
        plt.loglog(unified_grid, np.abs(model_output["a3"]*model_output["Ssc"]), 'X', label="a3*Ssc", linewidth=1,
                 markersize=3)
        plt.loglog(unified_grid, model_output["a4"]*model_output["Scc"], 'X', label="a4*Scc", linewidth=1,
                 markersize=3)
        plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
        plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
        title_str = "Individual contributions"
        plt.title(title_str)
        plt.legend()

    if i == len(qr_short):
        fig.savefig(directory + "all_optim_model_result_{}_{}.pdf".format(m.model_name, o.__name__),
                    format="pdf")
    else:
        fig.savefig(directory + file_names[i] + "_optim_model_result_{}_{}.pdf".format(m.model_name, o.__name__),
                    format="pdf")
    if True:
        optimizer_result = {
                "final parameters": res.x.tolist(),
                "fun": np.linalg.norm(res.fun),
                # "optimality condition": res.optimality,
                "function evaluations": res.nfev,
                # "gradient evaluations": res.njev,
                "reason for termination": str(res.message),
                "optimizer": str(o.__name__),
                "model": str(m.model_name)
               }
        if use_extrapolation:
            optimizer_result["init_slope"] = true_slope
        data_setting = {
            "qmin": q_min,
            "qmax": q_max,
            "lower_bound": m.lower_bound,
            "upper_bound": m.upper_bound,

        }
        if i == len(qr_short):
            open_file = directory + "all_opt_result_{}_{}.json".format(m.model_name, o.__name__)
        else:
            open_file = directory + file_names[i] + "_opt_result_{}_{}.json".format(m.model_name, o.__name__)
        inf = [optimizer_result, data_setting]
        with open(open_file, 'w') as f:
            json.dump(inf, f, indent=4, sort_keys=True)


