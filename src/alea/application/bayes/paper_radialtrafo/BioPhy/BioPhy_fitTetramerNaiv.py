from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares

from BioPhy_model import exp_func as model
from BioPhy_model import read_lists_from_data, get_files_from_directory, exp_fun_paras
# np.random.seed(8011990)

tetramer_directory = "KCl/twomodel_new/tetramer/"
t_used_files, t_file_names = get_files_from_directory(tetramer_directory)

optimizer_options = {
    "jac": "3-point",                               # x-point rule for jacobian. 2-point or 3-point
    "method": "trf",                                # optimizer method "trf" or "dogbox" for bounded optimization
    "ftol": 1e-10,                                  # function change tolerance
    "xtol": 1e-10,                                  # tolerance for variable change
    "max_nfev": 1000,                              # maximal number of function evaluations
    "verbose": 2,                                   # level of output: 0 none, 1 only final, 2 every iteration
    "loss": "cauchy"
}

t_q_min = 0.08
t_q_max = 2
# region Read given data files
t_file_dict = read_lists_from_data(t_used_files, t_q_min, t_q_max)
t_qr, t_qr_short = t_file_dict["qr"], t_file_dict["qr_short"]
t_intensity, t_intensity_short = t_file_dict["intensity"], t_file_dict["intensity_short"]
t_error, t_error_short = t_file_dict["error"], t_file_dict["error_short"]
assert len(t_qr_short) == 1
# endregion

M = exp_fun_paras()

# region loss function


def t_loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):

    # assert len(_qr_short) == 1
    # assert len(_intensity_short) == 1
    # assert len(_error_short) == 1

    _model = model(_qr_short[0], *theta)
    _retval = (_intensity_short[0] - _model)/_intensity_short[0]
    _retval[_qr_short[0] < 0.14] *= 1000
    # print(_retval[0:10])
    return _retval

# endregion


t_loss_dict = {
    "_qr_short"       : t_qr_short,
    "_intensity_short": t_intensity_short,
    "_error_short"    : t_error_short,
    "_add_noise"      : None,
}

t_res = least_squares(t_loss, np.append(3*np.ones(M-1), 1e-1), kwargs=t_loss_dict, **optimizer_options)

print("Tetramer optimisation:")
print("  final parameters: {}".format(t_res.x))
print("  optimality condition: {}".format(t_res.optimality))
print("  function evaluations: {}".format(t_res.nfev))
print("  gradient evaluations: {}".format(t_res.njev))
print("  reason for termination: {}".format(t_res.message))
t_parameter = t_res.x

grid = np.linspace(t_q_min, t_q_max, num=1000)
fig = plt.figure(figsize=(10, 10))
mod = model(grid, *t_parameter)
plt.loglog(grid, mod, '-b', label="fit")
plt.scatter(t_qr, t_intensity, marker="v", s=5, facecolor='lightgrey', edgecolor="r", label="data")
plt.loglog(t_qr[0], t_error[0], '--', label="err")

plt.axvline(t_q_min, linestyle="dashed", color="black", label="q min")
plt.axvline(t_q_max, linestyle="dashed", color="black", label="q max")
plt.legend()
fig.savefig(tetramer_directory + "tetramer_curve.pdf")
