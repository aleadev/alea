# region imports
from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from BioPhy_model import model, read_lists_from_data, gaussian, uniform
import json
import emcee
from Notebooks.corner import corner
from functools import partial
import cPickle as pickle
from multiprocessing import Pool, cpu_count
from contextlib import contextmanager

np.seterr(all='raise')
# endregion

# region parallel configuration

@contextmanager
def terminating(thing):
    try:
        yield thing
    finally:
        thing.terminate()

# endregion


# region variable definition
parallel_run = True                                 # flag to activate multiprocessing
save_chain = True                                   # flag to save the result of the MCMC run
read_chain_path = "Notebooks/data/chain_result_sigma0.dat" # path to a chain that can be read -> no calculation if not "None"
plot_raw_data = False                               # flag to plot the raw data as given
plot_fitted_curve = True                            # flag to plot the fitted curve
# export_data = False
# export_path = "Notebooks/data/Data_export.json"
# grid_len = 500                                      # number of equidistant data points in new grid
plot_x_lim = (0.07, 4.0)                            # area of x range for plotting the data


infer_sigma = False                                 # relative radial polydispersity
infer_lambda = False                                # chain density
infer_dr = False                                    # shell distance in terms of Rg
infer_c = False                                     # background

# parameter order: R, Rg, gamma, beta
initial_guess = [3, 2.17, 1e-6,  3.0]
lower_bound =   [0, 0, 0, 0]
upper_bound =   [15, 15, 10, 10]

q_min = 0.08
q_max = 3.0

plot_path = ""
export_path = "Notebooks/data/"

used_files = ["Notebooks/data/Data0.dat",
              "Notebooks/data/Data1.dat",
              "Notebooks/data/Data2.dat",
              ]


burn_in = 1000
mc_steps = 50000

marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5

# det_optimization_result = [3.29365283e+00, 2.71478376e+00, 1.90134725e-04, 3.01020197e+00, 0.134, 1.34375, 0, 0]
det_optimization_result = [2.54640574e+00,   3.14490887e+00,   5.78039958e-05,   6.32113428e+00]
assert len(used_files) <= len(marker)

infer_variables = ["R", "Rg", "gamma", "bc2bs"]
# prior model: either a uniform (uninformative) prior on [lower_bound, upper_bound]
#              or a standard gaussian distribution around the initial guess -> probably negative values
#              # TODO: exponential or log-normal priors
prior = [
    partial(uniform, a=lower_bound[0], b=upper_bound[0], logprior=True),
    partial(uniform, a=lower_bound[1], b=upper_bound[1], logprior=True),
    partial(uniform, a=lower_bound[2], b=upper_bound[2], logprior=True),
    partial(uniform, a=lower_bound[3], b=upper_bound[3], logprior=True),
]

if infer_sigma:
    initial_guess.append(0.134)
    lower_bound.append(0)
    upper_bound.append(10)
    prior.append(partial(uniform, a=lower_bound[-1], b=upper_bound[-1], logprior=True))
    infer_variables.append("sigma")

if infer_lambda:
    initial_guess.append(1.34375)
    lower_bound.append(0)
    upper_bound.append(10)
    prior.append(partial(uniform, a=lower_bound[-1], b=upper_bound[-1], logprior=True))
    infer_variables.append("_lambda")

if infer_dr:
    initial_guess.append(0)
    lower_bound.append(0)
    upper_bound.append(10)
    prior.append(partial(uniform, a=lower_bound[-1], b=upper_bound[-1], logprior=True))
    infer_variables.append("dr")

if infer_c:
    initial_guess.append(0)
    lower_bound.append(0)
    upper_bound.append(10)
    prior.append(partial(uniform, a=lower_bound[-1], b=upper_bound[-1], logprior=True))
    infer_variables.append("c")

para_dimension = len(initial_guess)
chain_walker = para_dimension * 2

# endregion

# region Read given data files
file_dict = read_lists_from_data(used_files, q_min, q_max)
qr, qr_short = file_dict["qr"], file_dict["qr_short"]
intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
error, error_short = file_dict["error"], file_dict["error_short"]
# endregion

if plot_raw_data:
    plt.figure(figsize=(16, 4))
    for lia in range(len(used_files)):
        plt.subplot(1, len(used_files), lia+1)
        plt.loglog(qr[lia], intensity[lia], '-o', label="intensity")
        plt.loglog(qr[lia], error[lia], '-r', label="error")
        plt.legend()
    plt.show()


# region MCMC functions
def loss(theta):
    input_param = {}
    for _lia, input_param_label in enumerate(infer_variables):
        input_param[input_param_label] = theta[_lia]
    _model = model(qr_short[0], timing=False, **input_param)
    _retval = (1/error_short[0])*(intensity_short[0] - _model["result"])
    for __qr, __int, __err in zip(qr_short[1:], intensity_short[1:], error_short[1:]):
        _model = model(__qr, timing=False, **input_param)
        _retval = np.append(_retval, (1/__err)*(__int - _model["result"]))
    return _retval


def phi(_xi):
    return 0.5*(np.linalg.norm(loss(_xi), ord=2)**2)


def lnprob(loc_xi):
    ln_prior = 0
    for lnprob_lia in range(len(loc_xi)):
        ln_prior += prior[lnprob_lia](loc_xi[lnprob_lia])
    # print("para={} -> prior: {}".format(loc_xi, ln_prior))
    if ln_prior < -1e8:
        return ln_prior
    _potential = -phi(loc_xi)
    # print("para={} -> potential: {}".format(loc_xi, _potential))
    return ln_prior + _potential
# endregion

# region find initial positions for MCMC runs. need to be different
position = []
for lia in range(chain_walker):
    loc_pos = []
    for lib in range(para_dimension):
        if lib < 4:                                 # R, Rg, beta, gamma must be acceptable initial guesses
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-3
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
        elif lib >= 4:                              # atm the same for sigma, lambda, dr, c
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-3
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
    position.append(loc_pos)
# endregion

if read_chain_path is not None:
    with open(read_chain_path, "rb") as f:
        result = pickle.load(f)

    initial_guess = result["initial_guess"]
    lower_bound = result["lb"]
    upper_bound = result["ub"]
    mc_steps = result["mc_steps"]
    print(result["chain"].shape)
    para_dimension = result["dimension"]
    burn_in = result["burn_in"]

    # burn_in = 20000
    samples = result["chain"][:, burn_in:, :].reshape((-1, para_dimension))
    full_samples = result["chain"]
    chain_walker = para_dimension * 2
    infer_variables = result["label"]

else:
    if not parallel_run:
        sampler = emcee.EnsembleSampler(chain_walker, para_dimension, lnprob)
        print("run burn in")
        position, prob, state = sampler.run_mcmc(position, burn_in)
        print("run mcmc")
        f = open(export_path + "chain.dat", "w")
        f.close()

        for result in sampler.sample(position, iterations=mc_steps, storechain=True):
            position = result[0]
            print("!!!!Mean acceptance fraction: {0:.3f}".format(np.mean(sampler.acceptance_fraction)))
            print(sampler.chain.shape)
            f = open(export_path + "chain.dat", "a")
            for k in range(position.shape[0]):
                f.write("{0:4d} {1:s}\n".format(k, "".join(str(position[k]))))
            f.close()
    else:
        with terminating(Pool(processes=cpu_count())) as pool:
            sampler = emcee.EnsembleSampler(chain_walker, para_dimension, lnprob, pool=pool)
            print("run {} burn-in samples".format(burn_in))
            position, prob, state = sampler.run_mcmc(position, burn_in)
            print("run {} MCMC samples".format(mc_steps))
            sampler.run_mcmc(position, mc_steps)

    if save_chain:
        with open(export_path + "chain_result_sigma0.dat", "wb") as f:
            retval = {
                "chain": sampler.chain,
                "mc_steps": mc_steps,
                "dimension": para_dimension,
                "burn_in": burn_in,
                "initial_guess": initial_guess,
                "lb": lower_bound,
                "ub": upper_bound,
                "label": infer_variables
            }
            pickle.dump(retval, f)

    samples = sampler.chain[:, burn_in:, :].reshape((-1, para_dimension))
    full_samples = sampler.chain

for lia, _label in enumerate(infer_variables):
    if _label == "gamma" or _label == "c" or _label == "dr":
        samples[:, lia] = np.log(samples[:, lia])

print(samples.shape)
# This is the empirical mean of the sample:
mc_mean = np.mean(samples, axis=0)
mc_q15 = np.percentile(samples, 15, axis=0)
mc_q85 = np.percentile(samples, 85, axis=0)
label_str = []
for _label in infer_variables:
    if _label == "gamma":
        label_str.append("log(g)")
        continue
    if _label == "c":
        label_str.append("log(c)")
        continue
    if _label == "dr":
        label_str.append("log(dr)")
        continue
    label_str.append(_label)
fig = corner(samples, labels=label_str,
             smooth=1.0, smooth1d=None,
             quantiles=[0.16, 0.5, 0.84], # only if you like full plots
             show_titles=True, title_kwargs={"fontsize": 12},
             # hist_kwargs={"log": True}
             )
# Extract the axes
axes = np.array(fig.axes).reshape((para_dimension, para_dimension))
# Loop over the diagonal
for i in range(para_dimension):
    ax = axes[i, i]
    ax.axvline(mc_mean[i], color="r")

# Loop over the histograms
for yi in range(para_dimension):
    for xi in range(yi):
        if yi == xi == 2:
            continue
        ax = axes[yi, xi]

        ax.axvline(mc_mean[xi], color="r")
        ax.axhline(mc_mean[yi], color="r")
        ax.plot(mc_mean[xi], mc_mean[yi], "sr")

fig.savefig(plot_path + "chain_result.png")

for lia, _label in enumerate(infer_variables):
    if _label == "gamma" or _label == "c" or _label == "dr":
        mc_mean[lia] = np.exp(mc_mean[lia])
        mc_q15[lia] = np.exp(mc_q15[lia])
        mc_q85[lia] = np.exp(mc_q85[lia])


print("mc mean: {}".format(mc_mean))
print("mc 15 quantile: {}".format(mc_q15))
print("mc 85 quantile: {}".format(mc_q85))

model_output = model(qr_short[0], *mc_mean)
model_output_q15 = model(qr_short[0], *mc_q15)
model_output_q85 = model(qr_short[0], *mc_q85)
if det_optimization_result is not None:
    det_model_output = model(qr_short[0], *det_optimization_result)

fig = plt.figure(figsize=(16, 8))

ax = plt.subplot(1, 1, 1)
for lia, (_qr, _int, _err) in enumerate(zip(qr, intensity, error)):
    ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
               facecolor='lightgrey', edgecolor=marker_edge_color[lia],
               label="int {}".format(lia+1))
    ax.loglog(_qr, _err, '--', label="error {}".format(lia+1))

plt.plot(qr_short[0], model_output["result"], 'X', label="MCMC mean", linewidth=1, markersize=3)
if det_optimization_result is not None:
    plt.plot(qr_short[0], det_model_output["result"], 'x', label="Optimization", linewidth=1, markersize=3)

plt.fill_between(qr_short[0], model_output_q15["result"], model_output_q85["result"], alpha=0.2)

plt.xlim(plot_x_lim)
plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
plt.legend()
title_str = "MCMC Parameter: R={}, Rg={}, $gamma$={}, " \
            "$beta$={}, loss={a}".format(*mc_mean[:4], a=np.linalg.norm(loss(mc_mean)))
if det_optimization_result is not None:
    title_str += "\n Optimization yields: R={}, Rg={}, $gamma$={}, " \
                 "$beta$={}, loss={a}".format(*det_optimization_result[:4], a=np.linalg.norm(loss(det_optimization_result)))
plt.title(title_str)
fig.savefig(plot_path + "mcmc_model_result.png")

fig = plt.figure(figsize=(16, 8))

for i in range(para_dimension):
    plt.subplot(para_dimension, 1, i+1)
    for m in range(chain_walker):
        if i == 2:
            plt.semilogy(range(full_samples[m, :, i].shape[0]), full_samples[m, :, i], 'k')
        else:
            plt.plot(range(full_samples[m, :, i].shape[0]), full_samples[m, :, i], 'k')
        plt.plot(range(full_samples[m, :, i].shape[0]), [mc_mean[i]]*(full_samples[m, :, i].shape[0]), '-r')
        plt.ylabel(infer_variables[i])
    plt.axvline(burn_in)
fig.savefig(plot_path + "mcmc_chain_runner.png")
# if export_data:
#     info = {
#             "x": list(grid),
#             "y": list(mean)
#            }
#     with open(export_path, 'w') as f:
#         json.dump(info, f, indent=4, sort_keys=True)

