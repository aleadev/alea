from __future__ import division, print_function
import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

from scipy.interpolate import InterpolatedUnivariateSpline as spline_variant
from functools import partial
# from BioPhy_model import exp_func as model
from alea.utils.corner import corner
from dolfin import *
from scipy.signal import argrelmax
from scipy.optimize import least_squares
import emcee
import json

import pickle as pickle
from functools import partial
from multiprocessing import Pool, cpu_count
from BioPhy_model import wiasModel2 as wiasmodel
from BioPhy_model import model as old_model
from BioPhy_model import (read_lists_from_data, get_files_from_directory, gaussian, uniform, lognormal,
                          new_naiv_tetramer_model, mode)
from BioPhy_helper_guinier import smooth
# region parallel configuration
from contextlib import contextmanager
@contextmanager
def terminating(thing):
    try:
        yield thing
    finally:
        thing.close()
        thing.join()
        thing.terminate()

# endregion

file_prefix = "tetramer3_"

# TODO This needs to be modified to fit your directory structure


tetramer_directory = "tetramer/1mg/"
t_used_files, t_file_names = get_files_from_directory(tetramer_directory)



save_chain = False
read_chain_path = None #filament_directory + "chain_result_wias.pick"

compute_det_result = False

smoothing_window = 5
t_q_min = 0.08
t_q_max = 1.15

problem_dict = {
    "KCl/twomodel_new/filament/": {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 100},
    "KCl/10mM/": {"q_min": 0.08, "q_max": 0.8, "salt": "KCl", "concentration": 10},
    "KCl/20mM/": {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 20},
    "KCl/30mM/": {"q_min": 0.08, "q_max": 0.8, "salt": "KCl", "concentration": 30},
    "KCl/40mM/": {"q_min": 0.08, "q_max": 0.7, "salt": "KCl", "concentration": 40},
    "KCl/50mM/": {"q_min": 0.08, "q_max": 0.6, "salt": "KCl", "concentration": 50},
    "KCl/80mM/": {"q_min": 0.08, "q_max": 0.6, "salt": "KCl", "concentration": 80},
    "KCl/100mM/": {"q_min": 0.08, "q_max": 0.6, "salt": "KCl", "concentration": 100},
    "KCl/150mM/": {"q_min": 0.08, "q_max": 0.6, "salt": "KCl", "concentration": 150},
    "KCl/br_100mM/": {"q_min": 0.08, "q_max": 1.5, "salt": "KCl", "concentration": 100},
    "KCl/twomodel_new/mix/": {"q_min": 0.08, "q_max": 0.6, "salt": "KCl", "concentration": -1},
    "KCl/twomodel_new/tetramer/": {"q_min": 0.08, "q_max": 0.6, "salt": "KCl", "concentration": 0}
}

#
# grid = np.linspace(-1, 5, num=1000)
# prior1 = lambda x: lognormal(x, mu=0.5, sigma=0.1)
# prior2 = lambda x: lognormal(x, mu=0.5, sigma=0.2)
# prior3 = lambda x: lognormal(x, mu=0.5, sigma=0.3)
# prior01 = lambda x: lognormal(x, mu=1, sigma=0.1)
# prior02 = lambda x: lognormal(x, mu=1, sigma=0.2)
# prior03 = lambda x: lognormal(x, mu=1, sigma=0.3)
#
# fig = plt.figure(figsize=(10, 10))
# plt.plot(grid, prior1(grid), label="1")
# plt.plot(grid, prior2(grid), label="2")
# plt.plot(grid, prior3(grid), label="3")
# plt.plot(grid, prior01(grid), label="01")
# plt.plot(grid, prior02(grid), label="02")
# plt.plot(grid, prior03(grid), label="03")
# plt.legend()
# fig.savefig(directory + "prior.png")
# exit()

use_additional_noise = False
start_observation_noise_at = 0.1
observation_noise_slope = -4

use_extrapolation = False
extrapolation_grid = np.logspace(-2.2, 1, num=100)

# initial_guess = [2.54640574, 3.14490887, 5.78039958e-05, 6.32113428e+00]
# t, R, Rg, gamma, beta, lambda, dr, c
initial_guess = [0.5, 6, 3.84490887, 5.78039958e-05, 6.32113428e-01, 1.34, 0, 0]
lower_bound =   [0, 1e-3, 1e-8, 1e-8, 1e-8, 1e-8, -1e-15, -1]
upper_bound =   [1, 15, 15, 1, 10, 100, 1, 1]

# R, Rg, gamma, beta, sigma, lambda, dr, c
det_initial_guess = [6, 3.84490887, 5.78039958e-05, 6.32113428e-01, 0.134, 1.34, 0, 0]
det_lower_bound =   [1e-3, 1e-8, 1e-8, 1e-8, 1e-8, 1e-8, -1e-15, -1]
det_upper_bound =   [15, 15, 1, 10, 10, 100, 1, 1]

fix_paras = True

prior = [
    partial(uniform, a=0, b=1, logprior=True),                              # t (weight of T and F)
    # partial(lognormal, mu=1.0, sigma=0.25, logprior=True),                  # R
    # partial(lognormal, mu=1.7, sigma=0.25, logprior=True),                  # RG
    partial(uniform, a=lower_bound[1], b=upper_bound[1], logprior=True),  # gamma
    partial(uniform, a=lower_bound[2], b=upper_bound[2], logprior=True),  # gamma

    partial(uniform, a=lower_bound[3], b=upper_bound[3], logprior=True),  # gamma
    partial(uniform, a=lower_bound[4], b=upper_bound[4], logprior=True),  # beta
    partial(uniform, a=1e-8, b=100, logprior=True),                    # lambda
    partial(uniform, a=-1e-15, b=1, logprior=True),                     # dr
    partial(uniform, a=-1, b=1, logprior=True),                     # c
]

# grid = np.linspace(-1, 10, num=1000)
# fig = plt.figure(figsize=(10, 10))
# plt.plot(grid, prior[0](grid), label="R")
# plt.plot(grid, prior[1](grid), label="Rg")
# plt.plot(grid, prior2(grid), label="2")
# plt.plot(grid, prior3(grid), label="3")
# plt.plot(grid, prior01(grid), label="01")
# plt.plot(grid, prior02(grid), label="02")
# plt.plot(grid, prior03(grid), label="03")
# plt.legend()
# fig.savefig(directory + "prior.png")

labels = ["t", "R", "Rg", "log(gamma)", "log(beta)", "lambda", "log(dr)", "c"]

marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5


burn_in = 100
mc_steps = 1000

para_dimension = len(initial_guess)
chain_walker_factor = 8
chain_walker = para_dimension * chain_walker_factor


# region find initial positions for MCMC runs. need to be different
position = []
for lia in range(chain_walker):
    loc_pos = []
    for lib in range(para_dimension):
        #if lib <= 2:  # R, Rg   lognormal
        #    while True:
        #        guess = initial_guess[lib] + np.random.randn()
        #        if prior[lib](guess) >= -1e10:
        #            loc_pos.append(guess)
        #            break

        if lib <= 7:                                 # beta, gamma must be acceptable initial guesses
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-3
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
        elif lib == 8:                              # dr = 0
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-8
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
        elif lib == 9:
            while True:
                guess = initial_guess[lib] + np.random.randn()*1e-3
                if prior[lib](guess) >= 0:
                    loc_pos.append(guess)
                    break
    position.append(loc_pos)
# endregion

# region Read given data files
t_file_dict = read_lists_from_data(t_used_files, t_q_min, t_q_max)
t_qr, t_qr_short = t_file_dict["qr"], t_file_dict["qr_short"]
t_intensity, t_intensity_short = t_file_dict["intensity"], t_file_dict["intensity_short"]
t_error, t_error_short = t_file_dict["error"], t_file_dict["error_short"]
assert len(t_qr_short) == 1

if smoothing_window > 1:
    assert smoothing_window % 2 == 1
    t_intensity_short[0] = smooth(t_intensity_short[0], smoothing_window)
    t_intensity[0] = smooth(t_intensity[0], smoothing_window)
# endregion


spline_variant = partial(spline_variant, k=3)
t_spline = spline_variant(t_qr_short, t_intensity_short)

# region loss function


def loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
    if fix_paras:
        theta[5] = 1.34
        theta[6] = 0
        theta[7] = 0
    if frozenset(_qr_short[0]) not in old_model_hash:
        old_model_hash[frozenset(_qr_short[0])] = t_spline(_qr_short[0])

    _t_model = old_model_hash[frozenset(_qr_short[0])]
    _model = model(_qr_short[0], *theta[1:])
    _retval = 100000*(_intensity_short[0] - ((1-theta[0])*_model["result"] + theta[0]*_t_model))
    # _retval = (_intensity_short[0] - _model["result"])
    if len(_qr_short) > 1:
        for __qr, __int, __err in zip(_qr_short[1:], _intensity_short[1:], _error_short[1:]):
            if frozenset(__qr) not in old_model_hash:
                old_model_hash[frozenset(__qr)] = t_spline(__qr)

            _t_model = old_model_hash[frozenset(__qr)]
            _model = model(__qr, *theta[1:])
            _retval = np.append(_retval, (1/__err)*(__int - ((1-theta[0])*_model["result"]) + theta[0]*_t_model))
            # _retval = np.append(_retval, (_int - _model["result"]))
    return _retval


def phi(_xi):
    return 0.5*(np.linalg.norm(loss(_xi, **loss_dict), ord=2)**2)


def lnprob(loc_xi):
    ln_prior = 0
    for lnprob_lia in range(len(loc_xi)):
        # if lnprob_lia < 2:
        #     if loc_xi[lnprob_lia] <= 0:
        #         return -np.inf

        ln_prior += prior[lnprob_lia](loc_xi[lnprob_lia])
    # print("para={} -> prior: {}".format(loc_xi, ln_prior))
    if ln_prior < -1e8:
        return ln_prior
    _potential = -phi(loc_xi)
    # print("para={} -> potential: {}".format(loc_xi, _potential))
    return ln_prior + _potential
# endregion


directory_list = ["KCl/10mM/",
                  # "KCl/20mM/", "KCl/30mM/", "KCl/40mM/", "KCl/50mM/", "KCl/80mM/",
                  # "KCl/100mM/", "KCl/150mM/", "KCl/br_100mM/"
                 ]
for filament_directory in directory_list:
    prob_info = problem_dict[filament_directory]
    old_model_hash = dict()
    # region read files
    f_used_files, f_file_names = get_files_from_directory(filament_directory)
    assert len(f_used_files) <= len(marker)
    q_min = problem_dict[filament_directory]["q_min"]
    q_max = problem_dict[filament_directory]["q_max"]
    f_file_dict = read_lists_from_data(f_used_files, q_min, q_max)
    f_qr, f_qr_short = f_file_dict["qr"], f_file_dict["qr_short"]
    f_intensity, f_intensity_short = f_file_dict["intensity"], f_file_dict["intensity_short"]
    f_error, f_error_short = f_file_dict["error"], f_file_dict["error_short"]
    assert len(f_qr_short) > 0

    if smoothing_window > 1:
        assert smoothing_window % 2 == 1
        for lif in range(len(f_intensity)):
            f_intensity_short[lif] = smooth(f_intensity_short[lif], smoothing_window)
            f_intensity[lif] = smooth(f_intensity[lif], smoothing_window)

    # endregion
    unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

    drop_out = 0
    for i in range(len(f_qr_short) + 1):
        para_dimension = len(initial_guess)
        # if i < len(f_file_names) and f_file_names[i].startswith("2018_08_01_2"):
        #     drop_out = i
        # if i < len(f_qr_short):
        #     continue

        if len(f_qr_short) == 1 and i == 1:
            continue

        if i == len(f_qr_short):
            # f_qr = np.delete(f_qr, drop_out)
            # f_intensity = np.delete(f_intensity, drop_out)
            # f_error = np.delete(f_error, drop_out)

            prob_info["measurement"] = "all"
            loss_dict = {
                "_qr_short"       : f_qr_short, #np.delete(f_qr_short, drop_out),
                "_intensity_short": f_intensity_short, #np.delete(f_intensity_short, drop_out),
                "_error_short"    : f_error_short, #np.delete(f_error_short, drop_out),
                "_add_noise"      : [None]
            }
        else:
            prob_info["measurement"] = i
            loss_dict = {
                "_qr_short": [f_qr_short[i]],
                "_intensity_short": [f_intensity_short[i]],
                "_error_short": [f_error_short[i]],
                "_add_noise": [None]
            }

        # compute det result for filament curve

        if compute_det_result:
            model = old_model
            res = least_squares(loss, det_initial_guess, bounds=(det_lower_bound, det_upper_bound), kwargs=loss_dict,
                                **optimizer_options)

            print("final parameters: {}".format(res.x))
            print("optimality condition: {}".format(res.optimality))
            print("function evaluations: {}".format(res.nfev))
            print("gradient evaluations: {}".format(res.njev))
            print("reason for termination: {}".format(res.message))
            det_model_para = res.x
            det_model_output = old_model(unified_grid, *res.x[1:])
        model = wiasmodel
        # TODO: adapt read chain path for multiple measurements
        if read_chain_path is not None:
            with open(read_chain_path, "rb") as f:
                result = pickle.load(f)

            initial_guess = result["initial_guess"]
            lower_bound = result["lb"]
            upper_bound = result["ub"]
            mc_steps = result["mc_steps"]
            print(result["chain"].shape)
            para_dimension = result["dimension"]
            # burn_in = result["burn_in"]

            samples = result["chain"][:, burn_in:, :].reshape((-1, para_dimension))
            full_samples = result["chain"]
            chain_walker = para_dimension * chain_walker_factor
            # infer_variables = result["label"]

        else:
            if i == len(f_qr_short):
                print("Run Problem: all of current directory")
            else:
                print("Run problem: {}".format(f_used_files[i]))
            with terminating(Pool(processes=cpu_count())) as pool:
                sampler = None
                sampler = emcee.EnsembleSampler(chain_walker, para_dimension, lnprob, pool=pool)
                print("run {} burn-in samples".format(burn_in))
                position, prob, state = sampler.run_mcmc(position, burn_in)
                print("run {} MCMC samples".format(mc_steps))
                sampler.run_mcmc(position, mc_steps)

            if save_chain:
                if i == len(f_qr_short):
                    fn = filament_directory + file_prefix + "all_chain_result_wias.pick"
                else:
                    fn = filament_directory + file_prefix + f_file_names[i] + "chain_result_wias.pick"
                with open(fn, "wb") as f:
                    retval = {
                        "chain": sampler.chain,
                        "mc_steps": mc_steps,
                        "dimension": para_dimension,
                        "burn_in": burn_in,
                        "initial_guess": initial_guess,
                        "lb": lower_bound,
                        "ub": upper_bound,
                    }
                    pickle.dump(retval, f)

            samples = sampler.chain[:, burn_in:, :].reshape((-1, para_dimension))
            full_samples = sampler.chain

        if fix_paras:
            samples = np.delete(samples, 5, 1)
            samples = np.delete(samples, 5, 1)
            samples = np.delete(samples, 5, 1)
            labels = labels[:5]
            para_dimension = 5
        else:
            samples[:, 6] = np.log(samples[:, 6])       # dr

        samples[:, 3] = np.log(samples[:, 3])  # beta
        samples[:, 4] = np.log(samples[:, 4])       # gamma
        # This is the empirical mean of the sample:
        mc_mean = np.mean(samples, axis=0)
        mc_q15 = np.percentile(samples, 15, axis=0)
        mc_q85 = np.percentile(samples, 85, axis=0)

        n_bins = 40
        fig, n = corner(samples, bins=n_bins, labels=labels,
                        smooth=1.0, smooth1d=None,
                        quantiles=[0.15, 0.85],  # only if you like full plots
                        show_titles=True, title_kwargs={"fontsize": 12},
                        return_n=True
                        # hist_kwargs={"log": True}
                        )
        mc_mode = np.zeros(samples.shape[1])
        for lis in range(samples.shape[1]):
            x = np.linspace(np.min(samples[:, lis]), np.max(samples[:, lis]), num=n_bins)
            ind_max = argrelmax(n[lis], mode="wrap")
            x_max = x[ind_max]
            y_max = n[lis][ind_max]
            index_first_max = np.argmax(y_max)
            mc_mode[lis] = x_max[index_first_max]
        # mc_mode = np.array(mode(((samples * 10).astype(np.int64)) / 10, axis=0)[0])

        # Extract the axes
        axes = np.array(fig.axes).reshape((para_dimension, para_dimension))
        # Loop over the diagonal
        for _i in range(para_dimension):
            ax = axes[_i, _i]
            ax.axvline(mc_mean[_i], color="r")
            ax.axvline(mc_mode[_i], color="g")

        # Loop over the histograms
        for yi in range(para_dimension):
            for xi in range(yi):
                if yi == xi == 2:
                    continue
                ax = axes[yi, xi]

                ax.axvline(mc_mean[xi], color="r")
                ax.axhline(mc_mean[yi], color="r")
                ax.axvline(mc_mode[xi], color="g")
                ax.axhline(mc_mode[yi], color="g")
                ax.plot(mc_mean[xi], mc_mean[yi], "sr")
                ax.plot(mc_mode[xi], mc_mode[yi], "sg")

        if i == len(f_qr_short):
            fig.savefig(filament_directory + file_prefix + "all_chain_result.pdf", format="pdf")
        else:
            fig.savefig(filament_directory + file_prefix + f_file_names[i] + "_chain_result.pdf", format="pdf")



        # for lia, _label in enumerate(infer_variables):
        #     if _label == "gamma" or _label == "c" or _label == "dr":
        #         mc_mean[lia] = np.exp(mc_mean[lia])
        #         mc_q15[lia] = np.exp(mc_q15[lia])
        #         mc_q85[lia] = np.exp(mc_q85[lia])

        mc_mean[3] = np.exp(mc_mean[3])
        mc_mode[3] = np.exp(mc_mode[3])
        mc_q15[3] = np.exp(mc_q15[3])
        mc_q85[3] = np.exp(mc_q85[3])
        mc_mean[4] = np.exp(mc_mean[4])
        mc_mode[4] = np.exp(mc_mode[4])
        mc_q15[4] = np.exp(mc_q15[4])
        mc_q85[4] = np.exp(mc_q85[4])
        if not fix_paras:
            mc_mean[6] = np.exp(mc_mean[6])
            mc_mode[6] = np.exp(mc_mode[6])
            mc_q15[6] = np.exp(mc_q15[6])
            mc_q85[6] = np.exp(mc_q85[6])

        print("mc mean: {}".format(mc_mean))
        print("mc mode: {}".format(mc_mode))
        print("mc 15 quantile: {}".format(mc_q15))
        print("mc 85 quantile: {}".format(mc_q85))
        mc_mean = mc_mode
        model_output = wiasmodel(unified_grid, *mc_mean[1:])
        t_model_grid = np.logspace(np.log10(t_q_min), np.log10(t_q_max), num=200)
        t_plot_output = t_spline(t_model_grid)
        t_model_output = t_spline(unified_grid)
        model_output_q15 = wiasmodel(unified_grid, *mc_q15[1:])
        model_output_q85 = wiasmodel(unified_grid, *mc_q85[1:])

        # if det_optimization_result is not None:
        #     det_model_output = model(qr_short[0], *det_optimization_result)

        fig = plt.figure(figsize=(20, 8))

        ax = plt.subplot(2, 2, 1)
        for lia, (_qr, _int, _err) in enumerate(zip(t_qr, t_intensity, t_error)):
            ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                       facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                       label="int {}".format(lia + 1))
            ax.loglog(_qr, _err, '--', label="error {}".format(lia + 1))

        plot_result = t_model_output
        plt.plot(t_model_grid, t_plot_output, 'X', label="Tetramer fit", linewidth=1, markersize=3)

        plt.axvline(t_q_min, linestyle="dashed", color="black", label="q min")
        plt.axvline(t_q_max, linestyle="dashed", color="black", label="q max")
        plt.legend()
        title_str = "Tetramer curve"

        # if det_optimization_result is not None:
        #     title_str += "\n Optimization yields: R={}, Rg={}, $gamma$={}, " \
        #                  "$beta$={}, loss={a}".format(*det_optimization_result[:4],
        #                                               a=np.linalg.norm(loss(det_optimization_result)))
        plt.title(title_str)

        ax = plt.subplot(2, 2, 2)
        if i == len(f_qr_short):
            for lia, (_qr, _int, _err) in enumerate(zip(f_qr, f_intensity, f_error)):
                ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                           facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                           label="int {}".format(lia + 1))
                ax.loglog(_qr, _err, '--', label="error {}".format(lia + 1))
        else:
            ax.scatter(f_qr[i], f_intensity[i], marker=marker[i], s=marker_size,
                       facecolor='lightgrey', edgecolor=marker_edge_color[i],
                       label="int")
            ax.loglog(f_qr[i], f_error[i], '--', label="error")
        plot_result = (1-mc_mean[0])*model_output["result"] + mc_mean[0]*t_model_output
        plt.plot(unified_grid, plot_result, 'X', label="MCMC mean", linewidth=1, markersize=1)
        if compute_det_result:
            det_model_plot = (1-det_model_para[0])*det_model_output["result"] + det_model_para[0]*t_model_output
            plt.plot(unified_grid, det_model_plot, 'x', label="Optimization", linewidth=1, markersize=3)

        plot_result_q15 = (1 - mc_q15[0]) * model_output_q15["result"] + mc_q15[0] * t_model_output
        plot_result_q85 = (1 - mc_q85[0]) * model_output_q85["result"] + mc_q85[0] * t_model_output
        plt.fill_between(unified_grid, plot_result_q15, plot_result_q85, alpha=0.2)

        plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
        plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
        plt.legend()
        title_str = "Parameter: ({})\nMCMC mean: {}\n".format(labels, mc_mean)
        if compute_det_result:
            if fix_paras:
                title_str += "Optimization: {}".format(det_model_para[:5])
            else:
                title_str += "Optimization: {}".format(det_model_para)
        plt.title(title_str)
        # fig.savefig(directory + "mcmc_model_result_wias.pdf")
        ax = plt.subplot(2, 2, 4, sharex=ax)

        fs = (1 - mc_mean[0]) * model_output["a1"]*model_output["Fs"] + mc_mean[0] * t_model_output
        plt.loglog(unified_grid, fs, 'X', label="a1*Fs", linewidth=1, markersize=3)
        fc = (1 - mc_mean[0]) * model_output["a2"] * model_output["Fc"] + mc_mean[0] * t_model_output
        plt.loglog(unified_grid, fc, 'X', label="a2*Fc", linewidth=1, markersize=3)
        ssc = (1 - mc_mean[0]) * np.abs(model_output["a3"] * model_output["Ssc"]) + mc_mean[0] * t_model_output
        plt.loglog(unified_grid, ssc, 'X', label="a3*Ssc", linewidth=1, markersize=3)
        scc = (1 - mc_mean[0]) * model_output["a4"] * model_output["Scc"] + mc_mean[0] * t_model_output
        plt.loglog(unified_grid, scc, 'X', label="a4*Scc", linewidth=1,
                   markersize=3)
        plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
        plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
        title_str = "Individual contributions"
        plt.title(title_str)
        plt.legend()

        ax = plt.subplot(2, 2, 3, sharex=ax)

        plt.plot(t_model_grid, t_plot_output, 'X', label="Tetramer", linewidth=1, markersize=3)
        plt.axvline(t_q_min, linestyle="dashed", color="black", label="q min")
        plt.axvline(t_q_max, linestyle="dashed", color="black", label="q max")
        title_str = "Tetramer: Individual contributions"
        plt.title(title_str)
        plt.legend()

        if i == len(f_qr_short):
            fig.savefig(filament_directory + file_prefix + "all_optim_model_result.pdf", format="pdf")
        else:
            fig.savefig(filament_directory + file_prefix + f_file_names[i] + "_optim_model_result.pdf", format="pdf")

        # region Save results

        data_setting = {
            "qmin"         : q_min,
            "qmax"         : q_max,
            "lower_bound"  : lower_bound,
            "upper_bound"  : upper_bound,
            "initial_guess": initial_guess
        }
        if fix_paras:
            mc_mean_parameter = {
                "t"     : mc_mean[0],
                "R"     : mc_mean[1],
                "Rg"    : mc_mean[2],
                "gamma" : mc_mean[3],
                "beta"  : mc_mean[4],
                "lambda": 1.34,
                "dr"    : 0,
                "c"     : 0
            }
            mc_q15_parameter = {
                "t"     : mc_q15[0],
                "R"     : mc_q15[1],
                "Rg"    : mc_q15[2],
                "gamma" : mc_q15[3],
                "beta"  : mc_q15[4],
                "lambda": 1.34,
                "dr"    : 0,
                "c"     : 0
            }

            mc_q85_parameter = {
                "t"     : mc_q85[0],
                "R"     : mc_q85[1],
                "Rg"    : mc_q85[2],
                "gamma" : mc_q85[3],
                "beta"  : mc_q85[4],
                "lambda": 1.34,
                "dr"    : 0,
                "c"     : 0
            }
        else:
            mc_mean_parameter = {
                "t"     : mc_mean[0],
                "R"     : mc_mean[1],
                "Rg"    : mc_mean[2],
                "gamma" : mc_mean[3],
                "beta"  : mc_mean[4],
                "lambda": mc_mean[5],
                "dr"    : mc_mean[6],
                "c"     : mc_mean[7]

            }

            mc_q15_parameter = {
                "t"     : mc_q15[0],
                "R"     : mc_q15[1],
                "Rg"    : mc_q15[2],
                "gamma" : mc_q15[3],
                "beta"  : mc_q15[4],
                "lambda": mc_q15[5],
                "dr"    : mc_q15[6],
                "c"     : mc_q15[7]

            }

            mc_q85_parameter = {
                "t"     : mc_q85[0],
                "R"     : mc_q85[1],
                "Rg"    : mc_q85[2],
                "gamma" : mc_q85[3],
                "beta"  : mc_q85[4],
                "lambda": mc_q85[5],
                "dr"    : mc_q85[6],
                "c"     : mc_q85[7]

            }

        inf = {
            # "optimizer_options": optimizer_options,
            "mc_mean_parameter": mc_mean_parameter,
            "mc_q15_parameter": mc_q15_parameter,
            "mc_q85_parameter": mc_q85_parameter,
            "data_setting": data_setting,
            "problem": prob_info
        }
        if compute_det_result:
            inf["optimizer_result"] = {
                "final parameters"      : res.x.tolist(),
                "optimality condition"  : res.optimality,
                "function evaluations"  : res.nfev,
                "gradient evaluations"  : res.njev,
                "reason for termination": res.message
            }
        if i == len(f_qr_short):
            open_file = filament_directory + file_prefix + "all_opt_result.json"
        else:
            open_file = filament_directory + file_prefix + f_file_names[i] + "_opt_result.json"
        with open(open_file, 'w') as f:
            json.dump(inf, f, indent=4, sort_keys=True)
        # endregion

        fig = plt.figure(figsize=(16, 8))

        for _i in range(para_dimension):
            plt.subplot(para_dimension, 1, _i + 1)
            for m in range(chain_walker):
                if _i == 3 or _i == 7:
                    plt.semilogy(range(full_samples[m, :, _i].shape[0]), full_samples[m, :, _i], 'k')
                else:
                    plt.plot(range(full_samples[m, :, _i].shape[0]), full_samples[m, :, _i], 'k')
                plt.plot(range(full_samples[m, :, _i].shape[0]), [mc_mean[_i]] * (full_samples[m, :, _i].shape[0]), '-r')
                plt.ylabel(labels[_i])
            plt.axvline(burn_in)
        if i == len(f_qr_short):
            fig.savefig(filament_directory + file_prefix + "all_mcmc_chain_runner.pdf", format="pdf")
        else:
            fig.savefig(filament_directory + file_prefix + f_file_names[i] + "_mcmc_chain_runner.pdf", format="pdf")