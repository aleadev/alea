from __future__ import division, print_function
import numpy as np
from alea.application.bayes.paper_radialtrafo.BioPhy.BioPhy_model import get_json_files_from_directory
from BioPhy_model import BrennichModelTrimmed, BrennichModelPaper, BrennichModelFull, BrennichModelMeanTrimmed
from BioPhy_optimizer import BashinHopping, DualAnnealing, DifferentialEvolution, Minimizer, LeastSquares

import json
import argparse
import re
from itertools import product as iter_product
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="BioPhy model optimizer")
parser.add_argument("dir", metavar="dir", type=str)
args = parser.parse_args()

directory = args.dir

if directory == "KCl/":
    directory_list = ["10", "20", "30", "40", "50", "80", "100", "150"]
elif directory == "CaCl2/":
    directory_list = ["0_5", "1_5", "1_", "2_0", "2_5", "4", "5", "10"]
elif directory == "CoCl3/":
    directory_list = ["0_1", "0_01", "0_2", "0_02", "0_03", "0_5", "0_05", "0_08", "1_0", "1_5", "2_0", "2_5"]
elif directory == "MgCl2/":
    directory_list = ["0_5", "1_0", "1_5", "2_0", "2_5", "4", "5", "10"]
elif directory == "NaCl/":
    directory_list = ["10", "20", "30", "40", "50", "80", "100", "150"]
elif directory == "Sp4/":
    directory_list = ["0_1", "0_01", "0_001", "0_02", "0_002", "0_03", "0_003", "0_04", "0_05", "0_005", "0_008", "0_08", "0_09"]
else:
    raise ValueError("unknown directory {}. Use KCl, CaCl2, CoCl3, MgCl2, NaCl, Sp4".format(directory))

directory_list = [directory + d + "mM/" for d in directory_list]
# print(directory_list)

problem_dict = {
    "Sp4/0_1mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.1},
    "Sp4/0_01mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.01},
    "Sp4/0_001mM/" : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.001},
    "Sp4/0_02mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.02},
    "Sp4/0_002mM/" : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.002},
    "Sp4/0_03mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.03},
    "Sp4/0_003mM/" : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.003},
    "Sp4/0_04mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.04},
    "Sp4/0_05mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.05},
    "Sp4/0_005mM/" : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.005},
    "Sp4/0_008mM/" : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.008},
    "Sp4/0_08mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.08},
    "Sp4/0_09mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "Sp4", "concentration": 0.09},

    "KCl/10mM/"    : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 10},
    "KCl/20mM/"    : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 20},
    "KCl/30mM/"    : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 30},
    "KCl/40mM/"    : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 40},
    "KCl/50mM/"    : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 50},
    "KCl/80mM/"    : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 80},
    "KCl/100mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 100},
    "KCl/150mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "KCl", "concentration": 150},

    "NaCl/10mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 10},
    "NaCl/20mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 20},
    "NaCl/30mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 30},
    "NaCl/40mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 40},
    "NaCl/50mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 50},
    "NaCl/80mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 80},
    "NaCl/100mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 100},
    "NaCl/150mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "NaCl", "concentration": 150},

    "MgCl2/0_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 0.5},
    "MgCl2/1_0mM/" : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 1.0},
    "MgCl2/1_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 1.5},
    "MgCl2/2_0mM/" : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 2},
    "MgCl2/2_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 2.5},
    "MgCl2/4mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 4.0},
    "MgCl2/5mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 5.0},
    "MgCl2/10mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "MgCl2", "concentration": 10.0},

    "CaCl2/0_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 0.5},
    "CaCl2/1_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 1.5},
    "CaCl2/1_mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 1.0},
    "CaCl2/2_0mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 2.0},
    "CaCl2/2_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 2.5},
    "CaCl2/4mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 4.0},
    "CaCl2/5mM/"   : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 5.0},
    "CaCl2/10mM/"  : {"q_min": 0.08, "q_max": 1, "salt": "CaCl2", "concentration": 10.0},

    "CoCl3/0_1mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.1},
    "CoCl3/0_01mM/": {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.01},
    "CoCl3/0_2mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.2},
    "CoCl3/0_02mM/": {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.02},
    "CoCl3/0_03mM/": {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.03},
    "CoCl3/0_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.5},
    "CoCl3/0_05mM/": {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.05},
    "CoCl3/0_08mM/": {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 0.08},
    "CoCl3/1_0mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 1.0},
    "CoCl3/1_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 1.5},
    "CoCl3/2_0mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 2.0},
    "CoCl3/2_5mM/" : {"q_min": 0.08, "q_max": 1, "salt": "CoCl3", "concentration": 2.5},
}

optimizer = [LeastSquares, Minimizer] #, DifferentialEvolution]
models = [BrennichModelTrimmed(), BrennichModelFull(), BrennichModelTrimmed("anything"), BrennichModelFull("anything")]
models = [BrennichModelTrimmed("anything"), BrennichModelMeanTrimmed("anything")]
problem_list = [(item[0], item[1], item[2]) for item in iter_product(*[models, optimizer, directory_list])]

R_list = dict()
Rg_list = dict()
gamma_list = dict()
beta_list = dict()
conc_list = dict()
t_list = dict()
m_list = dict()
o_list = dict()
salt_list = dict()
identifier_list = []
for item in problem_list:
    m, o, filament_directory = item[0], item[1], item[2]
    prob_info = problem_dict[filament_directory]
    identifier = "_{}_{}".format(m.model_name, o.__name__)
    identifier_list.append(identifier)
    if identifier not in conc_list:
        conc_list[identifier] = []
        R_list[identifier] = []
        Rg_list[identifier] = []
        t_list[identifier] = []
        gamma_list[identifier] = []
        beta_list[identifier] = []
        m_list[identifier] = []
        o_list[identifier] = []
        salt_list[identifier] = ""
    files, file_names = get_json_files_from_directory(filament_directory)

    for i in range(len(files)):
        file_count = len(file_names)
        for name in file_names:
            if identifier not in name:
                file_count -= 1
        if identifier not in file_names[i]:
            continue
        print("open {}".format(files[i]))
        with open(files[i], 'r') as f:
            text_data = f.read()                    # only a hack since the json is wrongly created
            # text_data = '[' + re.sub(r'\}{', '},{', text_data) + ']'
            inf = json.loads(text_data)
        inf = inf[0]
        t = inf["final parameters"][0] if m.tetramer is not None else 0
        R = inf["final parameters"][1 if m.tetramer is not None else 0]
        Rg = inf["final parameters"][2 if m.tetramer is not None else 1]
        gamma = 0
        beta = 0
        if ("BrennichModelTrimmed" in m.model_name or "BrennichModelFull" in m.model_name
                or "BrennichModelMean" in m.model_name):
            gamma = inf["final parameters"][3 if m.tetramer is not None else 2]
            beta = inf["final parameters"][4 if m.tetramer is not None else 3]

        salt, conc = prob_info["salt"], prob_info["concentration"]
        print(" {} {}mM measurement {}: R={}, Rg={}, t={}% tetramer left".format(salt, conc, file_names[i], R, Rg, t))

        if "all_opt_result" in file_names[i] or file_count == 1:
            conc_list[identifier].append(conc)
            R_list[identifier].append(R)
            Rg_list[identifier].append(Rg)
            gamma_list[identifier].append(gamma)
            beta_list[identifier].append(beta)
            t_list[identifier].append(t)
            m_list[identifier].append(m.model_name)
            o_list[identifier].append(o.__name__)
            salt_list[identifier] = salt

for identifier in identifier_list:

    fig, ax1 = plt.subplots(figsize=(10, 5))

    ax1.plot(conc_list[identifier], R_list[identifier], 'ro')
    ax1.set_xlabel('concentration')
    # Make the y-axis label, ticks and tick labels match the line color.
    ax1.set_ylabel('R', color='r')
    ax1.tick_params('y', colors='r')

    ax2 = ax1.twinx()
    ax2.plot(conc_list[identifier], Rg_list[identifier], 'b*')
    ax2.set_ylabel('Rg', color='b')
    ax2.tick_params('y', colors='b')

    fig.tight_layout()
    fig.savefig(directory + "r_rg_result_{}.pdf".format(identifier), format="pdf")
    fig.clf()
    plt.close(fig)

    type_check = m_list[identifier]
    if isinstance(type_check, list):
        model_check = type_check[0]
        if "tetramer" in model_check:
            fig = plt.figure()
            x_args = np.argsort(conc_list[identifier])
            plt.semilogy(np.array(conc_list[identifier])[x_args], np.array(t_list[identifier])[x_args])
            plt.xlabel('concentration')
            plt.ylabel("tetramer concentration")
            fig.savefig(directory + "t_result_log_{}.pdf".format(identifier), format="pdf")
            fig.clf()
            plt.close(fig)

            fig = plt.figure()
            x_args = np.argsort(conc_list[identifier])
            plt.plot(np.array(conc_list[identifier])[x_args], np.array(t_list[identifier])[x_args])
            plt.xlabel('concentration')
            plt.ylabel("tetramer concentration")
            fig.savefig(directory + "t_result_{}.pdf".format(identifier), format="pdf")
            fig.clf()
            plt.close(fig)
        if ("BrennichModelTrimmed" in model_check or "BrennichModelFull" in model_check
                or "BrennichModelMean" in model_check):
            fig, ax1 = plt.subplots(figsize=(10, 5))

            ax1.plot(conc_list[identifier], gamma_list[identifier], 'ro')
            ax1.set_xlabel('concentration')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('gamma', color='r')
            ax1.tick_params('y', colors='r')

            ax2 = ax1.twinx()
            ax2.plot(conc_list[identifier], beta_list[identifier], 'b*')
            ax2.set_ylabel('beta', color='b')
            ax2.tick_params('y', colors='b')

            fig.tight_layout()
            fig.savefig(directory + "g_b_result_{}.pdf".format(identifier), format="pdf")
            fig.clf()
            plt.close(fig)

            inf = {
                "concentration": conc_list[identifier],
                "R": R_list[identifier],
                "Rg": Rg_list[identifier],
                "gamma": gamma_list[identifier],
                "beta": beta_list[identifier],
                "t": t_list[identifier],
                "model": m_list[identifier],
                "optimizer": o_list[identifier],
                "salt": salt_list[identifier]
            }
            with open(directory + "parameter_{}.json".format(identifier), "w") as f:
                json.dump(inf, f, indent=4)
