from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quadrature
from scipy.signal import savgol_filter
from scipy.stats import linregress

directory = "KCl/twomodel/tetramer/tetramer.dat"


def read_file(_file_str):
    """
    Reads the Data.dat files as provided. Very restrictive in format.
    :param _file_str: path to the file including suffix. example: "data/Data.dat"
    :return: dictionary containing "qr", "intensity" and "error"
    """
    retval = {
        "qr"       : [],
        "intensity": [],
        "error"    : []
    }
    with open(_file_str, 'r') as _f:
        for (lib, line) in enumerate(_f):
            line_arr = line.split("\t")
            for lic in range(len(line_arr)):
                line_arr[lic] = line_arr[lic].strip().strip("\n")
            if lib == 0:
                assert len(line_arr) == 4
                assert (line_arr[0] == "qr")
                assert (line_arr[1] == "Intensity")
                assert (line_arr[2] == "Error")
                continue
            retval["qr"].append(float(line_arr[0]))
            retval["intensity"].append(float(line_arr[1]))
            retval["error"].append(float(line_arr[2]))
    retval["qr"] = np.array(retval["qr"])
    retval["intensity"] = np.array(retval["intensity"])
    retval["error"] = np.array(retval["error"])
    return retval


# endregion

data = read_file(directory)
x = data['qr']
y = data['intensity']

idx_mid_start = np.where(x > 0.09)[0][0]
print(idx_mid_start)
idx_mid_end = np.where(x > 1.1)[0][0]
print(idx_mid_end)

x_curve = x[idx_mid_start:idx_mid_end]
y_curve = y[idx_mid_start:idx_mid_end]

# y_smooth = savgol_filter(y_curve,5, 2, deriv=1, mode = 'nearest')
# min_idx = np.where(y_smooth==min(y_smooth))
x_fit = np.where(x_curve > 1)[0][0]

poly_deg = 10

fit = np.polyfit(x_curve[:x_fit], y_curve[:x_fit], deg=poly_deg)

fit_fn = np.poly1d(fit)

slope = np.polyder(fit_fn)
slope_mean = quadrature(slope, x_curve[0], x_curve[x_fit])[0] * 1 / (x_curve[x_fit] - x_curve[0])
curvature = np.polyder(slope)

print("mean of slope={}".format(slope_mean))
print("max of slope={}".format(np.max([np.abs(slope(np.linspace(x_curve[0], x_curve[x_fit], num=1000)))])))
# N = 10000
# samples = np.random.uniform(x_curve[0], x_curve[x_fit], N)
# print("sampling mean={}".format(1/N*np.sum([slope(y) for y in samples])))

plt.figure(figsize=(14, 4))
plt.scatter(x_curve, y_curve, label="Intensity")
# plt.scatter(x_curve,y_smooth)
# plt.scatter(x_curve[min_idx],y_smooth[min_idx], c='r',s=25,zorder=1)
plt.plot(x_curve, fit_fn(x_curve), c='m', label="Poly Fit deg={}".format(poly_deg))
plt.axvline(x_curve[0], linestyle="dashed", color="black");
plt.axvline(x_curve[x_fit], linestyle="dashed", color="black")
# plt.ylim(-0.01,0.15)
plt.xlim(0.1, 1)
plt.xlabel("q")
plt.ylabel("I(q)")
plt.legend()

plt.show()

plt.figure(figsize=(14, 4))
plt.plot(x_curve[:x_fit], slope(x_curve[:x_fit]), label="Der. Poly fit")
plt.xlabel("q")
plt.ylabel("dI/dq")
plt.xlim(0.1, 1)
plt.legend()
plt.show()
plt.figure(figsize=(14, 4))
plt.plot(x_curve[:x_fit], curvature(x_curve[:x_fit]), label="Der.^2 Poly fit")
plt.xlabel("q")
plt.ylabel("d^2I/dq^2")
plt.xlim(0.1, 1)
plt.legend()
plt.show()