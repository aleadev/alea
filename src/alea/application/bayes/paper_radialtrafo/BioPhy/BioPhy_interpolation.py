from __future__ import division
from functools import partial
import matplotlib.pyplot as plt
import numpy as np


def GuinierModel(q, _I0, _Rg):
    return _I0 / q * np.exp(-0.5 * _Rg ** 2 * q ** 2)


def fit_data(x_data, y_data,
             method="polyfit",
             poly_deg=None,
             spline_type=None,
             spline_kind=None,
             spline_smoothing=None,
             ):
    from functools import partial
    import numpy as np

    fit_type = method
    assert fit_type == "polyfit" or fit_type == "spline" or fit_type == "difference"
    if fit_type == "polyfit" or fit_type == "difference":
        assert poly_deg is not None
        fit_fn = np.poly1d(np.polyfit(x_data, y_data, deg=poly_deg))
        slope = np.polyder(fit_fn)
        curvature = np.polyder(slope)
    elif fit_type == "spline":
        if spline_type is None:
            spline_type = "interpolated"
        if spline_type == "interpolated":
            assert spline_kind is not None
            assert 2 <= spline_kind <= 5
            from scipy.interpolate import InterpolatedUnivariateSpline as spline_variant
            spline_variant = partial(spline_variant, k=spline_kind)
        elif spline_type == "smooth":
            assert spline_smoothing is not None
            assert spline_kind is not None
            assert 2 <= spline_kind <= 5
            from scipy.interpolate import UnivariateSpline as spline_variant
            spline_variant = partial(spline_variant, s=spline_smoothing, k=spline_kind)
        elif spline_type == "akima":
            from scipy.interpolate import Akima1DInterpolator as spline_variant
        elif spline_type == "pchip":
            from scipy.interpolate import PchipInterpolator as spline_variant
        else:
            raise ValueError("Unknown Spline type: use {interpolated, smooth, akima, pchip}")
        spline = spline_variant(x_data, y_data)
        fit_fn = lambda x: spline(x)
        slope = lambda x: spline.derivative(1)(x)
        curvature = lambda x: spline.derivative(2)(x)

    if fit_type == "difference":
        _f = np.diff(y_data)
        dx = np.diff(x_data)[::-1]
        cf = _f / dx
        assert poly_deg is not None
        slope = np.poly1d(np.polyfit(x_data[:-1], cf, deg=poly_deg))
        curvature = np.polyder(slope)

    return fit_fn, slope, curvature


if __name__ == "__main__":
    _I0 = 0.01
    _Rg = 2
    test_fun = partial(GuinierModel, _I0=_I0, _Rg=_Rg)
    test_fun_slope = lambda _x: 1 / _x * test_fun(_x) * (-_Rg ** 2 * _x ** 2 - 1)
    test_fun_curvature = lambda _x: 1 / (_x ** 2) * test_fun(_x) * (-_Rg ** 4 * _x ** 4 + _Rg ** 2 * _x ** 2 + 2)

    function_fit_options_fit = {
        "method"  : "polyfit",
        "poly_deg": 10
    }
    function_fit_options_spline = {
        "method"          : "spline",
        "spline_type"     : "interpolated",
        "spline_kind"     : 3,
        "spline_smoothing": 10
    }
    function_fit_options_diff = {
        "method"  : "difference",
        "poly_deg": 10
    }

    x = np.linspace(0.1, 1, num=100)
    y = test_fun(x)
    y_slope = test_fun_slope(x)
    y_curv = test_fun_curvature(x)
    fit_1, slope_1, curvature_1 = fit_data(x, y, **function_fit_options_fit)
    fit_2, slope_2, curvature_2 = fit_data(x, y, **function_fit_options_spline)
    fit_3, slope_3, curvature_3 = fit_data(x, y, **function_fit_options_diff)

    plt.figure(figsize=(14, 12))
    plt.subplot(311)
    plt.scatter(x, y, s=4, color="g", label="True fun")
    plt.plot(x, fit_1(x), '-r', label="poly deg {}".format(function_fit_options_fit["poly_deg"]))
    plt.plot(x, fit_2(x), '--b', label="spline kind {}".format(function_fit_options_spline["spline_kind"]))
    plt.plot(x, fit_3(x), 'xm', label="finite diff + poly deg{}".format(function_fit_options_diff["poly_deg"]))
    plt.title("original function")
    plt.legend()

    plt.subplot(312)
    plt.scatter(x, y_slope, s=4, color="g", label="True slope")
    plt.plot(x, slope_1(x), '-r', label="poly deg {}".format(function_fit_options_fit["poly_deg"]))
    plt.plot(x, slope_2(x), '--b', label="spline kind {}".format(function_fit_options_spline["spline_kind"]))
    plt.plot(x, slope_3(x), 'xm', label="finite diff + poly deg{}".format(function_fit_options_diff["poly_deg"]))
    plt.title("slope")
    plt.legend()

    plt.subplot(313)
    plt.scatter(x, y_curv, s=4, color="g", label="True curvature")
    plt.plot(x, curvature_1(x), '-r', label="poly deg {}".format(function_fit_options_fit["poly_deg"]))
    plt.plot(x, curvature_2(x), '--b', label="spline kind {}".format(function_fit_options_spline["spline_kind"]))
    plt.plot(x, curvature_3(x), 'xm', label="finite diff + poly deg{}".format(function_fit_options_diff["poly_deg"]))
    plt.title("curvature")
    plt.legend()
    plt.show()

    err_f_1 = np.linalg.norm(y - fit_1(x)) / np.linalg.norm(y)
    err_f_2 = np.linalg.norm(y_slope - slope_1(x)) / np.linalg.norm(y)
    err_f_3 = np.linalg.norm(y_curv - curvature_1(x)) / np.linalg.norm(y)

    err_s_1 = np.linalg.norm(y - fit_2(x)) / np.linalg.norm(y)
    err_s_2 = np.linalg.norm(y_slope - slope_2(x)) / np.linalg.norm(y)
    err_s_3 = np.linalg.norm(y_curv - curvature_2(x)) / np.linalg.norm(y)

    err_d_1 = np.linalg.norm(y - fit_3(x)) / np.linalg.norm(y)
    err_d_2 = np.linalg.norm(y_slope - slope_3(x)) / np.linalg.norm(y)
    err_d_3 = np.linalg.norm(y_curv - curvature_3(x)) / np.linalg.norm(y)

    print("relative error of approximations of order 0 to 2:")
    print("  polyfit:")
    print("          order 0 = {}".format(err_f_1))
    print("          order 1 = {}".format(err_f_2))
    print("          order 2 = {}".format(err_f_3))
    print("  splines:")
    print("          order 0 = {}".format(err_s_1))
    print("          order 1 = {}".format(err_s_2))
    print("          order 2 = {}".format(err_s_3))
    print("  finite diff.:")
    print("          order 0 = {}".format(err_d_1))
    print("          order 1 = {}".format(err_d_2))
    print("          order 2 = {}".format(err_d_3))
