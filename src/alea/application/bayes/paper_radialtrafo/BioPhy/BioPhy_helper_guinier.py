from __future__ import division, print_function
import numpy as np
from functools import partial
WIAS_CLUSTER = False
if WIAS_CLUSTER:
    import matplotlib
    matplotlib.use('Agg')
import matplotlib.pyplot as plt


def read_file(_file_str, intensity_mult_factor=1.0):
    """
    Reads the Data.dat files as provided. Very restrictive in format.
    :param _file_str: path to the file including suffix. example: "data/Data.dat"
    :return: dictionary containing "qr", "intensity" and "error"
    """
    retval = {
               "qr": [],
               "intensity": [],
               "error": []
             }
    with open(_file_str, 'r') as _f:
        for (lib, line) in enumerate(_f):
            line_arr = line.split("\t")
            # print(line_arr)
            for lic in range(len(line_arr)):
                line_arr[lic] = line_arr[lic].strip().strip("\n")
            if lib == 0:

                if len(line_arr) == 4:
                    assert (line_arr[0] == "qr")
                    assert (line_arr[1] == "Intensity")
                    assert (line_arr[2] == "Error")
                elif len(line_arr) == 7:
                    assert (line_arr[0] == "qr")
                    assert (line_arr[3] == "Intensity")
                    assert (line_arr[5] == "error")
                continue
            retval["qr"].append(float(line_arr[0]))
            retval["intensity"].append(float(line_arr[1])*intensity_mult_factor)
            retval["error"].append(float(line_arr[2]))
    retval["qr"] = np.array(retval["qr"])
    retval["intensity"] = np.array(retval["intensity"])
    retval["error"] = np.array(retval["error"])
    return retval


def trim_data(data, q_min, q_max):
    retval = {
        "qr": data["qr"][np.where((q_min < data["qr"]) & (data["qr"] < q_max))],
        "intensity": data["intensity"][np.where((q_min < data["qr"]) & (data["qr"] < q_max))],
        "error": data["error"][np.where((q_min < data["qr"]) & (data["qr"] < q_max))],
        "qr_trun_left": data["qr"][np.where(q_min < data["qr"])],
        "intensity_trun_left": data["intensity"][np.where(q_min < data["qr"])],
        "error_trun_left": data["error"][np.where(q_min < data["qr"])]
    }
    return retval


def read_lists_from_data(used_files, q_min, q_max, intensity_mult_factor=1.0):
    qr = []
    intensity = []
    error = []
    qr_short = []
    qr_trun_left = []
    intensity_short = []
    intensity_trun_left = []
    error_trun_left = []
    error_short = []
    for lia in range(len(used_files)):
        data = read_file(used_files[lia], intensity_mult_factor)
        data_small = trim_data(data, q_min, q_max)
        qr.append(data["qr"])
        qr_trun_left.append(data_small["qr_trun_left"])
        qr_short.append(data_small["qr"])
        intensity.append(data["intensity"])
        intensity_trun_left.append(data_small["intensity_trun_left"])
        intensity_short.append(data_small["intensity"])

        error.append(data["error"])
        error_trun_left.append(data_small["error_trun_left"])
        error_short.append(data_small["error"])
    retval = {
        "qr": qr,
        "qr_trun_left": qr_trun_left,
        "qr_short": qr_short,
        "intensity": intensity,
        "intensity_trun_left": intensity_trun_left,
        "intensity_short": intensity_short,
        "error": error,
        "error_trun_left": error_trun_left,
        "error_short": error_short
    }
    return retval


def get_files_from_directory(directory):
    from os import walk

    files = []
    files_name = []
    for (dirpath, dirnames, filenames) in walk(directory):
        for _file in filenames:
            if not _file.endswith(".dat"):
                continue
            files.append(directory + _file)
            files_name.append(_file[:-4])
        break
    return files, files_name


def guinier_model(q, _i0, _rg):
    return _i0/q*np.exp(-0.5*_rg**2*q**2)


def loss_diff(theta, x, delta, err, *args, **kwargs):
    return (delta - guinier_model(x, *theta))/err


def loss(theta, _qr, _intensity, _error, *args, **kwargs):
    return loss_diff(theta, _qr, _intensity, _error)
    # return 1/curr_err*(curr_int - guinier_model(curr_qr, *theta))


def loss_trafo(theta, _qr, _intensity, _error, max_value=1, p=2):
    curr_qr = _qr[_qr <= max_value]
    curr_int = _intensity[_qr <= max_value]
    curr_err = _error[_qr <= max_value]
    return loss_diff(theta, curr_qr/theta[1], curr_int, curr_err)/(np.abs(theta[1])**(1/p))


def loss_functional(theta, args):
    _lambda = 0
    mu = 0
    curr_loss = loss(theta, **args)

    # print("I0={:.2E}, Rg={:.2E}, loss={:.2E}, points={}".format(theta[0], theta[1], np.linalg.norm(curr_loss),
    #                                                             len(curr_loss)))
    # while len(curr_loss) <= 7:
    #     theta[1] = np.random.uniform(0, 30)
    #     print("  Rg={}".format(theta[1]))
    #     curr_loss = loss(theta, **args)
    if "p" not in args:
        p = 2
    else:
        p = args["p"]
    return np.linalg.norm(curr_loss, ord=p) + _lambda * np.linalg.norm(theta)**2 + mu/(len(curr_loss)+1)


def fit_data(x_data, y_data,
             method="polyfit",
             poly_deg=None,
             spline_type=None,
             spline_kind=None,
             spline_smoothing=None,
             ):

    fit_type = method
    assert fit_type == "polyfit" or fit_type == "spline" or fit_type == "difference"
    if fit_type == "polyfit" or fit_type == "difference":
        assert poly_deg is not None
        fit_fn = np.poly1d(np.polyfit(x_data, y_data, deg=poly_deg))
        slope = np.polyder(fit_fn)
        curvature = np.polyder(slope)
    elif fit_type == "spline":
        if spline_type is None:
            spline_type = "interpolated"
        if spline_type == "interpolated":
            assert spline_kind is not None
            assert 2 <= spline_kind <= 5
            from scipy.interpolate import InterpolatedUnivariateSpline as spline_variant
            spline_variant = partial(spline_variant, k=spline_kind)
        elif spline_type == "smooth":
            assert spline_smoothing is not None
            assert spline_kind is not None
            assert 2 <= spline_kind <= 5
            from scipy.interpolate import UnivariateSpline as spline_variant
            spline_variant = partial(spline_variant, s=spline_smoothing, k=spline_kind)
        elif spline_type == "akima":
            from scipy.interpolate import Akima1DInterpolator as spline_variant
        elif spline_type == "pchip":
            from scipy.interpolate import PchipInterpolator as spline_variant
        else:
            raise ValueError("Unknown Spline type: use {interpolated, smooth, akima, pchip}")
        spline = spline_variant(x_data, y_data)
        fit_fn = lambda x: spline(x)
        slope = lambda x: spline.derivative(1)(x)
        curvature = lambda x: spline.derivative(2)(x)
    else:
        raise ValueError("unknown fit type")

    if fit_type == "difference":
        _f = np.diff(y_data)
        dx = np.diff(x_data)[::-1]
        cf = _f / dx
        assert poly_deg is not None
        slope = np.poly1d(np.polyfit(x_data[:-1], cf, deg=poly_deg))
        curvature = np.polyder(slope)

    return fit_fn, slope, curvature


def plot_guinier(qr, intensity, grid, x_opt, path, error=None, marker=".", marker_size=4, marker_edge_color="m",
                 suffix="pdf"):

    fig = plt.figure(figsize=(6, 5))
    ax = plt.subplot(111)
    ax.scatter(qr ** 2, qr * intensity, marker=marker, s=marker_size,
               facecolor='lightgrey', edgecolor=marker_edge_color,
               label="intensity")
    if error is not None:
        plt.semilogy(qr ** 2, error, '--b', label="error")
    plt.semilogy(grid ** 2, grid * guinier_model(grid, *x_opt), "-b",
                 markersize=1, label="Guinier fit")
    plt.xlabel(r"$q^2$")
    plt.ylabel(r"$q*I(q)$")
    plt.axvline(qr[0] ** 2, linestyle="dashed", color="black")
    plt.axvline(qr[-1] ** 2, linestyle="dashed", color="black")
    plt.title(r"$Rg\times q^\ast={:.2f}$, Optimized parameter:  $I_0={:.2f}, Rg={:.2f}$".format(qr[-1]*np.abs(x_opt[1]), *x_opt))
    plt.legend()
    fig.savefig(path + "." + suffix)
    fig.clf()
    plt.close(fig)


def plot_scatter_fun(x, fun, path, y=None, x_min=None, x_max=None, marker=".", marker_size=4, marker_edge_color="m",
                     label="", x_label="q", y_label="f(q)", suffix="pdf", legend="nodes"):
    fig = plt.figure(figsize=(7, 5))
    if y is not None:
        plt.scatter(x, y, label=legend, marker=marker, s=marker_size, facecolor='lightgrey',
                    edgecolor=marker_edge_color)
    plt.plot(x, fun(x), '-b', label="fit")
    if x_min is not None:
        plt.axvline(x_min, linestyle="dashed", color="black")
    if x_max is not None:
        plt.axvline(x_max, linestyle="dashed", color="black")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(label)
    plt.legend()
    fig.savefig(path + "." + suffix)
    fig.clf()
    plt.close(fig)

    fig = plt.figure(figsize=(7, 5))
    if y is not None:
        plt.scatter(x, y, label=legend, marker=marker, s=marker_size, facecolor='lightgrey',
                    edgecolor=marker_edge_color)
    plt.loglog(x, fun(x), '-b', label="fit")
    if x_min is not None:
        plt.axvline(x_min, linestyle="dashed", color="black")
    if x_max is not None:
        plt.axvline(x_max, linestyle="dashed", color="black")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(label)
    plt.legend()
    fig.savefig(path + "_loglog." + suffix)
    fig.clf()
    plt.close(fig)


def smooth(a, window_size):
    # a: NumPy 1-D array containing the data to be smoothed
    # window_size: smoothing window size needs, which must be odd number,
    #              as in the original MATLAB implementation
    out0 = np.convolve(a, np.ones(window_size, dtype=int), 'valid') / window_size
    r = np.arange(1, window_size - 1, 2)
    start = np.cumsum(a[:window_size - 1])[::2] / r
    stop = (np.cumsum(a[:-window_size:-1])[::2] / r)[::-1]
    return np.concatenate((start, out0, stop))