from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.integrate import quadrature
from scipy.signal import savgol_filter
from scipy.stats import linregress
from BioPhy_interpolation import fit_data

directory = "KCl/twomodel/tetramer/tetramer.dat"


def read_file(_file_str):
    """
    Reads the Data.dat files as provided. Very restrictive in format.
    :param _file_str: path to the file including suffix. example: "data/Data.dat"
    :return: dictionary containing "qr", "intensity" and "error"
    """
    retval = {
        "qr"       : [],
        "intensity": [],
        "error"    : []
    }
    with open(_file_str, 'r') as _f:
        for (lib, line) in enumerate(_f):
            line_arr = line.split("\t")
            for lic in range(len(line_arr)):
                line_arr[lic] = line_arr[lic].strip().strip("\n")
            if lib == 0:
                assert len(line_arr) == 4
                assert (line_arr[0] == "qr")
                assert (line_arr[1] == "Intensity")
                assert (line_arr[2] == "Error")
                continue
            retval["qr"].append(float(line_arr[0]))
            retval["intensity"].append(float(line_arr[1]))
            retval["error"].append(float(line_arr[2]))
    retval["qr"] = np.array(retval["qr"])
    retval["intensity"] = np.array(retval["intensity"])
    retval["error"] = np.array(retval["error"])
    return retval


# endregion

data = read_file(directory)
x = data['qr']
y = data['intensity']

idx_whole_start = np.where(x > 0.09)[0][0]
print(idx_whole_start)
idx_whole_end = np.where(x > 1.1)[0][0]
print(idx_whole_end)

x_whole_curve = x[idx_whole_start:idx_whole_end]
y_whole_curve = y[idx_whole_start:idx_whole_end]

idx_mid_start = np.where(x > 0.2)[0][0]
print(idx_mid_start)
idx_mid_end = np.where(x > 0.5)[0][0]
print(idx_mid_end)

x_mid_curve = x[idx_mid_start:idx_mid_end]
y_mid_curve = y[idx_mid_start:idx_mid_end]

fit_options = {
    "method"          : "polyfit",
    "poly_deg"        : 10,
    "spline_type"     : "smooth",
    "spline_kind"     : 3,
    "spline_smoothing": 10
}

x_fit = np.where(x_whole_curve > 1)[0][0]

fit_fn, slope, curvature = fit_data(x_whole_curve[:x_fit], y_whole_curve[:x_fit], **fit_options)

plt.figure(figsize=(14, 4))
plt.scatter(x_whole_curve, y_whole_curve, label="Intensity")
plt.plot(x_whole_curve, fit_fn(x_whole_curve), c='m', label="spline")
plt.axvline(x_whole_curve[0], linestyle="dashed", color="black")
plt.axvline(x_whole_curve[x_fit], linestyle="dashed", color="black")
plt.xlim(0.1, 1)
plt.xlabel("q")
plt.ylabel("I(q)")
plt.legend()
plt.show()

plt.figure(figsize=(14, 4))
plt.plot(x_whole_curve[:x_fit], slope(x_whole_curve[:x_fit]), label="Der. fit")
plt.xlabel("q")
plt.ylabel("dI/dq")
plt.xlim(0.1, 1)
plt.legend()
plt.show()

plt.figure(figsize=(14, 4))
plt.plot(x_whole_curve[:x_fit], curvature(x_whole_curve[:x_fit]), label="Der.^2  fit")
plt.xlabel("q")
plt.ylabel("d^2I/dq^2")
plt.xlim(0.1, 1)
plt.legend()
plt.show()

