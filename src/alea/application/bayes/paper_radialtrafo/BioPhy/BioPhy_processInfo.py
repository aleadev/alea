from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from BioPhy_model import read_lists_from_data, get_files_from_directory, get_json_files_from_directory
from scipy.optimize import minimize
from scipy.integrate import quadrature
from BioPhy_helper_guinier import loss_functional
from BioPhy_model import (BrennichModelTrimmed, BrennichModelPaper, BrennichModelFull, BrennichModelMeanTrimmed,
                          get_tetramer_spline, BrennichModelTrimmedLambda)
from BioPhy_optimizer import BashinHopping, DualAnnealing, DifferentialEvolution, Minimizer, LeastSquares

from alea.application.bayes.paper_radialtrafo.BioPhy.BioPhy_helper_guinier import smooth
import json
from itertools import product as iter_product
import argparse

with open("problem_info_lambda.json") as f:
    problem_dict = json.load(f)

tet_path = "/home/fenics/shared/alea/src/alea/application/bayes/paper_radialtrafo/BioPhy/tetramer/1mg_modified/spline_data.json"
tetramer = get_tetramer_spline(tet_path)
# tetramer = None
optimizer = DifferentialEvolution
intensity_mult_factor = 2.64

#steps = ["create", "process", "write", "visualize"]
steps = ["process", "write", "visualize"]

# models = [BrennichModelTrimmed(), BrennichModelFull(), BrennichModelTrimmed(tetramer), BrennichModelFull(tetramer)]
# models = [BrennichModelTrimmedLambda(None, _lambda=l)] #, BrennichModelMeanTrimmed(tetramer)]

for step in steps:
    if step == "create":
        # TODO: create problem info. cf. BioPhy_CreateProblemInfo.py
        pass
    if step == "process":
        fig_extension = "png"
        marker_edge_color = "rgbcyk"
        marker = "v><^12"
        marker_size = 5
        smoothing_window = 0
        for directory, item in problem_dict.items():
            used_files, file_names = get_files_from_directory(directory)
            q_min = item["q_min"]
            q_max = item["q_max"]

            unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)
            if len(used_files) <= len(marker):
                print("WARNING: to few marker selected")

            # region Read given data files
            file_dict = read_lists_from_data(used_files, q_min, q_max, intensity_mult_factor)
            qr, qr_short = file_dict["qr"], file_dict["qr_short"]
            intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
            if smoothing_window > 0:
                for lia in range(len(intensity)):
                    intensity[lia] = smooth(intensity[lia], smoothing_window)
                    intensity_short[lia] = smooth(intensity_short[lia], smoothing_window)
            error, error_short = file_dict["error"], file_dict["error_short"]
            # endregion

            assert len(qr_short) > 0
            num_iter = 0
            for i in range(len(qr_short)+1):
                num_iter += 1
                m = BrennichModelTrimmedLambda(tetramer, _lambda=item["lambda"])
                # m = BrennichModelTrimmedLambda(tetramer, _lambda=1.34375)
                o = optimizer
                print("Directory: {} Run {}".format(directory, num_iter))
                if len(qr_short) == 1 and i == 1:
                    print("  All run left out")
                    continue
                if i == len(qr_short):
                    curr_qr = np.array([_y for _x in qr_short for _y in _x])
                    curr_inten = np.array([_y for _x in intensity_short for _y in _x])
                    curr_err = np.array([_y for _x in error_short for _y in _x])
                    print("  All run with length {}".format(len(curr_qr)))
                else:
                    curr_qr = qr_short[i]
                    curr_inten = intensity_short[i]
                    curr_err = error_short[i]
                    print("  Dataset {} with length {}".format(file_names[i], len(curr_qr)))

                print("  optimizer: {}".format(o.__name__))
                print("  model: {}".format(m.model_name))

                bnds = list(zip(m.lower_bound, m.upper_bound))
                # TODO: estimate correct weights
                #       - do an ordinary regression
                #       - obtain the residuals to the estimator
                #       - compute c_j = \sigma_j^2 / \sigma^2 = r^2 / r^2.sum()

                opt = o(curr_qr, curr_inten, m, noise=curr_err, p=np.inf)

                opt.prepare_optimizer(bnds, 1000, 1e-10, False)
                res = opt.run()
                if m.tetramer is not None:
                    model_output = m.eval_model(unified_grid, *res.x[1:])
                    model_output["result"] = (1-res.x[0])*model_output["result"] + res.x[0]*tetramer(unified_grid)
                else:
                    model_output = m.eval_model(unified_grid, *res.x)

                fig = plt.figure(figsize=(16, 8))

                if i == len(qr_short):
                    plt.suptitle(directory + " all measurements")
                else:
                    plt.suptitle(directory + file_names[i])

                ax = plt.subplot(2, 1, 1)
                if i == len(qr_short):
                    for lia, (_qr, _int, _err) in enumerate(zip(qr, intensity, error)):
                        ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                                   facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                                   label="int {}".format(lia+1))
                        ax.loglog(_qr, _err, '--', label="error {}".format(lia+1))

                else:
                    ax.scatter(qr[i], intensity[i], marker=marker[i], s=marker_size,
                               facecolor='lightgrey', edgecolor=marker_edge_color[i],
                               label="intensity")
                    ax.loglog(qr[i], error[i], '--', label="error")
                    # ax.loglog(curr_qr, np.abs(residuals), '-x', label="residuals")

                plt.plot(unified_grid, model_output["result"], 'X', label="Interpolated Curve", linewidth=1, markersize=3,
                         alpha=0.3)
                plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
                plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
                if True:
                    # title_str = "Optimization yields: R={}, Rg={}, g={}, b={}, lambda={}, sigma={}".format(model_output["R"],
                    #                                                                                        model_output["Rg"],
                    #                                                                                        model_output["gamma"],
                    #                                                                                        model_output["bc2bs"],
                    #                                                                                        model_output["lambda"],
                    #                                                                                        None)
                    # plt.title(title_str)
                    plt.legend()

                    ax = plt.subplot(2, 1, 2, sharex=ax)

                    plt.loglog(unified_grid, model_output["a1"]*model_output["Fs"], 'X', label="a1*F2", linewidth=1,
                             markersize=3)
                    plt.loglog(unified_grid, model_output["a2"]*model_output["Fc"], 'X', label="a2*Fc", linewidth=1,
                             markersize=3)
                    plt.loglog(unified_grid, np.abs(model_output["a3"]*model_output["Ssc"]), 'X', label="a3*Ssc", linewidth=1,
                             markersize=3)
                    plt.loglog(unified_grid, model_output["a4"]*model_output["Scc"], 'X', label="a4*Scc", linewidth=1,
                             markersize=3)
                    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
                    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
                    title_str = "Individual contributions"
                    plt.title(title_str)
                    plt.legend()

                if i == len(qr_short):
                    fig.savefig(directory + "all_optim_model_result_{}_{}_lambda2020.{}".format(m.model_name, o.__name__, fig_extension),
                                format=fig_extension)
                else:
                    fig.savefig(directory + file_names[i] + "_optim_model_result_{}_{}_lambda2020_0602.{}".format(m.model_name, o.__name__, fig_extension),
                                format=fig_extension)
                if True:
                    optimizer_result = {
                            "final parameters": res.x.tolist(),
                            "fun": np.linalg.norm(res.fun),
                            # "optimality condition": res.optimality,
                            "function evaluations": res.nfev,
                            # "gradient evaluations": res.njev,
                            "reason for termination": str(res.message),
                            "optimizer": str(o.__name__),
                            "model": str(m.model_name),
                            "lambda": item["lambda"]
                           }
                    data_setting = {
                        "qmin": q_min,
                        "qmax": q_max,
                        "lower_bound": m.lower_bound,
                        "upper_bound": m.upper_bound,

                    }
                    if i == len(qr_short):
                        open_file = directory + "all_opt_result_{}_{}_lambda2020_0602.json".format(m.model_name, o.__name__)
                    else:
                        open_file = directory + file_names[i] + "_opt_result_{}_{}_lambda2020_0602.json".format(m.model_name, o.__name__)
                    inf = [optimizer_result, data_setting]
                    with open(open_file, 'w') as f:
                        json.dump(inf, f, indent=4, sort_keys=True)

    if step == "write":
        R_list = dict()
        Rg_list = dict()
        gamma_list = dict()
        beta_list = dict()
        conc_list = dict()
        t_list = dict()
        m_list = dict()
        o_list = dict()
        salt_list = dict()
        o = optimizer
        identifier_list = []
        for directory, prob_info in problem_dict.items():
            m = BrennichModelTrimmedLambda(tetramer, _lambda=prob_info["lambda"])
            # m = BrennichModelTrimmedLambda(tetramer, _lambda=1.34375)
            identifier = "_{}_{}_lambda2020".format(m.model_name, o.__name__)
            if prob_info["salt"] not in salt_list:
                identifier_list.append(prob_info["salt"])
                conc_list[prob_info["salt"]] = []
                R_list[prob_info["salt"]] = []
                Rg_list[prob_info["salt"]] = []
                t_list[prob_info["salt"]] = []
                gamma_list[prob_info["salt"]] = []
                beta_list[prob_info["salt"]] = []
                m_list[prob_info["salt"]] = []
                o_list[prob_info["salt"]] = []
                salt_list[prob_info["salt"]] = prob_info["salt"]
            files, file_names = get_json_files_from_directory(directory)

            for i in range(len(files)):
                file_count = len(file_names)
                for name in file_names:
                    if identifier not in name:
                        file_count -= 1
                if identifier not in file_names[i]:
                    continue
                print("open {}".format(files[i]))
                with open(files[i], 'r') as f:
                    text_data = f.read()  # only a hack since the json is wrongly created
                    # text_data = '[' + re.sub(r'\}{', '},{', text_data) + ']'
                    inf = json.loads(text_data)
                inf = inf[0]
                t = inf["final parameters"][0] if m.tetramer is not None else 0
                R = inf["final parameters"][1 if m.tetramer is not None else 0]
                Rg = inf["final parameters"][2 if m.tetramer is not None else 1]
                gamma = 0
                beta = 0
                if ("BrennichModelTrimmed" in m.model_name or "BrennichModelFull" in m.model_name
                        or "BrennichModelMean" in m.model_name):
                    gamma = inf["final parameters"][3 if m.tetramer is not None else 2]
                    beta = inf["final parameters"][4 if m.tetramer is not None else 3]

                salt, conc = prob_info["salt"], prob_info["concentration"]
                print(
                    " {} {}mM measurement {}: R={}, Rg={}, t={}% tetramer left".format(salt, conc, file_names[i], R, Rg,
                                                                                       t))

                if "all_opt_result" in file_names[i] or file_count == 1:
                    conc_list[prob_info["salt"]].append(conc)
                    R_list[prob_info["salt"]].append(R)
                    Rg_list[prob_info["salt"]].append(Rg)
                    gamma_list[prob_info["salt"]].append(gamma)
                    beta_list[prob_info["salt"]].append(beta)
                    t_list[prob_info["salt"]].append(t)
                    m_list[prob_info["salt"]].append(m.model_name)
                    o_list[prob_info["salt"]].append(o.__name__)
                    salt_list[prob_info["salt"]] = salt

        for identifier in identifier_list:
            print("Do :{}".format(identifier))
            directory = identifier + "/"
            print("  Save at: {}".format(directory))
            fig, ax1 = plt.subplots(figsize=(10, 5))

            ax1.plot(conc_list[identifier], R_list[identifier], 'ro')
            ax1.set_xlabel('concentration')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('R', color='r')
            ax1.tick_params('y', colors='r')

            ax2 = ax1.twinx()
            ax2.plot(conc_list[identifier], Rg_list[identifier], 'b*')
            ax2.set_ylabel('Rg', color='b')
            ax2.tick_params('y', colors='b')

            fig.tight_layout()
            fig.savefig(directory + "r_rg_result_{}.pdf".format(identifier), format="pdf")
            fig.clf()
            plt.close(fig)

            type_check = m_list[identifier]
            if isinstance(type_check, list):
                model_check = type_check[0]
                if "tetramer" in model_check:
                    fig = plt.figure()
                    x_args = np.argsort(conc_list[identifier])
                    plt.semilogy(np.array(conc_list[identifier])[x_args], np.array(t_list[identifier])[x_args])
                    plt.xlabel('concentration')
                    plt.ylabel("tetramer concentration")
                    fig.savefig(directory + "t_result_log_{}_{}.pdf".format(identifier, m.model_name), format="pdf")
                    fig.clf()
                    plt.close(fig)

                    fig = plt.figure()
                    x_args = np.argsort(conc_list[identifier])
                    plt.plot(np.array(conc_list[identifier])[x_args], np.array(t_list[identifier])[x_args])
                    plt.xlabel('concentration')
                    plt.ylabel("tetramer concentration")
                    fig.savefig(directory + "t_result_{}.pdf".format(identifier), format="pdf")
                    fig.clf()
                    plt.close(fig)
                if ("BrennichModelTrimmed" in model_check or "BrennichModelFull" in model_check
                        or "BrennichModelMean" in model_check):
                    fig, ax1 = plt.subplots(figsize=(10, 5))

                    ax1.plot(conc_list[identifier], gamma_list[identifier], 'ro')
                    ax1.set_xlabel('concentration')
                    # Make the y-axis label, ticks and tick labels match the line color.
                    ax1.set_ylabel('gamma', color='r')
                    ax1.tick_params('y', colors='r')

                    ax2 = ax1.twinx()
                    ax2.plot(conc_list[identifier], beta_list[identifier], 'b*')
                    ax2.set_ylabel('beta', color='b')
                    ax2.tick_params('y', colors='b')

                    fig.tight_layout()
                    fig.savefig(directory + "g_b_result_{}.pdf".format(identifier), format="pdf")
                    fig.clf()
                    plt.close(fig)

                    inf = {
                        "concentration": conc_list[identifier],
                        "R"            : R_list[identifier],
                        "Rg"           : Rg_list[identifier],
                        "gamma"        : gamma_list[identifier],
                        "beta"         : beta_list[identifier],
                        "t"            : t_list[identifier],
                        "model"        : m_list[identifier],
                        "optimizer"    : o_list[identifier],
                        "salt"         : salt_list[identifier]
                    }
                    with open(directory + "parameter_{}_{}_0602.json".format(identifier, o.__name__), "w") as f:
                        json.dump(inf, f, indent=4)

    if step == "visualize":
        # write out everything we know
        all_inf = {}
        o = optimizer
        salt_list = ["KCl", "Sp4", "CaCl2", "CoCl3", "MgCl2", "NaCl"]
        directory_list = [directory + "/" for directory in salt_list]
        for salt_idx, directory in enumerate(directory_list):
            print(salt_idx)
            curr_salt = salt_list[salt_idx]
            if curr_salt not in all_inf:
                all_inf[curr_salt] = dict()
            with open(directory + "parameter_" + curr_salt + "_{}_0602.json".format(o.__name__), "r") as f:
                inf = json.load(f)

            all_inf[curr_salt]["concentration"] = inf["concentration"]
            all_inf[curr_salt]["R"] = inf["R"]
            all_inf[curr_salt]["Rg"] = inf["Rg"]
            all_inf[curr_salt]["gamma"] = inf["gamma"]
            all_inf[curr_salt]["beta"] = inf["beta"]
            all_inf[curr_salt]["t"] = inf["t"]
            all_inf[curr_salt]["model"] = inf["model"][0]
            all_inf[curr_salt]["optimizer"] = inf["optimizer"][0]

            # all_inf[curr_salt]["measurement"] = dict()
            #
            # if directory == "KCl/":
            #     loc_directory_list = ["10", "20", "30", "40", "50", "80", "100", "150"]
            # elif directory == "CaCl2/":
            #     loc_directory_list = ["0_5", "1_5", "1_", "2_0", "2_5", "4", "5", "10"]
            # elif directory == "CoCl3/":
            #     loc_directory_list = ["0_1", "0_01", "0_2", "0_02", "0_03", "0_5", "0_05", "0_08", "1_0", "1_5", "2_0",
            #                           "2_5"]
            # elif directory == "MgCl2/":
            #     loc_directory_list = ["0_5", "1_0", "1_5", "2_0", "2_5", "4", "5", "10"]
            # elif directory == "NaCl/":
            #     loc_directory_list = ["10", "20", "30", "40", "50", "80", "100", "150"]
            # elif directory == "Sp4/":
            #     loc_directory_list = ["0_1", "0_01", "0_001", "0_02", "0_002", "0_03", "0_003", "0_04", "0_05", "0_005",
            #                           "0_008",
            #                           "0_08", "0_09"]
            # else:
            #     raise ValueError("unknown directory {}. Use KCl, CaCl2, CoCl3, MgCl2, NaCl, Sp4".format(directory))
            #
            # for loc_dir in loc_directory_list:
            #     print(directory + loc_dir)
            #     all_inf[curr_salt]["measurement"][loc_dir] = dict()
            #     used_files, file_names = get_files_from_directory(directory + loc_dir + "mM/")
            #
            #     file_dict = read_lists_from_data(used_files, 1e-10, 10, intensity_mult_factor)
            #     qr, qr_short = file_dict["qr"], file_dict["qr_short"]
            #     intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
            #     error, error_short = file_dict["error"], file_dict["error_short"]
            #
            #     gui_rg_list = []
            #     gui_i0_list = []
            #     gui_maxidx_list = []
            #     for i in range(len(qr_short) + 1):
            #         if len(qr_short) == 1 and i == 1:
            #             print("  All run left out")
            #             continue
            #         if i == len(qr_short):
            #             curr_qr = np.array([_y for _x in qr_short for _y in _x])
            #             curr_inten = np.array([_y for _x in intensity_short for _y in _x])
            #             curr_err = np.array([_y for _x in error_short for _y in _x])
            #             print("  All run with length {}".format(len(curr_qr)))
            #             all_inf[curr_salt]["measurement"][loc_dir]["all"] = {
            #                 "qr"             : np.array([_y for _x in qr for _y in _x]).tolist(),
            #                 "intensity"      : np.array([_y for _x in intensity for _y in _x]).tolist(),
            #                 "error"          : np.array([_y for _x in error for _y in _x]).tolist(),
            #                 "qr_short"       : curr_qr.tolist(),
            #                 "intensity_short": curr_inten.tolist(),
            #                 "error_short"    : curr_err.tolist(),
            #                 "q_min"          : problem_dict[directory + loc_dir + "mM/"]["q_min"],
            #                 "q_max"          : problem_dict[directory + loc_dir + "mM/"]["q_max"],
            #                 "dataset"        : "all"
            #             }
            #         else:
            #             curr_qr = qr_short[i]
            #             curr_inten = intensity_short[i]
            #             curr_err = error_short[i]
            #             print("  Dataset {} with length {}".format(file_names[i], len(curr_qr)))
            #             all_inf[curr_salt]["measurement"][loc_dir][i] = {
            #                 "qr"             : qr[i].tolist(),
            #                 "intensity"      : intensity[i].tolist(),
            #                 "error"          : error[i].tolist(),
            #                 "qr_short"       : qr_short[i].tolist(),
            #                 "intensity_short": intensity_short[i].tolist(),
            #                 "error_short"    : error_short[i].tolist(),
            #                 "q_min"          : problem_dict[directory + loc_dir + "mM/"]["q_min"],
            #                 "q_max"          : problem_dict[directory + loc_dir + "mM/"]["q_max"],
            #                 "dataset"        : file_names[i]
            #             }
            #         # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            #         # Model parameter to modify
            #         # directory_list = ["KCl/30mM/"]
            #         max_Rgq = 1.3  # optimizer takes interval where Rg*q <= max_Rgq
            #         safety_offset = 0.2
            #         verbose = 0  # verbosity level. 0 None, 2 plots every iteration step
            #         use_first_node_after_max_Rgq = True
            #         # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            #
            #         Rg_list = []
            #         I0_list = []
            #         q_min = problem_dict[directory + loc_dir + "mM/"]["q_min"]
            #         iv = np.ones(2)
            #         curr_opt_para = np.ones(2)
            #         max_idx = 2
            #         while True:
            #             loss_dict = {
            #                 "_qr"       : curr_qr[q_min <= curr_qr][:max_idx],
            #                 "_intensity": curr_inten[q_min <= curr_qr][:max_idx],
            #                 "_error"    : np.ones(len(curr_qr[q_min <= curr_qr][:max_idx])),
            #                 # "_error": error_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx],
            #                 "p"         : 2
            #             }
            #             # bounds = [(l, b) for l, b in zip([i0_lb[lia], rg_lb[lia]], [i0_ub[lia], rg_ub[lia]])]
            #
            #             res = minimize(loss_functional, iv, args=loss_dict, method="BFGS",
            #                            options={"disp": False, "gtol": 1e-7, "norm": 2})
            #             curr_opt_para = res.x.tolist()
            #             curr_opt_para[1] = np.abs(curr_opt_para[1])
            #             if curr_qr[q_min <= curr_qr][max_idx - 1] * np.abs(curr_opt_para[1]) >= max_Rgq:
            #                 # print(qr[q_min <= qr][max_idx - 1] * np.abs(curr_opt_para[1]))
            #                 break
            #             max_idx += 1
            #             iv = res.x.tolist()
            #         # use_first_node_after_max_Rgq:
            #         if i == len(qr_short):
            #             all_inf[curr_salt]["measurement"][loc_dir]["all"]["Guinier_Rg"] = curr_opt_para[1]
            #             all_inf[curr_salt]["measurement"][loc_dir]["all"]["Guinier_I0"] = curr_opt_para[0]
            #             all_inf[curr_salt]["measurement"][loc_dir]["all"]["maxidx"] = max_idx
            #             gui_i0_list.append(curr_opt_para[0])
            #             gui_rg_list.append(curr_opt_para[1])
            #             gui_maxidx_list.append(max_idx)
            #         else:
            #             all_inf[curr_salt]["measurement"][loc_dir][i]["Guinier_Rg"] = curr_opt_para[1]
            #             all_inf[curr_salt]["measurement"][loc_dir][i]["Guinier_I0"] = curr_opt_para[0]
            #             all_inf[curr_salt]["measurement"][loc_dir][i]["maxidx"] = max_idx
            #             if len(qr_short) == 1:
            #                 gui_i0_list.append(curr_opt_para[0])
            #                 gui_rg_list.append(curr_opt_para[1])
            #                 gui_maxidx_list.append(max_idx)
            #     all_inf[curr_salt]["guinier_rg"] = gui_rg_list
            #     all_inf[curr_salt]["guinier_i0"] = gui_i0_list
            #     all_inf[curr_salt]["guinier_maxidx"] = max_idx
        print("alllllll")
        with open("all_info_lambda_{}_0602.json".format(o.__name__), "w") as f:
            json.dump(all_inf, f, indent=4)