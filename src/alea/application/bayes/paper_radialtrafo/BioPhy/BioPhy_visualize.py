from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares

from BioPhy_model import exp_func as model
from BioPhy_model import read_lists_from_data, get_files_from_directory, exp_fun_paras
# np.random.seed(8011990)


file_list = ["KCl/NewTetramer/1/Data.dat", "KCl/NewTetramer/2/Data.dat",
             "KCl/NewTetramer/3/Data.dat", "KCl/NewTetramer/4/Data.dat",
             "KCl/NewTetramer/5/Data.dat", "KCl/NewTetramer/6/Data.dat"]
plot_dir = "KCl/NewTetramer/"
t_q_min = 0.1
t_q_max = 1
# region Read given data files
t_file_dict = read_lists_from_data(file_list, t_q_min, t_q_max)
t_qr, t_qr_short = t_file_dict["qr"], t_file_dict["qr_short"]
t_intensity, t_intensity_short = t_file_dict["intensity"], t_file_dict["intensity_short"]
t_error, t_error_short = t_file_dict["error"], t_file_dict["error_short"]
# endregion

grid = np.linspace(t_q_min, t_q_max, num=1000)
fig = plt.figure(figsize=(10, 10))
marker = "v<>^os"
color = "rbgcmy"
for i, (qr, intensity, error) in enumerate(zip(t_qr, t_intensity, t_error)):
    if i == 0 or i == 1: continue
    plt.scatter(qr, intensity, marker=marker[i], s=5, facecolor='lightgrey', edgecolor=color[i], label="data{}".format(i))
    plt.loglog(qr, error, '--', label="err{}".format(i))

plt.axvline(t_q_min, linestyle="dashed", color="black", label="q min")
plt.axvline(t_q_max, linestyle="dashed", color="black", label="q max")
plt.legend()
fig.savefig(plot_dir + "tetramer_curve.pdf")
