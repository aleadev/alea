import json
import matplotlib.pyplot as plt
from itertools import product as iter_product
from scipy.optimize import minimize
from scipy.integrate import quadrature
from BioPhy_helper_guinier import (loss_functional, fit_data, plot_guinier, smooth, plot_scatter_fun)

import numpy as np
from BioPhy_model import read_lists_from_data, get_files_from_directory
from BioPhy_model import BrennichModelTrimmed, get_tetramer_spline
from BioPhy_optimizer import BashinHopping, DualAnnealing, DifferentialEvolution, Minimizer, LeastSquares

with open("problem_info.json", "r") as f:
    problem_dict = json.load(f)
with open("all_info.json", "r") as f:
    all_inf = json.load(f)

plt.rc('font', family='serif')

tet_path = "/home/fenics/shared/alea/src/alea/application/bayes/paper_radialtrafo/BioPhy/tetramer/1mg_modified/spline_data.json"
tetramer = get_tetramer_spline(tet_path)
model = BrennichModelTrimmed(tetramer)

plot_parameter_flag = False
plot_data_flag = True
plot_guinier_flag = False
plot_mean_flag = False

color = ["forestgreen", "lightcoral", "peru", "olivedrab", "gold", "aqua", "dodgerblue", "crimson",
         "indigo", "navy", "bisque", "silver"]
intensity_mult_factor = 2.64

ylim_r = [1, 20]
ylim_rg = [1, 20]
ylim_g = [0, 0.06]
ylim_b = [0, 8]
ylim_rg_guinier = [0, 30]
ylim_i0 = [0, 2]

para_plot_size = (6, 5)
mean_plot_para_size = (5, 5)
scatter_plot_size = (6, 6)

identifier = "__BrennichModelTrimmedtetramer_LeastSquares"
salt_list = ["KCl", "Sp4", "CaCl2", "CoCl3", "MgCl2", "NaCl"]
directory_list = [directory + "/" for directory in salt_list]
if False:
    for salt in salt_list:
        print(salt)
        if plot_parameter_flag:
            conc_list = all_inf[salt]["concentration"]
            R_list = all_inf[salt]["R"]
            Rg_list = all_inf[salt]["Rg"]
            gamma_list = all_inf[salt]["gamma"]
            beta_list = all_inf[salt]["beta"]
            t_list = all_inf[salt]["t"]

            fig, ax1 = plt.subplots(figsize=para_plot_size)

            ax1.plot(conc_list, R_list, 'ro')
            ax1.set_xlabel('concentration')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('R', color='r')
            ax1.tick_params('y', colors='r')
            ax1.set_ylim(*ylim_r)

            ax2 = ax1.twinx()
            ax2.plot(conc_list, Rg_list, 'b*')
            ax2.set_ylabel('Rg', color='b')
            ax2.tick_params('y', colors='b')
            ax2.set_ylim(*ylim_rg)

            fig.tight_layout()
            fig.savefig("results/"+salt+"/" + "r_rg.png", format="png")
            fig.clf()
            plt.close(fig)

            fig = plt.figure()
            x_args = np.argsort(conc_list)
            plt.semilogy(np.array(conc_list)[x_args], np.array(t_list)[x_args])
            plt.xlabel('concentration')
            plt.ylabel("tetramer concentration")
            fig.savefig("results/" + salt + "/" + "t_log.png", format="png")
            fig.clf()
            plt.close(fig)

            fig = plt.figure()
            x_args = np.argsort(conc_list)
            plt.plot(np.array(conc_list)[x_args], np.array(t_list)[x_args])
            plt.xlabel('concentration')
            plt.ylabel("tetramer concentration")
            fig.savefig("results/" + salt + "/" + "t.png", format="png")
            fig.clf()
            plt.close(fig)

            fig, ax1 = plt.subplots(figsize=para_plot_size)

            ax1.plot(conc_list, gamma_list, 'ro')
            ax1.set_xlabel('concentration')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('gamma', color='r')
            ax1.tick_params('y', colors='r')
            ax1.set_ylim(*ylim_g)

            ax2 = ax1.twinx()
            ax2.plot(conc_list, beta_list, 'b*')
            ax2.set_ylabel('beta', color='b')
            ax2.tick_params('y', colors='b')
            ax2.set_ylim(*ylim_b)
            fig.tight_layout()
            fig.savefig("results/" + salt + "/" + "g_b.png", format="png")
            fig.clf()
            plt.close(fig)
        if plot_data_flag:

            fig = plt.figure(figsize=scatter_plot_size)
            ax = plt.gca()
            ax.set_yscale('log')
            ax.set_xscale('log')

            marker = "v<>^os"
            color = ["forestgreen", "lightcoral", "peru", "olivedrab", "gold", "aqua", "dodgerblue", "crimson",
                     "indigo", "navy", "bisque", "silver"]
            for lia, (key, concentration) in enumerate(all_inf[salt]["measurement"].items()):
                q_min = concentration["0"]["q_min"]
                q_max = concentration["0"]["q_max"]
                plt.scatter(concentration["0"]["qr"],
                            concentration["0"]["intensity"],
                            marker=marker[0],
                            s=5, facecolor='lightgrey',
                            edgecolor=color[lia],
                            label="{}mM".format(str(key).replace("_", ".")))
                # plt.loglog(concentration["0"]["qr"], concentration["0"]["error"], '--', label="err{}".format(key))
                plt.axvline(q_min, linestyle="dashed", color="black")
                plt.axvline(q_max, linestyle="dashed", color="black")
                plt.legend()
            plt.xlabel("q")
            plt.ylabel("Intensity")
            fig.savefig("results/" + salt + "/all_scatter.png")
            for lia, (key, concentration) in enumerate(all_inf[salt]["measurement"].items()):
                if "all" in concentration:
                    fig = plt.figure(figsize=scatter_plot_size)
                    ax = plt.gca()
                    ax.set_yscale('log')
                    ax.set_xscale('log')
                    for lib, (loc_key, meas) in enumerate(concentration.items()):
                        if loc_key == "all": continue
                        q_min = meas["q_min"]
                        q_max = meas["q_max"]
                        plt.scatter(meas["qr"],
                                    meas["intensity"],
                                    marker=marker[0],
                                    s=5, facecolor='lightgrey',
                                    edgecolor=color[lib],
                                    label="{}".format(meas["dataset"]))
                        plt.axvline(q_min, linestyle="dashed", color="black")
                        plt.axvline(q_max, linestyle="dashed", color="black")
                        plt.legend()
                    para = [all_inf[salt]["t"][lia], all_inf[salt]["R"][lia], all_inf[salt]["Rg"][lia], all_inf[salt]["gamma"][lia],
                            all_inf[salt]["beta"][lia]]

                    unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

                    model_output = model.eval_model(unified_grid, *para[1:])
                    model_output["result"] = (1 - para[0]) * model_output["result"] + para[0] * tetramer(unified_grid)
                    plt.plot(unified_grid, model_output["result"], 'X', label="fit", linewidth=1,
                             markersize=3,
                             alpha=0.3)
                    plt.legend()

                    plt.xlabel("q")
                    plt.ylabel("Intensity")
                    plt.title("{} - {} mM".format(salt, key.replace("_", ".")))
                    fig.savefig("results/" + salt + "/{}_scatter.png".format(key))

                else:
                    fig = plt.figure(figsize=scatter_plot_size)
                    ax = plt.gca()
                    ax.set_yscale('log')
                    ax.set_xscale('log')
                    meas = concentration["0"]
                    q_min = meas["q_min"]
                    q_max = meas["q_max"]
                    plt.scatter(meas["qr"],
                                meas["intensity"],
                                marker=marker[0],
                                s=5, facecolor='lightgrey',
                                edgecolor=color[0],
                                label="{}mM".format(key.replace("_", ".")))
                    plt.axvline(q_min, linestyle="dashed", color="black")
                    plt.axvline(q_max, linestyle="dashed", color="black")
                    plt.legend()
                    para = [all_inf[salt]["t"][lia], all_inf[salt]["R"][lia], all_inf[salt]["Rg"][lia],
                            all_inf[salt]["gamma"][lia],
                            all_inf[salt]["beta"][lia]]

                    unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

                    model_output = model.eval_model(unified_grid, *para[1:])
                    model_output["result"] = (1 - para[0]) * model_output["result"] + para[0] * tetramer(unified_grid)
                    plt.plot(unified_grid, model_output["result"], 'X', linewidth=1,
                             markersize=3,
                             alpha=0.3, label="fit")
                    plt.legend()

                    plt.xlabel("q")
                    plt.ylabel("Intensity")
                    plt.title("{} - {} mM".format(salt, key.replace("_", ".")))
                    fig.savefig("results/" + salt + "/{}_example_scatter.png".format(key))
        if plot_guinier_flag:
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # Model parameter to modify
            # directory_list = ["KCl/30mM/"]
            max_Rgq = 1.3  # optimizer takes interval where Rg*q <= max_Rgq
            safety_offset = 0.2
            verbose = 0 # verbosity level. 0 None, 2 plots every iteration step
            use_first_node_after_max_Rgq = True
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            conc_list = all_inf[salt]["concentration"]
            Rg_list = []
            I0_list = []
            for lia, (key, concentration) in enumerate(all_inf[salt]["measurement"].items()):
                # if "all" in concentration:
                for lib, (loc_key, meas) in enumerate(concentration.items()):

                    q_min = meas["q_min"]
                    qr = np.array(meas["qr"])
                    intensity = np.array(meas["intensity"])
                    opt_parameter = [meas["Guinier_I0"], meas["Guinier_Rg"]]
                    max_idx = meas["Guinier_maxidx"]
                    plot_guinier(qr[q_min <= qr][:max_idx], intensity[q_min <= qr][:max_idx], np.linspace(qr[q_min <= qr][0], qr[q_min <= qr][:max_idx][-1], num=100),
                                 opt_parameter, "results/" + salt + "/{}_{}_guinier_fit".format(key,loc_key),
                                 suffix="png")

            Rg_list = all_inf[salt]["guinier_rg"]
            I0_list = all_inf[salt]["guinier_i0"]
            fig, ax1 = plt.subplots(figsize=mean_plot_para_size)

            ax1.plot(conc_list, Rg_list, 'ro')
            ax1.set_xlabel('concentration')
            # Make the y-axis label, ticks and tick labels match the line color.
            ax1.set_ylabel('Rg', color='r')
            ax1.tick_params('y', colors='r')
            ax1.set_ylim(*ylim_rg_guinier)

            ax2 = ax1.twinx()
            ax2.plot(conc_list, I0_list, 'b*')
            ax2.set_ylabel('I0', color='b')
            ax2.tick_params('y', colors='b')
            ax2.set_ylim(*ylim_i0)

            fig.tight_layout()
            fig.savefig("results/" + salt + "/" + "guinier_I0_Rg.png", format="png")
            fig.clf()
            plt.close(fig)
        if plot_mean_flag:
            smoothing_window = 5  # if smoothing_window > 1 do smoothing, Martha -> 5

            min_wc = 0.08
            max_wc = 1.1  # must have length = len(directory_list)
            # assert len(max_wc) == len(directory_list)
            min_mc = 0.19
            max_mc = 0.51
            fit_options = {
                "method"  : "polyfit",
                "poly_deg": 15
            }

            conc_list = all_inf[salt]["concentration"]
            mean_list = []
            slope_list = []
            curvature_list = []
            for lia, (key, concentration) in enumerate(all_inf[salt]["measurement"].items()):
                # if "all" in concentration:
                for lib, (loc_key, meas) in enumerate(concentration.items()):

                    x = np.array(meas["qr"])
                    y = np.array(meas["intensity"])  # + offset

                    if smoothing_window > 1:
                        assert smoothing_window % 2 == 1
                        y = smooth(y, smoothing_window)
                    x_wc = x[(min_wc <= x) & (x <= max_wc)]
                    y_wc = y[(min_wc <= x) & (x <= max_wc)]

                    x_mc = x[(min_mc <= x) & (x <= max_mc)]
                    y_mc = y[(min_mc <= x) & (x <= max_mc)]

                    _fit_fn, slope, curvature = fit_data(x_wc, y_wc, **fit_options)

                    def fit_fn(_x): return _fit_fn(_x)

                    curr_dir = "results/" + salt + "/"
                    plot_scatter_fun(x_wc, fit_fn, curr_dir + "fit_whole_{}_{}".format(key, loc_key), y_wc,
                                     min_wc, max_wc, "v", 5, color[lia],
                                     "{} - {}mM \nPolynomial Fit of order {}".format(salt, key.replace("_", "."),
                                                                                     fit_options["poly_deg"]),
                                     "q", "I(q)", suffix="png", legend="nodes")

                    # plots 1st derivative
                    cf = np.diff(fit_fn(x_wc)) / np.diff(x_wc)[::-1]
                    plot_scatter_fun(x_wc[:-1], slope, curr_dir + "slope_whole_{}_{}".format(key, loc_key), cf,
                                     min_wc, max_wc, "v", 5, color[lia],
                                     "{} - {}mM \nMean of first derivative: {:.3f}".format(salt, key.replace("_", "."),
                                                                               (fit_fn(max_wc) - fit_fn(min_wc)) / (max_wc - min_wc)),
                                     "q", r"$\frac{dI}{dq}(q)$", suffix="png", legend="nodes")

                    # plots second derivative
                    cf = np.diff(slope(x_wc)) / np.diff(x_wc)[::-1]
                    plot_scatter_fun(x_wc[:-1], curvature, curr_dir + "curvature_whole_{}_{}".format(key, loc_key), cf,
                                     min_wc, max_wc, "v", 5, color[lia],
                                     "{} - {}mM \nMean of second derivative: {:.3f}".format(salt, key.replace("_", "."),
                                                                                            (slope(max_wc) - slope(min_wc)) / (max_wc - min_wc)),
                                     "q", r"$\frac{d^2I}{dq^2}(q)$", suffix="png", legend="nodes")

                    # def slope_log_fun(_x): return _x / fit_fn(_x) * slope(_x)
                    # def curvature_log_fun(_x): return (_x/fit_fn(_x))**2 * (curvature(_x)*fit_fn(_x) - slope(_x)**2)
                    if loc_key == "all" or (len(concentration.items()) == 1):
                        mean_list.append(quadrature(fit_fn, min_wc, max_wc)[0] / (max_wc - min_wc))
                        slope_list.append((fit_fn(max_wc) - fit_fn(min_wc)) / (max_wc - min_wc))
                        curvature_list.append((slope(max_wc) - slope(min_wc)) / (max_wc - min_wc))
            fig = plt.figure(figsize=mean_plot_para_size)

            x_args = np.argsort(conc_list)

            plt.plot(np.array(conc_list)[x_args], np.array(mean_list)[x_args], '-ob', label="mean")
            plt.xlabel("concentration")
            plt.ylabel(r"$\int_{a}^{b}I(q) dq \frac{1}{b-a}$")
            plt.legend()
            plt.title("{} mean of function".format(salt))
            fig.savefig("results/" + salt + "/mean_comparison.png")

            fig = plt.figure(figsize=mean_plot_para_size)
            plt.plot(np.array(conc_list)[x_args], np.array(slope_list)[x_args], '-ob', label="slope")
            plt.xlabel("concentration")
            plt.ylabel(r"$\int_{a}^{b}\frac{dI}{dq}(q) dq \frac{1}{b-a}$")
            plt.legend()
            plt.title("{} mean of slope".format(salt))
            fig.savefig("results/" + salt + "/slope_comparison.png")

            fig = plt.figure(figsize=mean_plot_para_size)
            plt.plot(np.array(conc_list)[x_args], np.array(curvature_list)[x_args], '-ob', label="curvature")
            plt.xlabel("concentration")
            plt.ylabel(r"$\int_{a}^{b}\frac{d^2I}{dq^2}(q) dq \frac{1}{b-a}$")
            plt.legend()
            plt.title("{} mean of curvature".format(salt))
            fig.savefig("results/" + salt + "/curvature_comparison.png")

    exit()
else:
    # write out everything we know
    all_inf = {}

    for salt_idx, directory in enumerate(directory_list):
        print(salt_idx)
        curr_salt = salt_list[salt_idx]
        if curr_salt not in all_inf:
            all_inf[curr_salt] = dict()
        with open(directory + "parameter" + identifier + ".json", "r") as f:
            inf = json.load(f)

        all_inf[curr_salt]["concentration"] = inf["concentration"]
        all_inf[curr_salt]["R"] = inf["R"]
        all_inf[curr_salt]["Rg"] = inf["Rg"]
        all_inf[curr_salt]["gamma"] = inf["gamma"]
        all_inf[curr_salt]["beta"] = inf["beta"]
        all_inf[curr_salt]["t"] = inf["t"]
        all_inf[curr_salt]["model"] = inf["model"][0]
        all_inf[curr_salt]["optimizer"] = inf["optimizer"][0]

        all_inf[curr_salt]["measurement"] = dict()

        if directory == "KCl/":
            loc_directory_list = ["10", "20", "30", "40", "50", "80", "100", "150"]
        elif directory == "CaCl2/":
            loc_directory_list = ["0_5", "1_5", "1_", "2_0", "2_5", "4", "5", "10"]
        elif directory == "CoCl3/":
            loc_directory_list = ["0_1", "0_01", "0_2", "0_02", "0_03", "0_5", "0_05", "0_08", "1_0", "1_5", "2_0", "2_5"]
        elif directory == "MgCl2/":
            loc_directory_list = ["0_5", "1_0", "1_5", "2_0", "2_5", "4", "5", "10"]
        elif directory == "NaCl/":
            loc_directory_list = ["10", "20", "30", "40", "50", "80", "100", "150"]
        elif directory == "Sp4/":
            loc_directory_list = ["0_1", "0_01", "0_001", "0_02", "0_002", "0_03", "0_003", "0_04", "0_05", "0_005", "0_008",
                                  "0_08", "0_09"]
        else:
            raise ValueError("unknown directory {}. Use KCl, CaCl2, CoCl3, MgCl2, NaCl, Sp4".format(directory))

        for loc_dir in loc_directory_list:
            print(directory + loc_dir)
            all_inf[curr_salt]["measurement"][loc_dir] = dict()
            used_files, file_names = get_files_from_directory(directory + loc_dir + "mM/")

            file_dict = read_lists_from_data(used_files, 1e-10, 10, intensity_mult_factor)
            qr, qr_short = file_dict["qr"], file_dict["qr_short"]
            intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
            error, error_short = file_dict["error"], file_dict["error_short"]

            gui_rg_list = []
            gui_i0_list = []
            gui_maxidx_list = []
            for i in range(len(qr_short)+1):
                if len(qr_short) == 1 and i == 1:
                    print("  All run left out")
                    continue
                if i == len(qr_short):
                    curr_qr = np.array([_y for _x in qr_short for _y in _x])
                    curr_inten = np.array([_y for _x in intensity_short for _y in _x])
                    curr_err = np.array([_y for _x in error_short for _y in _x])
                    print("  All run with length {}".format(len(curr_qr)))
                    all_inf[curr_salt]["measurement"][loc_dir]["all"] = {
                        "qr": np.array([_y for _x in qr for _y in _x]).tolist(),
                        "intensity": np.array([_y for _x in intensity for _y in _x]).tolist(),
                        "error": np.array([_y for _x in error for _y in _x]).tolist(),
                        "qr_short": curr_qr.tolist(),
                        "intensity_short": curr_inten.tolist(),
                        "error_short": curr_err.tolist(),
                        "q_min": problem_dict[directory + loc_dir + "mM/"]["q_min"],
                        "q_max": problem_dict[directory + loc_dir + "mM/"]["q_max"],
                        "dataset": "all"
                    }
                else:
                    curr_qr = qr_short[i]
                    curr_inten = intensity_short[i]
                    curr_err = error_short[i]
                    print("  Dataset {} with length {}".format(file_names[i], len(curr_qr)))
                    all_inf[curr_salt]["measurement"][loc_dir][i] = {
                        "qr"             : qr[i].tolist(),
                        "intensity"      : intensity[i].tolist(),
                        "error"          : error[i].tolist(),
                        "qr_short"       : qr_short[i].tolist(),
                        "intensity_short": intensity_short[i].tolist(),
                        "error_short"    : error_short[i].tolist(),
                        "q_min"          : problem_dict[directory + loc_dir + "mM/"]["q_min"],
                        "q_max"          : problem_dict[directory + loc_dir + "mM/"]["q_max"],
                        "dataset"        : file_names[i]
                    }
                # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                # Model parameter to modify
                # directory_list = ["KCl/30mM/"]
                max_Rgq = 1.3  # optimizer takes interval where Rg*q <= max_Rgq
                safety_offset = 0.2
                verbose = 0  # verbosity level. 0 None, 2 plots every iteration step
                use_first_node_after_max_Rgq = True
                # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                Rg_list = []
                I0_list = []
                q_min = problem_dict[directory + loc_dir + "mM/"]["q_min"]
                iv = np.ones(2)
                curr_opt_para = np.ones(2)
                max_idx = 2
                while True:
                    loss_dict = {
                        "_qr"       : curr_qr[q_min <= curr_qr][:max_idx],
                        "_intensity": curr_inten[q_min <= curr_qr][:max_idx],
                        "_error"    : np.ones(len(curr_qr[q_min <= curr_qr][:max_idx])),
                        # "_error": error_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx],
                        "p"         : 2
                    }
                    # bounds = [(l, b) for l, b in zip([i0_lb[lia], rg_lb[lia]], [i0_ub[lia], rg_ub[lia]])]

                    res = minimize(loss_functional, iv, args=loss_dict, method="BFGS",
                                   options={"disp": False, "gtol": 1e-7, "norm": 2})
                    curr_opt_para = res.x.tolist()
                    curr_opt_para[1] = np.abs(curr_opt_para[1])
                    if curr_qr[q_min <= curr_qr][max_idx - 1] * np.abs(curr_opt_para[1]) >= max_Rgq:
                        # print(qr[q_min <= qr][max_idx - 1] * np.abs(curr_opt_para[1]))
                        break
                    max_idx += 1
                    iv = res.x.tolist()
                # use_first_node_after_max_Rgq:
                if i == len(qr_short):
                    all_inf[curr_salt]["measurement"][loc_dir]["all"]["Guinier_Rg"] = curr_opt_para[1]
                    all_inf[curr_salt]["measurement"][loc_dir]["all"]["Guinier_I0"] = curr_opt_para[0]
                    all_inf[curr_salt]["measurement"][loc_dir]["all"]["maxidx"] = max_idx
                    gui_i0_list.append(curr_opt_para[0])
                    gui_rg_list.append(curr_opt_para[1])
                    gui_maxidx_list.append(max_idx)
                else:
                    all_inf[curr_salt]["measurement"][loc_dir][i]["Guinier_Rg"] = curr_opt_para[1]
                    all_inf[curr_salt]["measurement"][loc_dir][i]["Guinier_I0"] = curr_opt_para[0]
                    all_inf[curr_salt]["measurement"][loc_dir][i]["maxidx"] = max_idx
                    if len(qr_short) == 1:
                        gui_i0_list.append(curr_opt_para[0])
                        gui_rg_list.append(curr_opt_para[1])
                        gui_maxidx_list.append(max_idx)
            all_inf[curr_salt]["guinier_rg"] = gui_rg_list
            all_inf[curr_salt]["guinier_i0"] = gui_i0_list
            all_inf[curr_salt]["guinier_maxidx"] = max_idx
    print("alllllll")
    with open("all_info.json", "w") as f:
        json.dump(all_inf, f, indent=4)
