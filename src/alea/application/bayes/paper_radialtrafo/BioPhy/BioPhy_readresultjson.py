import json
import matplotlib.pyplot as plt
from itertools import product as iter_product

import numpy as np

from BioPhy_model import BrennichModelTrimmed, BrennichModelPaper, BrennichModelFull, BrennichModelMeanTrimmed
from BioPhy_optimizer import BashinHopping, DualAnnealing, DifferentialEvolution, Minimizer, LeastSquares


with open("problem_info.json", "w") as f:
    problem_dict = json.load(f)

identifier = "__BrennichModelTrimmedtetramer_LeastSquares"
directory_list = ["KCl", "Sp4", "CaCl2", "CoCl3", "MgCl2", "NaCl"]

conc_list = []
r_list = []
rg_list = []
gamma_list = []
beta_list = []
model_list = []
optimizer_list = []
salt_list = []
for directory in directory_list:
    with open(directory + "/parameter" + identifier + ".json", "r") as f:
        inf = json.load(f)
    conc_list.append(inf["concentration"])
    r_list.append(inf["R"])
    rg_list.append(inf["Rg"])
    gamma_list.append(inf["gamma"])
    beta_list.append(inf["beta"])
    model_list.append(inf["model"][0])
    optimizer_list.append(inf["optimizer"][0])
    salt_list.append(inf["salt"])

    with open("parameter_{}.dat".format(inf["salt"]), "w") as f:
        f.write("concentration,R,Rg,gamma,beta,model,optimizer,salt\n")
        for lia in range(len(r_list[-1])):
            f.write("{},{},{},{},{},{},{},{}\n".format(inf["concentration"][lia],
                                                       inf["R"][lia],
                                                       inf["Rg"][lia],
                                                       inf["gamma"][lia],
                                                       inf["beta"][lia],
                                                       inf["model"][lia],
                                                       inf["optimizer"][lia],
                                                       inf["salt"]))
