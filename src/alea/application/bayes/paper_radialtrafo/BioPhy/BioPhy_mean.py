from __future__ import division, print_function
import numpy as np
from functools import partial
from scipy.integrate import quadrature
from scipy.signal import savgol_filter
from BioPhy_helper_guinier import (get_files_from_directory, read_lists_from_data, fit_data, plot_scatter_fun, smooth)
import json
quadrature = partial(quadrature, maxiter=100)

# region Model parameter to modify
directory_list = ["KCl/100mM/1/"]
filter_options = {                                  # options to pass to savgol_filter, None if not filter is used
    "window_length": 5,
    "polyorder": 2,
    "deriv": 0,
    "delta": 1.0,
    "axis": -1,
    "mode": "interp",
    "cval": 0.0
}
# noinspection PyRedeclaration
filter_options = None

fit_options = {
    "method": "polyfit",
    "poly_deg": 15
}

ignore_log = True
smoothing_window = 5                                # if smoothing_window > 1 do smoothing, Martha -> 5

offset = 0.05

min_wc = 0.08
max_wc = 1.1                                        # must have length = len(directory_list)
# assert len(max_wc) == len(directory_list)
min_mc = 0.19
max_mc = 0.51

marker = "v"*len(directory_list)
marker_size = 4
marker_edge_color = "m"*len(directory_list)

intensity_mult_factor = 2.64
# endregion

# region read files
qr_list, qr_short_list = [], []
error_list, error_short_list = [], []
intensity_list, intensity_short_list = [], []
file_name_list = []
for lia, filament_directory in enumerate(directory_list):
    f_used_files, f_file_names = get_files_from_directory(filament_directory)
    assert len(f_used_files) == 1
    assert len(f_file_names) == 1
    f_file_dict = read_lists_from_data(f_used_files, 1e-10, 100, intensity_mult_factor=intensity_mult_factor)
    for key, value in f_file_dict.items():
        assert len(value) == 1

    file_name_list.append(f_file_names[0])
    qr_list.append(f_file_dict["qr"][0])
    intensity_list.append(f_file_dict["intensity"][0])
    error_list.append(f_file_dict["error"][0])
# endregion

# region plot derivatives of order 0, 1, 2 on mid and whole interval
mean_list_fit = []
mean_list_slope = []
mean_list_slope_log = []
mean_list_curvature = []
mean_list_curvature_log = []
mean_list_fit_small = []
mean_list_slope_small = []
mean_list_slope_small_log = []
mean_list_curvature_small = []
mean_list_curvature_small_log = []
for lia, curr_dir in enumerate(directory_list):

    x = qr_list[lia]
    y = intensity_list[lia] # + offset
    if filter_options is not None:
        # noinspection PyArgumentList
        y = savgol_filter(y, **filter_options)

    if smoothing_window > 1:
        assert smoothing_window % 2 == 1
        y = smooth(y, smoothing_window)
    x_wc = x[(min_wc <= x) & (x <= max_wc)]
    y_wc = y[(min_wc <= x) & (x <= max_wc)]

    x_mc = x[(min_mc <= x) & (x <= max_mc)]
    y_mc = y[(min_mc <= x) & (x <= max_mc)]

    _fit_fn, slope, curvature = fit_data(x_wc, y_wc, **fit_options)

    def fit_fn(_x): return _fit_fn(_x)

    def slope_log_fun(_x): return _x / fit_fn(_x) * slope(_x)

    def curvature_log_fun(_x): return (_x/fit_fn(_x))**2 * (curvature(_x)*fit_fn(_x) - slope(_x)**2)

    mean_list_fit.append(quadrature(fit_fn, min_wc, max_wc)[0] / (max_wc - min_wc))

    mean_list_fit_small.append(quadrature(fit_fn, min_mc, max_mc)[0] / (max_mc - min_mc))

    mean_list_slope.append((fit_fn(max_wc) - fit_fn(min_wc)) / (max_wc - min_wc))

    mean_list_curvature.append((slope(max_wc) - slope(min_wc)) / (max_wc - min_wc))

    mean_list_slope_small.append((fit_fn(max_mc)-fit_fn(min_mc)) / (max_mc - min_mc))

    mean_list_curvature_small.append((slope(max_mc)-slope(min_mc)) / (max_mc - min_mc))

    if not ignore_log:
        mean_list_slope_log.append(quadrature(slope_log_fun, min_wc, max_wc)[0] / (max_wc - min_wc))
        mean_list_curvature_log.append(quadrature(curvature_log_fun, min_wc, max_wc)[0] / (max_wc - min_wc))
        mean_list_slope_small_log.append(quadrature(slope_log_fun, min_mc, max_mc)[0] / (max_mc - min_mc))
        mean_list_curvature_small_log.append(quadrature(curvature_log_fun, min_mc, max_mc)[0] / (max_mc - min_mc))

    # region whole line plots
    plot_scatter_fun(x_wc, fit_fn, curr_dir + "fit_whole", y_wc,
                     min_wc, max_wc, marker[lia], marker_size, marker_edge_color[lia],
                     "Polynomial Fit of order {}".format(fit_options["poly_deg"]), "q", "I(q)")

    # plots 1st derivative
    cf = np.diff(fit_fn(x_wc)) / np.diff(x_wc)[::-1]
    plot_scatter_fun(x_wc[:-1], slope, curr_dir + "slope_whole", cf,
                     min_wc, max_wc, marker[lia], marker_size, marker_edge_color[lia],
                     "Mean of first derivative: {:.3f}".format(mean_list_slope[lia]),
                     "q", r"$\frac{dI}{dq}(q)$")

    # plots second derivative
    cf = np.diff(slope(x_wc)) / np.diff(x_wc)[::-1]
    plot_scatter_fun(x_wc[:-1], curvature, curr_dir + "curvature_whole", cf,
                     min_wc, max_wc, marker[lia], marker_size, marker_edge_color[lia],
                     "Mean of second derivative: {:.3f}".format(mean_list_curvature[lia]),
                     "q", r"$\frac{d^2I}{dq^2}(q)$")

    import matplotlib.pyplot as plt
    fig = plt.figure(figsize=(14, 4))
    # plt.loglog(x, y, '.b') #, label="nodes", marker=marker[lia], s=marker_size, facecolor='lightgrey',
    #             # edgecolor=marker_edge_color[lia])
    plt.loglog(x_wc[:-1], curvature(x_wc[:-1]), '-b', label="fit")
    plt.axvline(min_wc, linestyle="dashed", color="black")
    plt.axvline(max_wc, linestyle="dashed", color="black")
    # plt.xlabel(x_label)
    # plt.ylabel(y_label)
    # plt.title(label)
    plt.xlim(0.1, 0.8)
    # plt.ylim(1e-2, 1)
    plt.legend()
    fig.savefig(curr_dir + "other_area.png")
    fig.clf()
    plt.close(fig)

    # endregion

    # region mid line plots
    plot_scatter_fun(x_mc, fit_fn, curr_dir + "fit_mid", y_mc,
                     min_mc, max_mc, marker[lia], marker_size, marker_edge_color[lia],
                     "Polynomial Fit of order {}".format(fit_options["poly_deg"]), "q", "I(q)")

    cf = np.diff(fit_fn(x_mc)) / np.diff(x_mc)[::-1]
    plot_scatter_fun(x_mc[:-1], slope, curr_dir + "slope_mid", cf,
                     min_mc, max_mc, marker[lia], marker_size, marker_edge_color[lia],
                     "Mean of first derivative: {:.3f}".format(mean_list_slope_small[lia]),
                     "q", r"$\frac{dI}{dq}(q)$")

    cf = np.diff(slope(x_mc)) / np.diff(x_mc)[::-1]
    plot_scatter_fun(x_mc[:-1], curvature, curr_dir + "curvature_mid", cf,
                     min_mc, max_mc, marker[lia], marker_size, marker_edge_color[lia],
                     "Mean of second derivative: {:.3f}".format(mean_list_curvature_small[lia]),
                     "q", r"$\frac{d^2I}{dq^2}(q)$")

    # endregion

    # region log plots
    if not ignore_log:
        plot_scatter_fun(x_wc, slope_log_fun, curr_dir + "slope_whole_log", None,
                         min_wc, max_wc, marker[lia], marker_size, marker_edge_color[lia],
                         "Mean of first derivative: {:.3f}".format(mean_list_slope_log[lia]),
                         "q", r"$\frac{d log I}{d log q}(q)$")

        plot_scatter_fun(x_wc, curvature_log_fun, curr_dir + "curvature_whole_log", None,
                         min_wc, max_wc, marker[lia], marker_size, marker_edge_color[lia],
                         "Mean of first derivative: {:.3f}".format(mean_list_curvature_log[lia]),
                         "q", r"$\frac{d^2 log I}{d log q^2}(q)$")

        plot_scatter_fun(x_mc, slope_log_fun, curr_dir + "slope_mid_log", None,
                         min_mc, max_mc, marker[lia], marker_size, marker_edge_color[lia],
                         "Mean of first derivative: {:.3f}".format(mean_list_slope_small_log[lia]),
                         "q", r"$\frac{d log I}{d log q}(q)$")

        plot_scatter_fun(x_mc, curvature_log_fun, curr_dir + "curvature_mid_log", None,
                         min_mc, max_mc, marker[lia], marker_size, marker_edge_color[lia],
                         "Mean of second derivative: {:.3f}".format(mean_list_curvature_small_log[lia]),
                         "q", r"$\frac{d^2 log I}{d log q^2}(q)$")
    # endregion
# endregion

# region save info
for lia, curr_dir in enumerate(directory_list):
    inf = {
        "mean_fit": mean_list_fit[lia],
        "mean_slope": mean_list_slope[lia],
        "mean_curvature": mean_list_curvature[lia],
        "mean_fit_mid": mean_list_fit_small[lia],
        "mean_slope_mid": mean_list_slope_small[lia],
        "mean_curvature_mid": mean_list_curvature_small[lia],
        "mean_slope_log": mean_list_slope_log[lia] if not ignore_log else 0,
        "mean_slope_mid_log": mean_list_slope_small_log[lia] if not ignore_log else 0,
        "mean_curvature_log": mean_list_curvature_log[lia] if not ignore_log else 0,
        "mean_curvature_mid_log": mean_list_curvature_small_log[lia] if not ignore_log else 0,
        "file_name": file_name_list[lia]
    }
    with open(curr_dir + "mean_info.json", "w") as outfile:
        json.dump(inf, outfile, indent=4, sort_keys=True)
# endregion
