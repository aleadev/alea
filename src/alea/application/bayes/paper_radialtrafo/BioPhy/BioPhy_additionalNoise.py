from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
# from BioPhy_model import exp_func as model
from alea.utils.corner import corner
from dolfin import *
from scipy.optimize import least_squares
import emcee
import json
import cPickle as pickle
from functools import partial
from multiprocessing import Pool, cpu_count
from BioPhy_model import tetramerModelWias as wiasmodel
from BioPhy_model import read_lists_from_data, get_files_from_directory
from BioPhy_model import read_lists_from_data, gaussian, uniform, lognormal


# region parallel configuration
from contextlib import contextmanager
@contextmanager
def terminating(thing):
    try:
        yield thing
    finally:
        thing.terminate()

# endregion

# TODO This needs to be modified to fit your directory structure

directory = "KCl/tetramer_model/tetramer/"
used_files, file_names = get_files_from_directory(directory)

optimizer_options = {
    "jac": "3-point",                               # x-point rule for jacobian. 2-point or 3-point
    "method": "trf",                                # optimizer method "trf" or "dogbox" for bounded optimization
    "ftol": 1e-10,                                  # function change tolerance
    "xtol": 1e-10,                                  # tolerance for variable change
    "max_nfev": 10000,                              # maximal number of function evaluations
    "verbose": 2,                                   # level of output: 0 none, 1 only final, 2 every iteration
}

save_chain = True
read_chain_path = None # directory + "chain_result_wias.pick"

compute_det_result = False


if directory == "KCl/tetramer_model/tetramer/":
    q_min = 0.08
    q_max = 3
if directory == "KCl/tetramer_model/filament/":
    q_min = 0.08
    q_max = 0.7
if directory == "KCl/tetramer_model/mix/":
    q_min = 0.08
    q_max = 1.5
if directory == "KCl/10mM/":
    q_min = 0.08
    q_max = 0.8

#
# grid = np.linspace(-1, 5, num=1000)
# prior1 = lambda x: lognormal(x, mu=0.5, sigma=0.1)
# prior2 = lambda x: lognormal(x, mu=0.5, sigma=0.2)
# prior3 = lambda x: lognormal(x, mu=0.5, sigma=0.3)
# prior01 = lambda x: lognormal(x, mu=1, sigma=0.1)
# prior02 = lambda x: lognormal(x, mu=1, sigma=0.2)
# prior03 = lambda x: lognormal(x, mu=1, sigma=0.3)
#
# fig = plt.figure(figsize=(10, 10))
# plt.plot(grid, prior1(grid), label="1")
# plt.plot(grid, prior2(grid), label="2")
# plt.plot(grid, prior3(grid), label="3")
# plt.plot(grid, prior01(grid), label="01")
# plt.plot(grid, prior02(grid), label="02")
# plt.plot(grid, prior03(grid), label="03")
# plt.legend()
# fig.savefig(directory + "prior.png")
# exit()

use_additional_noise = True
start_observation_noise_at = 1
observation_noise_slope = 0.1
additional_noise_offset = 0.3

use_extrapolation = False
extrapolation_grid = np.logspace(-2.2, 1, num=100)

unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5

assert len(used_files) <= len(marker)

# region Read given data files
file_dict = read_lists_from_data(used_files, q_min, q_max)
qr, qr_short = file_dict["qr"], file_dict["qr_short"]
intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
error, error_short = file_dict["error"], file_dict["error_short"]
# endregion

# region additional noise
additional_noise = []
for lia, _err in enumerate(error_short):
    additional_noise.append(np.zeros(len(_err)))
    if use_additional_noise:
        last_qr = qr_short[0]
        init = True
        for lib, _qr in enumerate(qr_short[lia]):
            if _qr < start_observation_noise_at:
                init_noise = intensity_short[lia][lib] * additional_noise_offset
                last_qr = _qr
                continue
            if init:
                additional_noise[lia][lib] = init_noise * additional_noise_offset
                init = False
                continue
            additional_noise[lia][lib] = additional_noise[lia][lib-1] * np.exp(observation_noise_slope * (_qr - last_qr))
            last_qr = _qr

print(additional_noise)
# endregion

def loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
    _model = model(_qr_short[0], *theta)
    _retval = (1/(_error_short[0]+_add_noise[0]))*(_intensity_short[0] - _model["result"])
    # _retval = (_intensity_short[0] - _model["result"])
    if len(_qr_short) > 1:
        for _qr, _int, _err, _noise in zip(_qr_short[1:], _intensity_short[1:], _error_short[1:], _add_noise[1:]):
            _model = model(_qr, *theta)
            _retval = np.append(_retval, (1/(_err + _noise))*(_int - _model["result"]))
            # _retval = np.append(_retval, (_int - _model["result"]))
    return _retval

assert len(qr_short) > 0

for i in range(len(qr_short) + 1):
    if len(qr_short) == 1 and i == 1:
        continue
    if i == len(qr_short):
        loss_dict = {
            "_qr_short"       : [qr_short],
            "_intensity_short": [intensity_short],
            "_error_short"    : [error_short],
            "_add_noise"      : [additional_noise]
        }
    else:
        loss_dict = {
            "_qr_short": [qr_short[i]],
            "_intensity_short": [intensity_short[i]],
            "_error_short": [error_short[i]],
            "_add_noise": [additional_noise[i]]
        }

        # model = old_model
    # TODO: adapt read chain path for multiple measurements

    fig = plt.figure(figsize=(16, 8))

    ax = plt.subplot(1, 1, 1)
    if i == len(qr_short):
        for lia, (_qr, _int, _err, _noise) in enumerate(zip(qr, intensity, error, additional_noise)):
            ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                       facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                       label="int {}".format(lia+1))
            ax.loglog(_qr, _err, '--', label="error {}".format(lia+1))
            if use_additional_noise:
                ax.loglog(_qr, _noise, '-.', label="noise {}".format(lia+1))
    else:
        ax.scatter(qr[i], intensity[i], marker=marker[i], s=marker_size,
                   facecolor='lightgrey', edgecolor=marker_edge_color[i],
                   label="intensity")
        ax.loglog(qr[i], error[i], '--', label="error")
        if use_additional_noise:
            ax.loglog(qr_short[i], additional_noise[i], '-.', label="noise")
    # plt.plot(unified_grid, model_output["result"], 'X', label="MCMC mean", linewidth=1, markersize=3)
    # if compute_det_result:
    #     plt.plot(unified_grid, det_model_output["result"], 'x', label="Optimization", linewidth=1, markersize=3)
    # plt.fill_between(unified_grid, model_output_q15["result"], model_output_q85["result"], alpha=0.2)

    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    plt.legend()
    title_str = "MCMC Parameter: "
    # if det_optimization_result is not None:
    #     title_str += "\n Optimization yields: R={}, Rg={}, $gamma$={}, " \
    #                  "$beta$={}, loss={a}".format(*det_optimization_result[:4],
    #                                               a=np.linalg.norm(loss(det_optimization_result)))
    plt.title(title_str)
    # fig.savefig(directory + "mcmc_model_result_wias.pdf")

    if i == len(qr_short):
        fig.savefig(directory + "noise_all_optim_model_result.pdf", format="pdf")
    else:
        fig.savefig(directory + file_names[i] + "_noise_optim_model_result.pdf", format="pdf")

