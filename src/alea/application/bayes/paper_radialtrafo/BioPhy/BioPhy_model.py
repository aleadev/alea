from __future__ import (division, print_function, absolute_import)
import numpy as np
import numpy.ma as ma
from scipy.stats import find_repeats
from scipy.special import j0 as besselj0
from scipy.special import j1 as besselj1
from scipy.integrate import quad as quadrature
from alea.application.bayes.paper_radialtrafo.BioPhy.own_quad import own_fixed_quad
import time


def wiasModel2(__q, R, Rg, gamma,  bc2bs, _lambda=1.34375, dr=0, c=0, timing=False):

    # sigma = 0.00001 # !!!!!!! strange test
    # sigma = 0.134

    TET = 0
    _q = __q[:, np.newaxis]
    # tetdat = TET(jlow:jup, 2);

    # setup parameters
    # radius R in nm
    # R = xc[0]
    # gyration radius Rg in nm
    # Rg = xc[1]
    # shell distance in terms of Rg
    # dr = 0
    #  "chain density" lambda
    # _lambda = 2.6875
    # _lambda = 1.34375;
    # scaling factor gamma
    # gamma = xc[2]
    # scaling ration beta
    # bc2bs = xc[3]
    # constant background c
    # c = 0
    # c = xc(6)
    # tetramer
    t = 0
    # t = xc(5)
    # relative radial polydispersity
    # sigma = 0.1702

    # scaling factors
    a1 = gamma
    a2 = gamma * _lambda * bc2bs ** 2
    a3 = 2 * gamma * bc2bs
    a4 = gamma * bc2bs ** 2

    # radial polydispersity model Hammouda SANS toolbox p.302
    if timing:
        t = time.time()
    dR = Rg * dr           # array of chain - core distances

    # do actual form factor calculation
    # form factor amplitude of cylinder
    # cylindrical cross - correlation
    #print(_q[:, np.newaxis].dot(Rs[np.newaxis, :]))
    if timing:
        t = time.time()
    buffer_result = _q.dot(R)
    if timing:
        print("fcs creation dot: {}".format(time.time() - t))

    if timing:
        t = time.time()
    bessel1 = besselj1(buffer_result)

    if timing:
        print("bessel 1 creation: {}".format(time.time() - t))
    if timing:
        t = time.time()
    fcs = 2 * bessel1 / buffer_result
    if timing:
        print("fcs creation: {}".format(time.time() - t))
    if timing:
        t = time.time()

    Fcs = (fcs**2)
    Fl = np.pi / _q
    Fs = Fcs*Fl

    if timing:
        print("Fcs creation: {}".format(time.time() - t))
    # gaussian(qp. * Rg). ^ 2) cross - correlation

    if timing:
        t = time.time()
    x = (_q.dot(Rg))**2
    try:
        Fc = 2 * (np.exp(-x) - 1 + x) / (x**2)
    except FloatingPointError:
        # print("underflow encountered in Fc: use 0 instead, q={}, Rg={}".format(np.linalg.norm(_q),np.linalg.norm(Rg)))
        Fc = 2*x/(x**2)

    if timing:
        print("Fc creation: {}".format(time.time() - t))
    # core - chain correlation

    if timing:
        t = time.time()
    try:
        if np.any(x)<=0:
            print("x is greater 0: {}".format(x))
        psi = (1 - np.exp(-x)) / x
    except FloatingPointError:
        print("underflow encountered in psi: use 0 instead")
        psi = 0

    if timing:
        print("psi creation: {}".format(time.time() - t))

    if timing:
        t = time.time()
    if dr == 0:
        bessel0 = besselj0(buffer_result)

    if timing:
        print("bessel 0 creation: {}".format(time.time() - t))
    if timing:
        t = time.time()
    if dr == 0:
        Ssc = psi * ((fcs * bessel0)) * Fl
    else:
        Ssc = psi * ((fcs * besselj0(_q.dot(R + dR))))*Fl

    if timing:
        print("Ssc creation: {}".format(time.time() - t))
    # % Ssc = psi. * (fcs. * besselj(0, q * (Rdist.T+dR)))*Fl

    # chain-chain correlation

    if timing:
        t = time.time()
    if dr == 0:
        Scc = np.power(psi, 2) * ((bessel0 ** 2)) * Fl
    else:
        Scc = np.power(psi, 2) * (((besselj0(_q.dot(R + dR)))**2))*Fl

    if timing:
        print("Scc creation: {}".format(time.time() - t))
    # % Scc = power(psi, 2). * ((((besselj(0, q * (Rdist'+dR))).^(2)))).*Fl;

    # % put together entire model function 'g'
    # % tetramer (first when only tetramers)

    if timing:
        t = time.time()
    if TET == 0 or t == 0:
        # atm this is always the case
        g = (a1 * Fs + a2 * Fc + a3 * Ssc + a4 * Scc) + c
    else:
        g = (1-t) * (a1 * Fs + a2 * Fc + a3 * Ssc + a4 * Scc) + t * tetdat + c

    if timing:
        print("g creation: {}".format(time.time() - t))
        print("#"*20)
    retval = {
        "result": g[:, 0],
        "a1": a1,
        "Fs": Fs[:, 0],
        "a2": a2,
        "Fc": Fc[:, 0],
        "a3": a3,
        "Ssc": Ssc[:, 0],
        "a4": a4,
        "Scc": Scc[:, 0],
        "c": c,
        "R": R,
        "Rg": Rg,
        "gamma": gamma,
        "lambda": _lambda,
        "bc2bs": bc2bs,
        "dr": dr
    }
    return retval


def wiasModel(q, R, Rg, beta_s, beta_c, N_ulf, n, ell, dr, c, min_area=3, timing=False):
    """
    Own model type
    :param q: scatter variable
    :param R: Radius
    :param Rg: gyration Radius
    :param beta_s: total excess scattering length per chain of core
    :param beta_c: total excess scattering length per chain of corona
    :param N_ulf: number of units
    :param n: number of Gaussian chains
    :param ell: length of unit
    :param dr: offset of core and gaussian chains (usually assumed centered -> dr=0)
    :param c: offset / background
    :return:
    """
    TET = 0
    _q = q[:, np.newaxis]
    t = 0

    # scaling factors
    piBylq = np.pi/(ell*_q)
    a1 = n**2*beta_s**2
    a2 = n*beta_c**2
    a3 = 2 * n**2*beta_s*beta_c
    a4 = n**2*beta_c
    dR = Rg * dr  # array of chain - core distances
    # do actual form factor calculation
    # form factor amplitude of cylinder
    # cylindrical cross - correlation
    # print(_q[:, np.newaxis].dot(Rs[np.newaxis, :]))
    if timing:
        t = time.time()
    buffer_result = _q.dot(R)
    if timing:
        print("fcs creation dot: {}".format(time.time() - t))

    if timing:
        t = time.time()
    bessel1 = besselj1(buffer_result)

    if timing:
        print("bessel 1 creation: {}".format(time.time() - t))
    if timing:
        t = time.time()
    fcs = 2 * bessel1 / buffer_result
    if timing:
        print("fcs creation: {}".format(time.time() - t))
    if timing:
        t = time.time()

    Fcs = fcs ** 2
    Fl = np.pi / _q
    Fs = Fcs * Fl * piBylq

    if timing:
        print("Fcs creation: {}".format(time.time() - t))
    # gaussian(qp. * Rg). ^ 2) cross - correlation

    if timing:
        t = time.time()
    x = (_q.dot(Rg)) ** 2
    try:
        Fc = 2 * (np.exp(-x) - 1 + x) / (x ** 2)
    except FloatingPointError:
        # print("underflow encountered in Fc: use 0 instead, q={}, Rg={}".format(np.linalg.norm(_q),np.linalg.norm(Rg)))
        Fc = 2 * x / (x ** 2)

    if timing:
        print("Fc creation: {}".format(time.time() - t))
    # core - chain correlation

    if timing:
        t = time.time()
    try:
        if np.any(x) <= 0:
            print("x is greater 0: {}".format(x))
        psi = (1 - np.exp(-x)) / x
    except FloatingPointError:
        print("underflow encountered in psi: use 0 instead")
        psi = 0

    if timing:
        print("psi creation: {}".format(time.time() - t))

    if timing:
        t = time.time()
    if dr == 0:
        bessel0 = besselj0(buffer_result)

    if timing:
        print("bessel 0 creation: {}".format(time.time() - t))
    if timing:
        t = time.time()
    if dr == 0:
        Ssc = psi * (fcs * bessel0) * Fl * piBylq
    else:
        Ssc = psi * (fcs * besselj0(_q.dot(R + dR))) * Fl * piBylq

    if timing:
        print("Ssc creation: {}".format(time.time() - t))
    # % Ssc = psi. * (fcs. * besselj(0, q * (Rdist.T+dR)))*Fl

    # chain-chain correlation

    if timing:
        t = time.time()
    if dr == 0:
        Scc = np.power(psi, 2) * ((bessel0 ** 2)) * Fl * piBylq
    else:
        Scc = np.power(psi, 2) * ((besselj0(_q.dot(R + dR))) ** 2) * Fl * piBylq

    if timing:
        print("Scc creation: {}".format(time.time() - t))
    # % Scc = power(psi, 2). * ((((besselj(0, q * (Rdist'+dR))).^(2)))).*Fl;

    # % put together entire model function 'g'
    # % tetramer (first when only tetramers)

    if timing:
        t = time.time()
    if TET == 0 or t == 0:
        # atm this is always the case
        g = N_ulf* (a1 * Fs + a2 * Fc + a3 * Ssc + a4 * Scc) + c
    else:
        g = N_ulf * (1 - t) * (a1 * Fs + a2 * Fc + a3 * Ssc + a4 * Scc) + t * tetdat + c

    if timing:
        print("g creation: {}".format(time.time() - t))
        print("#" * 20)
    retval = {
        "result" : g[:, 0],
        "a1"     : a1,
        "Fs"     : Fs[:, 0],
        "a2"     : a2,
        "Fc"     : Fc[:, 0],
        "a3"     : a3,
        "Ssc"    : Ssc[:, 0],
        "a4"     : a4,
        "Scc"    : Scc[:, 0],
        "c"      : c,
        "R"      : R,
        "Rg"     : Rg,
        "beta_s" : beta_s,
        "beta_c" : beta_c,
        "N_ulf"  : N_ulf,
        "ell"    : ell,
        "n"      : n,
        "dr"     : dr
    }
    return retval


def modelTwoLambda(__q, R_T, R_F, Rg_T, Rg_F, gamma_T, gamma_F,  bc2bs_T, bc2bs_F,
                   sigma_T=0.134, sigma_F=0.134, _lambda_T=60/4, _lambda_F=1.34375, dr=0, c=0, timing=False):

    ret_T = model(__q, R_T, Rg_T, gamma_T, bc2bs_T, sigma_T, _lambda_T, dr, c, timing=timing)
    ret_F = model(__q, R_F, Rg_F, gamma_F, bc2bs_F, sigma_F, _lambda_F, dr, c, timing=timing)

    return ret_F, ret_T


def model(__q, R, Rg, gamma,  bc2bs, sigma=0.134, _lambda=1.34375, dr=0, c=0, timing=False, min_area=3):
    # sigma = 0.00001 # !!!!!!! strange test
    # sigma = 0.134

    TET = 0
    _q = __q[:, np.newaxis]
    # tetdat = TET(jlow:jup, 2);

    # setup parameters
    # radius R in nm
    # R = xc[0]
    # gyration radius Rg in nm
    # Rg = xc[1]
    # shell distance in terms of Rg
    # dr = 0
    #  "chain density" lambda
    # _lambda = 2.6875
    # _lambda = 1.34375;
    # scaling factor gamma
    # gamma = xc[2]
    # scaling ration beta
    # bc2bs = xc[3]
    # constant background c
    # c = 0
    # c = xc(6)
    # tetramer
    t = 0
    # t = xc(5)
    # relative radial polydispersity
    # sigma = 0.1702

    # scaling factors
    a1 = gamma
    a2 = gamma * _lambda * bc2bs ** 2
    a3 = 2 * gamma * bc2bs
    a4 = gamma * bc2bs ** 2

    # radial polydispersity model Hammouda SANS toolbox p.302
    if timing:
        t = time.time()
    rs = sigma * R
    _a = max(0.01, R-min_area*rs)
    _b = R+min_area*rs
    if _b - _a < 1e-5:
        print("b={}, a={} --> R={}".format(_b, _a, R))
    _num = int(np.floor((_b-_a)/rs*100))
    Rs = np.linspace(_a, _b, num=_num, endpoint=True)[np.newaxis, :] # array of  radii
    dR = np.ones(Rs.shape) * Rg * dr           # array of chain - core distances

    # weight of radii
    # Rdist = (1 / (100 * sqrt(pi * 2))) * exp(-(Rs - R). ^ 2 / (2 * rs ^ 2));
    Rdist = (1 / (rs * np.sqrt(np.pi * 2))) * np.exp(-(Rs - R)**2 / (2 * rs ** 2))
    Rdist = Rdist/np.sum(Rdist)                        # normalisation to 1
    if timing:
        print("radii creation: {}".format(time.time()-t))

    # do actual form factor calculation
    # form factor amplitude of cylinder
    # cylindrical cross - correlation
    #print(_q[:, np.newaxis].dot(Rs[np.newaxis, :]))
    if timing:
        t = time.time()
    buffer_result = _q.dot(Rs)
    if timing:
        print("fcs creation dot: {}".format(time.time() - t))

    if timing:
        t = time.time()
    bessel1 = besselj1(buffer_result)

    if timing:
        print("bessel 1 creation: {}".format(time.time() - t))
    if timing:
        t = time.time()
    fcs = 2 * bessel1 / buffer_result
    if timing:
        print("fcs creation: {}".format(time.time() - t))
    if timing:
        t = time.time()

    Fcs = (fcs**2).dot(Rdist.T)
    Fl = np.pi / _q
    Fs = Fcs*Fl

    if timing:
        print("Fcs creation: {}".format(time.time() - t))
    # gaussian(qp. * Rg). ^ 2) cross - correlation

    if timing:
        t = time.time()
    x = (_q.dot(Rg))**2
    try:
        Fc = 2 * (np.exp(-x) - 1 + x) / (x**2)
    except FloatingPointError:
        # print("underflow encountered in Fc: use 0 instead, q={}, Rg={}".format(np.linalg.norm(_q),np.linalg.norm(Rg)))
        Fc = 2*x/(x**2)

    if timing:
        print("Fc creation: {}".format(time.time() - t))
    # core - chain correlation

    if timing:
        t = time.time()
    try:
        if np.any(x)<=0:
            print("x is greater 0: {}".format(x))
        psi = (1 - np.exp(-x)) / x
    except FloatingPointError:
        print("underflow encountered in psi: use 0 instead")
        psi = 0

    if timing:
        print("psi creation: {}".format(time.time() - t))

    if timing:
        t = time.time()
    if dr == 0:
        bessel0 = besselj0(buffer_result)

    if timing:
        print("bessel 0 creation: {}".format(time.time() - t))
    if timing:
        t = time.time()
    if dr == 0:
        Ssc = psi * ((fcs * bessel0).dot(Rdist.T)) * Fl
    else:
        Ssc = psi * ((fcs * besselj0(_q.dot(Rs + dR))).dot(Rdist.T))*Fl

    if timing:
        print("Ssc creation: {}".format(time.time() - t))
    # % Ssc = psi. * (fcs. * besselj(0, q * (Rdist.T+dR)))*Fl

    # chain-chain correlation

    if timing:
        t = time.time()
    if dr == 0:
        Scc = np.power(psi, 2) * ((bessel0 ** 2).dot(Rdist.T)) * Fl
    else:
        Scc = np.power(psi, 2) * (((besselj0(_q.dot(Rs + dR)))**2).dot(Rdist.T))*Fl

    if timing:
        print("Scc creation: {}".format(time.time() - t))
    # % Scc = power(psi, 2). * ((((besselj(0, q * (Rdist'+dR))).^(2)))).*Fl;

    # % put together entire model function 'g'
    # % tetramer (first when only tetramers)

    if timing:
        t = time.time()
    if TET == 0 or t == 0:
        # atm this is always the case
        g = (a1 * Fs + a2 * Fc + a3 * Ssc + a4 * Scc) + c
    else:
        g = (1-t) * (a1 * Fs + a2 * Fc + a3 * Ssc + a4 * Scc) + t * tetdat + c

    if timing:
        print("g creation: {}".format(time.time() - t))
        print("#"*20)
    retval = {
        "result": g[:, 0],
        "a1": a1,
        "Fs": Fs[:, 0],
        "a2": a2,
        "Fc": Fc[:, 0],
        "a3": a3,
        "Ssc": Ssc[:, 0],
        "a4": a4,
        "Scc": Scc[:, 0],
        "c": c,
        "R": R,
        "Rg": Rg,
        "gamma": gamma,
        "sigma": sigma,
        "lambda": _lambda,
        "bc2bs": bc2bs,
        "dr": dr
    }
    return retval

def hat_fun(x, _a, _b):
    h = (_b - _a)/2
    _retval = np.zeros(len(x))
    for _lia in range(len(_retval)):
        if x[_lia] < _a or x[_lia] > _b:
            _retval[_lia] = 0
        elif x[_lia] < _a+h:
            _retval[_lia] = (x[_lia] - _a)/h
        else:
            _retval[_lia] = 1- (x[_lia] - (_a+h)) / h
    return _retval


class LossModel(object):
    def __init__(self,
                 tetramer_curve=None):
        self.params = None
        self.tetramer = tetramer_curve
        self._lambda = None

    def __call__(self, x, *args):
        pass

    def loss_diff(self, x, delta, theta):
        if self.tetramer is None:
            return delta - self(x, *theta)
        return delta - ((1 - theta[0])*self(x, *theta[1:]) + theta[0]*self.tetramer(x))

    def loss(self, theta, qr, inten, err=None, *args, **kwargs):
        if err is None:
            err = 1
        return self.loss_diff(qr, inten, theta) / err

    def loss_functional_explicit(self, theta, qr, inten, err=None, p=2):
        arg_dict = {"qr": qr, "inten": inten, "err": err, "p": p}
        return self.loss_functional(theta, arg_dict)

    def loss_functional(self, theta, args):
        _lambda = 0
        mu = 0
        curr_loss = self.loss(theta, **args)

        # print("I0={:.2E}, Rg={:.2E}, loss={:.2E}, points={}".format(theta[0], theta[1], np.linalg.norm(curr_loss),
        #                                                             len(curr_loss)))
        if "p" not in args:
            p = 2
        else:
            p = args["p"]
        return np.linalg.norm(curr_loss, ord=p) + _lambda * np.linalg.norm(theta) ** 2 + mu / (len(curr_loss) + 1)


class LogLogisticFractionalPolynomial(LossModel):
    """
    Taken from https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0146021 figure
    """
    def __init__(self):
        super(LogLogisticFractionalPolynomial, self).__init__()
        self.params = 6

    def __call__(self, x, *args):
        assert len(args) == self.params
        # retval = np.zeros(len(x))
        c = args[0]
        d = args[1]
        b = args[2]
        p1 = args[3]
        p2 = args[4]
        e = args[5]
        retval = c + (d-c)/(1+np.exp(b*np.log(x+1)**p1 + e*np.log(x+1)**p2))
        return retval


class BrennichModel(LossModel):
    def __init__(self,
                 tetramer_curve=None):
        super(BrennichModel, self).__init__(tetramer_curve)
        self._model = None

    def __call__(self, x, *args):
        return self._model(x, *args)["result"]

    def eval_model(self, x, *args):
        return self._model(x, *args)

    @property
    def model_name(self):
        retval = self.__class__.__name__
        retval += "tetramer" if self.tetramer is not None else ""
        retval += "_lambda_{}".format(self._lambda).replace(".", "_") if self._lambda is not None else ""
        return retval


class BrennichModelTrimmed(BrennichModel):
    def __init__(self,
                 tetramer_curve=None):
        super(BrennichModelTrimmed, self).__init__(tetramer_curve)
        self.params = 4
        self.lower_bound = [0.1, 0.1, 1e-8, 1e-8]
        self.upper_bound = [20, 20, 50, 50]
        if tetramer_curve is not None:
            self.params += 1
            self.lower_bound.insert(0, 0)
            self.upper_bound.insert(0, 1)
        from functools import partial
        self._model = wiasModel2


class BrennichModelTrimmedLambda(BrennichModelTrimmed):
    def __init__(self,
                 tetramer_curve=None,
                 _lambda=1.34):
        super(BrennichModelTrimmedLambda, self).__init__(tetramer_curve)
        self.params = 4
        self.lower_bound = [0.1, 0.1, 1e-8, 1e-8]
        self.upper_bound = [20, 20, 50, 50]
        self._lambda = _lambda
        if tetramer_curve is not None:
            self.params += 1
            self.lower_bound.insert(0, 0)
            self.upper_bound.insert(0, 1)
        from functools import partial
        self._model = partial(wiasModel2, _lambda=_lambda)


class BrennichModelFull(BrennichModelTrimmed):
    def __init__(self,
                 tetramer_curve=None):
        super(BrennichModelFull, self).__init__(tetramer_curve)
        self.params = 7
        self.lower_bound = [0.1, 0.1, 1e-8, 1e-8, 1e-8, 0, -1]
        self.upper_bound = [20, 20, 50, 500, 500, 500, 1]
        if tetramer_curve is not None:
            self.params += 1
            self.lower_bound.insert(0, 0)
            self.upper_bound.insert(0, 1)


class BrennichModelPaper(BrennichModel):
    def __init__(self,
                 tetramer_curve=None):
        super(BrennichModelPaper, self).__init__(tetramer_curve)
        self.params = 9
        self.lower_bound = [0.1, 0.1, 1e-8, 1e-8, 1, 1, 1e-8, 1e-8, -1]
        self.upper_bound = [20, 20, 50, 500, 500, 500, 100, 100, 1]
        if tetramer_curve is not None:
            self.params += 1
            self.lower_bound.insert(0, 0)
            self.upper_bound.insert(0, 1)
        self._model = wiasModel


class BrennichModelMeanTrimmed(BrennichModel):
    def __init__(self,
                 tetramer_curve=None):
        super(BrennichModelMeanTrimmed, self).__init__(tetramer_curve)
        self.params = 4
        self.lower_bound = [0.1, 0.1, 1e-8, 1e-8]
        self.upper_bound = [20, 20, 50, 50]
        if tetramer_curve is not None:
            self.params += 1
            self.lower_bound.insert(0, 0)
            self.upper_bound.insert(0, 1)
        self._model = model


class BrennichModelMean(BrennichModelMeanTrimmed):
    def __init__(self,
                 tetramer_curve=None):
        super(BrennichModelMeanTrimmed, self).__init__(tetramer_curve)
        self.params = 8
        self.lower_bound = [0.1, 0.1, 1e-8, 1e-8, 1e-8, 1e-8, 1e-8, -1]
        self.upper_bound = [20, 20, 50, 500, 500, 100, 100, 1]
        if tetramer_curve is not None:
            self.params += 1
            self.lower_bound.insert(0, 0)
            self.upper_bound.insert(0, 1)

def exp_func(x, limit=0.13, a=None, b=None, c=None, d=None, e=None, f=None, g=None, h=None, i=None, j=None):
    # retval = np.exp(-a*np.exp(-b*x)-c*np.exp(-d*x**2)) + f
    # retval = np.exp(a*np.exp(-b*x) + c*np.exp(-d*x**2) + e) + np.exp(-f*x) + g*x + h
    # retval = np.exp(a + (b - a)/((c + np.exp(-d*x))**(1/e))) + f      # exp(logistic)
    retval = np.zeros(len(x))
    if np.any(x[x>=limit] + e) <= 0:
        return retval
    if np.any(x[x>=limit] + h) <= 0:
        return retval
    if f < 1 or h < 1:
        return retval
    retval[x >= limit] = a + (b - a)/(c + np.exp(d*np.log(x[x >= limit]+e)**f + g*(np.log(x[x >= limit]+h))**i))
    retval[x < limit] = np.exp(j)
    return retval
def exp_fun_paras():
    return 10

def new_naiv_tetramer_model(x):
    curr_model = LogLogisticFractionalPolynomial()
    args = [-0.0154004902, 0.0342135786, 0.934618862, 0.141718936, -1.5907865106, -0.079630248 ]
    return curr_model(x, *args)

def naiv_tetramer_model(x):
    a = 0.0000603916
    b = 1.0901226897
    c = 5.5161468034
    d = 3.9089935855
    e = 0.9105288475
    f = 0.5948512589
    g = 3.9089834654
    h = 0.9104025108
    i = 0.594842421
    j = -2.0774453747
    limit = 0.13
    retval = np.zeros(len(x))
    retval[x >= limit] = a + (b - a) / (
                c + np.exp(d * np.log(x[x >= limit] + e) ** f + g * (np.log(x[x >= limit] + h)) ** i))
    retval[x < limit] = np.exp(j)
    return retval

# region function to read the data files
def read_file(_file_str, intensity_mult_factor):
    """
    Reads the Data.dat files as provided. Very restrictive in format.
    :param _file_str: path to the file including suffix. example: "data/Data.dat"
    :return: dictionary containing "qr", "intensity" and "error"
    """
    retval = {
               "qr": [],
               "intensity": [],
               "error": []
             }
    with open(_file_str, 'r') as _f:
        for (lib, line) in enumerate(_f):
            line_arr = line.split("\t")
            # print(line_arr)
            for lic in range(len(line_arr)):
                line_arr[lic] = line_arr[lic].strip().strip("\n")
            if lib == 0:

                if len(line_arr) == 4:
                    assert (line_arr[0] == "qr")
                    assert (line_arr[1] == "Intensity")
                    assert (line_arr[2] == "Error")
                elif len(line_arr) == 7:
                    assert (line_arr[0] == "qr")
                    assert (line_arr[3] == "Intensity")
                    assert (line_arr[5] == "error")
                continue
            retval["qr"].append(float(line_arr[0]))
            retval["intensity"].append(float(line_arr[1])*intensity_mult_factor)
            retval["error"].append(float(line_arr[2]))
    retval["qr"] = np.array(retval["qr"])
    retval["intensity"] = np.array(retval["intensity"])
    retval["error"] = np.array(retval["error"])
    return retval
# endregion


def trim_data(data, q_min, q_max):
    retval = {
        "qr"       : data["qr"][np.where((q_min < data["qr"]) & (data["qr"] < q_max))],
        "intensity": data["intensity"][np.where((q_min < data["qr"]) & (data["qr"] < q_max))],
        "error"    : data["error"][np.where((q_min < data["qr"]) & (data["qr"] < q_max))]
    }
    return retval


def read_lists_from_data(used_files, q_min, q_max, intensity_mult_factor=1.0):
    qr = []
    intensity = []
    error = []
    qr_short = []
    intensity_short = []
    error_short = []
    for lia in range(len(used_files)):
        data = read_file(used_files[lia], intensity_mult_factor)
        data_small = trim_data(data, q_min, q_max)
        qr.append(data["qr"])
        qr_short.append(data_small["qr"])
        intensity.append(data["intensity"])
        intensity_short.append(data_small["intensity"])

        error.append(data["error"])
        error_short.append(data_small["error"])
    retval = {
        "qr": qr,
        "qr_short": qr_short,
        "intensity": intensity,
        "intensity_short": intensity_short,
        "error": error,
        "error_short": error_short
    }
    return retval


def uniform(x, a=-1, b=1, logprior=False):
    if not a <= x <= b:
        if logprior:
            return -np.inf
        return 0
    if logprior:
        return 0
    return 1/np.abs(b-a)


def gaussian(x, mu=0, sigma=1, logprior=False):
    if logprior:
        return -0.5*(x-mu)**2/sigma**2
    return 1/np.sqrt(2*np.pi*sigma**2)*np.exp(-0.5*(x-mu)**2/sigma**2)


def lognormal(x, mu=1, sigma=1, logprior=False):
    _x = np.array(x)
    if logprior:
        if _x.shape == ():
            if _x <= 1e-9:
                return -np.inf
            return np.log((1 / (np.sqrt(2 * np.pi * sigma ** 2) * _x)) * np.exp(-0.5 * ((np.log(_x) - mu) ** 2) / sigma ** 2))
        retval = np.ones(_x.shape)
        # print(retval)
        retval[_x > 0] = np.log((1 / (np.sqrt(2 * np.pi * sigma ** 2) * _x[_x > 0])) * np.exp(
            -0.5 * ((np.log(_x[_x > 0]) - mu) ** 2) / sigma ** 2))
        return retval

    retval = np.zeros(_x.shape)
    retval[_x > 0] = (1/(np.sqrt(2*np.pi*sigma**2)*_x[_x > 0]))*np.exp(-0.5*((np.log(_x[_x > 0])-mu)**2)/sigma**2)
    return retval

def get_files_from_directory(directory):
    from os import walk

    files = []
    files_name = []
    for (dirpath, dirnames, filenames) in walk(directory):
        for _file in filenames:
            if not _file.endswith(".dat"):
                continue
            files.append(directory + _file)
            files_name.append(_file[:-4])
        break
    return files, files_name


def get_json_files_from_directory(directory):
    from os import walk

    files = []
    files_name = []
    for (dirpath, dirnames, filenames) in walk(directory):
        for _file in filenames:
            if not _file.endswith(".json"):
                continue
            files.append(directory + _file)
            files_name.append(_file[:-4])
        break
    return files, files_name


def tetramerModelWias(q, R, Rg, beta_s, beta_c, L, dr, c, N=8, timing=False):
    quad_degree = 10
    use_alpha_2 = False
    t = None
    # scaling factors
    q = q[:, np.newaxis]

    a1 = N ** 2 * beta_s ** 2
    a2 = N * beta_c ** 2
    a3 = 2 * N ** 2 * beta_s * beta_c
    a4 = N*(N-1) * beta_c**2
    # dR = Rg * dr  # array of chain - core distances
    dR = dr
    if timing:
        t = time.time()
    if not use_alpha_2:
        def Psi(alpha):
            retval = np.array(2*besselj1(q*R*np.sin(alpha))/(q*R*np.sin(alpha))*np.sin(q*L*np.cos(alpha)/2) / (
                q*L*np.cos(alpha)/2))
            # print(retval.shape)
            return retval

    else:
        def Psi(alpha):
            retval = np.array(2*besselj1(q*R*np.sin(alpha))/(q*R*np.sin(alpha))*np.sin(q*L*np.cos(alpha/2)) / (
                q*L*np.cos(alpha/2)))
            # print(retval.shape)
            return retval

    Fs = own_fixed_quad(lambda alpha: (Psi(alpha)**2 * np.sin(alpha)).T, 0, np.pi/2, n=quad_degree)

    # print("Fs_err={}".format(Fs_err))
    if timing:
        print("Fs creation: {}".format(time.time() - t))

    if timing:
        t = time.time()

    x = (q*Rg) ** 2
    try:
        Fc = 2 * (np.exp(-x) - 1 + x) / (x ** 2)
    except FloatingPointError:
        # print("underflow encountered in Fc: use 0 instead, q={}, Rg={}".format(np.linalg.norm(_q),np.linalg.norm(Rg)))
        Fc = 2 * x / (x ** 2)

    if timing:
        print("Fc creation: {}".format(time.time() - t))
    # core - chain correlation

    if timing:
        t = time.time()
    try:
        if np.any(x) <= 0:
            print("x is greater 0: {}".format(x))
        psi = (1 - np.exp(-x)) / x
    except FloatingPointError:
        print("underflow encountered in psi: use 0 instead")
        psi = 0

    if not use_alpha_2:
        Xi_1 = lambda alpha, _r, _l: 2*besselj1(q*_r*np.sin(alpha))/(q*_r*np.sin(alpha)) * np.cos(q*_l*np.cos(alpha)/2)
        Xi_2 = lambda alpha, _r, _l: besselj0(q*_r*np.sin(alpha))*np.sin(q*_l*np.cos(alpha)/2)/(q*_l*np.cos(alpha)/2)
    else:
        Xi_1 = lambda alpha, _r, _l: 2*besselj1(q*_r*np.sin(alpha))/(q*_r*np.sin(alpha))*np.cos(q*_l*np.cos(alpha/2))
        Xi_2 = lambda alpha, _r, _l: besselj0(q*_r*np.sin(alpha))*np.sin(q*_l*np.cos(alpha/2))/(q*_l*np.cos(alpha/2))
    Xi = lambda alpha, _r, _l: _r/(_r+_l)*Xi_1(alpha, _r, _l) + _l/(_r+_l)*Xi_2(alpha, _r, _l)
    Ssc = own_fixed_quad(lambda alpha: (psi*Psi(alpha)*Xi(alpha, R+dR, L+2*dR)*np.sin(alpha)).T, 0, np.pi/2, n=quad_degree)

    # print("Ssc_err={}".format(Ssc_err))
    if timing:
        print("Ssc creation: {}".format(time.time() - t))

    # chain-chain correlation

    if timing:
        t = time.time()
    Scc = own_fixed_quad(lambda alpha: (psi**2 * Xi(alpha, R + dR, L + 2 * dR)**2 * np.sin(alpha)).T, 0, np.pi / 2, n=quad_degree)

    # print("Scc_err={}".format(Scc_err))
    if timing:
        print("Scc creation: {}".format(time.time() - t))
    # % Scc = power(psi, 2). * ((((besselj(0, q * (Rdist'+dR))).^(2)))).*Fl;

    # % put together entire model function 'g'
    # % tetramer (first when only tetramers)

    if timing:
        t = time.time()

    g = (a1 * Fs + a2 * Fc[:, 0] + a3 * Ssc + a4 * Scc) + c

    if timing:
        print("g creation: {}".format(time.time() - t))
        print("#" * 20)
    retval = {
        "result" : g,
        "a1"     : a1,
        "Fs"     : Fs,
        # "Fs_err" : Fs_err,
        "a2"     : a2,
        "Fc"     : Fc,
        "a3"     : a3,
        "Ssc"    : Ssc,
        # "Ssc_err": Ssc_err,
        "a4"     : a4,
        "Scc"    : Scc,
        # "Scc_err": Scc_err,
        "c"      : c,
        "R"      : R,
        "Rg"     : Rg,
        "beta_s" : beta_s,
        "beta_c" : beta_c,
        "L"      : L,
        "N"      : N,
        "dr"     : dr
    }
    return retval


def mode(a, axis=None):
    def _mode1D(a):
        (rep, cnt) = find_repeats(a)
        if not cnt.ndim:
            return (0, 0)
        elif cnt.size:
            return (rep[cnt.argmax()], cnt.max())
        else:
            return (a.min(), 1)

    output = ma.apply_along_axis(_mode1D, axis, a)
    newshape = list(a.shape)
    newshape[axis] = 1
    slices = [slice(None)] * output.ndim
    slices[axis] = 0
    modes = output[tuple(slices)].reshape(newshape)
    slices[axis] = 1
    counts = output[tuple(slices)].reshape(newshape)
    return modes


def get_tetramer_spline(path_to_json_file):
    """
    Load spline using:
    from BioPhy_model import get_tetramer_spline
    tetramer = get_tetramer_spline(tetramer_directory + "spline_data.json")

    :param path_to_json_file:
    :return: callable
    """
    from scipy.interpolate import InterpolatedUnivariateSpline as spline_variant
    from functools import partial
    import json
    with open(path_to_json_file, 'r') as f:
        inf = json.load(f)
    spline_variant = partial(spline_variant, k=inf['k'])
    spline = spline_variant(inf["x"], inf["y"])
    return lambda x: spline(x)
