from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from dolfin import *
from BioPhy_model import modelTwoLambda as model
from BioPhy_model import read_lists_from_data, get_files_from_directory
from scipy.optimize import minimize, least_squares
from mpltools import annotation
import json

from scipy import special
import math

# TODO This needs to be modified to fit your directory structure

directory = "KCl/twomodel/filament/"
used_files, file_names = get_files_from_directory(directory)

optimizer_options = {
    "jac": "3-point",                               # x-point rule for jacobian. 2-point or 3-point
    "method": "trf",                                # optimizer method "trf" or "dogbox" for bounded optimization
    "ftol": 1e-10,                                  # function change tolerance
    "xtol": 1e-10,                                  # tolerance for variable change
    "max_nfev": 10000,                              # maximal number of function evaluations
    "verbose": 2,                                   # level of output: 0 none, 1 only final, 2 every iteration
}

q_min = 0.07
q_max = 1

use_additional_noise = False
start_observation_noise_at = 0.1
observation_noise_slope = -4

use_extrapolation = True
extrapolation_grid = np.logspace(-2.2, 1, num=100)

unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

# initial_guess = [2.54640574, 3.14490887, 5.78039958e-05, 6.32113428e+00]
initial_guess = [6, 6, 3.84490887, 3.84490887, 5.78039958e-05, 5.78039958e-05, 6.32113428e-01, 6.32113428e-01]


# initial_guess.append(0.134)                         # append sigma
# initial_guess.append(1.34375)                       # append lambda
lower_bound =   [1e-5, 1e-5, 1e-8, 1e-8, 1e-8, 1e-8, 1e-8, 1e-8]
# lower_bound.append(0)
# lower_bound.append(0)
upper_bound =   [15, 15, 5, 5, 1, 1, 10, 10]
# upper_bound.append(10)
# upper_bound.append(100)

marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5

assert len(used_files) <= len(marker)

# region Read given data files
file_dict = read_lists_from_data(used_files, q_min, q_max)
qr, qr_short = file_dict["qr"], file_dict["qr_short"]
intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
error, error_short = file_dict["error"], file_dict["error_short"]
# endregion

additional_noise = []
for lia, _err in enumerate(error_short):
    additional_noise.append(np.zeros(len(_err)))
    if use_additional_noise:
        last_qr = qr_short[0]
        init = True
        for lib, _qr in enumerate(qr_short[lia]):
            if _qr < start_observation_noise_at:
                init_noise = intensity_short[lia][lib]
                last_qr = _qr
                continue
            if init:
                additional_noise[lia][lib] = init_noise
                init = False
                continue
            additional_noise[lia][lib] = additional_noise[lia][lib-1] * np.exp(observation_noise_slope * (_qr - last_qr))
            last_qr = _qr


assert len(qr_short) > 0


def loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
    _model_f, _model_t = model(_qr_short[0], *theta, timing=False)
    _retval = (1/(_error_short[0]+_add_noise[0]))*(_intensity_short[0] - (_model_f["result"] + _model_t["result"]))
    if len(_qr_short) > 1:
        for _qr, _int, _err, _noise in zip(_qr_short[1:], _intensity_short[1:], _error_short[1:], _add_noise[1:]):
            _model_f, _model_t = model(_qr, *theta, timing=False)
            _retval = np.append(_retval, (1/(_err + _noise))*(_int - (_model_f["result"] + _model_t["result"])))
    return _retval


for i in range(len(qr_short) + 1):
    if len(qr_short) == 1 and i == 1:
        continue
    if i == len(qr_short):
        loss_dict = {
            "_qr_short"       : [qr_short],
            "_intensity_short": [intensity_short],
            "_error_short"    : [error_short],
            "_add_noise"      : [additional_noise]
        }
    else:
        loss_dict = {
            "_qr_short": [qr_short[i]],
            "_intensity_short": [intensity_short[i]],
            "_error_short": [error_short[i]],
            "_add_noise": [additional_noise[i]]
        }

    res = least_squares(loss, initial_guess, bounds=(lower_bound, upper_bound), kwargs=loss_dict, **optimizer_options)

    print("final parameters: {}".format(res.x))
    print("optimality condition: {}".format(res.optimality))
    print("function evaluations: {}".format(res.nfev))
    print("gradient evaluations: {}".format(res.njev))
    print("reason for termination: {}".format(res.message))

    model_output_f, model_output_t = model(unified_grid, *res.x)

    fig = plt.figure(figsize=(16, 8))
    if i == len(qr_short):
        plt.suptitle(directory + " all measurements")
    else:
        plt.suptitle(directory + file_names[i])

    ax = plt.subplot(2, 1, 1)
    if i == len(qr_short):
        for lia, (_qr, _int, _err, _noise) in enumerate(zip(qr, intensity, error, additional_noise)):
            ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                       facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                       label="int {}".format(lia+1))
            ax.loglog(_qr, _err, '--', label="error {}".format(lia+1))
            if use_additional_noise:
                ax.loglog(_qr, _noise, '-.', label="noise {}".format(lia+1))
    else:
        ax.scatter(qr[i], intensity[i], marker=marker[i], s=marker_size,
                   facecolor='lightgrey', edgecolor=marker_edge_color[i],
                   label="intensity")
        ax.loglog(qr[i], error[i], '--', label="error")
        if use_additional_noise:
            ax.loglog(qr_short[i], additional_noise[i], '-.', label="noise")
    plt.plot(unified_grid, model_output_f["result"]+model_output_t["result"], 'X', label="Interpolated Curve", linewidth=1, markersize=3,
             alpha=0.3)
    plt.plot(unified_grid, model_output_t["result"], 'X', label="Tetramer Curve",
             linewidth=1, markersize=3,
             alpha=0.3)

    plt.plot(unified_grid, model_output_f["result"], 'X', label="Filament Curve",
             linewidth=1, markersize=3,
             alpha=0.3)
    if use_extrapolation:
        mf, mt = model(extrapolation_grid, *res.x)
        plt.plot(extrapolation_grid, mf["result"] + mt["result"], 'X', label="extrapolated Curve",
                 linewidth=1, markersize=3)

        
        def _f(_x):
            mf, mt = model(np.array([_x]), *res.x)
            return mf["result"][0] + mt["result"][0]
        _x1 = q_min/10
        _x2 = q_min
        true_slope = np.round(np.log10((_f(_x2)/_f(_x1)))/(np.log10(_x2 / _x1)), 2)
        print("plot {} slope triangle at {}".format(true_slope, (q_min, _f(np.array([q_min])))))
        annotation.slope_marker((q_min/5, _f(q_min/5)*6), (true_slope, 1), ax=ax)
    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    title_str = "Optimization yields: \n " \
                "Tetramer: R={}, Rg={}, g={}, b={}, lambda={}, sigma={}\n".format(model_output_t["R"],
                                                                                  model_output_t["Rg"],
                                                                                  model_output_t["gamma"],
                                                                                  model_output_t["bc2bs"],
                                                                                  model_output_t["lambda"],
                                                                                  model_output_t["sigma"])
    title_str += "Filament: R={}, Rg={}, g={}, b={}, lambda={}, sigma={}".format(model_output_f["R"],
                                                                                 model_output_f["Rg"],
                                                                                 model_output_f["gamma"],
                                                                                 model_output_f["bc2bs"],
                                                                                 model_output_f["lambda"],
                                                                                 model_output_f["sigma"])
    plt.title(title_str)
    plt.legend()

    ax = plt.subplot(2, 1, 2, sharex=ax)

    plt.loglog(unified_grid, model_output_t["a1"]*model_output_t["Fs"], 'X', label="a1*F2(T)", linewidth=1,
             markersize=3)
    plt.loglog(unified_grid, model_output_t["a2"]*model_output_t["Fc"], 'X', label="a2*Fc(T)", linewidth=1,
             markersize=3)
    plt.loglog(unified_grid, np.abs(model_output_t["a3"]*model_output_t["Ssc"]), 'X', label="a3*Ssc(T)", linewidth=1,
             markersize=3)
    plt.loglog(unified_grid, model_output_t["a4"]*model_output_t["Scc"], 'X', label="a4*Scc(T)", linewidth=1,
             markersize=3)

    plt.loglog(unified_grid, model_output_f["a1"]*model_output_f["Fs"], 'X', label="a1*F2(F)", linewidth=1,
             markersize=3)
    plt.loglog(unified_grid, model_output_f["a2"]*model_output_f["Fc"], 'X', label="a2*Fc(F)", linewidth=1,
             markersize=3)
    plt.loglog(unified_grid, np.abs(model_output_f["a3"]*model_output_f["Ssc"]), 'X', label="a3*Ssc(F)", linewidth=1,
             markersize=3)
    plt.loglog(unified_grid, model_output_f["a4"]*model_output_f["Scc"], 'X', label="a4*Scc(F)", linewidth=1,
             markersize=3)
    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    title_str = "Individual contributions"
    plt.title(title_str)
    plt.legend()

    if i == len(qr_short):
        fig.savefig(directory + "all_optim_model_result.pdf", format="pdf")
    else:
        fig.savefig(directory + file_names[i] + "_optim_model_result.pdf", format="pdf")

    optimizer_result = {
            "final parameters": res.x.tolist(),
            "optimality condition": res.optimality,
            "function evaluations": res.nfev,
            "gradient evaluations": res.njev,
            "reason for termination": res.message
           }
    if use_extrapolation:
        optimizer_result["init_slope"] = true_slope
    data_setting = {
        "qmin": q_min,
        "qmax": q_max,
        "lower_bound": lower_bound,
        "upper_bound": upper_bound,
        "initial_guess": initial_guess
    }
    if i == len(qr_short):
        open_file = directory + "all_opt_result.json"
    else:
        open_file = directory + file_names[i] + "_opt_result.json"
    with open(open_file, 'w') as f:
        json.dump(optimizer_options, f, indent=4, sort_keys=True)
        json.dump(optimizer_result, f, indent=4, sort_keys=True)
        json.dump(data_setting, f, indent=4, sort_keys=True)

