from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
# from BioPhy_model import exp_func as model
from alea.utils.corner import corner
from dolfin import *
from scipy.optimize import least_squares
import json

import cPickle as pickle
from functools import partial
from multiprocessing import Pool, cpu_count
from BioPhy_model import model as model
from BioPhy_model import read_lists_from_data, get_files_from_directory
from BioPhy_model import read_lists_from_data, gaussian, uniform, lognormal

# region parallel configuration
from contextlib import contextmanager


@contextmanager
def terminating(thing):
    try:
        yield thing
    finally:
        thing.terminate()


# endregion

# TODO This needs to be modified to fit your directory structure

tetramer_directory = "KCl/twomodel_new/tetramer/"
t_used_files, t_file_names = get_files_from_directory(tetramer_directory)
filament_directory = "KCl/twomodel_new/filament/"
f_used_files, f_file_names = get_files_from_directory(filament_directory)

optimizer_options = {
    "jac"     : "3-point",  # x-point rule for jacobian. 2-point or 3-point
    "method"  : "trf",  # optimizer method "trf" or "dogbox" for bounded optimization
    "ftol"    : 1e-10,  # function change tolerance
    "xtol"    : 1e-10,  # tolerance for variable change
    "max_nfev": 10000,  # maximal number of function evaluations
    "verbose" : 2,  # level of output: 0 none, 1 only final, 2 every iteration
}

save_chain = True
read_chain_path = None  # directory + "chain_result_wias.pick"

compute_det_result = True

t_q_min = 0.08
t_q_max = 1.2
if filament_directory == "KCl/twomodel_new/filament/":
    q_min = 0.08
    q_max = 0.8
elif filament_directory == "KCl/twomodel_new/mix/":
    q_min = 0.08
    q_max = 0.6
elif filament_directory == "KCl/10mM/":
    q_min = 0.08
    q_max = 0.8
elif filament_directory == "KCl/twomodel_new/tetramer/":
    q_min = t_q_min
    q_max = t_q_max
else:
    raise NotImplementedError

#
# grid = np.linspace(-1, 5, num=1000)
# prior1 = lambda x: lognormal(x, mu=0.5, sigma=0.1)
# prior2 = lambda x: lognormal(x, mu=0.5, sigma=0.2)
# prior3 = lambda x: lognormal(x, mu=0.5, sigma=0.3)
# prior01 = lambda x: lognormal(x, mu=1, sigma=0.1)
# prior02 = lambda x: lognormal(x, mu=1, sigma=0.2)
# prior03 = lambda x: lognormal(x, mu=1, sigma=0.3)
#
# fig = plt.figure(figsize=(10, 10))
# plt.plot(grid, prior1(grid), label="1")
# plt.plot(grid, prior2(grid), label="2")
# plt.plot(grid, prior3(grid), label="3")
# plt.plot(grid, prior01(grid), label="01")
# plt.plot(grid, prior02(grid), label="02")
# plt.plot(grid, prior03(grid), label="03")
# plt.legend()
# fig.savefig(directory + "prior.png")
# exit()

use_additional_noise = False
start_observation_noise_at = 0.1
observation_noise_slope = -4

use_extrapolation = False
extrapolation_grid = np.logspace(-2.2, 1, num=100)

unified_grid = np.logspace(np.log10(q_min), np.log10(q_max), num=200)

# t, R, Rg, gamma, beta, sigma, lambda, dr, c
det_initial_guess = [0.5, 6, 3.84490887, 5.78039958e-05, 6.32113428e-01, 0.134, 1.34, 0, 0]
det_lower_bound = [0, 1e-3, 1e-8, 1e-8, 1e-8, 1e-8, 1e-8, -1e-15, -1]
det_upper_bound = [1, 15, 5, 1, 10, 10, 100, 1, 1]

fix_paras = True

# grid = np.linspace(-1, 10, num=1000)
# fig = plt.figure(figsize=(10, 10))
# plt.plot(grid, prior[0](grid), label="R")
# plt.plot(grid, prior[1](grid), label="Rg")
# plt.plot(grid, prior2(grid), label="2")
# plt.plot(grid, prior3(grid), label="3")
# plt.plot(grid, prior01(grid), label="01")
# plt.plot(grid, prior02(grid), label="02")
# plt.plot(grid, prior03(grid), label="03")
# plt.legend()
# fig.savefig(directory + "prior.png")

labels = ["t", "R", "Rg", "log(gamma)", "log(beta)", "lambda", "log(dr)", "c"]

marker_edge_color = "rgbcyk"
marker = "v><^12"
marker_size = 5

assert len(f_used_files) <= len(marker)

# region Read given data files
t_file_dict = read_lists_from_data(t_used_files, t_q_min, t_q_max)
t_qr, t_qr_short = t_file_dict["qr"], t_file_dict["qr_short"]
t_intensity, t_intensity_short = t_file_dict["intensity"], t_file_dict["intensity_short"]
t_error, t_error_short = t_file_dict["error"], t_file_dict["error_short"]

f_file_dict = read_lists_from_data(f_used_files, q_min, q_max)
f_qr, f_qr_short = f_file_dict["qr"], f_file_dict["qr_short"]
f_intensity, f_intensity_short = f_file_dict["intensity"], f_file_dict["intensity_short"]
f_error, f_error_short = f_file_dict["error"], f_file_dict["error_short"]
# endregion


old_model_hash = dict()

assert len(f_qr_short) > 0
assert len(t_qr_short) == 1


# region loss function

def t_loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
    if fix_paras:
        theta[4] = 0.134
        theta[5] = 1.34
        theta[6] = 0
        theta[7] = 0
    # assert len(_qr_short) == 1
    # assert len(_intensity_short) == 1
    # assert len(_error_short) == 1

    _model = model(_qr_short[0], *theta)
    _retval = (1 / (_error_short[0])) * (_intensity_short[0] - _model["result"])
    return _retval


def loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
    if fix_paras:
        theta[5] = 0.134
        theta[6] = 1.34
        theta[7] = 0
        theta[8] = 0
    if (frozenset(_qr_short[0]), frozenset(t_parameter)) not in old_model_hash:
        old_model_hash[(frozenset(_qr_short[0]), frozenset(t_parameter))] = model(_qr_short[0], *t_parameter)[
            "result"]

    _t_model = old_model_hash[(frozenset(_qr_short[0]), frozenset(t_parameter))]
    _model = model(_qr_short[0], *theta[1:])
    _retval = (1 / (_error_short[0])) * (
                _intensity_short[0] - ((1 - theta[0]) * _model["result"] + theta[0] * _t_model))
    # _retval = (_intensity_short[0] - _model["result"])
    if len(_qr_short) > 1:
        for __qr, __int, __err in zip(_qr_short[1:], _intensity_short[1:], _error_short[1:]):
            if (frozenset(__qr), frozenset(t_parameter)) not in old_model_hash:
                old_model_hash[(frozenset(__qr), frozenset(t_parameter))] = model(__qr, *t_parameter)["result"]

            _t_model = old_model_hash[(frozenset(_qr_short[0]), frozenset(t_parameter))]
            _model = model(__qr, *theta[1:])
            _retval = np.append(_retval,
                                (1 / __err) * (__int - ((1 - theta[0]) * _model["result"]) + theta[0] * _t_model))
            # _retval = np.append(_retval, (_int - _model["result"]))
    return _retval


# endregion


for i in range(len(f_qr_short) + 1):
    if len(f_qr_short) == 1 and i == 1:
        continue
    if i == len(f_qr_short):
        loss_dict = {
            "_qr_short"       : [f_qr_short],
            "_intensity_short": [f_intensity_short],
            "_error_short"    : [f_error_short],
            "_add_noise"      : [None]
        }
    else:
        loss_dict = {
            "_qr_short"       : [f_qr_short[i]],
            "_intensity_short": [f_intensity_short[i]],
            "_error_short"    : [f_error_short[i]],
            "_add_noise"      : [None]
        }

    # compute det result for filament curve

    t_loss_dict = {
        "_qr_short"       : [t_qr_short[0]],
        "_intensity_short": [t_intensity_short[0]],
        "_error_short"    : [t_error_short[0]],
        "_add_noise"      : [None]
    }
    t_res = least_squares(t_loss, det_initial_guess[1:], bounds=(det_lower_bound[1:], det_upper_bound[1:]), 
                          kwargs=t_loss_dict, **optimizer_options)

    print("Tetramer optimisation:")
    print("  final parameters: {}".format(t_res.x))
    print("  optimality condition: {}".format(t_res.optimality))
    print("  function evaluations: {}".format(t_res.nfev))
    print("  gradient evaluations: {}".format(t_res.njev))
    print("  reason for termination: {}".format(t_res.message))
    t_parameter = t_res.x
    t_model_output = model(unified_grid, *t_parameter)
    res = least_squares(loss, det_initial_guess, bounds=(det_lower_bound, det_upper_bound), kwargs=loss_dict,
                        **optimizer_options)

    print("final parameters: {}".format(res.x))
    print("optimality condition: {}".format(res.optimality))
    print("function evaluations: {}".format(res.nfev))
    print("gradient evaluations: {}".format(res.njev))
    print("reason for termination: {}".format(res.message))
    det_model_para = res.x
    det_model_output = model(unified_grid, *res.x[1:])

    fig = plt.figure(figsize=(24, 8))

    ax = plt.subplot(2, 2, 1)
    for lia, (_qr, _int, _err) in enumerate(zip(t_qr, t_intensity, t_error)):
        ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                   facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                   label="int {}".format(lia + 1))
        ax.loglog(_qr, _err, '--', label="error {}".format(lia + 1))

    plot_result = t_model_output["result"]
    plt.plot(unified_grid, plot_result, 'X', label="Tetramer fit", linewidth=1, markersize=3)

    plt.axvline(t_q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(t_q_max, linestyle="dashed", color="black", label="q max")
    plt.legend()
    title_str = "Tetramer Parameter: ({})\n".format(["R", "Rg", "log(gamma)", "log(beta)"])
    title_str += "Optimization: {}".format(t_parameter[:5])
    # if det_optimization_result is not None:
    #     title_str += "\n Optimization yields: R={}, Rg={}, $gamma$={}, " \
    #                  "$beta$={}, loss={a}".format(*det_optimization_result[:4],
    #                                               a=np.linalg.norm(loss(det_optimization_result)))
    plt.title(title_str)

    ax = plt.subplot(2, 2, 2)
    for lia, (_qr, _int, _err) in enumerate(zip(f_qr, f_intensity, f_error)):
        ax.scatter(_qr, _int, marker=marker[lia], s=marker_size,
                   facecolor='lightgrey', edgecolor=marker_edge_color[lia],
                   label="int {}".format(lia + 1))
        ax.loglog(_qr, _err, '--', label="error {}".format(lia + 1))

    det_model_plot = (1 - det_model_para[0]) * det_model_output["result"] + det_model_para[0] * t_model_output["result"]
    plt.plot(unified_grid, det_model_plot, 'x', label="Optimization", linewidth=1, markersize=3)

    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    plt.legend()
    title_str = "Parameter: ({})\n".format(labels)
    if fix_paras:
        title_str += "Optimization: {}".format(det_model_para[:5])
    else:
        title_str += "Optimization: {}".format(det_model_para)
    plt.title(title_str)
    # fig.savefig(directory + "mcmc_model_result_wias.pdf")
    ax = plt.subplot(2, 2, 4, sharex=ax)

    fs = (1 - det_model_para[0]) * det_model_output["a1"] * det_model_output["Fs"] + det_model_para[0] * t_model_output["a1"] * \
         t_model_output["Fs"]
    plt.loglog(unified_grid, fs, 'X', label="a1*Fs", linewidth=1, markersize=3)
    fc = (1 - det_model_para[0]) * det_model_output["a2"] * det_model_output["Fc"] + det_model_para[0] * t_model_output["a2"] * \
         t_model_output["Fc"]
    plt.loglog(unified_grid, fc, 'X', label="a2*Fc", linewidth=1, markersize=3)
    ssc = (1 - det_model_para[0]) * np.abs(det_model_output["a3"] * det_model_output["Ssc"]) + \
          det_model_para[0] * np.abs(t_model_output["a3"] * t_model_output["Ssc"])
    plt.loglog(unified_grid, ssc, 'X', label="a3*Ssc", linewidth=1, markersize=3)
    scc = (1 - det_model_para[0]) * det_model_output["a4"] * det_model_output["Scc"] + det_model_para[0] * t_model_output["a3"] * \
          t_model_output["Scc"]
    plt.loglog(unified_grid, scc, 'X', label="a4*Scc", linewidth=1,
               markersize=3)
    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    title_str = "Individual contributions"
    plt.title(title_str)
    plt.legend()

    ax = plt.subplot(2, 2, 3, sharex=ax)

    plt.plot(unified_grid, t_model_output["result"], 'X', label="Tetramer", linewidth=1, markersize=3)
    plt.loglog(unified_grid, t_model_output["a1"] * t_model_output["Fs"], 'X', label="a1*F2", linewidth=1,
               markersize=3)
    plt.loglog(unified_grid, t_model_output["a2"] * t_model_output["Fc"], 'X', label="a2*Fc", linewidth=1,
               markersize=3)
    plt.loglog(unified_grid, np.abs(t_model_output["a3"] * t_model_output["Ssc"]), 'X', label="a3*Ssc", linewidth=1,
               markersize=3)
    plt.loglog(unified_grid, t_model_output["a4"] * t_model_output["Scc"], 'X', label="a4*Scc", linewidth=1,
               markersize=3)
    plt.axvline(q_min, linestyle="dashed", color="black", label="q min")
    plt.axvline(q_max, linestyle="dashed", color="black", label="q max")
    title_str = "Tetramer: Individual contributions"
    plt.title(title_str)
    plt.legend()

    if i == len(f_qr_short):
        fig.savefig(filament_directory + "all_optim_model_result_oldmodel.pdf", format="pdf")
    else:
        fig.savefig(filament_directory + f_file_names[i] + "_optim_model_result_oldmodel.pdf", format="pdf")

    # region Save results
    optimizer_result = {
        "final parameters"      : res.x.tolist(),
        "optimality condition"  : res.optimality,
        "function evaluations"  : res.nfev,
        "gradient evaluations"  : res.njev,
        "reason for termination": res.message
    }
    data_setting = {
        "qmin"         : q_min,
        "qmax"         : q_max,
        "lower_bound"  : det_lower_bound,
        "upper_bound"  : det_upper_bound,
        "initial_guess": det_initial_guess
    }
    if i == len(f_qr_short):
        open_file = filament_directory + "all_opt_result_oldmodel.json"
    else:
        open_file = filament_directory + f_file_names[i] + "_opt_result_oldmodel.json"
    with open(open_file, 'w') as f:
        json.dump(optimizer_options, f, indent=4, sort_keys=True)
        json.dump(optimizer_result, f, indent=4, sort_keys=True)
        json.dump(data_setting, f, indent=4, sort_keys=True)
    # endregion
