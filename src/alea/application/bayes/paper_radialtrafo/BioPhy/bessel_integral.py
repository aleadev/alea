from __future__ import division, print_function
from scipy.special import j0 as besselj0
from scipy.special import j1 as besselj1
from scipy.integrate import quad, fixed_quad
import numpy as np
from scipy.special.orthogonal import p_roots
import timeit


grid = np.linspace(-1, 1, num=100)

def f(_x):
    loc_x = np.array(_x)[:, np.newaxis]
    return loc_x**2*grid

def f2(_x, lia):
    return _x**2*grid[lia]

t = timeit.default_timer()

def quad1():
    res = [fixed_quad(f2, 0, np.pi / 2, args=(lia,))[0] for lia in range(len(grid))]

def quad2():
    res3 = [fixed_quad(lambda x: f2(x, lia), 0, np.pi / 2)[0] for lia in range(len(grid))]

def quad3():
    res = own_fixed_quad(f, 0, np.pi / 2)


duration = timeit.timeit(quad1, number=1000)
print("duration args: {}".format(duration))
duration = timeit.timeit(quad2, number=1000)
print("duration list: {}".format(duration))
duration = timeit.timeit(quad3, number=1000)
print("duration own quad: {}".format(duration))
exit()







R = 3
L = 40

def Psi(alpha, loc_q):
    retval = np.array(2 * besselj1(loc_q * R * np.sin(alpha)) / (loc_q * R * np.sin(alpha)) * np.sin(
        loc_q * L * np.cos(alpha) / 2) / (
                              loc_q * L * np.cos(alpha) / 2))
    # print(retval.shape)
    return retval

def BJ1(loc_q):
    return (2*np.sin(1-besselj0(loc_q*R*np.pi)))


grid = np.linspace(0.001, 10, num=100)
quad_result = np.zeros(len(grid))
exact_result = np.zeros(len(grid))
for lia, q in enumerate(grid):
    curr_fun = lambda alpha, _q: Psi(alpha, _q)
    quad_result[lia] = quad(curr_fun, 0, np.pi/2)[0]
    # exact_result =
