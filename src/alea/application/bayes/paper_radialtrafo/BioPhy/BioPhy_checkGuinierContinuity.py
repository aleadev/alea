import json
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from BioPhy_helper_guinier import (loss_functional, fit_data, plot_guinier, smooth, plot_scatter_fun)

import numpy as np
from BioPhy_model import read_lists_from_data, get_files_from_directory


with open("problem_info.json", "r") as f:
    problem_dict = json.load(f)
with open("all_info.json", "r") as f:
    all_inf = json.load(f)

plt.rc('font', family='serif')

plot_parameter_flag = False
plot_data_flag = True
plot_guinier_flag = False
plot_mean_flag = False

color = ["forestgreen", "lightcoral", "peru", "olivedrab", "gold", "aqua", "dodgerblue", "crimson",
         "indigo", "navy", "bisque", "silver"]
intensity_mult_factor = 8

ylim_rg_guinier = [0, 30]
ylim_i0 = [0, 2]

para_plot_size = (6, 5)
mean_plot_para_size = (5, 5)
scatter_plot_size = (6, 6)

identifier = "__BrennichModelTrimmedtetramer_LeastSquares"
salt_list = ["KCl"]
directory_list = [directory + "/" for directory in salt_list]
for salt_idx, directory in enumerate(directory_list):
    if directory == "KCl/":
        loc_directory_list = ["40"]
    else:
        raise ValueError("unknown directory {}. Use KCl, CaCl2, CoCl3, MgCl2, NaCl, Sp4".format(directory))

    for loc_dir in loc_directory_list:
        print(directory + loc_dir)
        used_files, file_names = get_files_from_directory(directory + loc_dir + "mM/")

        file_dict = read_lists_from_data(used_files, 1e-10, 10, intensity_mult_factor)
        qr, qr_short = file_dict["qr"], file_dict["qr_short"]
        intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
        error, error_short = file_dict["error"], file_dict["error_short"]

        gui_rg_list = []
        gui_i0_list = []
        gui_maxidx_list = []
        for i in range(len(qr_short)+1):
            if len(qr_short) == 1 and i == 1:
                print("  All run left out")
                continue
            if i == len(qr_short):
                curr_qr = np.array([_y for _x in qr_short for _y in _x])
                curr_inten = np.array([_y for _x in intensity_short for _y in _x])
                curr_err = np.array([_y for _x in error_short for _y in _x])
                print("  All run with length {}".format(len(curr_qr)))

            else:
                curr_qr = qr_short[i]
                curr_inten = intensity_short[i]
                curr_err = error_short[i]
                print("  Dataset {} with length {}".format(file_names[i], len(curr_qr)))

            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            # Model parameter to modify
            # directory_list = ["KCl/30mM/"]
            max_Rgq = 1.3  # optimizer takes interval where Rg*q <= max_Rgq
            safety_offset = 0.2
            verbose = 0  # verbosity level. 0 None, 2 plots every iteration step
            use_first_node_after_max_Rgq = True
            # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            Rg_list = []
            I0_list = []
            q_min = problem_dict[directory + loc_dir + "mM/"]["q_min"]
            iv = np.ones(2)
            curr_opt_para = np.ones(2)
            max_idx = 2
            while True:
                loss_dict = {
                    "_qr"       : curr_qr[q_min <= curr_qr][:max_idx],
                    "_intensity": curr_inten[q_min <= curr_qr][:max_idx],
                    "_error"    : np.ones(len(curr_qr[q_min <= curr_qr][:max_idx])),
                    # "_error": error_list[lia][q_min_list[lia] <= qr_list[lia]][:max_idx],
                    "p"         : 2
                }
                # bounds = [(l, b) for l, b in zip([i0_lb[lia], rg_lb[lia]], [i0_ub[lia], rg_ub[lia]])]

                res = minimize(loss_functional, iv, args=loss_dict, method="BFGS",
                               options={"disp": False, "gtol": 1e-7, "norm": 2})
                curr_opt_para = res.x.tolist()
                curr_opt_para[1] = np.abs(curr_opt_para[1])
                if curr_qr[q_min <= curr_qr][max_idx - 1] * np.abs(curr_opt_para[1]) >= max_Rgq:
                    # print(qr[q_min <= qr][max_idx - 1] * np.abs(curr_opt_para[1]))
                    break
                max_idx += 1
                iv = res.x.tolist()
            # use_first_node_after_max_Rgq:
            if i == len(qr_short):
                gui_i0_list.append(curr_opt_para[0])
                gui_rg_list.append(curr_opt_para[1])
                gui_maxidx_list.append(max_idx)
            else:
                if len(qr_short) == 1:
                    gui_i0_list.append(curr_opt_para[0])
                    gui_rg_list.append(curr_opt_para[1])
                    gui_maxidx_list.append(max_idx)
print("Faktor: {}".format(intensity_mult_factor))
print("Guinier I0: {}".format(gui_i0_list[0]))
print("Guinier Rg: {}".format(gui_rg_list[0]))
print("Guinier maximal index: {}".format(gui_maxidx_list[0]))