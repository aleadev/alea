from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares, minimize

import json
from BioPhy_model import read_lists_from_data, get_files_from_directory, exp_fun_paras, LogLogisticFractionalPolynomial
from BioPhy_helper_guinier import smooth
# np.random.seed(8011990)


tetramer_directory = "tetramer/1mg_modified/"
t_used_files, t_file_names = get_files_from_directory(tetramer_directory)

optimizer_options = {
    "jac": "3-point",                               # x-point rule for jacobian. 2-point or 3-point
    "method": "trf",                                # optimizer method "trf" or "dogbox" for bounded optimization
    "ftol": 1e-10,                                  # function change tolerance
    "xtol": 1e-10,                                  # tolerance for variable change
    "max_nfev": 10000,                              # maximal number of function evaluations
    "verbose": 2,                                   # level of output: 0 none, 1 only final, 2 every iteration
    "loss": "arctan"                                # "linear", "soft_l1", "huber", "cauchy", "arctan"
}

t_q_min = 0.08
t_q_max = 1.5
smoothing_window = 0
intensity_mult_factor = 2.64

# region Read given data files
t_file_dict = read_lists_from_data(t_used_files, t_q_min, t_q_max, intensity_mult_factor)
t_qr, t_qr_short = t_file_dict["qr"], t_file_dict["qr_short"]
t_intensity, t_intensity_short = t_file_dict["intensity"], t_file_dict["intensity_short"]
t_error, t_error_short = t_file_dict["error"], t_file_dict["error_short"]
assert len(t_qr_short) == 1
# endregion

curr_model = LogLogisticFractionalPolynomial()
M = curr_model.params
limit = 0.0
if smoothing_window > 1:
    assert smoothing_window % 2 == 1
    t_intensity_short[0] = smooth(t_intensity_short[0], smoothing_window)
    t_intensity[0] = smooth(t_intensity[0], smoothing_window)
t_loss_dict = {
    "qr"       : t_qr_short[0],
    "inten": t_intensity_short[0],
    "err"    : t_error_short[0],
    "p"               : 2
}


use_polyfit = True

if use_polyfit:
    # fit_fn = np.poly1d(np.polyfit(t_loss_dict["qr"], t_loss_dict["inten"], deg=50))
    from scipy.interpolate import UnivariateSpline as spline_variant

    from functools import partial
    # spline_variant = partial(spline_variant, k=3, s=0.00035)
    spline_variant = partial(spline_variant, k=3, s=0.00006)
    spline = spline_variant(t_loss_dict["qr"], t_loss_dict["inten"])
    fit_fn = lambda x: spline(x)

    spline_data = {
        "x": t_loss_dict["qr"].tolist(),
        "y": t_loss_dict["inten"].tolist(),
        "k": 3
    }
else:
    iv = np.append(1 * np.ones(M - 1), 1e-1)
    verbose = 2
    method = "BFGS"
    res = minimize(curr_model.loss_functional, iv, args=t_loss_dict, method=method,
                   options={"disp": verbose, "gtol": 1e-7, "norm": 2})
    print(res)
    t_parameter = res.x.tolist()

# t_res = least_squares(t_loss, iv, kwargs=t_loss_dict, **optimizer_options)

# print("Tetramer optimisation:")
# print("  final parameters: {}".format(t_res.x))
# print("  optimality condition: {}".format(t_res.optimality))
# print("  function evaluations: {}".format(t_res.nfev))
# print("  gradient evaluations: {}".format(t_res.njev))
# print("  reason for termination: {}".format(t_res.message))
# t_parameter = t_res.x

grid = np.linspace(t_q_min, t_q_max, num=10000)
fig = plt.figure(figsize=(10, 10))
if use_polyfit:
    mod = fit_fn(grid)
else:
    mod = curr_model(grid, *t_parameter)
plt.loglog(grid, mod, '-b', label="fit")
plt.scatter(t_qr, t_intensity, marker="v", s=5, facecolor='lightgrey', edgecolor="r", label="data")
plt.loglog(t_qr[0], t_error[0], '--', label="err")

plt.axvline(t_q_min, linestyle="dashed", color="black", label="q min")
plt.axvline(t_q_max, linestyle="dashed", color="black", label="q max")
plt.legend()
fig.savefig(tetramer_directory + "tetramer_curve.pdf")

open_file = tetramer_directory + "spline_data.json"
with open(open_file, 'w') as f:
    json.dump(spline_data, f, indent=4, sort_keys=True)
