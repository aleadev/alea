from __future__ import (division, print_function, absolute_import)
import sys
import numpy as np
from alea.application.bayes.paper_radialtrafo.quadratic_transport import Banana_perturbed_prior
from alea.application.bayes.paper_radialtrafo.radial_tt_simple import (reconstruct_tt_uq, polar2cart, RadialTT,
                                                                       MCMC_Sampler)
from alea.application.bayes.paper_radialtrafo.moments import moment_generator_quadratic_transport as mqt
from scipy.stats import multivariate_normal
from alea.utils.progress.percentage import PercentageBar
import time
import json

import os
if sys.version_info < (3, 0):
    from dolfin import (set_log_level, WARNING)
    set_log_level(WARNING)
else:
    from dolfin import (set_log_level, LogLevel)
    set_log_level(LogLevel.ERROR)

np.random.seed(8011990)

create_table_files = False
create_mcmc_samples = False
use_quadrature = False

radi = [2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 26, 31] #, 6, 7, 8, 9, 10, 12, 15, 20, 25]
r_poly_deg = 10
theta_poly_deg = 21
n_recon_sample = 1000
d = 2

true_0_moment = np.array(2.7175467088793495)
true_1_moment = np.array([0, -2])
true_2_moment = np.array([[1, 0.9], [0.9, 7]])

for alpha_t in [0.0]: #, 0.25, 0.5, 0.75, 1.0]:
    pert_prior = Banana_perturbed_prior(alpha_t)
    experiment_path = "experiments/convex/alpha_{}/".format(alpha_t)
    if not os.path.exists(experiment_path):
        os.makedirs(experiment_path)

    tensor_save_path = experiment_path + "tensors/"
    if not os.path.exists(tensor_save_path):
        os.makedirs(tensor_save_path)

    p_prior_sample_path = experiment_path + "p_prior_samples/"
    if not os.path.exists(p_prior_sample_path):
        os.makedirs(p_prior_sample_path)
    p_prior_sample_path = experiment_path + "p_prior_samples/samples_{}.dat".format(n_recon_sample)

    prior_sample_path = experiment_path + "prior_samples/"
    if not os.path.exists(prior_sample_path):
        os.makedirs(prior_sample_path)
    prior_sample_path = experiment_path + "prior_samples/samples_{}.dat".format(n_recon_sample)

    posterior_sample_path = experiment_path + "posterior_samples/"
    if not os.path.exists(posterior_sample_path):
        os.makedirs(posterior_sample_path)
    posterior_sample_path = experiment_path + "posterior_samples/samples_{}.dat".format(n_recon_sample)

    quad_path = experiment_path + "quadrature/"
    if not os.path.exists(quad_path):
        os.makedirs(quad_path)

    result_path = experiment_path + "radi_convergence_{}.dat".format(n_recon_sample)
    result_table_path = experiment_path + "radi_convergence_result_{}.dat".format(n_recon_sample)

    if use_quadrature:
        from scipy.integrate import dblquad
        from alea.utils.decorators import count_calls

        lb = -np.inf
        ub = np.inf
        for eps in [10, 1, 0.1, 0.01, 0.001, 0.0001]:

            def ownquad(i, j):
                return dblquad(lambda x, y: (x**i) * (y**j) * curr_post(np.array([x, y]).reshape(1, 2)), lb, ub,
                                     lambda x: lb,
                                     lambda x: ub, epsrel=eps, epsabs=eps)
            mean = np.zeros(2)
            calls_mean = 0
            @count_calls
            def curr_post(x):
                return pert_prior.post(x)

            mean[0], err = ownquad(1, 0)

            print("mean0 = {}, err={}, calls={}".format(mean[0], err, curr_post.num_calls))
            mean[1], err = ownquad(0, 1)
            calls_mean += curr_post.num_calls
            print("mean1 = {}, err={}, calls={}".format(mean[1], err, calls_mean))

            quad_2_moment = np.zeros((2, 2))
            calls_2_moment = 0


            @count_calls
            def curr_post(x):
                return pert_prior.post(x)


            quad_2_moment[0, 0], err = ownquad(2, 0)

            print("quad[0, 0] = {}, err={}, calls={}".format(quad_2_moment[0, 0], err, curr_post.num_calls))
            quad_2_moment[0, 1], err = ownquad(1, 1)
            quad_2_moment[1, 0] = quad_2_moment[0, 1]

            print("quad[0, 1] = {}, err={}, calls={}".format(quad_2_moment[0, 1], err, curr_post.num_calls))

            quad_2_moment[1, 1], err = ownquad(0, 2)

            calls_2_moment += curr_post.num_calls
            print("quad[1, 1] = {}, err={}, calls={}".format(quad_2_moment[1, 1], err, calls_2_moment))

            inf = {
                "quad_1_moment": mean.tolist(),
                "quad_1_calls": calls_mean,
                "rel": eps,
                "quad_2_moment": quad_2_moment.tolist(),
                "quad_2_calls": calls_2_moment
            }
            with open(quad_path + "eps{}.json".format(str(eps).replace(".","_")), "w") as f:
                json.dump(inf, f, indent=2)
        exit()

    if create_table_files:
        assert os.path.exists(result_path)
        with open(result_path, "r") as f:
            info = json.load(f)

        if False:
            assert os.path.exists(posterior_sample_path.replace(".dat", ".json"))

            with open(posterior_sample_path.replace(".dat", ".json"), "r") as f:
                mc_info = json.load(f)

            with open(posterior_sample_path.replace(".dat", ".txt"), "w") as out:
                out.write("sample,"
                          "z_err_l2,"
                          "z_err_l2_rel,"
                          "mean_err_l2,"
                          "mean_err_l2_rel,"
                          "cov_err_l2,"
                          "cov_err_l2_rel"
                          "\n")
                for lia in range(len(mc_info["samples"])):
                    z = np.array(mc_info["z"][lia])
                    mean = np.array(mc_info["mean"][lia])
                    cov = np.array(mc_info["cov"][lia])
                    z_err = np.linalg.norm(true_0_moment - z)
                    mean_err = np.linalg.norm(true_1_moment - mean)
                    cov_err = np.linalg.norm(true_2_moment - cov)
                    out.write("{},{},{},{},{},{},{}"
                              "\n".format(mc_info["samples"][lia], z_err, z_err/np.linalg.norm(true_0_moment),
                                          mean_err, mean_err/np.linalg.norm(true_1_moment),
                                          cov_err, cov_err/np.linalg.norm(true_2_moment)))
        mean_list = []
        cov_list = []
        z_list = []
        with open(result_table_path, "w") as out:
            out.write("r,"
                      "z_err_l2,"
                      "z_err_l2_rel,"
                      "mean_err_l2,"
                      "mean_err_l2_rel,"
                      "cov_err_l2,"
                      "cov_err_l2_rel,"
                      "kl,"
                      "calls"
                      "\n")
            for inf in info:
                r = np.array(inf["num_radi"])
                tt_0_moment = np.array(inf["tt_0_moment"])
                tt_1_moment = np.array(inf["tt_1_moment"])
                tt_2_moment = np.array(inf["tt_2_moment"])
                # kl_dist = np.array(inf["kl_dist"])
                calls = np.array(inf["calls"])

                curr_0_moment_err = np.linalg.norm(true_0_moment - tt_0_moment)
                curr_1_moment_err = np.linalg.norm(true_1_moment - tt_1_moment)
                curr_2_moment_err = np.linalg.norm(true_2_moment - tt_2_moment)

                out.write("{},{},{},{},{},{},{},{},{}"
                          "\n".format(r, curr_0_moment_err, curr_0_moment_err / np.linalg.norm(true_0_moment),
                                      curr_1_moment_err, curr_1_moment_err / np.linalg.norm(true_1_moment),
                                      curr_2_moment_err, curr_2_moment_err / np.linalg.norm(true_2_moment),
                                      0, calls))

        continue

    if create_mcmc_samples:
        print("create {}*walker + 1000 burn in MCMC samples for error sampling later from true posterior".format(n_recon_sample))

        mc_ref = 100000
        aquire = (mc_ref - 10000)*5
        func = lambda x: np.log(pert_prior(x))
        sampler = MCMC_Sampler(func,
                               mc_ref,
                               10000,
                               2,
                               4 + 2,
                               p0=multivariate_normal.rvs(mean=np.zeros(2), cov=np.eye(2, 2),
                                                          size=2 * 2 + 2))

        print(len(sampler))
        samples = sampler.get(aquire)

        splits = np.logspace(np.log10(10), np.log10(aquire), 100)
        sample_list = []
        Z_list = []
        mean_list = []
        cov_list = []
        bar = PercentageBar(len(splits))
        print("Compute moments")
        for split in splits:
            curr_n = int(split)
            curr_samples = samples[:int(split), :]
            # print("Compute moments with {} samples".format(len(samples)))
            sample_list.append(curr_n)
            curr_1_moment = (1/curr_n) * np.sum([pert_prior._convex_T.T(s) for s in curr_samples], axis=0)
            mean_list.append(curr_1_moment.tolist())
            curr_2_moment = (1/curr_n) * np.sum([np.outer(pert_prior._convex_T.T(s), pert_prior._convex_T.T(s)) for s in curr_samples], axis=0)
            cov_list.append(curr_2_moment.tolist())
            # This does nothing. 1mio samples will never give you anything in the concentration region of the liklihood
            # curr_z = (1/curr_n) * np.sum([np.exp(-problem.Phi(s)) for s in prior_samples], axis=0)
            Z_list.append(1)
            bar.next()

        curr_inf = {"mean"   : mean_list,
                    "cov"    : cov_list,
                    "z"      : Z_list,
                    "samples": sample_list}
        with open(posterior_sample_path.replace(".dat", ".json"), "w") as f:
            json.dump(curr_inf, f, indent=4)
        continue

    # run with different number of radi
    info = []
    for r in radi:
        curr_path = tensor_save_path + "r{}_mc{}.dat".format(r, n_recon_sample)
        recon_dict = {
            "radi": np.linspace(0, 10, num=r),
            "maxdegs": [[r_poly_deg] * (r-1)] + [theta_poly_deg] * (d - 1),
            "func": lambda theta: pert_prior(polar2cart(theta)),
            "n_samples": n_recon_sample,
            "_check_onb": True,
            "save_path": tensor_save_path + "r{}_mc{}_pr{}_pt{}_tt.dat".format(r, n_recon_sample,
                                                                               r_poly_deg,
                                                                               theta_poly_deg),
        }
        start = time.time()
        if recon_dict["save_path"] is not None and True:
            print("Try to read tt list")
            ten_list = RadialTT.load(recon_dict["save_path"])
            if ten_list is not None:
                pass
                # print("tt list read: {}".format(ten_list))
            else:
                ten_list = reconstruct_tt_uq(**recon_dict)
        else:
            ten_list = reconstruct_tt_uq(**recon_dict)

        print("Tensor reconstruction result")
        print(" Dimensions: {}".format([t.n for t in ten_list]))
        print(" Ranks: {}".format([t.r for t in ten_list]))

        rad_tt = RadialTT(ten_list, recon_dict["radi"])
        if recon_dict["save_path"] is not None:
            rad_tt.save(recon_dict["save_path"])

        if False:
            r = np.linspace(0, 10, 50)  # np.linspace(-2,2,ndiscr) #
            t = np.linspace(0, 2 * np.pi, 50)  # np.linspace(-2,2, ndiscr) #
            rr, tt = np.meshgrid(r, t)

            vals_polar = []

            for t_val in t:
                for r_val in r:
                    vals_polar.append(recon_dict["func"]([r_val, t_val]))

            vals_polar = np.array(vals_polar)
            vals_polar = vals_polar.reshape(rr.shape)

            vals_polar_tt = []

            for t_val in t:
                for r_val in r:
                    vals_polar_tt.append(rad_tt([r_val, t_val], normalised=False))

            vals_polar_tt = np.array(vals_polar_tt)
            vals_polar_tt = vals_polar_tt.reshape(rr.shape)

            fig = plt.figure()
            plt.subplot(311)
            plt.title("polar coordinate view True")

            surf = plt.pcolormesh(rr, tt, vals_polar, cmap='coolwarm', edgecolor='none')
            fig.colorbar(surf, shrink=0.5, aspect=5)

            plt.subplot(312)
            plt.title("polar coordinate view TT")
            surf = plt.pcolormesh(rr, tt, vals_polar_tt, cmap='coolwarm', edgecolor='none')
            fig.colorbar(surf, shrink=0.5, aspect=5)
            plt.subplot(313)
            plt.title("polar coordinate view")
            surf = plt.pcolormesh(rr, tt, np.abs(vals_polar-vals_polar_tt), cmap='coolwarm', edgecolor='none')
            fig.colorbar(surf, shrink=0.5, aspect=5)
            plt.tight_layout()
            fig.savefig("banana_test_TT_diff_alpha{}.png".format(str(alpha_t).replace(".", "_")))
        if False:
            from scipy.integrate import quad
            n = len(rad_tt._TTlist[0].basis[0])

            for idx, ten in enumerate(rad_tt._TTlist):
                gramian = np.zeros((n, n))
                for lia, f1 in enumerate(ten.basis[0]):
                    for lib, f2 in enumerate(ten.basis[0]):
                        gramian[lia, lib], err = quad(lambda _r: f1(_r)*f2(_r)*_r, recon_dict["radi"][idx], recon_dict["radi"][idx+1])
                        print("f1", lia, "f2", lib, "err", err, "value", gramian[lia, lib])
                print(gramian)
                input()
            exit()

        print("Compute Z with TT")
        Z_list, Z_tt = rad_tt.compute_Z()
        rad_tt.set_normalisation(Z_tt)


        def contract(l, v):
            return rad_tt._TTlist[l].contract_veclist(v)

        tbasis = [[ten.basis[0] for ten in rad_tt._TTlist]] + rad_tt._TTlist[0].basis[1:]
        gen = mqt(2, pert_prior._convex_T, tbasis, contract, r_layers=[(r1, r2) for r1, r2 in zip(recon_dict["radi"][:-1],
                                                                                                  recon_dict["radi"][1:])])
        # compute Z by moments
        tt_0_moment = Z_tt

        tt_1_moment = np.zeros(2)
        tt_1_moment[0] = gen([1, 0], verbose=False)
        tt_1_moment[1] = gen([0, 1], verbose=False)

        tt_2_moment = np.zeros((2, 2))
        tt_2_moment[0, 0] = gen([2, 0], verbose=False)
        tt_2_moment[1, 0] = gen([1, 1], verbose=False)
        tt_2_moment[0, 1] = gen([1, 1], verbose=False)
        tt_2_moment[1, 1] = gen([0, 2], verbose=False)

        duration = time.time() - start

        print("TT 0 moment:   {}; diff: {}".format(tt_0_moment, np.abs(true_0_moment - tt_0_moment)))
        print("TT 1st moment: {}; diff: {}".format(tt_1_moment, np.abs(true_1_moment - tt_1_moment)))
        print("TT 2nd moment: {}; diff: {}".format(tt_2_moment, np.abs(true_2_moment - tt_2_moment)))
        if True:
            import matplotlib.pyplot as plt

            fig = plt.figure()
            num = 20
            _r = np.linspace(0, 10, num=num)
            _theta = np.linspace(0, 2*np.pi, num=num)
            R, T = np.meshgrid(_r, _theta)
            pert_p = np.zeros(R.shape)
            pert_p_tt = np.zeros(R.shape)
            for lia_r in range(R.shape[0]):
                for lia_t in range(R.shape[1]):
                    pert_p[lia_r, lia_t] = pert_prior(polar2cart([R[lia_r, lia_t], T[lia_r, lia_t]])) / true_0_moment
                    pert_p_tt[lia_r, lia_t] = rad_tt([R[lia_r, lia_t], T[lia_r, lia_t]], normalised=False)

            plt.subplot(311)
            plt.title("perturbed prior")
            im = plt.contourf(R, T, pert_p)
            plt.colorbar(im)
            plt.subplot(312)
            plt.title("TT perturbed prior")
            im = plt.contourf(R, T, pert_p_tt)
            plt.colorbar(im)
            plt.subplot(313)
            plt.title("diff perturbed prior")
            im = plt.contourf(R, T, np.abs(pert_p - pert_p_tt))
            plt.colorbar(im)
            plt.tight_layout()
            fig.savefig(tensor_save_path + "r{}_mc{}_diff.png".format(r, n_recon_sample))
        inf = {
            "num_radi"           : r,
            "r_deg"              : r_poly_deg,
            "theta_deg"          : theta_poly_deg,
            "duration"           : duration,
            "calls"              : (r - 1) * n_recon_sample,
            "true_0_moment"      : true_0_moment.tolist(),
            "tt_0_moment"        : tt_0_moment,
            "z_tail"             : rad_tt.Z_tail,
            "true_1_moment"      : true_1_moment.tolist(),
            "true_2_moment"      : true_2_moment.tolist(),
            "tt_1_moment"        : tt_1_moment.tolist(),
            "tt_2_moment"        : tt_2_moment.tolist(),
            "dim"                : 2,
            "n_recon_samples"    : n_recon_sample,
            "dofs"               : rad_tt.dofs(),
            "average_r"          : rad_tt.average_r(),
            "radial_tt_save_path": recon_dict["save_path"]
        }
        with open(curr_path, "w") as f:
            json.dump(inf, f, indent=4)
        info.append(inf)
    with open(result_path, "w") as f:
        json.dump(info, f, indent=4)
exit()

