import numpy as np

from dolfin import as_matrix

from  scipy.linalg import expm as expm



import math




def getGammaRV(j, n, delta):
    """
    Implements a RV that is Gamma distributed with pdf

        p(x) =  1/\Gamma(shape) * x^{ shape - 1} e^{-x}

    with
                        n+1         1-j
            shape = -----------  + -----
                     2*delta^2       2


    :param j:
    :param n:
    :param delta:
    :return:
    """

    assert delta > 0 and  delta < np.sqrt( (n+1)/(n+5) )

    shape = (n+1)/(2*delta**2) + (1.-j)/2

    def RV(N=1):
       return np.random.gamma(shape, 1.0, N)

    return RV


def getGaussianSample(n,delta):
    """
    Returns a standard normal vector.

    The vector has length   sum_{i=1}^{n-1} (n-i)   =  (n+1)*n / 2 - n
    for being reshaped into the strictly upper triangular part of a

        n\times n  square matrix

    :param n:
    :param delta:
    :return:
    """

    assert delta > 0 and delta < np.sqrt((n + 1) / (n + 5))

    N = (n+1)*n/2 - n

    return np.random.normal(0.,1., size = int(N))


def Ensemble_SG0_p(n, delta):
    """
    Generates a random sample with image in symmetric positive definite random matrix with identity as mean value.
    See Ch. 5.4.7.1 in Soize : Uncertainty Quantification
    :param n:
    :param delta:
    :return:
    """



    #print("max range of delta : ( 0, {v} )".format(v = np.sqrt((n + 1) / (n + 5))))
    assert delta > 0 and delta < np.sqrt((n + 1) / (n + 5))

    sigma  = delta * 1./np.sqrt(n+1)

    Xi = sigma * getGaussianSample(n, delta)
    Nu = [ sigma * np.sqrt(2*getGammaRV(j, n, delta)()) for j in range(1,n+1)]

    L = np.zeros((n, n))

    for i in range(n):
        L[i,i] = Nu[i]

    k = 0
    for i in range(n):
        for j in range(i+1,n):
            L[i,j] = Xi[k]
            k+=1


    #print(L)
    res = L.transpose().dot(L)
    #print(np.linalg.eigvals(res))


    #print(res)
    return res


def Ensemble_SGeps_p(eps, n,delta):
    return 1./(1+eps)*(Ensemble_SG0_p(n, delta) + eps*np.identity(n))


def Ensemble_SE0_p(mean, n, delta):
    """

    See 5.4.8.2 in Soize

    Returns a realisation of a random symmetric positive definite matrix with prescribed mean.
    :param mean: predescribed nxn symmetric positive matrix representing the mean
    :param n:
    :param delta:
    :return:
    """

    assert mean.shape == (n,n)
    L = np.linalg.cholesky(mean)
    G0 = Ensemble_SG0_p(n, delta)
    return L.transpose().dot(G0.dot(L))


def Ensemble_SEeps_p(mean, eps, n, delta):
    """

    See 5.4.8.2 in Soize

    Returns a realisation of a random symmetric positive definite matrix with prescribed mean.
    :param mean: predescribed nxn symmetric positive matrix representing the mean
    :param n:
    :param delta:
    :return:
    """

    assert mean.shape == (n,n)
    L = np.linalg.cholesky(mean)
    G0 = Ensemble_SGeps_p(eps, n, delta)
    return L.transpose().dot(G0.dot(L))



def FourRankTensor_to_VoigtMatrix(T):
    """
     Assume T is a
     Rank 4 tensor of orthotropic material
     with the following symmetries:

        T[i,j,k,l] = T[k,l,i,j]
        T[i,j,k,l] = T[j,i,k,l]
        T[i,j,k,l] = T[i,j,l,k]


    This so called major and minor symmetry allows for Voigt Matrix representation of the rank 4 Tensor.

    Indices shift of the symmetries


    ij     11   22   33 23/32  13/31  12,21

    alph    1    2    3    4      5     6

    Leading

        M_[alph, beta] = T[i,j, k,l]    with ij -> alph, and kl -> beta

    """

    S = T.shape
    #print(S)

    # checking for major and minor symmetry
    for i in range(S[0]):
        for j in range(S[1]):
            for k in range(S[2]):
                for l in range(S[3]):
                    assert T[i, j, k, l] == T[k, l, i, j] and \
                           T[i, j, k, l] == T[j, i, k, l] and \
                           T[i, j, k, l] == T[i, j, l, k]

    # 3x3x3x3 tensor is assumed
    if S[0] == 3 and S[1]== 3 and S[2] == 3 and S[3] == 3:

        def voigt_3d(i,j):
            if i == j :
                return i
            elif i == 1 and j == 2 or i == 2 and j == 1:
                return 3
            elif i == 0 and j == 2 or i == 2 and j == 0:
                return 4
            elif i == 1 and j == 0 or i == 0 and j == 1:
                return 5

        # Then matrix is a 6x6 matrix:
        M = np.zeros((6,6))
        for i in range(S[0]):
            for j in range(S[1]):
                for k in range(S[2]):
                    for l in range(S[3]):
                        alph = voigt_3d(i,j)
                        beta = voigt_3d(k,l)

                        # alph and beta < 3
                        sigma = 1.0
                        #if alph < 3 and beta > 2: # alpha 1 2 3   and beta 4,5,6
                        #    sigma = 2.0
                        #if alph > 2 and beta > 2:
                        #    sigma = 4.0


                        M[alph,beta] = sigma * T[i,j,k,l]


        return M

    elif S[0] == 2 and S[1]== 2 and S[2] == 2 and S[3] == 2:

        def voigt_2d(i, j):
            if i == j:
                return i
            else:
                return 2

        # Then matrix is a 3x3 matrix:
        M = np.zeros((3, 3))
        for i in range(S[0]):
            for j in range(S[1]):
                for k in range(S[2]):
                    for l in range(S[3]):
                        alph = voigt_2d(i, j)
                        beta = voigt_2d(k, l)

                        # alph and beta < 3
                        sigma = 1.0
                        # if alph < 3 and beta > 2: # alpha 1 2 3   and beta 4,5,6
                        #    sigma = 2.0
                        # if alph > 2 and beta > 2:
                        #    sigma = 4.0
                        M[alph, beta] = sigma * T[i, j, k, l]

        return M


class symClassExponential():
    """

    Representation of positive definite matrices A following a symmetry class.

    A is a dim \times dim square matrix, which is
        - symmetric
        - positive definite
        - of sym class symClass (subset of s.p.d)

    Let  {E_i} be a matrix algebraic basis of the symClass.
    That is
        E_i is real, symmetric but not positive definite nor an element of symClass.


    Symclass includes
        - isotropic :
            A = Diag(a) with a in IR^n.
            With that  E_i = Diag(e_i)

        - transverse isotropy/ polar anisotropy, orthotrophy  (e.g. https://en.wikipedia.org/wiki/Linear_elasticity)


    See https://comet-fenics.readthedocs.io/en/latest/demo/elasticity/orthotropic_elasticity.py.html
    for fenics related introduction to the 2D linear elasticity case.

    Relations in 3d with the voigt notation follow from
    spectral decompositon of the linear elastic tensorfor monoclinic syymetry


    """

    def __init__(self,dim, symClass, rotate = False):

        assert symClass in ['isotrop', 'orthotropic', 'anisotropic']

        self._symClass = symClass
        self._dim = dim
        self._rotate = rotate

        self._N = None
        # basis methods sets the value of self.N
        self._basis = self.basis()

    @property
    def N(self):
        return self._N


    def realisation(self,y, trafo = None):
        """
        ComputesY
                    exp_M ( sum y_i basis_i )

        with the matrix exponential mapping exp_M or an rotated version

                T^{-1} exp_M( sum y_i basis_i) T^{-t}

        based on the flag rotate.

        Ordering rules :

         - Isotropic case:
                    y = [lam_I, lam_II]

         - Orthotropic case:
            d = 2 : y = [lam_I, lam_II, lam_III, t1]
            d = 3 : y = [lam_I, lam_II, lam_III, lam_IV, lam_V, lam_VI, t1, t2, t3]

        :param y:
        :param trafo: list of len(y) with transformations of y
                      application:

                        Let y_i be std distributed for each i, i = 1,2
                        Using
                            trafo = [ lambda x: np.exp(x) ] * 2


                        then T(y_i) is lognormal distributed.
                        Can be used to work in normal distributed space but respecting different distributions.

        :return:
        """


        if trafo == None:
            T = [lambda x: x for _ in range(len(y))]
        else:
            T= trafo

        A = None
        y_rot = None

        if self._symClass == 'isotropic':
            pass

        elif self._symClass == 'orthotropic':
            if self._dim == 2:
                if self._rotate == False:
                    self._check_parameter_match(y,4)
                else:
                    self._check_parameter_match(y,5)
                    y_rot = y[4:]
                y_tensor = y[:4]

                lam_I, lam_II, lam_III, t1 = [T[i](y_i) for i, y_i in enumerate(y_tensor.tolist())]




                A = lam_I * self._basis[0](t1) + lam_II * self._basis[1](t1) + lam_III * self._basis[2]

            else:
               self._check_parameter_match(y,9)
               raise NotImplementedError("exp realisation not implemented")

        elif self._symClass == 'anisotropic':
            if self._dim ==2:
                self._check_parameter_match(y,6)


                A = expm(sum(T[i](y[i])*self._basis[i] for i in range(6)))

            else:
                raise NotImplementedError("only 2d anisotropic")

        else:
            raise NotImplementedError("Not implemented")

        if self._rotate:
            Tinv, T_t_inv = self.rotation(y_rot)
            return  Tinv.dot(A.dot(T_t_inv))
        else:
            return A


    def basis(self):
        """
        Returns the N dimensional matrix algebraic basis
        The dimension N depends on the choice of symClass and dim.
        :return:
        """
        basis = []

        if self._symClass == 'isotropic':
            pass

        elif self._symClass == 'orthotropic':
            if self._dim == 2:
                w_I = lambda t: np.array([[cos(t), 0],
                                          [0, sin(t)]])
                w_II = lambda t: np.array([[-sin(t), 0],
                                           [0, cos(t)]])
                w_III = 1. / np.sqrt(2) * np.array([[0, 1],
                                                    [1, 0]])

                E3 = np.zeros((2, 2, 2, 2))
                for i in range(2):
                    for j in range(2):
                        for k in range(2):
                            for l in range(2):
                                E3[i, j, k, l] = w_III[i, j] * w_III[k, l]
                E3 = FourRankTensor_to_VoigtMatrix(E3)

                def E1(theta):
                    E1 = np.zeros((2,2,2,2))
                    for i in range(2):
                        for k in range(2):
                            E1[i, i, k, k] = w_I(theta)[i, i] * w_I(theta)[k, k]
                    return FourRankTensor_to_VoigtMatrix(E1)

                def E2(theta):
                    E2 = np.zeros((2,2,2,2))
                    for i in range(2):
                        for k in range(2):
                            E2[i, i, k, k] = w_II(theta)[i, i] * w_II(theta)[k, k]
                    return FourRankTensor_to_VoigtMatrix(E2)


                return [E1,E2,E3]

            elif self._dim == 3:
                raise NotImplementedError("3d case not imoplemented")



        elif self._symClass == 'anisotropic':
            # naive basis  with {0,1} entries without spectral decompositions of tensors in background
            # sorted rowwise  :

            #  [1 0 0      [0, 1, 0   [0, 0 , 1,    [0, 0, 0   [0, 0, 0,   [0, 0, 0,
            #   0 0 0    ,  1, 0, 0    0, 0 , 0,     0, 1, 0    0, 0, 1     0, 0, 0,
            #   0 0 0 ]     0, 0, 0]   1, 0 , 0]     0, 0, 0]   0, 1, 0]    0, 0, 1]


            if self._dim == 2:
                self._N = int(self._dim*(self._dim+1) / 2)


                for i in range(3):
                    for j in range(i, 3):
                        E = np.zeros((3, 3))
                        E[i,j] = E[j,i] = 1.0
                        basis.append(E)
                #print(basis)
                #exit()
                assert(len(basis)==6)

        else:
            raise NotImplementedError("Not implemented so far")

        return basis



    def rotation(self, alphas):
        """
        For the 2d case :

                RTR^{-1}  =  transpose(T^{-1})

        so avoid Reuter matrix.

        :param dim:
        :param alph:
        :return:
        """
        if self._dim == 2:
            assert len(alphas) == 1

            alph = alphas[0]
            Tinv = np.array([[cos(alph) ** 2, sin(alph) ** 2, -2 * sin(alph) * cos(alph)],
                               [sin(alph) ** 2, cos(alph) ** 2, 2 * sin(alph) * cos(alph)],
                               [sin(alph) * cos(alph), -sin(alph) * cos(alph), cos(alph) ** 2 - sin(alph) ** 2]
                               ])

            T_t_inv = np.array([[cos(alph) ** 2, sin(alph) ** 2, sin(alph) * cos(alph)],
                                  [sin(alph) ** 2, cos(alph) ** 2, -sin(alph) * cos(alph)],
                                  [-2 * sin(alph) * cos(alph), 2 * sin(alph) * cos(alph),
                                   cos(alph) ** 2 - sin(alph) ** 2]
                                  ])

            return Tinv, T_t_inv
        elif self._dim == 3:
            raise NotImplementedError("3d case not implemented")

        else:
            raise NotImplementedError("Other dimensions than d = 2, 3 not supported.")



    def _check_parameter_match(self, y, d):
        if len(y) != d:
            raise ValueError(
                "Input does not match {sym} material in {N}d. dim(y)={dy}, but {d} parameters required".format(
                        sym=self._symClass, N=self._dim, dy=len(y), d=d))


def test_isotropic():
    pass
def cos(theta):
    return np.cos(theta)
def sin(theta):
    return np.sin(theta)

def test_Orthotropic_2d():

    w_I = lambda t: np.array([[cos(t), 0],
                              [0, sin(t)]])
    w_II = lambda t: np.array([[-sin(t), 0],
                               [0, cos(t)]])
    w_III= 1./np.sqrt(2)*np.array([[0 , 1],
                                   [1 , 0]])

    t = 2*np.pi*np.random.rand(1)[0]
    W = [np.zeros((2,2,2,2)) for _ in range(3)]
    for i in range(2):
        for j in range(2):
            for k in range(2):
                for l in range(2):
                    W[0][i,j,k,l] = w_I(t)[i,j]*w_I(t)[k,l]
                    W[1][i, j, k, l] = w_II(t)[i, j] * w_II(t)[k, l]
                    W[2][i, j, k, l] = w_III[i, j] * w_III[k, l]

    for K in range(3):
        print("K={K}:\n{v}".format(K=K+1,v=FourRankTensor_to_VoigtMatrix(W[K]).round(4)))

    for K in range(3):
        for L in range(K,3):
            if K != L:
                print("E_{K}^V E_{L}^V = {K},{L} : \n{v}".format(K=K+1,L=L+1,
                                                 v =FourRankTensor_to_VoigtMatrix(W[K]).dot(FourRankTensor_to_VoigtMatrix(W[L])).round(4) ))

    E = 100.
    v = 0.6
    K = E / (3 * (1 - 2 * v))
    G = E / (2 * (1 + v))

    print("G = {G}".format(G=G))
    print("K = {K}".format(K=K))

    iso_2d_ref = np.array([[E / (1 - v ** 2), v * E / (1 - v ** 2), 0.],
                           [v * E / (1 - v ** 2), E / (1 - v ** 2), 0.],
                           [0., 0., E / (2 * (1 + v))]
                           ], dtype=np.float64)

    w = [w_I, w_II, w_III]

    def get_órthotropic(x):
        l1, l2, l3, t = x

        V = [np.zeros((2,2,2,2)) for _ in range(6)]

        for i in range(2):
            for j in range(2):
                for k in range(2):
                    for l in range(2):
                        for pos in range(0,2):
                            V[pos][i, j, k, l] = w[pos](t)[i, j] * w[pos](t)[k, l]
                        V[2][i, j, k, l] = w[2][i, j] * w[2][k, l]

        sum1 = sum(x[pos] * FourRankTensor_to_VoigtMatrix(V[pos]) for pos in range(0, 2))
        sum2 = x[2] * FourRankTensor_to_VoigtMatrix(V[2])

        return sum1, sum2


    def fit_isotropic(x):

        res1, res2 = get_órthotropic(x)
        res = res1 + res2
        val = np.linalg.norm(res - iso_2d_ref) ** 2
        #print(val)
        return val

    from scipy.optimize import minimize

    #x0 = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, np.pi, np.pi, np.pi]
    x0 = np.random.rand(4)
    print(x0)
    #x0[0] = 12.
    theta = minimize(fit_isotropic, x0)


    theta = theta.x.copy(True).tolist()


    while theta[3] > np.pi:
        print("old : ", theta[3])
        theta[3] -= np.pi
        print("new : ", theta[3])

    while theta[3] < 0 :
        print("old : ", theta[3])
        theta[3] += np.pi
        print("new : ", theta[3])
    print(theta)

    print(sin(theta[3]))
    print(cos(theta[3]))

def test_Orthotropic_3d_new():

    w_I = lambda t1,t2,t3 : np.diag(np.array([cos(t1)*cos(t2),
                                              cos(t1)*sin(t2),
                                              sin(t1)
                                              ]))

    w_II = lambda t1,t2,t3: np.diag(np.array([-cos(t3)*sin(t2) + sin(t3)*sin(t1)*cos(t2),
                                              cos(t3)*cos(t2)+sin(t3)*sin(t1)*sin(t2),
                                              -sin(t3)*cos(t1)
                                             ]))

    w_III = lambda t1, t2, t3: np.diag(np.array([sin(t3)*sin(t2)+cos(t3)*sin(t1)*cos(t2),
                                                 -sin(t3)*cos(t2)+cos(t3)*sin(t1)*sin(t2),
                                                 -cos(t3)*cos(t1)
                                                ]))

    w_IV = 1. / np.sqrt(2) * np.array([[0, 0, 0],
                                        [0, 0, 1],
                                        [0, 1, 0]])

    w_V = 1. / np.sqrt(2) * np.array([[0, 0, 1],
                                       [0, 0, 0],
                                       [1, 0, 0]])

    w_VI = 1. / np.sqrt(2) * np.array([[0, 1, 0],
                                      [1, 0, 0],
                                      [0, 0, 0]])

    a, b, c = (2 * np.pi * np.random.rand(3)).tolist()

    w = [w_I(a,b,c), w_II(a,b,c), w_III(a,b,c), w_IV, w_V, w_VI]

    if False:
        for K in range(0, 6):
            for L in range(K, 6):
                if K != L:
                    print("w_{K} ° w_{L} = \n {v}".format(K=K, L=L, v=np.einsum("ij,ij", w[K], w[L]).round(4)))


    w = [w_I, w_II, w_III, w_IV, w_V, w_VI]

    def isotropic_basis():
        def kron_delta(i, j):
            return 1.0 if i == j else 0.

        E1_tensor = np.zeros((3, 3, 3, 3))
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        if i == j and k == l:
                            E1_tensor[i, j, k, l] = 1. / 3.

        I4 = np.zeros((3, 3, 3, 3))
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        I4[i, j, k, l] = 0.5 * (
                        kron_delta(i, k) * kron_delta(j, l) + kron_delta(i, l) * kron_delta(j, k))

        E2_tensor = I4 - E1_tensor

        E1_mat = FourRankTensor_to_VoigtMatrix(E1_tensor)
        E2_mat = FourRankTensor_to_VoigtMatrix(E2_tensor)

        return E1_mat, E2_mat

    E1_mat, E2_mat = isotropic_basis()

    def get_órthotropic(x):
        l1, l2, l3, l4, l5, l6, t1, t2, t3 = x

        V = [np.zeros((3, 3, 3, 3)) for _ in range(6)]

        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        for pos in range(0,3):
                            V[pos][i, j, k, l] = w[pos](t1, t2, t3)[i, j] * w[pos](t1, t2, t3)[k, l]
                        for pos in range(3, 6):
                            V[pos][i, j, k, l] = w[pos][i, j] * w[pos][k, l]

        sum1 = sum(x[pos] * FourRankTensor_to_VoigtMatrix(V[pos]) for pos in range(0, 3))
        sum2 = sum(x[pos] * FourRankTensor_to_VoigtMatrix(V[pos]) for pos in range(3, 6))

        return sum1, sum2

    iso_mat = 4 * 3 * E1_mat + 4 * 2 * E2_mat

    def fit_isotropic(x):

        y = [12.]+ [8.]*5 + x.tolist()

        res1, res2 = get_órthotropic(y)
        res = res1 + res2
        val = np.linalg.norm(res - iso_mat) ** 2
        print(val)
        return val

    from scipy.optimize import minimize

    #x0 = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, np.pi, np.pi, np.pi]
    x0 = np.random.rand(3)
    #x0[0] = 12.
    theta = minimize(fit_isotropic, x0)

    if True:
        print("t1 : asin(1/sqrt(3) = {}".format(math.asin(1./np.sqrt(3))))
        print("t2 : acos(1/sqrt(2) = {}".format(math.acos(1. / np.sqrt(2))))
    print(theta.x.round(4))


def test_Orthotropic_3d():
    w_I = 1. / np.sqrt(2) * np.array([[0, 1, 0],
                                      [1, 0, 0],
                                      [0, 0, 0]])
    w_II = 1. / np.sqrt(2) * np.array([[0, 0, 1],
                                       [0, 0, 0],
                                       [1, 0, 0]])
    w_III = 1. / np.sqrt(2) * np.array([[0, 0, 0],
                                        [0, 0, 1],
                                        [0, 1, 0]])
    w_IV = lambda t_1, t_2, t_3: np.diag(np.array([cos(t_1),
                                                   sin(t_1) * cos(t_2),
                                                   sin(t_1) * sin(t_2)]))
    w_V = lambda t_1, t_2, t_3: np.diag(np.array([-cos(t_3) * sin(t_1),
                                                   cos(t_1) * cos(t_2) * cos(t_3) - sin(t_2) * sin(t_3),
                                                   cos(t_1) * sin(t_2) * cos(t_3) + sin(t_3) * cos(t_2)
                                                  ]))
    w_VI = lambda t_1, t_2, t_3: np.diag(np.array([ sin(t_1) * sin(t_3),
                                                   -sin(t_3) * cos(t_1) * cos(t_2) - cos(t_3) * sin(t_2),
                                                   -sin(t_3) * cos(t_1) * sin(t_2) - cos(t_3) * cos(t_2)
                                                   ]))

    a,b,c = (2*np.pi*np.random.rand(3)).tolist()

    w = [w_I, w_II, w_III, w_IV(a,b,c), w_V(a,b,c), w_VI(a,b,c)]

    for K in range(3,6):
        for L in range(K,6):
            if K != L :
                print("w_{K} ° w_{L} = \n {v}".format(K=K,L=L,v=np.einsum("ij,ij",w[K],w[L])))
    exit()



def testOrthotropic():


    def cos(theta):
        return np.cos(theta)
    def sin(theta):
        return np.sin(theta)


    two_d = True


    if two_d:

        w_I = lambda t :np.array([[cos(t),    0  ],
                                 [   0  , sin(t)]])

        w_II = lambda t: np.array([[-sin(t), 0],
                                  [0, cos(t)]])


        for t in [0.1,3,0.5, np.pi/2, np.pi/4]:
            print("t = {}".format(t))
            print(w_I(t).dot(w_II(t)).round(4))


    exit()


    lam_I, lam_II, lam_III, lam_IV, lam_V, lam_VI, theta_1,theta_2,theta_3 = [0]* 6 + [np.pi/4, np.pi/4, np.pi/4]

    #np.pi/2, np.pi/4, np.pi/8

    w_I   = 1. / np.sqrt(2) * np.array([[0, 1, 0],
                                        [1, 0, 0],
                                        [0, 0, 0]])

    w_II  = 1. / np.sqrt(2) * np.array([[0, 0, 1],
                                        [0, 0, 0],
                                        [1, 0, 0]])

    w_III = 1. / np.sqrt(2) * np.array([[0, 0, 0],
                                        [0, 0, 1],
                                        [0, 1, 0]])




    w_IV = lambda t_1,t_2,t_3:  np.diag(np.array([cos(t_1),
                                                  sin(t_1)*cos(t_2),
                                                  sin(t_1)*sin(t_2)]))

    w_V = lambda t_1,t_2,t_3: np.diag(np.array([-cos(t_3)*sin(t_1),
                                                 cos(t_1)*cos(t_2)*cos(t_3)-sin(t_2)*sin(t_3),
                                                 cos(t_1)*sin(t_2)*cos(t_3)+sin(t_3)*cos(t_2)
                           ]))


    w_VI= lambda t_1,t_2,t_3: np.diag(np.array([ sin(t_1)*sin(t_3),
                                                -sin(t_3)*cos(t_1)*cos(t_2)-cos(t_3)*sin(t_2),
                                                -sin(t_3)*cos(t_1)*sin(t_2)-cos(t_3)*cos(t_2)
                                                ]))






    #print("w_IV = \n{w}\n".format(w = w_IV(0.,0.,0.)))
    #print("w_IV = \n{w}\n".format(w = w_V(0.,0.,0.)))
    #print("w_IV = \n{w}\n".format(w = w_VI(0.,0.,0.)))
    if False:
        z = (np.pi*np.random.rand(3)).tolist()
        #w_IV(x[0],x[1],x[2])
        #w_V(x[0], x[1], x[2])
        #w_VI(x[0], x[1], x[2])


        W_IV = lambda y: w_IV(y[0] ,y[1], y[2])
        W_V  = lambda y:  w_V(y[0], y[1], y[2])
        W_VI = lambda y: w_VI(y[0], y[1], y[2])

        w = [w_I, w_II, w_III, W_IV, W_V, W_VI]

        W = [np.zeros((3, 3, 3, 3)) for _ in range(6)]

        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        for pos in range(3):
                            W[pos][i, j, k, l] = w[pos][i, j] * w[pos][k, l]
                        for pos in range(3, 6):
                            W[pos][i, j, k, l] = w[pos](z)[i, j] * w[pos](z)[k, l]



        #print("IV : \n{}".format(FourRankTensor_to_VoigtMatrix(W_IV_t)))
        #print(" V : \n{}".format(FourRankTensor_to_VoigtMatrix(W_V_t)))
        #print("VI : \n{}".format(FourRankTensor_to_VoigtMatrix(W_VI_t)))

        for i in range(3,6):
            for j in range(i+1,6):
                print("{i}/{j} = \n{val}".format(i=i+1,j=j+1,val=np.einsum("ijmn,mnkl->ijkl", W[i], W[j]).round(4)))



        exit()

        print(np.einsum("ijmn,mnkl->ijkl", W_IV_t, W_V_t).round(4))
        print(np.einsum("ijmn,mnkl->ijkl", W_IV_t, W_VI_t).round(4))
        print(np.einsum("ijmn,mnkl->ijkl", W_V_t, W_VI_t).round(4))


        exit()



        if False:

            c11 = sum( w[p](x)[0,0]**2 for p in range(3))

            c12 = sum( w[p](x)[0,0]*w[p](x)[1,1] for p in range(3))

            c13 = sum( w[p](x)[0,0]*w[p](x)[2,2] for p in range(3))

            c33 = sum( w[p](x)[2,2]**2 for p in range(3))

            c23 = sum( w[p](x)[1,1]*w[p](x)[2,2] for p in range(3))


            c_1_22 = c12 - c13*c23/c33
            c_1_11 = c11 - c13*c13/c33

            print(c_1_11)
            #print(c_1_22)


        exit()









        print(w_IV(x[0],x[1],x[2]).dot(w_V(x[0],x[1],x[2])))
        print(w_IV(x[0],x[1],x[2]).dot(w_VI(x[0],x[1],x[2])))
        print(w_V(x[0],x[1],x[2]).dot(w_VI(x[0],x[1],x[2])))

        print("\n")
        print(w_IV(x[0], x[1], x[2]))
        print(w_V(x[0], x[1], x[2]))
        print(w_VI(x[0], x[1], x[2]))


        exit()

    theta_1, theta_2, theta_3 = [np.pi / 4, np.pi / 4, np.pi / 4]

    w = [w_I, w_II, w_III, w_IV, w_V, w_VI ]

    W = [np.zeros((3,3,3,3)) for _ in range(6)]

    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    for pos in range(3):
                        W[pos][i,j,k,l] = w[pos][i,j]*w[pos][k,l]
                    for pos in range(3,6):
                        W[pos][i, j, k, l] = w[pos](theta_1,theta_2,theta_3)[i, j] * w[pos](theta_1,theta_2,theta_3)[k, l]

    print("sum I II III = \n{}".format(FourRankTensor_to_VoigtMatrix(W[0]+W[1]+ W[2]).round(4)))
    print("sum IV V VI = \n{}".format(FourRankTensor_to_VoigtMatrix(W[3]+W[4]+W[5]).round(4)))

    #exit()




    #print("Voigt notations : ")

    #for pos in range(6):
    #    print("W_"+str(pos+1)+" = \n{}\n".format(FourRankTensor_to_VoigtMatrix(W[pos]).round(4)))


    # ISOTROPIC BASIS Part
    def isotropic_basis():
        def kron_delta(i, j):
            return 1.0 if i == j else 0.

        E1_tensor = np.zeros((3, 3, 3, 3))
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        if i == j and k == l:
                            E1_tensor[i, j, k, l] = 1. / 3.

        I4 = np.zeros((3, 3, 3, 3))
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        I4[i, j, k, l] = 0.5 * (kron_delta(i, k) * kron_delta(j, l) + kron_delta(i, l) * kron_delta(j, k))

        E2_tensor = I4 - E1_tensor

        E1_mat = FourRankTensor_to_VoigtMatrix(E1_tensor)
        E2_mat = FourRankTensor_to_VoigtMatrix(E2_tensor)

        return E1_mat, E2_mat

    E1_mat, E2_mat = isotropic_basis()




    def get_órthotropic(x):
        l1,l2,l3,l4,l5,l6,t1,t2,t3 = x

        V = [np.zeros((3, 3, 3, 3)) for _ in range(6)]

        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        for pos in range(3):
                            V[pos][i, j, k, l] = w[pos][i, j] * w[pos][k, l]
                        for pos in range(3, 6):
                            V[pos][i, j, k, l] = w[pos](t1, t2, t3)[i, j] * \
                                                 w[pos](t1, t2, t3)[k, l]

        sum1 = sum(x[pos] * FourRankTensor_to_VoigtMatrix(V[pos]) for pos in range(0, 3))
        sum2 = sum(x[pos] * FourRankTensor_to_VoigtMatrix(V[pos]) for pos in range(3, 6))

        return sum1,sum2



    iso_mat = 4*3*E1_mat + 4*2*E2_mat

    def fit_isotropic(x):
        res1,res2 = get_órthotropic(x)
        res = res1+res2
        val = np.linalg.norm(res -  iso_mat  ) ** 2
        return val




    from scipy.optimize import minimize

    theta = minimize(fit_isotropic, [1.0,1.0,1.0,1.0,1.0,1.0, np.pi, np.pi, np.pi])

    print(theta.x.round(4))

    print("iso mat = \n{}".format(iso_mat.round(4)))
    print("fitted ortho mat = \n{}".format(get_órthotropic(theta.x)[0].round(4)))
    print("fitted ortho mat = \n{}".format(get_órthotropic(theta.x)[1].round(4)))

    print("sum_i=I II III W_i = \n{}".format(get_órthotropic([1.,1.,1.,1.,1.,1.,theta.x[6],theta.x[7],theta.x[8]])[0].round(4)))

    print("sum_i=IV,V,VI W_i[angle*] = \n{}".format(
        get_órthotropic([1., 1., 1., 1., 1., 1., theta.x[6], theta.x[7], theta.x[8]])[1].round(4)))

    exit()
    #delete :
    l1, l2, l3, l4, l5, l6, t1, t2, t3 = [theta.x[i] for i in range(9)]

    w_4_test = w[3](t1,t2,t3)
    w_5_test = w[4](t1,t2,t3)
    w_6_test = w[5](t1, t2, t3)


    def dev(A):
        d = 3
        return A - np.trace(A)/3*np.diag([1.,1.,1.])


    #print(dev(w_6_test))


    eta_1 = np.trace(dev(w_6_test).dot(dev(w_6_test)))

    eta_2 = np.linalg.det(dev(w_6_test))/np.trace(w_6_test)**3

    eta_3 = np.trace(w_6_test.dot(w_6_test).dot(w_5_test))/np.trace(w_5_test)

    eta_3_p = np.linalg.det(dev(w_5_test))**2
    eta_3_pp= np.linalg.det(dev(w_5_test))/(np.trace(w_5_test)**3)

    print("eta_1   = {}".format(eta_1))
    print("eta_2   = {}".format(eta_2))
    print("eta_3   = {}".format(eta_3))
    print("eta_3*  = {}".format(eta_3_p))
    print("eta_3** = {}".format(eta_3_pp))


    #eta_1   = 0.6880492438109072
    #eta_2   = -0.08578857048687126
    #eta_3   = -0.23457703089717521
    #eta_3*  = 0.015256847575937958
    #eta_3** = 1.5199040485080524



    #0.2 / 0.7
    #[0.7     0.7     0.7     0.7     0.3522  0.5478 - 0.7854  0. - 0.2756]

    exit()

    def get_angels(x):
        l1,l2,l3,t1,t2,t3 = x

        V = [np.zeros((3, 3, 3, 3)) for _ in range(3)]

        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        for pos in range(0, 3):
                            V[pos][i, j, k, l] = w[pos+3](t1, t2, t3)[i, j] * \
                                                 w[pos+3](t1, t2, t3)[k, l]



        res = sum(x[pos]*FourRankTensor_to_VoigtMatrix(V[pos]) for pos in range(0,3))

        val = np.linalg.norm(res[0:3,0:3] - 1./3*np.ones((3,3)))**2

        print(val)

        return val


    from scipy.optimize import minimize

    theta = minimize(get_angels,[0.1,0.1,0.1,0.,0.,0.])

    print(theta.x.round(4))

    # [0.      1.      0. - 0.6352  0.4994  0.2324]


    #print("angle value : {}".format(np.pi/theta.x[3:6].round(4) ))


    exit()



    print(sum(FourRankTensor_to_VoigtMatrix(W[pos]) for pos in range(3,6)).round(4))
    exit()


    def build_orthotropic(x):
        # kelvin modili
        k1,k2,k3,k4,k5,k6 = x[0:6]
        # three angles
        t1,t2,t3          = x[6:]


        for i in range(3):
            for j in range(3):
                for k in range(3):
                    for l in range(3):
                        for pos in range(3, 6):
                            W[pos][i, j, k, l] = w[pos](t1, t2, t3)[i, j] * \
                                                 w[pos](t1, t2, t3)[k, l]



        return sum( x[i]*W[i] for i in range(6) )




    print(FourRankTensor_to_VoigtMatrix(build_orthotropic([0.1*i for i in range(1,7)] + [np.pi/2, np.pi/4, np.pi/8])))

















def main():

    C_generator = symClassExponential(dim = 2, symClass = 'orthotropic', rotate = True)
    E = C_generator.basis()
    #print(1./np.sqrt(2))
    #print(E[0](0.))
    #print(E[1](0.))
    #print(E[2])
    #exit()


    E = 100.
    v = 0.4
    K = E/(3*(1-2*v))
    G = E/(2*(1+v))

    print("G = {G}".format(G=G))
    print("K = {K}".format(K=K))

    iso_2d_ref  = np.array([[   E/(1-v**2), v*E/(1-v**2), 0.],
                            [ v*E/(1-v**2),   E/(1-v**2), 0.],
                            [   0.       ,   0.        , E/(2*(1+v))]
                            ], dtype = np.float64)



    def f(x):

        C_fit = C_generator.realisation(x)

        return np.linalg.norm(iso_2d_ref  - C_fit)

    from scipy.optimize import minimize


    print(math.sin(0.78539816))
    print(np.sqrt(2))
    print(np.sqrt(3))
    print(np.sqrt(2)**(-1))
    print(np.sqrt(3)**(-1))



    for i in range(20):
        res = minimize(f, np.random.rand(5), tol=5e-8)

        #print(iso_2d_ref)
        #print(C_generator.realisation(res.x))

        #print(res)

        x = res.x

        print(x)
        #t1 = x[3]

        #E = C_generator.basis()
        #print(E[0](t1))
        #print(E[1](t1))
        #print(E[2])
    # exit()

    exit()






    for k in range(1):
        y = np.concatenate((np.random.randn(3), np.pi*np.random.rand(2)))


        C = C_generator.realisation(y)
        print(C)
        print(np.linalg.eigvals(C))
        print("#########")


        as_matrix(C)







    exit()
    test_Orthotropic_2d()
    #test_Orthotropic_3d()

    #test_Orthotropic_3d_new()


    exit()

    testOrthotropic()

    exit()

    K = 10
    G = 2.


    reference_isotropic = np.array([[ K + 4*G/3, K - 2*G/3, K - 2*G/3, 0 , 0, 0],
                                    [ K - 2*G/3, K + 4*G/3, K - 2*G/3, 0 , 0, 0],
                                    [ K - 2*G/3, K - 2*G/3, K + 4*G/3, 0 , 0, 0],
                                    [     0    ,      0   ,    0     , G , 0, 0],
                                    [     0    ,      0   ,    0     , 0 , G, 0],
                                    [     0    ,      0   ,    0     , 0 , 0, G]])



    def kron_delta(i,j):
        return 1.0 if i == j else 0.



    E1_tensor = np.zeros((3,3,3,3))
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    if i==j and k == l:
                        E1_tensor[i,j,k,l] = 1./3.





    I4 = np.zeros((3,3,3,3))
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    I4[i,j,k,l] = 0.5*(kron_delta(i, k) * kron_delta(j, l) + kron_delta(i, l) * kron_delta(j, k))


    E2_tensor = I4-E1_tensor



    print(np.einsum("ijmn,mnkl->ijkl",E1_tensor,E2_tensor).round(5))
    #print(np.inner(E1_tensor,E2_tensor))

    #print(np.einsum("ijmn,mnkl->ijkl",E1_tensor,E1_tensor)-E1_tensor)


    #print(E2_tensor)


    E1_mat = FourRankTensor_to_VoigtMatrix(E1_tensor)
    E2_mat = FourRankTensor_to_VoigtMatrix(E2_tensor)

    print("-- d = 3  voigt proct test")
    print("- E1 = I2(x)I2")
    print(E1_mat)
    print("- E2 = I^4 - 1/3 I2(x)I2")
    print(E2_mat)
    print("- I^4 ")
    print(FourRankTensor_to_VoigtMatrix(I4))

    print(np.einsum("ik,kj->ij", E1_mat, E2_mat).round(5))
    print(np.einsum("ik,kj->ij", E1_mat, E1_mat).round(5))
    print(np.einsum("ik,kj->ij", E2_mat, E2_mat).round(5))


    print("-----------_")



    #print(E1_mat)
    #print("-------------------------------")
    #print(E2_mat)

    #print(np.einsum("ij,ij", E1_mat, E2_mat))

    print("-------------- test ------------")

    isotropic_mat = FourRankTensor_to_VoigtMatrix( 3*K*E1_tensor + 2*G*E2_tensor)
    #print(isotropic_mat)

    #print(reference_isotropic)

    isotropic_mat = 3*K*E1_mat + 2*G*E2_mat
    print(isotropic_mat)

    w, v = np.linalg.eig(isotropic_mat)

    print("w = {w} \n".format(w=w))
    print("v = {v} \n".format(v=v))

    #u, s, vh = np.linalg.svd(isotropic_mat, full_matrices=True)
    #print("u = {u}".format(u=u))
    #print("s = {s}".format(s=s))
    #print("vh={vh}".format(vh=vh))


    print("============ 2D section =====================")

    E = 10.
    v = 0.4
    K = E/(3*(1-2*v))
    G = E/(2*(1+v))

    print("G = {G}".format(G=G))
    print("K = {K}".format(K=K))

    iso_2d_ref  = np.array([[   E/(1-v**2), v*E/(1-v**2), 0.],
                            [ v*E/(1-v**2),   E/(1-v**2), 0.],
                            [   0.       ,   0.        , E/(2*(1+v))]
                            ], dtype = np.float64)


    I2I2 = np.zeros((2,2,2,2))
    for i in range(2):
        for j in range(2):
            for k in range(2):
                for l in range(2):
                    if i==j and k == l:
                        I2I2[i,j,k,l] = 1.

    I4_2d = np.zeros((2,2,2,2))
    for i in range(2):
        for j in range(2):
            for k in range(2):
                for l in range(2):
                    I4_2d[i,j,k,l] = 0.5*(kron_delta(i, k) * kron_delta(j, l) + kron_delta(i, l) * kron_delta(j, k))




    print(FourRankTensor_to_VoigtMatrix(I2I2))
    print(FourRankTensor_to_VoigtMatrix(I4_2d))



    #x = 0
    #print(" I2I2 * I4 in 2d : ")
    #print(np.einsum("ijmn,mnkl->ijkl", I2I2, I4_2d ))


    #print(np.linalg.norm(np.einsum("ijmn,mnkl->ijkl", I2I2, I4_2d - x * I2I2)))

    #def get_basis(x):
    #    return np.linalg.norm(np.einsum("ijmn,mnkl->ijkl", I2I2, I4_2d - x[0] * I2I2))

    #from scipy.optimize import minimize

    #res = minimize(get_basis, [1.0], tol=5e-8)

    E1 = 0.5* I2I2
    E2 = I4_2d-E1

    print(" I2I2 * (I4-x*I2I2) in 2d : ")
    print(np.einsum("ijmn,mnkl->ijkl", E1,E2))
    print(np.einsum("ijmn,mnkl->ijkl", E2, E1))
    #print(np.einsum("ijkl,ijkl", E1, E1))
    #print(np.einsum("ijkl,ijkl", E2, E2))


    print(np.einsum("ijmn,mnkl->ijkl", E1, E1) - E1)
    print(np.einsum("ijmn,mnkl->ijkl", E2, E2) - E2)
    #print(np.einsum("ijkl,ijkl", E1, E2))

    print("=====================")
    print(FourRankTensor_to_VoigtMatrix(E1))
    print(FourRankTensor_to_VoigtMatrix(E2))

    E1_mat = FourRankTensor_to_VoigtMatrix(E1)
    E2_mat = FourRankTensor_to_VoigtMatrix(E2)

    print(E1_mat.shape)
    print(E2_mat.shape)

    #print(E1_mat.dot(E2_mat))

    print(FourRankTensor_to_VoigtMatrix(E2).dot(FourRankTensor_to_VoigtMatrix(E2)))

    exit()

    print(np.einsum("ijmn,mnkl->ijkl",E1_tensor,E2_tensor).round(5))











    def f_isotrop(x):

        a,b = x[0],x[1]

        return np.linalg.norm(iso_2d_ref  - (a * 3*E1_2d  + b*2*E2_2d))

    from scipy.optimize import minimize


    res = minimize(f_isotrop, [5,5], tol=5e-8)

    print(res)

    exit()





    E1_2d = np.array([ [1./3, 1./3, 0.],
                       [1./3, 1./3, 0.],
                       [0.  , 0.  , 0.]
                     ])

    E2_2d = np.array([[ 2. / 3, -1. / 3, 0.],
                      [-1. / 3,  2. / 3, 0.],
                      [ 0.    ,  0.    , 1/2.]
                      ])


    E = 10.
    v = 0.4

    K = E/(3*(1-2*v))
    G = E/(2*(1+v))

    print()
    print("G = {G}".format(G=G))
    print("K = {K}".format(K=K))
    print(21.42857142857143)

    iso_2d_ref  = np.array([[   E/(1-v**2), v*E/(1-v**2), 0.],
                            [ v*E/(1-v**2),   E/(1-v**2), 0.],
                            [   0.       ,   0.        , E/(2*(1+v))]
                            ], dtype = np.float64)



    w,v = np.linalg.eig(iso_2d_ref)


    print(v[:,0].shape)

    print(v[:,0].dot(v[:,0].transpose()))

    print("w = {w}".format(w=w))
    print("v = {v}".format(v=v))






    print(np.einsum("ik,kj->ij", E1_2d, E2_2d))
    print(E1_2d.dot(E2_2d))


    exit()




    print(iso_2d_ref)

    print( 1.2857142857142854* K*E1_2d + 2*G*E2_2d)
















    exit()

    n = 3
    matSymClass = symClassExponential(dim = n, symClass = 'anisotropic')

    N = matSymClass.N # number of degrees of freedom

    y = np.random.randn(N)

    E_sym = matSymClass.expRealisation(y)

    print(E_sym)
    print(np.linalg.eigvals(E_sym))

    exit()



    n = 3

    def kron_delta(i,j):
        return 1.0 if i == j else 0.

    T = np.zeros((3,3,3,3))
    for i in range(3):
        T[i,i,i,i] = 1

    S = np.zeros((3,3,3,3))

    for i in range(n):
        for j in range(n):
            for k in range(n):
                for l in range(n):
                    S[i,j,k,l] = kron_delta(i,k)*kron_delta(j,l) + kron_delta(i,l)*kron_delta(j,k) - 2./3.*kron_delta(i,j)*kron_delta(k,l)


    K = 2.0
    G = 3.0



    M1 = FourRankTensor_to_VoigtMatrix(K*T)
    M2 = FourRankTensor_to_VoigtMatrix(G*S)

    print(M1)
    print(M2)

    print(M1+M2)




    exit()
    Ensemble_SG0_p(n = 3, delta = 0.7)


    n = 3
    delta = 0.7
    eps = 0.0001

    mean = np.zeros((n,n))
    mean[0,0] = 1
    mean[1,1] = 2
    mean[2,2] = 3

    s = {}
    for i in range(n):
        s[i] = []


    for i in range(10000):
        mat = Ensemble_SEeps_p(mean, eps, n, delta)

        evs = np.linalg.eigvals(mat)

        ev = sorted(evs)
        for i in range(n):
            s[i].append(ev[i])
        #print(mat)
        #print(ev)




    import matplotlib.pyplot as plt
    for i in range(n):
        fig = plt.figure(str(i))
        count, bins, ignored = plt.hist(s[i], 50, density=True)
        plt.draw()
        plt.pause(0.001)
    plt.show()




if __name__ == "__main__":
        main()  # sys.argv)