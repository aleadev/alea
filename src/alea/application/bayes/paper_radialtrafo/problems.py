from __future__ import (division, print_function, absolute_import)
from dolfin import FunctionSpace, DirichletBC, Constant
import numpy as np
from alea.math_utils.param_pde.affine_field import AffineField
from alea.math_utils.param_pde.lognormal_field import LognormalField

from alea.math_utils.param_pde.forward_operator.poisson import ParametricPoisson, SolutionCache
from alea.math_utils.param_pde.mesh_util import (get_mesh, get_boundary)

from alea.application.bayes.paper_radialtrafo.BioPhy.BioPhy_model import model as model
from alea.application.bayes.paper_radialtrafo.BioPhy.BioPhy_model import read_lists_from_data, get_files_from_directory
from alea.application.bayes.paper_radialtrafo.trafo_util import compute_map, RadialTrafo

from scipy import special
import numdifftools as nd
from scipy.stats import multivariate_normal as mn
import math
import functools
from alea.utils.decorators import count_calls
import os


from scipy import special

import pickle #as pickle
import os

from randomMatrices import symClassExponential

def save_obj(foldname, obj, name ):
    with open(foldname + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(foldname, name ):
    with open(foldname + name + '.pkl', 'rb') as f:
        return pickle.load(f)


class GenericModel(object):
    def __init__(self):
        pass

    def post(self, x):
        pass

    def compute_map(self,
                    approximate=False,
                    log_optimizer=False,
                    use_difftool=True):
        pass


class PoissonModel(GenericModel):
    def __init__(self,
                 M=2,
                 n_points=9,
                 noise=1e-6,

                 ):
        super(PoissonModel, self).__init__()
        exp_field_options = {
            "coef_type": "cos",
            "amptype"  : "decay-inf",
            "decayexp" : 2,
            "gamma"    : 0.9,
            "freqscale": 1.0,
            "freqskip" : 0,
            "scale"    : 1.0,
            "coef_mean": 0.0,
            "rv_type"  : "normal"
        }

        # create the affine field for the exponent
        exp_field_af = AffineField(**exp_field_options)
        # wrap the affine field with a log-normal description
        exp_field = LognormalField(exp_field_af)

        # given a Fenics FunctionSpace, boundary condition, a rhs and the coefficient above,
        # we define the forward problem as
        # problem = ParametricPoisson(exp_field, rhs, FunctionSpace, boundaryCondition)

        ref_mesh = get_mesh("square", mesh_nodes=100)
        boundary = get_boundary("dirichlet")
        ref_fs = FunctionSpace(ref_mesh, 'CG', 1)
        ref_bc = DirichletBC(ref_fs, Constant(0.0), boundary)
        rhs = Constant(1.0)

        self.ref_problem = ParametricPoisson(exp_field, rhs, ref_fs, ref_bc)
        print("dimension of reference space: {}".format(self.ref_problem.fs.dim()))

        self.noise = noise
        self.alpha = 0                              # by possible rotation

        self.dimension = M
        self.true_para = np.random.randn(self.dimension)

        points = []
        for x in np.linspace(0 + 1 / (2 * np.sqrt(n_points)), 1 - 1 / (2 * np.sqrt(n_points)), num=np.sqrt(n_points)):
            for y in np.linspace(0 + 1 / (2 * np.sqrt(n_points)), 1 - 1 / (2 * np.sqrt(n_points)),
                                 num=np.sqrt(n_points)):
                points.append([x, y])

        self.solver_cache = SolutionCache()
        true_sol = self.ref_problem.solve(self.true_para,
                                          reference_m=self.dimension,
                                          cache=self.solver_cache)

        self.obs = {"x"    : points,
                    "delta": []}
        for lia in range(len(points)):
            self.obs["delta"].append(true_sol(points[lia]) + (np.random.randn() * self.noise))

    def Phi(self,
            _xi):
        _sol = self.ref_problem.solve(_xi,
                                      reference_m=self.dimension,
                                      cache=self.solver_cache)
        return 0.5 / self.noise * np.sum([(_sol(self.obs["x"][_lia]) - self.obs["delta"][_lia]) ** 2
                                          for _lia in range(len(self.obs["x"]))],
                                         axis=0)

    @staticmethod
    def prior(x):
        return (1/np.sqrt((2*np.pi))**len(x))*np.exp(-0.5*np.linalg.norm(x)**2)

    @count_calls
    def post(self,
             x):
        return np.exp(-self.Phi(x))*self.prior(x)

    def compute_map(self,
                    approximate=True,
                    init_value=None,
                    log_optimizer=False,
                    use_difftool=True):
        M    = None
        hess = None

        if approximate:
            if log_optimizer:
                map_info = compute_map(lambda x: -np.log(self.post(x)), init_value)
            else:
                map_info = compute_map(lambda x: -self.post(x), init_value)

            if use_difftool:
                hess = nd.Hessian(lambda x: -np.log(self.post(x)), method="central")(map_info["x"])
            else:
                hess = np.linalg.inv(map_info["hess_inv"])

            hess = -hess
            M = map_info["x"]
        else:
            raise NotImplementedError("We do not know the map here.")
        trafo = RadialTrafo(self.dimension, M, hess)

        return trafo

class BioPhy(object):
    def __init__(self):

        # TODO This needs to be modified to fit your directory structure

        directory = "KCl/10mM/"
        used_files, file_names = get_files_from_directory(directory)

        q_min = 0.07
        q_max = 0.8

        self.lower_bound = [3.1, 1e-8, 1e-8, 1e-8]
        self.upper_bound = [15, 5, 1, 10]

        # region Read given data files
        file_dict = read_lists_from_data(used_files, q_min, q_max)
        qr, qr_short = file_dict["qr"], file_dict["qr_short"]
        intensity, intensity_short = file_dict["intensity"], file_dict["intensity_short"]
        error, error_short = file_dict["error"], file_dict["error_short"]
        # endregion

        assert len(qr_short) > 0

        self.loss_dict = {
            "_qr_short"       : qr_short,
            "_intensity_short": intensity_short,
            "_error_short"    : error_short
        }

    @staticmethod
    def loss(theta, _qr_short=None, _intensity_short=None, _error_short=None, _add_noise=None):
        _model = model(_qr_short[0], *theta, timing=False)
        _retval = (1 / _error_short[0]) * (_intensity_short[0] - _model["result"])
        if len(_qr_short) > 1:
            for _qr, _int, _err, _noise in zip(_qr_short[1:], _intensity_short[1:], _error_short[1:]):
                _model = model(_qr, *theta, timing=False)
                _retval = np.append(_retval, (1 / (_err + _noise)) * (_int - _model["result"]))
        return _retval

    @staticmethod
    def prior(x):
        # for __lia, _x in enumerate(x):
        #     if not lower_bound[__lia] <= _x <= upper_bound[__lia]:
        #         return -100
        # return 0
        return (1/np.sqrt((2*np.pi))**len(x))*np.exp(-0.5*np.linalg.norm(x)**2)

    def lnlike(self,
               x):
        # choose uniform bounds
        _x = np.zeros(len(self.lower_bound))
        for __lia in range(len(self.lower_bound)):
            _x[__lia] = self.lower_bound[__lia] + (self.upper_bound[__lia] - self.lower_bound[__lia]) * \
                        special.erfc(x[__lia] / math.sqrt(2.))
        # print("  trafo prior: {}".format(_x))
        return np.linalg.norm(self.loss(_x, **self.loss_dict))**2

    @count_calls
    def post(self,
             x):
        _x = x  #*normalisation
        # print(" x: {}".format(_x))
        pr = self.prior(_x)
        if pr < 0:
            return 0
        # print("   prior: {}".format(pr))
        if not np.isfinite(pr):
            return -np.inf
        try:
            like = self.lnlike(_x)
            # print("    like: {}".format(like))
            __retval = np.exp(-0.5*like - pr)
        except RuntimeWarning:
            print("#"*100)

            __retval = 100
        return __retval


class GaussianModel(GenericModel):
    def __init__(self, mean, var):
        super(GaussianModel, self).__init__()
        self.dim = len(mean)
        self.mean = mean
        self.var = var
        self.pdf = lambda x: mn.pdf(x, self.mean, self.var, allow_singular=False)

    @count_calls
    def post(self,
             _xi):
        retval = self.pdf(_xi)
        if np.isnan(retval):
            print(" post({}) is nan".format(_xi))

        if np.isinf(retval):
            print(" post({}) is inf".format(_xi))
        return retval

    def compute_map(self,
                    approximate=False,
                    log_optimizer=False,
                    use_difftool=True):
        M    = None
        hess = None
        init_value = self.mean + np.random.randn(self.dim) * self.var[0, 0]
        if approximate:
            if log_optimizer:
                map_info = compute_map(lambda x: -np.log(self.post(x)), init_value)
            else:
                map_info = compute_map(lambda x: -self.post(x), init_value)

            if use_difftool:
                hess = nd.Hessian(lambda x: -np.log(self.post(x)), method="central")(map_info["x"])
            else:
                hess = np.linalg.inv(map_info["hess_inv"])

            hess = -hess
            M = map_info["x"]
        else:
            M = self.mean
            hess = - np.linalg.inv(self.var)
        trafo = RadialTrafo(self.dim, M, hess)

        return trafo


class LinearModel(object):
    def __init__(self, dim):

        self.dim = dim

    @count_calls
    def post(self, x):
        return -0.5*np.linalg.norm(x, ord=2)**2

    def compute_map(self):
        hess = np.eye(self.dim, self.dim)
        return RadialTrafo(self.dim, np.zeros(self.dim), hess)




class ElasticityUpscaling(object):

    def __init__(self):
        self.dim = 4
        data_name = "micro_data"
        folder_name = "data/multi_materials/0.03/"

        M = 1

        microdata = None
        if os.path.exists(folder_name + data_name + '.pkl'):
            microdata = load_obj(folder_name, data_name)
        else:
            raise AssertionError("data file missing")

        microdata_all = []
        for m in range(M):
               microdata_all += microdata[m]
        microdata = np.array(microdata_all)

        # Prepare the prior distribution  here standard normal prior  due to trafos shifted to other distributions
        mean_theta = math.asin(1. / np.sqrt(2))
        a, b = mean_theta - 1./8*np.pi, mean_theta + 1./8*np.pi

        self.trafos = [lambda x: 40000 * np.exp(x),
                  lambda x: 45000 * np.exp(x),  # 45000
                  lambda x: 45000 * np.exp(x),
                  lambda x: a + (b - a) * special.erfc(x / math.sqrt(2.))]  # lambda x: x] for uniform take the latter

        # ADD NOISE OF YOUR CHOICE !!
        self.observered_variance = 10000  # change at our needs !
        self.delta = microdata  # + self.observered_variance * np.random.randn(6) # numerical value of micro data is already not exact

        # prepare the prior model class
        symclass = "orthotropic"
        self.C_generator = symClassExponential(dim=2, symClass=symclass) #orthotropic


    def _solve_macro_problem(self, y_meso):
        C = self.C_generator.realisation(y_meso, trafo=self.trafos)  # .tolist()
        # C = as_matrix(C)

        macro_strains = [np.array([[1, 0], [0, 0]]),
                        np.array([[0, 0], [0, 1]]),
                        np.array([[0, 1], [1, 0]]),
                        np.array([[1, 1], [1, 0]]),
                        np.array([[0, 1], [1, 1]]),
                        np.array([[1, 0], [0, 1]])]

        def solve_macro(C, macrostrain):
            ms_V = np.array(([macrostrain[0, 0], macrostrain[1, 1], 2 * macrostrain[0, 1]]))
            res = ms_V.transpose().dot(C.dot(ms_V))
            return res

        macro_results = [solve_macro(C, macro_strain) for macro_strain in macro_strains]
        return np.array(macro_results)


    @count_calls
    def post(self,
             _xi):
        lnpost =  self._lnlike(_xi) + self._lnprior(_xi)
        retval =  np.exp(lnpost)
        if np.isnan(retval):
            print(" post({}) is nan".format(_xi))

        if np.isinf(retval):
            print(" post({}) is inf".format(_xi))
        # print(_xi)
        # print(retval)
        # print("#"*20)
        return retval

    def _lnlike(self, y_meso):
        # print(self.delta)
        # print("#"*20)
        # print(self._solve_macro_problem(y_meso))
        diff = self.delta - self._solve_macro_problem(y_meso)
        # print("diff: {}".format(diff))
        res = -0.5*np.dot(diff, self.observered_variance**(-1) * diff)
        # print("res: {}".format(res))
        return res
    def _lnprior(self, y_meso):
        if len(y_meso) == 4:
            retval = -0.5 * np.dot(y_meso[:3], y_meso[:3])
            # print("prior: {}".format(retval))
            return retval
        else:
            raise NotImplementedError("len = 4 of input only supported")

    def compute_map(self,
                    approximate=False,
                    log_optimizer=False,
                    use_difftool=True,
                    init_value=None):
        M    = None
        hess = None
        #init_value = self.mean + np.random.randn(self.dim) * self.var[0, 0]
        if approximate:
            if log_optimizer:
                map_info = compute_map(lambda x: -np.log(self.post(x)), init_value)
            else:
                map_info = compute_map(lambda x: -self.post(x), init_value)

            if use_difftool:
                hess = nd.Hessian(lambda x: -np.log(self.post(x)), method="central")(map_info["x"])
            else:
                hess = np.linalg.inv(map_info["hess_inv"])

            hess = -hess
            M = map_info["x"]
        else:
            M = self.mean
            hess = - np.linalg.inv(self.var)
        trafo = RadialTrafo(self.dim, M, hess)

        return trafo
