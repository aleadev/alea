from decimal import *
from decimal import Decimal as Dec
import numpy as np


class AP_Poly_fabric(object):
    """
    a fabric for generating polynomials with underlying arbitrary precession.
    """





class ArbitraryPrecision_Poly(object):
    def __init__(self, coef):
        self.coef = coef

    def __call__(self, x):
        result = None
        if isinstance(x, np.ndarray):
            res = [Dec(0.)] * len(x)
            for i, xi in enumerate(x):
                for j, c in enumerate(self.coef):
                    if j == 0:
                        res[i] += c
                        continue
                    res[i] += c * Dec(xi) ** j
            result = np.array([float(r) for r in res])

        else:
            result = Dec(0.)
            for i, c in enumerate(self.coef):
                if i == 0:
                    result += c
                    continue
                result += c * Dec(x) ** i
            result = float(result)

        return result

    def __getitem__(self, key):
        return self.coef[key]

    def __len__(self):
        return len(self.coef)

    @staticmethod
    def polymul(c1, c2):
        """

        :param p1: list of Decimal numbers representing polynomial coefficients
        :param p2:
        :return:   returns a list of coefficients



        example:
        # weight coefficient arbitrary precision
                    coeff = [Dec(0.)]*(dim-1) + [Dec(1.)]

                    representing   x -> x^(dim-1)
        """
        p = [Decimal(0.)] * (len(c1) + len(c2) - 1)
        for i in range(len(c1)):
            for j in range(len(c2)):
                p[i + j] += c1[i] * c2[j]
        return p

    @staticmethod
    def set_precision(prec):
        getcontext().prec = prec



    @staticmethod
    def skp(f,g, w, subdomain):
        """
                    int_{subdomain}  f(r)*g(r) r^{dim-1} dr
        :param f: ArbitraryPrecision_Poly or list of Decimals
        :param g: ArbitraryPrecision_Poly or list of Decimals
        :param w: ArbitraryPrecision_Poly or list of Decimals
        :param subdomain:
        :return:
        """
        r0, r1 = Dec(subdomain[0]), Dec(subdomain[1])
        int_coeff = ArbitraryPrecision_Poly.polymul(ArbitraryPrecision_Poly.polymul(f, g), w)
        primitive = [Dec(0.)] + [Dec(1.) / Dec(i + 1) * int_coeff[i] for i in range(len(int_coeff))]
        # primitive integral function
        res = Dec(0.)
        for i, c in enumerate(primitive):
            if i == 0:
                continue
            res += c * (r1 ** i - r0 ** i)
            # total integral
        return res


def compute_orth_polynomials_radial(N, subdomain, dim, prec=8, verbose=False):
    """
    orthonormal basis computation with underlying arbitrary precision

    computes orthonormal polynomials w.r.t. the skp

         int_{subdomain}  f(r)*g(r) r^{dim-1} dr
    """

    assert dim >= 2
    ArbitraryPrecision_Poly.set_precision(prec)

    #r0, r1 = Dec(subdomain[0]), Dec(subdomain[1])
    # weight
    w_c_a = ArbitraryPrecision_Poly([Dec(0.)] * (dim - 1) + [Dec(1.)])

    pp = {}
    for i in range(N):
        pp[i] = [Dec(0.)] * N
        pp[i][i] = Dec(1.)

    def skp(p1, p2):
        return ArbitraryPrecision_Poly.skp(p1,p2,w_c_a, subdomain)
        #int_coeff = ArbitraryPrecision_Poly.polymul(ArbitraryPrecision_Poly.polymul(p1, p2), w_c_a)
        #primitive = [Dec(0.)] + [Dec(1.) / Dec(i + 1) * int_coeff[i] for i in range(len(int_coeff))]
        # primitive integral function
        #res = Dec(0.)
        #for i, c in enumerate(primitive):
        #    res += c * (r1 ** i - r0 ** i)
        #    # total integral
        #return res

    # modified gram schmidt procedure
    def orthonormalize(start_coeffs):
        q = {}
        for j in range(N):
            c = skp(pp[j], pp[j])
            norm_q_j = c.sqrt()
            q[j] = [pp[j][k] / norm_q_j for k in range(N)]
            for k in range(j + 1, N):
                c = skp(q[j], pp[k])
                pp[k] = [pp[k][l] - c * q[j][l] for l in range(N)]
        return q
    q = orthonormalize(pp)

    from scipy.integrate import quad
    n = len(q)

    o_basis = [ArbitraryPrecision_Poly(q[j]) for j in range(N)]
    return o_basis


def graveyard():
    "old code possible to use elsewhere"
    def compute_orth_polynomials_radial(N, subdomain, dim):
        """
        Commputes N orthonormal polynomials

         w.r.t to subdomain = [a,b] and scalar product

            (f,g) := int_[a,b] f(r)*g(r) *r^{dim-1} dr.

        returns a list of length N, of callable polynomials of instance

                numpy.polynomial.polynomial.Polynomial

        @param N : number of basis functions
        @param subdomain: tuple of real numbers
        @param dim : dimension of the underlying problem to specify the r^{dim-1} weight.
        """
        assert dim >= 2

        r0, r1 = subdomain
        assert r0 < r1

        def compute_B():
            B = np.zeros((N, N))
            # coefficient representing the weight w : r -> r^{dim-1}
            w_coeff = [0.] * (dim - 1) + [1]

            # p[i] represents r -> r^i as coefficient
            p = {}
            for i in range(N):
                p[i] = [0.] * (i + 1)
                p[i][i] = 1.0

            for i in range(N):
                for j in range(i, N):
                    # integrant coeff representing p[i](r)*p[j](r) * w(r) as coefficient
                    int_coeff = Poly.polymul(Poly.polymul(p[i], p[j]), w_coeff)
                    # primitive integral function
                    Int = np.polynomial.Polynomial(Poly.polyint(int_coeff, m=1))
                    # total integral
                    B[i, j] = B[j, i] = Int(r1) - Int(r0)
            return B

        # modified gram schmidt procedure for functions on vector representation
        B = compute_B()

        v, c = [None] * N, [None] * N

        for j in range(N):
            v[j] = np.zeros((N, 1))
            v[j][j] = 1.0

        for j in range(N):
            c[j] = v[j] / (v[j].transpose().dot(B.dot(v[j]))) ** 0.5
            for k in range(j + 1, N):
                v[k] = v[k] - c[j].transpose().dot(B.dot(v[k])) * c[j]

        # return c
        o_basis = [np.polynomial.Polynomial(c[j].flatten()) for j in range(N)]
        return o_basis

    def check_orth(basis, dim, subdomain):
        w = lambda t: t ** (dim - 1)
        b = basis

        for i in range(len(basis)):
            res = 0

            l, r = subdomain

            f = lambda t: b[i](t) * b[i](t) * w(t)
            val, err = quadrature(f, l, r)

            assert (err < 1e-7)

            print("(i,i)=({i},{i}) -> {v}".format(i=i, v=val))

        print("#############################")

        for i in range(len(basis)):
            for j in range(i + 1, len(basis)):
                l, r = subdomain
                f = lambda t: b[i](t) * b[j](t) * w(t)
                val, err = quadrature(f, l, r)
                assert (err < 1e-7)
                print("(i,j)=({i},{j}) -> {v}".format(i=i, j=j, v=val))



def test():
    N = 10
    subdomain = [9.6, 10.0]
    dim = 5
    b = compute_orth_polynomials_radial(N, subdomain, dim, prec=100)
    import matplotlib.pyplot as plt


    rr = np.linspace(subdomain[0], subdomain[1], 1000)  #
    plt.figure(figsize=(15, 25))
    for i in range(N):
        plt.subplot(10, 2, i + 1)
        plt.plot(rr, b[i](rr))

    plt.show()


# test()