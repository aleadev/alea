from __future__ import division, print_function, absolute_import
from dolfin import Function, IntervalMesh, FunctionSpace, TrialFunction, TestFunction, inner, dx, solve, assemble
import matplotlib.pyplot as plt
import numpy as np


class FemBasis(object):
    def __init__(self):
        self.a = None
        self.b = None
        self.fs = None
        self.num = None
        pass

    def __getitem__(self, item):
        pass

    def in_support(self, x, i):
        pass

    def get_support(self,
                    x                               # type: float
                    ):                              # type: (...) -> int
        """
        loops over the in_support function to return the support of the given point
        returns -1 if not in any support
        # TODO: Make this more educated. Maybe via bisection.
        :param x: point
        :return: support index
        """
        for i in range(self.num):
            if self.in_support(x, i): return i
        return -1

    def plot_marginals(self,
                       sc,
                       true_para=None,
                       normalise=False,
                       xlabel="x",
                       ylabel="f(x)",
                       title="Parameter",
                       save_path="tmp/marginals.pdf"):
        fig = plt.figure(figsize=(14, 8))

        grid = np.linspace(self.a, self.b, num=100)

        u = TestFunction(self.fs)
        v = TrialFunction(self.fs)
        stiff = inner(u, v) * dx
        rhs = Function(self.fs)
        rhs.vector()[:] = np.ascontiguousarray(sc)
        ell = inner(rhs, v) * dx
        sol = Function(self.fs)
        solve(stiff == ell, sol)
        if normalise:
            z = assemble(sol * dx)
        else:
            z = 1
        sol.set_allow_extrapolation(True)
        val = [sol(x) / z for x in grid]
        plt.plot(grid, val)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if true_para is not None:
            plt.vlines(true_para, 0, max(val), colors='r', linestyles="--")
        plt.title(title)

        # gauss_pdf = lambda x: 1/np.sqrt(2*np.pi*0.01)*np.exp(-0.5*(x**2)/(0.01))
        # grid = np.linspace(self.a, self.b, num=100)
        # plt.plot(grid, gauss_pdf(grid), '-b')

        fig.savefig(save_path)


class HatBasis(FemBasis):
    def __init__(self,
                 a,
                 b,
                 num):
        FemBasis.__init__(self)
        self.num = num
        self.a = a
        self.b = b
        mesh = IntervalMesh(num, a, b)
        self.fs = FunctionSpace(mesh, 'CG', 1)
        self.h = (self.b - self.a) / (self.fs.dim() - 1)

    def __getitem__(self, item):
        retval = Function(self.fs)
        retval.vector()[:] = np.zeros(self.fs.dim())
        retval.vector()[item] = 1

        return retval

    def in_support(self,
                   x,                               # type: float
                   i,                               # type: int
                   ):
        """
        Check if x is in  supp phi_i, with phi_i \in V([a,b]), dim V = dim,
        here phi_i is a hat function.

        :param x: the eval point
        :param i:  number of basis function
        :return: bool
        """
        if not self.a < x < self.b:
            return False

        if i == 0:
            return (self.a <= x) and (x <= self.a + self.h)
        elif i == self.fs.dim():
            return (self.b - self.h <= x) and (x <= self.b)
        else:
            pos = int((x - self.a) / self.h)
            return (i - 1 <= pos) and (pos <= i + 1)


class PwConstant(FemBasis):
    def __init__(self,
                 a,
                 b,
                 num):
        FemBasis.__init__(self)
        self.num = num
        self.a = a
        self.b = b
        mesh = IntervalMesh(num, a, b)
        self.fs = FunctionSpace(mesh, 'DG', 0)
        self.h = (self.b - self.a) / (self.fs.dim())

    def __getitem__(self, item):
        retval = Function(self.fs)
        retval.vector()[:] = np.zeros(self.fs.dim())
        retval.vector()[item] = 1

        return retval

    def in_support(self,
                   x,  # type: float
                   i,  # type: int
                   ):
        """
        Check if x is in  supp phi_i, with phi_i \in V([a,b]), dim V = dim,
        here phi_i is a hat function.

        :param x: the eval point
        :param i:  number of basis function
        :return: bool
        """
        if not self.a < x < self.b:
            return False

        pos = int((x - self.a) / self.h)
        return pos == i
