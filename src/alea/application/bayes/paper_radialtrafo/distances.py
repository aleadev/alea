from __future__ import (division, print_function, absolute_import)
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler
import numpy as np
import torch
import torch.nn as nn
import os
import json


# Taken from https://github.com/dfdazac/wassdistance.git
# Adapted from https://github.com/gpeyre/SinkhornAutoDiff
class SinkhornDistance(nn.Module):
    r"""
    Given two empirical measures each with :math:`P_1` locations
    :math:`x\in\mathbb{R}^{D_1}` and :math:`P_2` locations :math:`y\in\mathbb{R}^{D_2}`,
    outputs an approximation of the regularized OT cost for point clouds.
    Args:
        eps (float): regularization coefficient
        max_iter (int): maximum number of Sinkhorn iterations
        reduction (string, optional): Specifies the reduction to apply to the output:
            'none' | 'mean' | 'sum'. 'none': no reduction will be applied,
            'mean': the sum of the output will be divided by the number of
            elements in the output, 'sum': the output will be summed. Default: 'none'
    Shape:
        - Input: :math:`(N, P_1, D_1)`, :math:`(N, P_2, D_2)`
        - Output: :math:`(N)` or :math:`()`, depending on `reduction`
    """
    def __init__(self, eps, max_iter, reduction='none'):
        super(SinkhornDistance, self).__init__()
        self.eps = eps
        self.max_iter = max_iter
        self.reduction = reduction

    def forward(self, x, y):
        # The Sinkhorn algorithm takes as input three variables :
        C = self._cost_matrix(x, y)  # Wasserstein cost function
        x_points = x.shape[-2]
        y_points = y.shape[-2]
        if x.dim() == 2:
            batch_size = 1
        else:
            batch_size = x.shape[0]

        # both marginals are fixed with equal weights
        mu = torch.empty(batch_size, x_points, dtype=torch.float,
                         requires_grad=False).fill_(1.0 / x_points).squeeze()
        nu = torch.empty(batch_size, y_points, dtype=torch.float,
                         requires_grad=False).fill_(1.0 / y_points).squeeze()

        u = torch.zeros_like(mu)
        v = torch.zeros_like(nu)
        # To check if algorithm terminates because of threshold
        # or max iterations reached
        actual_nits = 0
        # Stopping criterion
        thresh = 1e-1

        # Sinkhorn iterations
        for i in range(self.max_iter):
            u1 = u  # useful to check the update
            u = self.eps * (torch.log(mu+1e-8) - torch.logsumexp(self.M(C, u, v), dim=-1)) + u
            v = self.eps * (torch.log(nu+1e-8) - torch.logsumexp(self.M(C, u, v).transpose(-2, -1), dim=-1)) + v
            err = (u - u1).abs().sum(-1).mean()

            actual_nits += 1
            if err.item() < thresh:
                break

        U, V = u, v
        # Transport plan pi = diag(a)*K*diag(b)
        pi = torch.exp(self.M(C, U, V))
        # Sinkhorn distance
        cost = torch.sum(pi * C, dim=(-2, -1))

        if self.reduction == 'mean':
            cost = cost.mean()
        elif self.reduction == 'sum':
            cost = cost.sum()

        return cost, pi, C

    def M(self, C, u, v):
        "Modified cost for logarithmic updates"
        "$M_{ij} = (-c_{ij} + u_i + v_j) / \epsilon$"
        return (-C + u.unsqueeze(-1) + v.unsqueeze(-2)) / self.eps

    @staticmethod
    def _cost_matrix(x, y, p=2):
        "Returns the matrix of $|x_i-y_j|^p$."
        x_col = x.unsqueeze(-2)
        y_lin = y.unsqueeze(-3)
        C = torch.sum((torch.abs(x_col - y_lin)) ** p, -1)
        return C

    @staticmethod
    def ave(u, u1, tau):
        "Barycenter subroutine, used by kinetic acceleration through extrapolation."
        return tau * u + (1 - tau) * u1


class DensityDistance(object):
    def __init__(self,
                 rho1,                              # type: callable
                 rho2,                              # type: callable
                 dim,                               # type: int
                 ):
        self.rho1 = rho1
        self.rho2 = rho2
        self.dim = dim

    def compute(self,
                n_used_samples                      # type: int
                ):
        pass


class Hellinger(DensityDistance):
    def __init__(self,
                 rho1,                              # type: callable
                 rho2,                              # type: callable
                 dim,                               # type: int
                 # domain                             # type: TODO. 03.20 Do not know why this is a TODO (MM)
                 rho1_sampler,  # type: MCMC_Sampler
                 rho2_polar=False  # type: bool
                 ):
        super(Hellinger, self).__init__(rho1, rho2, dim)

        self.rho1_sampler = rho1_sampler
        self.rho2_polar = rho2_polar

    @classmethod
    def from_path(cls, rho1, rho2, dim, path):
        if path is not None:
            assert os.path.exists(path)
        with open(path, "r") as f:
            inf = json.load(f)
        if "x" in inf[0] and "pc" in inf[0]:
            rho2_polar = True
        else:
            rho2_polar = False
        sampler = MCMC_Sampler(log_prob=None,
                               N=None,
                               burn_in=None,
                               ndim=None,
                               nwalkers=None,
                               p0=None,
                               samples=inf
                               )
        return cls(rho1, rho2, dim, sampler, rho2_polar)

    def compute(self,
                n_used_samples                      # type: int
                ):
        if n_used_samples > len(self.rho1_sampler):
            print("# of used samples > # of existing samples. "
                  "Use max number: {} instead".format(len(self.rho1_sampler)))
            n_used_samples = len(self.rho1_sampler)
        if self.rho2_polar:
            sample_list = [np.sqrt(self.rho2(x["pc"])/self.rho1(x["x"]))
                           for x in self.rho1_sampler.get(n_used_samples)]
        else:
            sample_list = [np.sqrt( self.rho2(x)/self.rho1(x))
                           for x in self.rho1_sampler.get(n_used_samples)]
        off_samples = []
        off_sample_args = []
        for idx, (sample, x) in enumerate(zip(sample_list, self.rho1_sampler.get(n_used_samples))):
            if np.abs(sample) > 10 or not np.isfinite(sample):
                if self.rho2_polar:
                    print("sample {} = {}, x:{}, pc:{}, rho1:{}, rho2:{}".format(idx, sample,
                                                           self.rho1_sampler.get(n_used_samples)[idx]["x"],
                                                           self.rho1_sampler.get(n_used_samples)[idx]["pc"],
                                                                             self.rho1(x["x"]),
                                                                             self.rho2(x["pc"])))
                else:
                    print("sample {} = {}, x:{}, rho1:{}, rho2:{}".format(idx, sample,
                                                           self.rho1_sampler.get(n_used_samples)[idx],
                                                                             self.rho1(x),
                                                                             self.rho2(x)))
                off_samples.append(sample)
                off_sample_args.append(off_sample_args)

        retval = 1 - 1/n_used_samples * np.sum([sample for sample in sample_list])
        import matplotlib.pyplot as plt
        fig = plt.figure()
        grid = range(len(sample_list))
        plt.plot(grid, sample_list, 'rx')
        plt.title("dist={}".format(retval))
        fig.savefig("test_hell_{}".format(self.dim))

        return retval


class KL(DensityDistance):
    def __init__(self,
                 rho1,                              # type: callable
                 rho2,                              # type: callable
                 dim,                               # type: int
                 rho1_sampler,                      # type: MCMC_Sampler
                 rho2_polar=False                   # type: bool
                 ):
        super(KL, self).__init__(rho1, rho2, dim)
        self.rho1_sampler = rho1_sampler
        self.rho2_polar = rho2_polar

    @classmethod
    def from_path(cls, rho1, rho2, dim, path):
        if path is not None:
            assert os.path.exists(path)
        with open(path, "r") as f:
            inf = json.load(f)
        if "x" in inf[0] and "pc" in inf[0]:
            rho2_polar = True
        else:
            rho2_polar = False
        sampler = MCMC_Sampler(log_prob=None,
                               N=None,
                               burn_in=None,
                               ndim=None,
                               nwalkers=None,
                               p0=None,
                               samples=inf
                               )
        return cls(rho1, rho2, dim, sampler, rho2_polar)

    @classmethod
    def from_sampler(cls,
                     rho1,
                     rho2,
                     dim,
                     n_samples=1000,                # type: int
                     n_burn_in=100,                 # type: int
                     n_walkers=2,                   # type: int
                     p0=None,                       # type: list or np.array
                     ):
        if n_walkers <= 2 * dim:
            n_walkers = 2 * dim + 2
            print("WARNING: changed number of walkers to {}".format(n_walkers))
        p0 = p0 if p0 is not None else np.random.randn(n_walkers, dim)

        def log_prob(x):
            # print(x, self.rho1(x), np.log(self.rho1(x)))
            return np.log(rho1(x))

        rho1_sampler = MCMC_Sampler(log_prob,
                                    n_samples + n_burn_in,
                                    n_burn_in,
                                    dim,
                                    n_walkers,
                                    p0
                                    )
        return cls(rho1, rho2, dim, rho1_sampler)

    def compute(self,
                n_used_samples                      # type: int
                ):
        if n_used_samples > len(self.rho1_sampler):
            print("# of used samples > # of existing samples. "
                  "Use max number: {} instead".format(len(self.rho1_sampler)))
            n_used_samples = len(self.rho1_sampler)
        if self.rho2_polar:
            sample_list = [np.log(self.rho1(x["x"])/self.rho2(x["pc"]))
                           for x in self.rho1_sampler.get(n_used_samples)]
        else:
            sample_list = [np.log(self.rho1(x) / self.rho2(x))
                           for x in self.rho1_sampler.get(n_used_samples)]
        off_samples = []
        off_sample_args = []
        for idx, (sample, x) in enumerate(zip(sample_list, self.rho1_sampler.get(n_used_samples))):
            if np.abs(sample) > 1 or not np.isfinite(sample):
                if self.rho2_polar:
                    print("sample {} = {}, x:{}, pc:{}, rho1:{}, rho2:{}".format(idx, sample,
                                                           self.rho1_sampler.get(n_used_samples)[idx]["x"],
                                                           self.rho1_sampler.get(n_used_samples)[idx]["pc"],
                                                                             self.rho1(x["x"]),
                                                                             self.rho2(x["pc"])))
                else:
                    print("sample {} = {}, x:{}, rho1:{}, rho2:{}".format(idx, sample,
                                                           self.rho1_sampler.get(n_used_samples)[idx],
                                                                             self.rho1(x),
                                                                             self.rho2(x)))
                off_samples.append(sample)
                off_sample_args.append(off_sample_args)
        retval = 1/n_used_samples * np.sum([sample for sample in sample_list])
        import matplotlib.pyplot as plt
        fig = plt.figure()
        grid = range(len(sample_list))
        plt.plot(grid, sample_list, 'rx')
        plt.title("dist={}".format(retval))
        fig.savefig("test_kl_{}".format(self.dim))

        return retval


class Wasserstein(DensityDistance):
    def __init__(self,
                 rho1,                              # type: callable
                 rho2,                              # type: callable
                 dim,                               # type: int
                 sampler1,
                 sampler2,
                 rho2_polar=False,                  # type: bool
                 sinkhorn_eps=0.1,
                 sinkhorn_iter=100,
                 ):
        super(Wasserstein, self).__init__(rho1, rho2, dim)

        self.sinkhorn_iter = sinkhorn_iter
        self.sinkhorn_eps = sinkhorn_eps
        self.rho2_polar = rho2_polar
        self.rho1_sampler = sampler1
        self.rho2_sampler = sampler2
        self.x = None
        self.y = None
        self.dist = np.inf
        self.P = None
        self.C = None
        
    @classmethod
    def from_path(cls, rho1, rho2, dim, path1, path2,
                  sinkhorn_eps=0.1, sinkhorn_iter=100):
        if path1 is not None:
            assert os.path.exists(path1)
        if path2 is not None:
            assert os.path.exists(path2)
        with open(path1, "r") as f:
            inf1 = json.load(f)
        with open(path2, "r") as f:
            inf2 = json.load(f)
        if "x" in inf2[0] and "pc" in inf2[0]:
            rho2_polar = True
        else:
            rho2_polar = False
        sampler1 = MCMC_Sampler(log_prob=None,
                                N=None,
                                burn_in=None,
                                ndim=None,
                                nwalkers=None,
                                p0=None,
                                samples=inf1
                                )

        sampler2 = MCMC_Sampler(log_prob=None,
                                N=None,
                                burn_in=None,
                                ndim=None,
                                nwalkers=None,
                                p0=None,
                                samples=inf2
                                )
        return cls(rho1, rho2, dim, sampler1, sampler2, rho2_polar, sinkhorn_eps, sinkhorn_iter)

    @classmethod
    def from_sampler(cls,
                     rho1,
                     rho2,
                     dim,
                     n_samples=1000,  # type: int
                     n_burn_in=100,  # type: int
                     n_walkers=2,  # type: int
                     p0=None,  # type: list or np.array
                     p0_2=None,  # type: list or np.array
                     sinkhorn_eps=0.1,  # type: float
                     sinkhorn_iter=100  # type: int
                     ):
        if n_walkers <= 2 * dim:
            n_walkers = 2 * dim + 2
            print("WARNING: changed number of walkers to {}".format(n_walkers))
        p0 = p0 if p0 is not None else np.random.randn(n_walkers, dim)

        def log_prob(x):
            # print(x, self.rho1(x), np.log(self.rho1(x)))
            return np.log(rho1(x))
        if n_walkers <= 2 * dim:
            n_walkers = 2 * dim + 2
            print("WARNING: changed number of walkers to {}".format(n_walkers))
        p0 = p0 if p0 is not None else np.random.randn(n_walkers, dim)
        p0_2 = p0_2 if p0_2 is not None else np.random.randn(n_walkers, dim)

        rho1_sampler = MCMC_Sampler(lambda x: np.log(rho1(x)),
                                    n_samples + n_burn_in,
                                    n_burn_in,
                                    dim,
                                    n_walkers,
                                    p0
                                    )

        rho2_sampler = MCMC_Sampler(lambda x: np.log(rho2(x)),
                                    n_samples + n_burn_in,
                                    n_burn_in,
                                    dim,
                                    n_walkers,
                                    p0_2
                                    )

        return cls(rho1, rho2, dim, rho1_sampler, rho2_sampler, False, sinkhorn_eps, sinkhorn_iter)
    
    def compute(self,
                n_used_samples  # type: int
                ):
        if n_used_samples > len(self.rho1_sampler) or n_used_samples > len(self.rho2_sampler):
            max_num = min(len(self.rho1_sampler), len(self.rho2_sampler))
            print("# of used samples > # of existing samples. "
                  "Use max number: {} instead".format(max_num))
            n_used_samples = max_num

        if self.rho2_polar:
            a = np.zeros((n_used_samples, self.dim))
            b = np.zeros((n_used_samples, self.dim))
            s1 = self.rho1_sampler.get(n_used_samples)
            s2 = self.rho2_sampler.get(n_used_samples)

            for lia in range(n_used_samples):
                a[lia, :] = s1[lia]["x"]
                b[lia, :] = s2[lia]["x"]
        else:
            a = self.rho1_sampler.get(n_used_samples)
            b = self.rho2_sampler.get(n_used_samples)
        self.x = torch.tensor(a, dtype=torch.float)
        self.y = torch.tensor(b, dtype=torch.float)

        sinkhorn = SinkhornDistance(eps=self.sinkhorn_eps, max_iter=self.sinkhorn_iter, reduction=None)
        self.dist, self.P, self.C = sinkhorn(self.x, self.y)
        # print("Sinkhorn distance: {:.3f}".format(self.dist.item()))

        return self.dist.item()

    def visualize(self,
                  path="",                          # type: str
                  ):
        assert self.P is not None
        assert self.C is not None
        import matplotlib.pyplot as plt
        fig = plt.figure()
        plt.imshow(self.P)
        plt.title("Coupling Matrix")
        fig.savefig(path + "coupling.pdf")

        fig = plt.figure()
        plt.imshow(self.C)
        plt.colorbar()
        plt.title("Distance Matrix")
        fig.savefig(path + "distance.pdf")

        def show_assignments(a, b, P):
            norm_P = P / P.max()
            for i in range(a.shape[0]):
                for j in range(b.shape[0]):
                    plt.arrow(a[i, 0], a[i, 1], b[j, 0] - a[i, 0], b[j, 1] - a[i, 1],
                              alpha=norm_P[i, j].item())
            plt.title('Assignments')
            plt.scatter(a[:, 0], a[:, 1])
            plt.scatter(b[:, 0], b[:, 1])
            # plt.axis('off')

        fig = plt.figure()
        show_assignments(self.x.detach().numpy(), self.y.detach().numpy(), self.P)
        fig.savefig(path + "assignments.pdf")
