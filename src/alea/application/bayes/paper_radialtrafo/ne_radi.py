from __future__ import (division, print_function, absolute_import)
import sys
import numpy as np
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
from alea.application.bayes.paper_radialtrafo.config import RadialTrafoConfig as Config
from alea.utils.progress.percentage import PercentageBar
import time
import json
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler

import os
if sys.version_info < (3, 0):
    from dolfin import (set_log_level, WARNING)
    set_log_level(WARNING)
else:
    from dolfin import (set_log_level, LogLevel)
    set_log_level(LogLevel.ERROR)

np.random.seed(8011990)

problem_type = "upscaling"

d = 4
var = 1e-6
radi = [2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20]
n_recon_sample = 500
mean = 1
mc_ref = 100
repeated_sampling = 1

create_mcmc_samples = False
create_table_files = False

experiment_path = "experiments/{}/d{}/m{}/var{}/".format(problem_type, d, mean, var)
if not os.path.exists(experiment_path):
    os.makedirs(experiment_path)

tensor_save_path = experiment_path + "tensors/"
if not os.path.exists(tensor_save_path):
    os.makedirs(tensor_save_path)
for run in range(repeated_sampling):
    if not os.path.exists(tensor_save_path + "run{}/".format(run)):
        os.makedirs(tensor_save_path + "run{}/".format(run))

p_prior_sample_path = experiment_path + "p_prior_samples/"
if not os.path.exists(p_prior_sample_path):
    os.makedirs(p_prior_sample_path)
p_prior_sample_path = experiment_path + "p_prior_samples/samples.dat"

prior_sample_path = experiment_path + "prior_samples/"
if not os.path.exists(prior_sample_path):
    os.makedirs(prior_sample_path)
prior_sample_path = experiment_path + "prior_samples/samples.dat"

posterior_sample_path = experiment_path + "posterior_samples/"
if not os.path.exists(posterior_sample_path):
    os.makedirs(posterior_sample_path)
posterior_sample_path = experiment_path + "posterior_samples/samples.dat"

config_file_path = "{}_conf.json".format(problem_type)
result_path = experiment_path + "radi_convergence.dat"
result_table_path = experiment_path + "radi_convergence_result.dat"

if create_table_files:

    assert os.path.exists(result_path)
    with open(result_path, "r") as f:
        info = json.load(f)
    mean_true = np.array(info[0]["mean_true"])
    cov_true = np.array(info[0]["cov_true"])
    z_true = np.array(info[0]["z_true"])

    if False:       # Something with MCMC samples for reference...
        assert os.path.exists(posterior_sample_path.replace(".dat", ".json"))
        with open(posterior_sample_path.replace(".dat", ".json"), "r") as f:
            mc_info = json.load(f)

        with open(posterior_sample_path.replace(".dat", ".txt"), "w") as out:
            out.write("sample,"
                      "z_err_l2,"
                      "z_err_l2_rel,"
                      "mean_err_l2,"
                      "mean_err_l2_rel,"
                      "cov_err_l2,"
                      "cov_err_l2_rel"
                      "\n")
            for lia in range(len(mc_info["samples"])):
                z = np.array(mc_info["z"][lia])
                mean = np.array(mc_info["mean"][lia])
                cov = np.array(mc_info["cov"][lia])
                z_err = np.linalg.norm(z_true - z)
                mean_err = np.linalg.norm(mean_true - mean)
                cov_err = np.linalg.norm(cov_true - cov)
                out.write("{},{},{},{},{},{},{}"
                          "\n".format(mc_info["samples"][lia], z_err, z_err/np.linalg.norm(z_true),
                                      mean_err, mean_err/np.linalg.norm(mean_true),
                                      cov_err, cov_err/np.linalg.norm(cov_true)))
    info_plot = []
    
    with open(result_table_path, "w") as out:
        out.write("r,"
                  "z_err_l2,"
                  "z_err_l2_rel,"
                  "mean_err_l2,"
                  "mean_err_l2_rel,"
                  "cov_err_l2,"
                  "cov_err_l2_rel,"
                  "kl,"
                  "hell,"
                  "calls"
                  "\n")
        old_r = info[0]["num_radi"]
        counter = 0
        mean_approx = []
        cov_approx = []
        z_approx = []
        # z_tail = 0
        kl_dist = []
        kl_dist_counter = 0
        hell_dist = []
        hell_dist_counter = 0
        for lia, inf in enumerate(info):
            r = np.array(inf["num_radi"])
            if r == old_r and lia < len(info)-1:
                mean_approx.append(inf["mean_approx"])
                cov_approx.append(inf["cov_approx"])
                z_approx.append(inf["z_tt"])
                # z_tail = np.array(inf["z_tail"])
                if np.array(inf["kl_dist"]) > 0 or True:
                    kl_dist.append(np.abs(inf["kl_dist"]))
                    kl_dist_counter += 1
                if np.array(inf["hell_dist"]) > 0 or True:
                    hell_dist.append(np.abs(inf["hell_dist"]))
                    hell_dist_counter += 1

                counter += 1
            else:
                calls = 100 * (old_r - 1)  # np.array(inf["calls"])
                if kl_dist_counter <= 0:
                    print("WARNING: no valid KL distance found")
                if hell_dist_counter <= 0:
                    print("WARNING: no valid Hellinger distance found")
                old_r = r
                
                mean_approx = np.array(mean_approx)
                cov_approx = np.array(cov_approx)
                z_approx = np.array(z_approx)
                kl_dist = np.array(kl_dist)
                hell_dist = np.array(hell_dist)

                info_plot.append({
                    "mean" : mean_approx,
                    "cov"  : cov_approx,
                    "z"    : z_approx,
                    "kl"   : kl_dist,
                    "hell" : hell_dist,
                    "calls": calls
                })

                curr_mean_err = np.linalg.norm(mean_true - mean_approx/counter)
                curr_cov_err = np.linalg.norm(cov_true - cov_approx/counter)
                curr_z_err = np.linalg.norm(z_true - z_approx/counter)
                kl_dist_err = np.linalg.norm(kl_dist/kl_dist_counter)
                hell_dist_err = np.linalg.norm(hell_dist/hell_dist_counter)
                
                out.write("{},{},{},{},{},{},{},{},{},{}"
                          "\n".format(r-1, curr_z_err, curr_z_err / np.linalg.norm(z_true),
                                      curr_mean_err, curr_mean_err / np.linalg.norm(mean_true),
                                      curr_cov_err, curr_cov_err / np.linalg.norm(cov_true),
                                      kl_dist_err, hell_dist_err, calls))

                counter = 0
                mean_approx = []
                cov_approx = []
                z_approx = []
                # z_tail = 0
                kl_dist = []
                kl_dist_counter = 0
                hell_dist = []
                hell_dist_counter = 0
                mean_approx.append(inf["mean_approx"])
                cov_approx.append(inf["cov_approx"])
                z_approx.append(inf["z_tt"])
                # z_tail = np.array(inf["z_tail"])
                if np.array(inf["kl_dist"]) > 0 or True:
                    kl_dist.append(np.abs(inf["kl_dist"]))
                    kl_dist_counter += 1
                if np.array(inf["kl_dist"]) > 0 or True:
                    hell_dist.append(np.abs(inf["hell_dist"]))
                    hell_dist_counter += 1

    import matplotlib as mpl
    import matplotlib.pyplot as plt

    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

    perc_up = 99
    perc_down = 1
    plot_cov = np.zeros(len(radi))
    plot_cov_perc_down = np.zeros(len(radi))
    plot_cov_perc_up = np.zeros(len(radi))
    plot_cov_all = np.zeros((50, len(radi)))

    plot_kl = np.zeros(len(radi))
    plot_kl_perc_down = np.zeros(len(radi))
    plot_kl_perc_up = np.zeros(len(radi))
    plot_kl_all = np.zeros((50, len(radi)))

    plot_hell = np.zeros(len(radi))
    plot_hell_perc_down = np.zeros(len(radi))
    plot_hell_perc_up = np.zeros(len(radi))
    plot_hell_all = np.zeros((50, len(radi)))
    x = np.zeros(len(radi))
    for lia, loc_inf in enumerate(info_plot):
        plot_cov_all[:loc_inf["cov"].shape[0], lia] = np.linalg.norm(cov_true - loc_inf["cov"], "fro", axis=(1,2))/np.linalg.norm(cov_true, 'fro')
        plot_cov[lia] = np.mean(np.linalg.norm(cov_true - loc_inf["cov"], "fro", axis=(1,2))/np.linalg.norm(cov_true, 'fro'))
        plot_cov_perc_down[lia] = np.nanpercentile(np.linalg.norm(cov_true - loc_inf["cov"], axis=(1,2))/np.linalg.norm(cov_true, 'fro'), perc_down)
        plot_cov_perc_up[lia] = np.nanpercentile(
            np.linalg.norm(cov_true - loc_inf["cov"], axis=(1, 2)) / np.linalg.norm(cov_true, 'fro'), perc_up)

        plot_kl_all[:loc_inf["kl"].shape[0], lia] = loc_inf["kl"]
        plot_kl[lia] = np.nanmean(loc_inf["kl"])
        plot_kl_perc_down[lia] = np.nanpercentile(loc_inf["kl"], perc_down)
        plot_kl_perc_up[lia] = np.nanpercentile(loc_inf["kl"], perc_up)

        plot_hell_all[:loc_inf["hell"].shape[0], lia] = loc_inf["hell"]
        plot_hell[lia] = np.nanmean(loc_inf["hell"])
        plot_hell_perc_down[lia] = np.nanpercentile(loc_inf["hell"], perc_down)
        plot_hell_perc_up[lia] = np.nanpercentile(loc_inf["hell"], perc_up)

        
        x[lia] = loc_inf["calls"]
    # print(x)
    # plt.fill_between(x, plot_mean - 0.434*plot_mean_std/plot_mean, plot_mean + 0.434*plot_mean_std/plot_mean, alpha=1, color="blue")

    # plt.loglog(x, plot_cov, '-x', c="blue")
    # plt.fill_between(x, plot_cov_perc_down, plot_cov_perc_up, alpha=0.6, color="blue")
    def plot_quantiles(nodes, values, qrange=(0, 1), ax=None, num_quantiles=4, linewidth_fan=0, **kwargs):
        """
        Plot the quantiles for a stochastic process.

        Parameters
        ----------
        nodes : ndarray (shape: (n,))
            Nodes at which the process is measured (index set, plotted on the x-axis).
        values : ndarray (shape: (m,n))
            Realizations of the the process (plotted on the y-axis).
            Each row of values contains a different path of the stochastic process.
        qrange : float (2,)
            Quantile range to use. (Outer bounds are currently excluded.)
        ax : matplotlib.axes.Axes, optional
            The axis object used for plotting. (default: matplotlib.pyplot.gca())
        num_quantiles : int (>= 0), optional
            Number of quantiles to plot. (default: 4)
        linewidth_fan : float, optional
            Linewidth of the bounding lines of each quantile. (default: 0)
        """
        errors = values
        values = nodes
        assert values.ndim == 1
        assert len(values) == errors.shape[1]
        assert num_quantiles >= 0
        if ax is None:
            ax = plt.gca()

        if num_quantiles == 0:
            ps = np.full((1,), 0.5)
            alphas = np.full((1,), kwargs.get('alpha', 1))
        else:
            ps = np.linspace(0, 1, 2 * (num_quantiles + 1) + 1)[1:-1]
            alphas = np.empty(num_quantiles)
            alphas[0] = 2 * ps[0]
            for i in range(1, num_quantiles):
                alphas[i] = 2 * (ps[i] - ps[i - 1]) / (1 - 2 * ps[i - 1])
            alphas *= kwargs.get('alpha', 1)
        print(ps)
        exit()
        qs = np.nanquantile(errors, ps, axis=0)
        zorder = kwargs.pop('zorder', 1)
        base_line, = ax.loglog(values, qs[num_quantiles], zorder=zorder + num_quantiles, **kwargs)
        ls = [base_line]
        for e in range(num_quantiles):
            l = ax.fill_between(values, qs[e], qs[-1 - e], color=base_line.get_color(), alpha=alphas[e],
                                zorder=zorder + e, linewidth=linewidth_fan)
            ls.append(l)
        return ls, alphas


    figshape = (1, 2)
    # figsize = compute_figsize(figshape, 2)
    # print(figsize)
    fig, ax = plt.subplots(*figshape, figsize=(8, 4), dpi=300)

    fontsize = 10
    plot_quantiles(x, plot_hell_all, ax=ax[0], num_quantiles=10, color="red", label="$\mathrm{d}_{\mathrm{Hell}}$")
    # plt.loglog(x, plot_kl, '-x', c="green")
    # plt.fill_between(x, plot_kl_perc_down, plot_kl_perc_up, alpha=0.6, color="green")
    plot_quantiles(x, plot_kl_all, ax=ax[0], num_quantiles=10, color="blue", label=r"$\mathrm{d}_{\mathrm{KL}}$")
    ax[0].set_xlabel("number of posterior calls", fontsize=fontsize)
    ax[0].set_ylabel("error", fontsize=fontsize)
    ax[0].legend()

    plot_quantiles(x, plot_cov_all, ax=ax[1], num_quantiles=10, color="red", label=r"$\mathrm{err}_{\Sigma}$")
    # plt.loglog(x, plot_kl, '-x', c="green")
    # plt.fill_between(x, plot_kl_perc_down, plot_kl_perc_up, alpha=0.6, color="green")
    # plot_quantiles(x, plot_kl_all, ax=ax, num_quantiles=10, color="blue")
    ax[1].set_xlabel("number of posterior calls", fontsize=fontsize)
    # plt.ylabel("error")
    plt.legend()
    fig.savefig("ne_rad_sigma_kl_hell.png", dpi=300)
    import matplotlib2tikz
    matplotlib2tikz.save("iamge.tex")
    exit()



info = []
config = Config(config_file_path)

if config_file_path == "gaussian_conf.json":
    config["dim"] = d
    config["problem"]["mean"] = np.ones(d) * mean
    config["problem"]["var"] = np.eye(d, d) * var
    config["recon"]["n_samples"] = n_recon_sample
    # config["sample_path"] = "gauss_sample_d{}_m{}_v{}_r{}.dat".format(d, var, mean, len(config["recon"]["radi"]))
    # config["sample_path_tt"] = "gauss_sample_d{}_m{}_v{}_r{}_tt.dat".format(d, var,
    #                                                                         mean, len(config["recon"]["radi"]))
    config["recon"]["force_recalc"] = False
    # config degs can not be defined here since they depend on the radi
    r_poly_deg = 7
    theta_poly_deg = 1

elif config_file_path == "poisson_conf.json":
    config["dim"] = d
    config["problem"]["M"] = d
    config["problem"]["n_points"] = 144
    config["problem"]["noise"] = var
    r_poly_deg = 7
    theta_poly_deg = 5
    config["optimizer"]["use_difftool"] = True
    config["optimizer"]["init_value"] = np.zeros(d)
    config["recon"]["n_samples"] = n_recon_sample
    config["recon"]["adf_iteration"] = 1000
    config["sample_path"] = "poisson_sample_d{}.dat".format(d)
    config["sample_path_tt"] = "poisson_sample_d{}_tt.dat".format(d, len(config["recon"]["radi"]))
    config["sample_path_prior"] = "numerical_examples_radi/poisson_sample_prior_d{}.dat".format(d)
elif config_file_path == "upscaling_conf.json":
    r_poly_deg = 7
    theta_poly_deg = 5
    config["dim"] = 4
    config["optimizer"]["use_difftool"] = False
    config["optimizer"]["approximate"] = True
    config["optimizer"]["log_optimizer"] = True
    config["optimizer"]["init_value"] = np.zeros(4)
    config["recon"]["n_samples"] = n_recon_sample
    config["recon"]["adf_iteration"] = 1000
    config["sample_path"] = "upscaling_sample.dat"
    config["sample_path_tt"] = "upscaling_sample_tt.dat".format(len(config["recon"]["radi"]))
    config["sample_path_prior"] = "numerical_examples_radi/upscaling_sample_prior.dat"

else:
    raise NotImplementedError()
    # #######

if not config.load():
    raise ValueError("something went wrong in the configuration")
problem = config["model"]

if problem_type == "poisson":
    config["optimizer"]["init_value"] = problem.true_para
elif problem_type == "upscaling":
    config["optimizer"]["init_value"] = np.array([0.52, -0.0146, -0.0146, 0.66])

print("#" * 20)
print("compute MAP")
trafo = problem.compute_map(**config["optimizer"])
print("#" * 20)
print("log: {}, difftool: {}".format(config["optimizer"]["log_optimizer"], config["optimizer"]["use_difftool"]))
print("  MAP = {}".format(trafo.map))
print("  hessian: \n{}".format(trafo.hess))
optimizer_calls = config["model"].post.num_calls
last_calls = optimizer_calls
if create_mcmc_samples:
    print("create {}*walker + 1000 burn in MCMC samples for error sampling later from true prior".format(mc_ref))

    trafo.create_trafo_samples(mc_ref, prior_sample_path, problem.prior)
    with open(prior_sample_path, "r") as f:
        prior_inf = json.load(f)
    prior_sampler = MCMC_Sampler(log_prob=None,
                                 N=None,
                                 burn_in=None,
                                 ndim=None,
                                 nwalkers=None,
                                 p0=None,
                                 samples=prior_inf
                                 )

    print("create {}*walker + 1000 burn in MCMC samples for error sampling later from true posterior".format(mc_ref))

    trafo.create_trafo_samples(mc_ref, posterior_sample_path, problem.post)
    with open(posterior_sample_path, "r") as f:
        inf = json.load(f)
    sampler = MCMC_Sampler(log_prob=None,
                           N=None,
                           burn_in=None,
                           ndim=None,
                           nwalkers=None,
                           p0=None,
                           samples=inf
                           )

    assert len(sampler) == len(prior_sampler)
    splits = np.linspace(10, len(sampler), 100)
    sample_list = []
    Z_list = []
    mean_list = []
    cov_list = []
    bar = PercentageBar(len(splits))
    print("Compute moments")
    for split in splits:
        curr_n = int(split)
        samples = sampler.get(curr_n)
        prior_samples = np.array(prior_sampler.get(curr_n))
        # print("Compute moments with {} samples".format(len(samples)))
        sample_list.append(curr_n)
        mean_list.append(np.mean(samples, axis=0).tolist())
        cov_list.append(np.cov(np.array(samples).T, bias=False).tolist())
        # This does nothing. 1mio samples will never give you anything in the concentration region of the liklihood
        # curr_z = (1/curr_n) * np.sum([np.exp(-problem.Phi(s)) for s in prior_samples], axis=0)
        curr_z = 1
        Z_list.append(curr_z)
        bar.next()

        curr_inf = {"mean"   : mean_list,
                    "cov"    : cov_list,
                    "z"      : Z_list,
                    "samples": sample_list}
    with open(posterior_sample_path.replace(".dat", ".json"), "w") as f:
        json.dump(curr_inf, f, indent=4)
    exit()

# load reference
if config_file_path == "gaussian_conf.json":
    mean_true = np.ones(d)*mean
    cov_true = np.eye(d, d)*var
    Z_true = 1
elif config_file_path == "poisson_conf.json":
    if False:
        if False:
            if not os.path.exists(config["sample_path_prior"]):
                print("create MCMC samples for error sampling later from true prior")

                def prior(x):
                    return config["model"].prior(x)

                trafo.create_trafo_samples(mc_ref, config["sample_path_prior"], prior)

            print("create MCMC samples")
            with open(config["sample_path_prior"], "r") as f:
                inf = json.load(f)
            sampler = MCMC_Sampler(log_prob=None,
                                   N=None,
                                   burn_in=None,
                                   ndim=None,
                                   nwalkers=None,
                                   p0=None,
                                   samples=inf
                                   )

            print("compute Z approx")
            Z_true = (1 / mc_ref) * np.sum([np.exp(-config["model"].Phi(s)) for s in sampler.get(mc_ref)], axis=0)

            print("compute mean approx")
            # mean_true = np.ones(d)
            mean_true = (1 / Z_true) * (1 / mc_ref) * np.sum(
                    [np.array(s) * np.exp(-config["model"].Phi(s)) for s in sampler.get(mc_ref)], axis=0)
            print("compute covariance approx")
            cov_true = (1 / Z_true) * (1 / mc_ref) * np.sum(
                    [np.outer(np.array(s) - mean_true, np.array(s) - mean_true) * np.exp(-config["model"].Phi(s)) for s
                     in
                     sampler.get(mc_ref)], axis=0)
        else:
            posterior_sample_path = "poisson_reference_samples_2.dat"

            with open(posterior_sample_path, "r") as f:
                inf = json.load(f)
            sampler = MCMC_Sampler(log_prob=None,
                                   N=None,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  burn_in=None,
                                   ndim=None,
                                   nwalkers=None,
                                   p0=None,
                                   samples=inf
                                   )

            Z_true = 1
            samples = sampler.get(mc_ref)
            samples = np.array(samples)
            print(samples.shape)
            mean_true = np.mean(samples, axis=0)
            cov_true = np.cov(samples.T, bias=True)
        inf = {"Z": Z_true, "mean": mean_true.tolist(), "cov": cov_true.tolist()}
        with open("poisson_d{}_truth.dat".format(d), "w") as f:
            json.dump(inf, f, indent=4)
        print(Z_true)
        print(mean_true)
        print(cov_true)
        exit()
    elif False:
        from scipy.integrate import dblquad

        # Z_true, Z_err = dblquad(lambda x, y: config["model"].post([x, y]), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)
        # print(Z_true, Z_err)
        print("compute_mean_true")
        mean_true = np.ones(d)
        mean_true[0], mean_err = dblquad(lambda x, y: x*config["model"].post([x, y]), -np.inf, np.inf, lambda x: -np.inf, lambda x: np.inf)
        mean_true[1], mean_err = dblquad(lambda x, y: y * config["model"].post([x, y]), -np.inf,
                                         np.inf, lambda x: -np.inf, lambda x: np.inf)

        print(mean_true, mean_err)
        cov_true = np.ones((d, d))
        cov_true[0, 0], cov_err = dblquad(lambda x, y: np.outer(np.array([x, y])-mean_true,np.array([x, y])-mean_true)[0, 0] * config["model"].post([x, y]), -np.inf, np.inf,
                                      lambda x: -np.inf, lambda x: np.inf)

        print(cov_true, cov_err)
        cov_true[0, 1], cov_err = dblquad(
            lambda x, y: np.outer(np.array([x, y]) - mean_true, np.array([x, y]) - mean_true)[0, 1] * config[
                "model"].post([x, y]), -np.inf, np.inf,
            lambda x: -np.inf, lambda x: np.inf)

        print(cov_true, cov_err)
        cov_true[1, 1], cov_err = dblquad(
            lambda x, y: np.outer(np.array([x, y]) - mean_true, np.array([x, y]) - mean_true)[1, 1] * config[
                "model"].post([x, y]), -np.inf, np.inf,
            lambda x: -np.inf, lambda x: np.inf)
        cov_true[1, 0] = cov_true[0, 1]
        print(cov_true, cov_err)
        exit()
    else:
        config["recon"]["radi"] = np.linspace(0, 10, num=30)
        config["recon"]["n_samples"] = 1000
        config["recon"]["maxdegs"] = [[r_poly_deg] * len(config["recon"]["radi"])] + [theta_poly_deg] * (d - 1)
        config["recon"]["save_path"] = tensor_save_path + "r{}_mc{}_pr{}_pt{}_tt.dat".format(30, 1000,
                                                                                             r_poly_deg,
                                                                                             theta_poly_deg)
        config["recon"]["force_recalc"] = False
        ten_list = trafo.reconstruct_tt_uq(**config["recon"])
        print("Tensor reconstruction result")
        print(" Dimensions: {}".format([t.n for t in ten_list]))
        print(" Ranks: {}".format([t.r for t in ten_list]))

        rad_tt = RadialTT(ten_list, trafo)

        print("Compute Z with TT")
        Z_list, Z_tt = trafo.compute_Z()
        rad_tt.set_normalisation(Z_tt)

        Z_true = Z_tt
        mean_true = rad_tt.mean()
        cov_true = rad_tt.covariance()

        config["recon"]["n_samples"] = n_recon_sample
elif config_file_path == "upscaling_conf.json":
    mean_true = np.ones(d)*mean
    cov_true = np.eye(d, d)*var
    Z_true = 1
else:
    raise NotImplementedError()


for r in radi:
    for run in range(repeated_sampling):
        last_calls = problem.post.num_calls - last_calls
        curr_path = tensor_save_path + "run{}/r{}_mc{}.dat".format(run, r, n_recon_sample)
        if config_file_path == "gaussian_conf.json":

            config["recon"]["radi"] = np.linspace(0, 10, num=r)
            config["recon"]["maxdegs"] = [[r_poly_deg] * len(config["recon"]["radi"])] + [theta_poly_deg] * (d - 1)

        elif config_file_path == "poisson_conf.json":
            config["recon"]["radi"] = np.linspace(0, 10, num=r)
            config["recon"]["maxdegs"] = [[r_poly_deg] * len(config["recon"]["radi"])] + [theta_poly_deg] * (d - 1)
        elif config_file_path == "upscaling_conf.json":
            config["recon"]["radi"] = np.linspace(0, 10, num=r)
            config["recon"]["maxdegs"] = [[r_poly_deg] * len(config["recon"]["radi"])] + [theta_poly_deg] * (d - 1)
        else:
            raise NotImplementedError()
        if os.path.exists(curr_path) and False:
            with open(curr_path, "r") as f:
                inf = json.load(f)
            info.append(inf)
            continue
        start = time.time()
        # #####
        print("Start reconstruction")
        config["recon"]["save_path"] = tensor_save_path + "run{}/r{}_mc{}_pr{}_pt{}_tt.dat".format(run, r,
                                                                                                   n_recon_sample,
                                                                                                   r_poly_deg,
                                                                                                   theta_poly_deg)
        config["recon"]["rerun_on_fail"] = 20
        ten_list = trafo.reconstruct_tt_uq(**config["recon"])
        print("Tensor reconstruction result")
        print(" Dimensions: {}".format([t.n for t in ten_list]))
        print(" Ranks: {}".format([t.r for t in ten_list]))

        rad_tt = RadialTT(ten_list, trafo)

        print("Compute Z with TT")
        Z_list, Z_tt = trafo.compute_Z()
        rad_tt.set_normalisation(Z_tt)
        duration = time.time() - start

        calls = problem.post.num_calls - last_calls
        if problem_type == "gaussian":
            samples = np.random.multivariate_normal(mean_true, cov_true, mc_ref)
            with open(p_prior_sample_path, "w") as f:
                json.dump(samples.tolist(), f)

        else:
            if not os.path.exists(p_prior_sample_path):
                print("create MCMC samples for error sampling later from true perturbed prior")

                def perturbed_prior(x):
                    """
                    \tilde f_0 = f \circ \tilde T \|det J_T\|
                    where \tilde T is the approximated transport, here in the affine case
                    \tilde T = H^\frac{1}{2} \cdot + M
                    :param x: sample
                    :return: transported sample
                    """
                    f = problem.post
                    tilde_T = trafo.transport_trafo
                    return f(tilde_T(x)) * trafo.det_hessian()


                trafo.create_trafo_samples(mc_ref, p_prior_sample_path, perturbed_prior)
        kl_dist = rad_tt.kl_distance(mc_ref, p_prior_sample_path, Z_true, Z_tt)
        hell_dist = rad_tt.hell_distance(mc_ref, p_prior_sample_path)
        print("Z err    : {}".format(np.abs(Z_true - Z_tt)/Z_true))
        print("mean err : {}".format(np.linalg.norm(mean_true - (rad_tt.mean())) / np.linalg.norm(mean_true)))
        print("KL dist  : {}".format(kl_dist))
        print("Hell dist: {}".format(hell_dist))
        inf ={
            "num_radi": r,
            "duration": duration,
            "calls": (r-1)*n_recon_sample,
            "z_true": Z_true,
            "z_tt": Z_tt,
            "z_tail": rad_tt.Z_tail,
            "mean_true": mean_true.tolist(),
            "cov_true": cov_true.tolist(),
            "mean_approx": (rad_tt.mean()).tolist(),
            "cov_approx": (rad_tt.covariance()).tolist(),
            "dim": d,
            "var": var,
            "mean": mean,
            "n_recon_samples": n_recon_sample,
            "mc_sample": mc_ref,
            "optimizer_calls": optimizer_calls,
            "dofs": rad_tt.dofs(),
            "average_r": rad_tt.average_r(),
            "kl_dist": kl_dist,
            "hell_dist": hell_dist,
            "radial_tt_save_path": config["recon"]["save_path"],
            "run": run
        }
        with open(curr_path, "w") as f:
            json.dump(inf, f, indent=4)
        info.append(inf)
with open(result_path, "w") as f:
    json.dump(info, f, indent=4)
