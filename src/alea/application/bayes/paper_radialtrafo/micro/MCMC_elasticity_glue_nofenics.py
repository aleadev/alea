from __future__ import division, print_function, absolute_import
from randomMatrices import symClassExponential

# imports for displaying the mesh
#from dolfin import *
import matplotlib.pyplot as plt

# extras
import numpy as np
import random as rd
import os

global C_generator

import emcee

import matplotlib.pyplot as plt

import math

from scipy import special

from alea.application.bayes.paper_radialtrafo.trafo_util import compute_map, RadialTrafo, plot_marginals_hatfun, plot_tt_trafo
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
import numpy as np
from dolfin import *
from functools import partial

#%matplotlib notebook
import cPickle as pickle

def save_obj(foldname, obj, name ):
    with open(foldname + name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)



def load_obj(foldname, name ):
    with open(foldname + name + '_2.pkl', 'rb') as f:
        return pickle.load(f)


global trafos,delta

width = 1.
height = width

vol = height*width

mean_theta = math.asin(1. / np.sqrt(2))

# Now we want to build an topology object from a json file
# and build the .geo file
# Load topology with refinement, refinement will go from 50 to 20

global folder_name, topology_fname, file_name, data_name, mesh, subdomains_filled


global uniform_theta

build_mesh = False
plot_mesh = False

def set_data(fold_name,top_name, fname, d_name):
    global folder_name, topology_fname, file_name, data_name, file_name_filled, mesh, subdomains_filled

    folder_name = fold_name
    topology_fname = top_name
    file_name = fname
    data_name = d_name

    file_name_filled = folder_name+file_name + "filled"




class Radial_TT():
    def __init__(self, TTlist, rad_list):
        self._TTlist = TTlist,
        self._rad_list = rad_list

        self._r0 = self._rad_list[0]
        assert(self._r0 == 0.)

        self._h = self._rad_list[1] - self._rad_list[0]

        for i in range(1, len(self._rad_list)-1):
            assert( self._h == self._rad_list[i+1] - self._rad_list[i])

        assert(len(TTlist) +1 == len(rad_list))

    def __call__(self, pc):

        r = pc[0]
        i = int(( r-self._r0 )/self._h)
        return self._TTlist[i](pc)




def in_support(_x,                                   # type: float
               a,                                   # type: float
               b,                                   # type: float
               _i,                                   # type: int
               dim                                  # type: int
               ):
    """
    Check if x is in  supp phi_i, with phi_i \in V([a,b]), dim V = dim,
    here phi_i is a hat function.

    :param _x: the eval point
    :param a:  lhs
    :param b:  rhs
    :param _i:  number of basis function
    :param dim: total number of basisfunctions
    :return:
    """

    if not a < _x < b:
        return False

    if False:   # CG
        h = (b - a) / (dim - 1)
        if _i == 0:
            return (a <= _x) and (_x <= a + h)
        elif _i == dim:
            return (b-h <= _x) and (_x <= b)
        else:
            pos = int((_x-a)/h)
            return (_i-1 <= pos) and (pos <= _i+1)
    else:       # DG
        h = (b - a) / (dim)
        pos = int((_x - a) / h)
        return i == pos



def solve_macro_problem(y_meso):
    C = C_generator.realisation(y_meso, trafo=trafos)  # .tolist()
    # C = as_matrix(C)

    macro_strains = [np.array([[1, 0], [0, 0]]),
                     np.array([[0, 0], [0, 1]]),
                     np.array([[0, 1], [1, 0]]),
                     np.array([[1, 1], [1, 0]]),
                     np.array([[0, 1], [1, 1]]),
                     np.array([[1, 0], [0, 1]])]

    def solve_macro(C, macrostrain):
        ms_V = np.array(([macrostrain[0, 0], macrostrain[1, 1], 2 * macrostrain[0, 1]]))
        res = ms_V.transpose().dot(C.dot(ms_V))
        return res

    macro_results = [solve_macro(C, macro_strain) for macro_strain in macro_strains]
    return np.array(macro_results)

def lnlike(y_meso, delta, observered_variance = 1.0, M=1):
    diff = delta - np.tile(solve_macro_problem(y_meso),M)
    #print("current fit = {v}  / rel fit = {w}".format(v=np.linalg.norm(diff),w=np.linalg.norm(diff)/np.linalg.norm(delta)))
    res = -0.5*np.dot(diff, observered_variance**(-1) * diff)
    print("lnlike res = {v}".format(v=res))
    return res

def lnprior(y_meso):
    if len(y_meso) == 4:
        #if y_meso[3] < 0. or y_meso[3] > 0.5*np.pi:

        if uniform_theta:
            if y_meso[3]  < mean_theta - 0.25*np.pi or y_meso[3] > mean_theta + 0.25*np.pi:
                return -np.inf
            return -0.5 * np.dot(y_meso[:2], y_meso[:2])
        else:
            #normal prior everywhere
            return -0.5 * np.dot(y_meso[:3], y_meso[:3])
    elif len(y_meso) == 6:
        return -0.5 * np.dot(y_meso, y_meso)
    else:
        raise NotImplementedError("len = 4 of input only supported")
def lnprob(y_meso, delta, observered_variance = 1, M=1):
    lp = lnprior(y_meso)
    result = 0
    if not np.isfinite(lp):
        result =  -np.inf
    else:
        #print(lnlike(y_meso, delta, observered_variance,M))
        #print(lp)
        result = lp + lnlike(y_meso, delta, observered_variance,M)
    return result



def compute_multi_micro_data(M=5, verbose = False):

    data_name = "micro_data"
    global folder_name, folder_name_img
    folder_name = "data/multi_materials/0.03/"
    folder_name_img = folder_name + "img/"

    macro_strains = [np.array([[1, 0], [0, 0]]),
                     np.array([[0, 0], [0, 1]]),
                     np.array([[0, 1], [1, 0]]),
                     np.array([[1, 1], [1, 0]]),
                     np.array([[0, 1], [1, 1]]),
                     np.array([[1, 0], [0, 1]])]

    data = None

    if os.path.exists(folder_name + data_name + '_2.pkl'):
        data = load_obj(folder_name, data_name)

        #pickle.dump(data, open(folder_name+data_name+"_2.pkl", "wb"), protocol=2)

    else:
        raise AssertionError("data file missing")
        data = {}

    return data

def main():

    global uniform_theta
    uniform_theta = False
    # uniform prior section

    a, b = mean_theta - 1./8*np.pi, mean_theta + 1./8*np.pi


    M = 2
    multi = True

    for M in [1]:

        global trafos
        if uniform_theta:
            trafos = [lambda x: 40000 * np.exp(x),
                      lambda x: 45000 * np.exp(x), #45000
                      lambda x: 45000 * np.exp(x),
                      lambda x: x]
        else:
            trafos = [lambda x: 40000 * np.exp(x),
                      lambda x: 45000 * np.exp(x),  # 45000
                      lambda x: 45000 * np.exp(x),
                      lambda x: a + (b - a) * special.erfc(x / math.sqrt(2.))]  # lambda x: x] for uniform take the latter


        #trafos = [lambda x: 5+x for _ in range(6)]

        micro_data = None

        if multi == True:
            verbose = False
            micro_data = compute_multi_micro_data(5, verbose)

            if verbose == True :
                for m in range(M):
                    print(micro_data[m])
                    print(type(micro_data[m]))

            micro_data_all = []
            for m in range(M):
                micro_data_all += micro_data[m]
            micro_data = np.array(micro_data_all)
        else:
            raise AssertionError("multi must be true here")


        #exit()
        delta = micro_data  # + observered_variance * np.random.randn(6)

        symclass = "orthotropic"

        global C_generator
        C_generator = symClassExponential(dim=2, symClass=symclass) #orthotropic

        ndim = 4
        observered_variance = 5000  # ** (-2)


        #print(delta)
        #y = np.array([0.52171164, -0.01359939, -0.0145173,   0.66620557])
        #lnprob(y,delta,observered_variance,M)

        #exit()

        # ndim = 4 #3 * sDx**2
        nwalkers = 50
        pos = [1e-2 * np.random.randn(ndim) for _ in range(nwalkers)]  # or burn in
        sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob,
                                        args=(delta, observered_variance, M), threads=8)

        sampler.run_mcmc(pos, 2000)



        fig = plt.figure(1, figsize=(12, 12))
        plt.title("MCMC walker history")
        titles = None
        if symclass == "anisotropic":
            titles = [r"$y_1$",r"$y_2$",r"$y_3$",r"$y_4$",r"$y_5$",r"$y_6$"]
        elif symclass == "orthotropic":
            titles = [r"$\lambda_{I}$", r"$\lambda_{II}$", r"$\lambda_{III}$", r"$\theta$"]

        for i in range(ndim):
            splt = plt.subplot2grid((ndim, 1), (i, 0))
            for w in range(nwalkers):
                splt.plot(trafos[i](sampler.chain[w, :, i]), 'k')
            splt.set_title(titles[i])
        plt.tight_layout()
        if multi:
            fig.savefig(folder_name_img + "walker_" + str(M) + ".png")

        # /////////////////////////////////////////////

        data = sampler.chain[:, 1000:, :].reshape(-1, ndim)

        print(data.shape)

        #for i in range(ndim):
        #    data[:,i] = trafos[i](data[:,i])

        #data[:, 0] = 75000 * np.exp(data[:, 0])
        #data[:, 1:3] = 1 * np.exp(data[:, 1:3]) #45000


        fig = corner.corner(data, bins=100, labels=titles)
        #plt.title("Corner plot")
        plt.tight_layout()
        #plt.gca().ticklabel_format(style = 'sci', scilimits=(5,0))


        print("Mean acceptance fraction: {0:.3f}"
              .format(np.mean(sampler.acceptance_fraction)))

        # This is the true mean of the second mode that we used above:
        # value1 = theta_star

        # This is the empirical mean of the sample:
        value2 = np.mean(data, axis=0)

        # Extract the axes
        axes = np.array(fig.axes).reshape((ndim, ndim))

        # Loop over the diagonal
        for i in range(ndim):
            ax = axes[i, i]
            # ax.axvline(value1[i], color="g")
            if i == 3 and symclass == "orthotropic":
                ax.axvline(math.asin(1. / np.sqrt(2)), linestyle = '--', color='g')
            ax.axvline(value2[i], color="r", linestyle = '-.', alpha = 0.5)

        # Loop over the histograms
        for yi in range(ndim):
            for xi in range(yi):
                ax = axes[yi, xi]
                # ax.axvline(value1[xi], color="g")
                ax.axvline(value2[xi], color="r", alpha = 0.5)
                # ax.axhline(value1[yi], color="g")
                ax.axhline(value2[yi], color="r", alpha = 0.5)
                # ax.plot(value1[xi], value1[yi], "sg")
                ax.plot(value2[xi], value2[yi], "sr")

        #plt.legend()
        if multi:
            fig.savefig(folder_name_img + "corner_" + str(M) + ".png")

        #, trafo=trafos no trafos needed since data was mapped already
        C = sum(C_generator.realisation(data[k,:]) for k in range(data.shape[0]))* 1./data.shape[0]

        print(C)

        print(data.mean(axis=0))

        fig = plt.figure("Marginal w.r.t."+r"$\theta")
        plt.title("Marginal w.r.t."+r"$\theta")
        dat = data[:,3] if uniform_theta else trafos[3](data[:,3])

        plt.hist(dat, bins=50, density = True, color = '#56B4E9', label = r"$f_\theta")
        plt.gca().axvline(math.asin(1. / np.sqrt(2)), linestyle = '--', color='g')
        tol = 0.05
        if multi:
            fig.savefig(folder_name_img + "ftheta_" + str(M) + ".png")
        #plt.gca().set_xlim(mean_theta- tol*0.25*np.pi, mean_theta+tol*0.25*np.pi)



        fig = plt.figure("Deviation from isotropy")
        plt.title("Deviation from isotropy")

        d = 0.5*(np.abs(data[:,1]-data[:,2])/np.abs(data[:,2])+np.abs(data[:,1]-data[:,2])/np.abs(data[:,1]))\

        d2 = np.abs(math.asin(1./np.sqrt(2.))-data[:,3])/math.asin(1./np.sqrt(2.))

        colors = ['#E69F00', '#56B4E9']
        names  = [r"$rel_{\lambda}$", r"$rel_{\theta}$"]
        plt.hist([d,d2], bins = 50, density = True, color = colors, label = names)
        plt.legend()
        if multi:
            fig.savefig(folder_name_img + "deviation_" + str(M) + ".png")

        plt.draw()
        plt.pause(0.01)

    plt.show()


def prepare_posterior():
    global uniform_theta
    uniform_theta = False
    # uniform prior section

    a, b = mean_theta - 1. / 8 * np.pi, mean_theta + 1. / 8 * np.pi

    M = 2
    multi = True

    if True:

        global trafos
        if uniform_theta:
            trafos = [lambda x: 40000 * np.exp(x),
                      lambda x: 45000 * np.exp(x),  # 45000
                      lambda x: 45000 * np.exp(x),
                      lambda x: x]
        else:
            trafos = [lambda x: 40000 * np.exp(x),
                      lambda x: 45000 * np.exp(x),  # 45000
                      lambda x: 45000 * np.exp(x),
                      lambda x: a + (b - a) * special.erfc(
                          x / math.sqrt(2.))]  # lambda x: x] for uniform take the latter

        # trafos = [lambda x: 5+x for _ in range(6)]

        micro_data = None

        if multi == True:
            verbose = False
            micro_data = compute_multi_micro_data(5, verbose)

            if verbose == True:
                for m in range(M):
                    print(micro_data[m])
                    print(type(micro_data[m]))

            micro_data_all = []
            for m in range(M):
                micro_data_all += micro_data[m]
            micro_data = np.array(micro_data_all)
        else:
            raise AssertionError("multi must be true")

        # exit()
        global delta
        delta = micro_data  # + observered_variance * np.random.randn(6)

        #print(delta)

        symclass = "orthotropic"

        global C_generator
        C_generator = symClassExponential(dim=2, symClass=symclass)  # orthotropic

        ndim = 4
        observered_variance = 1  # ** (-2)


def unnormalized_posterior(y):
    assert len(y) == 4

    return np.exp(lnprob(y, delta, observered_variance=5000, M=2))


def test():
    prepare_posterior()

    MAP_apprx = [ 0.52213348, -0.01240733, -0.01502065,  0.66840127]

    y = MAP_apprx

    print(unnormalized_posterior(np.array(y)))


def tt_recon():

    prepare_posterior()

    MAP_apprx = [ 0.52213348, -0.01240733, -0.01502065,  0.66840127]

    def post(x):
        return unnormalized_posterior(x)

    M = 4
    max_r = 1
    min_z_volume = 1e-10

    n_circles = 10
    samples_per_circle = 100
    radi = np.linspace(0, max_r, num=n_circles + 1, endpoint=True)
    maxdegs = [[4] * (n_circles)] + [5] * (M - 1)
    MAP_info = compute_map(lambda x: -post(x), MAP_apprx)


    print("MAP = {} vs initial guess: {}".format(MAP_info["x"], MAP_apprx))
    print("hessian: \n{}".format(np.linalg.inv(MAP_info["hess_inv"])))

    trafo = RadialTrafo(M, MAP_info["x"], -post(MAP_info["x"])*np.linalg.inv(MAP_info["hess_inv"]))

    # plot_trafo_hessian(lambda x: post(x), trafo, num=20, progress=True)
    # plot_onb(trafo, maxdegs, radi)

    recon_dict = {
                  "radi": radi,                         # type: list
                  "maxdegs": maxdegs,                   # type: list
                  "func": post,                         # type: callable
                  "sample_list": None,                  # type: list or None
                  "n_samples": samples_per_circle,      # type: int or None
                  "_check_onb": True,                   # type: bool
                  "progress": True,                     # type: bool
                  "adf_iteration": 1000,                # type: int
                  "adf_targetresnorm": 1e-12,           # type: float
                  "adf_minresnormdecrease": 0.9999,     # type: float
                  "adf_verbose": True,                  # type: bool
                  "adf_maxrank": 10,                    # type: int
                  "adf_initrank": 4,                    # type: int
    }

    ten_list = trafo.reconstruct_tt(**recon_dict)
    print("Tensor reconstruction result")
    print(" Dimensions: {}".format([t.n for t in ten_list]))
    print(" Ranks: {}".format([t.r for t in ten_list]))

    # Z, err = dblquad(lambda x1, x2: post([x1, x2]), -1, 1, lambda x: -1, lambda x: 1)
    # print(err)
    # plot_tt_trafo(ten_list, radi, 20, post, trafo)
    print("Compute Z with TT")
    Z_list, Z_tt = trafo.compute_Z()
    for lia, Z in enumerate(Z_list):
        print("circular disc [{}, {}] Z_TT = {} / {}".format(radi[lia], radi[lia + 1], Z, Z_tt))

    lower_limit, upper_limit, max_radi = trafo.compute_bbox(min_z_volume)

    marginals = []
    samples_per_interval = 100
    max_trials = 500
    n_monomials = 5

    from alea.utils.progress.percentage import PercentageBar



    def monomial(x, power, index):
        return x[index]**power



    rad_tt = RadialTT(ten_list, radi)


    moments = np.zeros((n_monomials, M))
    det_hessian = trafo.det_hessian()

    to = 0
    print("lower limits: {}".format(lower_limit))
    print("upper limits: {}".format(upper_limit))
    if True:
    # while len(np.nonzero(moments[:, 1])) < n_monomials/3 and len(np.nonzero(moments[:, 0])) < n_monomials/3:
        moments = np.zeros((n_monomials, M))
        for lib in range(M):
            to += 1
            print("nonzeros in moments {}/{}".format(len(np.nonzero(moments[:, lib])), n_monomials))
            print("current limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
            # print("update interval")
            line = IntervalMesh(n_monomials, lower_limit[lib], upper_limit[lib])
            fs = FunctionSpace(line, 'DG', 0)
            init_hat = 0
            last_hat = n_monomials
            for lia in range(n_monomials):
                # print("  sample basis function {}/{}".format(lia + 1, n_monomials))
                def hat_fun(_x, index, _fs):
                    __f = Function(_fs)
                    __f.vector()[index] = 1
                    # print("eval f at {} in [{}, {}]".format(x[comp], lower_limit[comp], upper_limit[comp]))
                    return __f(_x)
                curr_fun = partial(hat_fun, index=lia, _fs=fs)
                val = 0
                bar = PercentageBar(samples_per_interval)
                for _ in range(samples_per_interval):
                    no_sample_found = False
                    for lid in range(max_trials):
                        curr_sample = [np.random.uniform(0, radi[max_radi])]
                        for lic in range(M - 1):
                            if lic == 0:
                                curr_sample.append(np.random.uniform(0, 2 * np.pi))
                            else:
                                curr_sample.append(np.random.uniform(0, np.pi))
                        cart_cord = trafo.compute_trafo(curr_sample)

                        #print("curr cartesian coordinates: {}".format(cart_cord))
                        if in_support(cart_cord[lib], lower_limit[lib], upper_limit[lib], lia, fs.dim()):
                            break
                        if lid == max_trials -1:
                            no_sample_found = True
                    if no_sample_found:
                        continue
                    print(" cord accepted: {}".format(cart_cord))
                    curr_update = curr_fun(cart_cord[lib])*rad_tt(curr_sample)*det_hessian *\
                           np.prod([tp(curr_sample[_lia]) for _lia, tp in enumerate(trafo.det_spherical_trafo())]) *\
                                  (upper_limit[lib] - lower_limit[lib])*2*np.pi*np.prod([np.pi for _ in range(M-2)]) * 1/Z_tt
                    if curr_update > 1e-8:
                        print("    cord has mass")
                        if init_hat < 2 and lia > 2:
                            init_hat = lia - 2
                        if lia < n_monomials - 3:
                            last_hat = lia + 2
                    val += curr_update
                    bar.next()
                moments[lia, lib] = (1 / samples_per_interval) * val
            # print(moments)
            if init_hat > 2 and last_hat < n_monomials - 3:
                h = (upper_limit[lib] - lower_limit[lib]) / (fs.dim() - 1)
                print("old limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
                old_l, old_r = lower_limit[lib], upper_limit[lib]
                lower_limit[lib] = old_l + h * init_hat
                upper_limit[lib] = old_l + h * last_hat
                print("new limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
        # plot_bbox(post, trafo, max_r, n_circles, lower_limit, upper_limit, dir="tmp/bbox_{}.png".format(to))
            # print(moments)
    print(moments)
    np.save("tmp/moments", moments)
    np.save("tmp/lower", np.array(lower_limit))
    np.save("tmp/upper", np.array(upper_limit))
    plot_marginals_hatfun(moments, lower_limit, upper_limit, MAP_apprx)


if __name__ == "__main__":
    # test()
    #main()
    tt_recon()