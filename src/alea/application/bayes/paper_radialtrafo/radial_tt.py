from __future__ import division, print_function, absolute_import
import numpy as np
from alea.application.bayes.paper_radialtrafo.moments import moment_generator, moment_indices
from alea.application.bayes.paper_radialtrafo.basis import PwConstant as Basis
from alea.utils.progress.percentage import PercentageBar
from alea.math_utils.basis import PickableBase
from alea.math_utils.tensor.extended_tt import ExtendedTT
from scipy.stats import multivariate_normal
from scipy.special import gamma, gammaincc
# DO not include this. Somehow we ended up with a cross ref. That is bad style
# from alea.application.bayes.paper_radialtrafo.trafo_util import RadialTrafo
from alea.application.bayes.paper_radialtrafo.distances import (KL, Hellinger, Wasserstein)
import h5py
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler
import json
import math
import matplotlib.pyplot as plt


class RadialTT:

    def __init__(self,
                 TTlist,                            # type: list
                 trafo                              # type: RadialTrafo
                 ):
        self.trafo = trafo
        self._TTlist = TTlist
        self._rad_list = trafo.radi
        self.equidistant_radi = True
        assert (len(TTlist) + 1 == len(trafo.radi))

        self._r0 = self._rad_list[0]
        assert (self._r0 == 0.)

        self._h = self._rad_list[1] - self._rad_list[0]

        self.dim = len(self._TTlist[0].n)
        for i in range(1, len(self._rad_list) - 1):
            if not np.abs(self._h - (self._rad_list[i + 1] - self._rad_list[i])) < 1e-10:
                self.equidistant_radi = False
                break
        self.moments = {}
        covariance = np.linalg.inv(-trafo.hess)
        mean = trafo.map

        def tail_approximation(_pc, _cart=None):
            if _cart is not None:
                return multivariate_normal.pdf(_cart, mean=mean, cov=covariance)
            else:
                return multivariate_normal.pdf(trafo.compute_trafo(_pc), mean=mean, cov=covariance)
        self.tail_approximation = tail_approximation
        self.mean_cache = None
        self.cov_cache = None
        self.cov_cache_shift = None
        self.Z_tt = 0
        self.Z_tail = 0

    def __call__(self,
                 pc,                                # type: list or np.array
                 cart=None,                         # type: None
                 normalised=True                    # type: bool
                 ):
        """
        call method that evaluates the radial TT representation
        :param pc: node to evaluate at in polar coordinates
        :param cart: Keine Ahnung was das soll
        :param normalised: flag to return the normalised result
        :return: float
        """
        # assert self.Z_tt > 0
        # assert self.Z_tail > 0
        z = 1 / (self.Z_tt + self.Z_tail)
        r = pc[0]
        if not self.equidistant_radi:
            retval = -1
            for i in range(len(self._rad_list)-1):
                if self._rad_list[i] <= r <= self._rad_list[i+1]:
                    retval = self._TTlist[i](pc)
                    break
            if retval == -1:
                print("not equidistant and not in area")
                retval = self.tail_approximation(pc, _cart=cart)
            # return 1 / (self.Z_tail + self.Z_tail) * retval
            if normalised:
                return self.trafo.det_hessian() * retval * z
            else:
                return self.trafo.det_hessian() * retval
        i = int((r - self._r0) / self._h)
        if 0 <= i < len(self._TTlist):
            retval = self._TTlist[i](pc)
        else:
            retval = self.tail_approximation(pc, _cart=cart)
        # return 1/(self.Z_tail + self.Z_tail) * retval
        if normalised:
            return self.trafo.det_hessian() * z*retval
        else:
            return self.trafo.det_hessian() * retval

    def set_normalisation(self, Z_tt):
        # assert Z_tt > 0
        self.Z_tt = Z_tt

        def int_sin(k):
            if k == 1:
                return 2
            if k == 2:
                return 0.5*np.pi
            return (k-1)/k * int_sin(k-2)

        c = (2*np.pi)**(-0.5*self.dim) * np.sqrt(np.linalg.det(np.linalg.inv(-self.trafo.hess)))**(-1)
        c *= 2*np.pi
        c *= np.prod([int_sin(lia) for lia in range(1, self.dim-2)])
        c *= self.trafo.det_hessian()
        T = self.trafo.radi[-1]
        int_r = 2**(self.dim/2 - 1) * T**self.dim * (T**2)**(-self.dim/2) * (gamma(self.dim/2) - gamma(self.dim/2)*gammaincc(self.dim/2, T**2/2))
        self.Z_tail = 1 - c*int_r
        # for l in range(len(self._TTlist)):
        #     self._TTlist[l] *= 1/(self.Z_tail + self.Z_tt)

    def _compute_shifted_moments(self,
                         max_order,
                         shift = None,
                         verbose=False):
        def contract(l, v):
            return self._TTlist[l].contract_veclist(v)

        tbasis = [[ten.basis[0] for ten in self._TTlist]] + self._TTlist[0].basis[1:]
        h12 = self.trafo.u.dot(np.diag(self.trafo.sigma ** (-0.5)).dot(self.trafo.v))

        if shift is not  None:
            if not self.trafo.map.shape == shift.shape:
                print("Error upcoming...")
                print("MAP = {v} \n shift = {w}".format(v=self.trafo.map, w = shift))
                raise ValueError("Shift array does not match the MAP structure :")

            gen = moment_generator(max_order=max_order, M=self.trafo.map+shift , H=h12, tensor_basis=tbasis,
                                   contract=contract, r_layers=[(r1, r2) for r1, r2 in zip(self.trafo.radi[:-1],
                                                                                           self.trafo.radi[1:])])
        else:
            gen = moment_generator(max_order=max_order, M=self.trafo.map, H=h12, tensor_basis=tbasis,
                               contract=contract, r_layers=[(r1, r2) for r1, r2 in zip(self.trafo.radi[:-1],
                                                                                       self.trafo.radi[1:])])

        shift_idx = tuple(shift.tolist()) if shift is not None else tuple([0] * self.dim)

        if not shift_idx in self.moments:
            self.moments[shift_idx] = {}

        for order in range(max_order + 1):
            alphas = moment_indices(order, self._TTlist[0].dim)
            self.moments[shift_idx][order] = {}
            for alpha in alphas:
                self.moments[shift_idx][order][tuple(alpha)] = gen(alpha, verbose=False)
                if verbose:
                    print("moments[shift_idx={}]"
                          "[order={}][alpha={}] = {}".format(shift_idx, order, tuple(alpha),
                                                             self.moments[shift_idx][order][tuple(alpha)]))
        if verbose:
            print("===========================================================================")
            print("=========================== Testcase 1 : Application Moment computation ===")
            print("===========================================================================")

            for order in self.moments.keys():
                print("============= order = {} =============".format(order))
                print("=== number of moments : {} ===".format(len(self.moments[shift_idx][order])))
                for alpha, moment in self.moments[shift_idx][order].items():
                    print("{a} -> {v}".format(a=alpha, v=moment[0]))

    def mean(self):
        if self.mean_cache is not None:
            return self.mean_cache
        alphas = moment_indices(1, self._TTlist[0].dim)
        retval = np.zeros(self._TTlist[0].dim)

        shift_idx = tuple([0] * self.dim)

        if (not shift_idx in self.moments) or (not 1 in self.moments[shift_idx]):
            self._compute_shifted_moments(1)

        for alpha in alphas:
            retval[tuple(alpha).index(1)] = self.moments[shift_idx][1][tuple(alpha)]

        retval *= self.Z_tt**(-1)
        self.mean_cache = retval
        return retval

    def covariance(self, useshift = True):
        if useshift:
            if self.cov_cache_shift is not None:
                return self.cov_cache_shift
        else:
            if self.cov_cache is not None:
                return self.cov_cache
        m2_alphas = moment_indices(2, self._TTlist[0].dim)
        retval = np.zeros((self._TTlist[0].dim, self._TTlist[0].dim))

        default_shift_idx = tuple([0] * self.dim)

        if not useshift :

            if len(self.moments[default_shift_idx]) <= 2:
                self._compute_shifted_moments(2)

            for alpha in m2_alphas:
                if tuple(alpha) not in self.moments[default_shift_idx][2]:
                    self._compute_shifted_moments(2)

                if 2 in alpha:
                    idx2 = tuple(alpha).index(2)
                    retval[idx2, idx2] = self.moments[default_shift_idx][2][tuple(alpha)]
                if 1 in alpha:
                    idx1 = tuple(alpha).index(1, 0)
                    idx2 = tuple(alpha).index(1, idx1+1)
                    retval[idx1, idx2] = self.moments[default_shift_idx][2][tuple(alpha)]

            retval = retval + (retval.T - np.diag(retval.diagonal()))
            m1 = self.mean()
            retval = retval /self.Z_tt - (np.outer(m1, m1))
            self.cov_cache = retval
            return retval


        else:

            if not 1 in self.moments[default_shift_idx]:
                self._compute_shifted_moments(1)

            mean_shift = -self.mean()

            # print("type :" , type(mean_shift))

            # now self.moments[mean_shift] exists and can be accessed
            mean_shift_idx = tuple(mean_shift.tolist())

            if not mean_shift_idx in self.moments or not 2 in self.moments[mean_shift_idx]:
                self._compute_shifted_moments(2, shift = mean_shift)


            for alpha in m2_alphas:

                if 2 in alpha:
                    idx2 = tuple(alpha).index(2)
                    retval[idx2, idx2] = self.moments[mean_shift_idx][2][tuple(alpha)]
                if 1 in alpha:
                    idx1 = tuple(alpha).index(1, 0)
                    idx2 = tuple(alpha).index(1, idx1 + 1)
                    retval[idx1, idx2] = self.moments[mean_shift_idx][2][tuple(alpha)]

            retval = retval + (retval.T - np.diag(retval.diagonal()))
            self.cov_cache_shift = retval
            return retval

    def compute_marginals(self,
                          min_z_volume,
                          z_tt,
                          samples_per_interval,
                          n_base_fun):
        dim = self._TTlist[0].dim
        det_hessian = self.trafo.det_hessian()
        lower_limit, upper_limit, max_radi = self.trafo.compute_bbox(min_z_volume)

        marginals = np.zeros((n_base_fun, dim))
        accepted_samples = np.zeros((dim, n_base_fun, samples_per_interval, dim))
        accepted_samples_cart = np.zeros((dim, n_base_fun, samples_per_interval, dim))
        acceptance_ratio = np.zeros((dim, n_base_fun))
        to = 0
        for lib in range(dim):
            to += 1
            print(
                "current limits: ({},{}) for marginal {}/{}".format(lower_limit[lib], upper_limit[lib], lib + 1,
                                                                    dim))
            # print("update interval")
            base = Basis(lower_limit[lib], upper_limit[lib], n_base_fun)
            init_hat = 0
            last_hat = n_base_fun
            bar = PercentageBar(n_base_fun)

            for lia in range(n_base_fun):
                # print("  sample basis function {}/{}".format(lia + 1, n_monomials))

                val = 0
                curr_N = 0
                for lid in range(samples_per_interval):
                    while True:
                        curr_N += 1
                        curr_sample = [np.random.uniform(0, self.trafo.radi[max_radi])]
                        for lic in range(dim - 1):
                            if lic == 0:
                                curr_sample.append(np.random.uniform(0, 2 * np.pi))
                            else:
                                curr_sample.append(np.random.uniform(0, np.pi))
                        cart_cord = self.trafo.compute_trafo(curr_sample)
                        if base.in_support(cart_cord[lib], lia):
                            break
                    accepted_samples[lib, lia, lid, :] = curr_sample
                    accepted_samples_cart[lib, lia, lid, :] = cart_cord
                    tt_val = self(curr_sample)
                    tt_val *= det_hessian
                    # if np.linalg.norm(cart_cord) < 1e-3:
                    #     print(curr_sample, cart_cord)
                    # tt_val *= curr_sample[0]
                    # buff = np.prod([tp(curr_sample[_lia]) for _lia, tp in enumerate(trafo.det_spherical_trafo())])
                    # assert np.linalg.norm(buff - curr_sample[0]) < 1e-10
                    tt_val *= np.prod(
                            [tp(curr_sample[_lia]) for _lia, tp in enumerate(self.trafo.det_spherical_trafo())])
                    # tt_val *= (upper_limit[lib] - lower_limit[lib])*2*np.pi
                    # tt_val *= np.pi*np.prod([np.pi for _ in range(dim-2)])

                    # tt_val = problem.post(cart_cord)
                    tt_val *= 1 / z_tt
                    curr_update = base[lia](cart_cord[lib]) * tt_val
                    if curr_update > 1e-8:
                        if init_hat < 2 < lia:
                            init_hat = lia - 2
                        if lia < n_base_fun - 3:
                            last_hat = lia + 2
                    val += curr_update
                marginals[lia, lib] = (1 / curr_N) * val
                acceptance_ratio[lib, lia] = samples_per_interval / curr_N
                bar.next()
            # print(moments)

            if init_hat > 2 and last_hat < n_base_fun - 3:
                print("old limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
                old_l, old_r = lower_limit[lib], upper_limit[lib]
                lower_limit[lib] = old_l + base.h * init_hat
                upper_limit[lib] = old_l + base.h * last_hat
                print("new limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
            base.plot_marginals(marginals[:, lib], true_para=self.mean(),
                                normalise=True, xlabel="x_{}".format(lib), title="Parameter {}".format(lib),
                                save_path="tmp/marginal_{}.pdf".format(lib))

    def compute_marginals2(self,
                           min_z_volume,
                           z_tt,
                           samples_per_interval,
                           n_base_fun):
        dim = self._TTlist[0].dim
        det_hessian = self.trafo.det_hessian()
        lower_limit, upper_limit, max_radi = self.trafo.compute_bbox(min_z_volume)

        marginals = np.zeros((n_base_fun, dim))
        for lib in range(dim):
            print(
                "current limits: ({},{}) for marginal {}/{}".format(lower_limit[lib], upper_limit[lib], lib + 1,
                                                                    dim))
            # print("update interval")
            base = Basis(lower_limit[lib], upper_limit[lib], n_base_fun)
            init_hat = 0
            last_hat = n_base_fun
            bar = PercentageBar(n_base_fun)

            val = np.zeros(base.num)
            sample_dist = np.zeros(base.num)
            while np.any(sample_dist < samples_per_interval):
                curr_sample = [np.random.uniform(0, self.trafo.radi[max_radi])]
                for lic in range(dim - 1):
                    if lic == 0:
                        curr_sample.append(np.random.uniform(0, 2 * np.pi))
                    else:
                        curr_sample.append(np.random.uniform(0, np.pi))
                cart_cord = self.trafo.compute_trafo(curr_sample)
                support = base.get_support(cart_cord[lib])
                if support == -1:
                    print("sample {:.2f} not in support".format(cart_cord[lib]))
                # accepted_samples[lib, lia, lid, :] = curr_sample
                # accepted_samples_cart[lib, lia, lid, :] = cart_cord
                tt_val = self(curr_sample)
                tt_val *= det_hessian
                tt_val *= np.prod(
                        [tp(curr_sample[_lia]) for _lia, tp in enumerate(self.trafo.det_spherical_trafo())])
                tt_val *= 1 / z_tt
                curr_update = base[support](cart_cord[lib]) * tt_val
                if curr_update > 1e-8:
                    if init_hat < 2 < support:
                        init_hat = support - 2
                    if support < n_base_fun - 3:
                        last_hat = support + 2
                val[support] += curr_update
                sample_dist[support] += 1
            marginals[:, lib] = val
            bar.next()

            if init_hat > 2 and last_hat < n_base_fun - 3:
                print("old limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
                old_l, old_r = lower_limit[lib], upper_limit[lib]
                lower_limit[lib] = old_l + base.h * init_hat
                upper_limit[lib] = old_l + base.h * last_hat
                print("new limits: {},{}".format(lower_limit[lib], upper_limit[lib]))
            base.plot_marginals(marginals[:, lib], true_para=self.mean(),
                                normalise=True, xlabel="x_{}".format(lib), title="Parameter {}".format(lib),
                                save_path="tmp/marginal_{}.pdf".format(lib))




    # region save
    def save(self, path):
        try:
            with h5py.File(path, "w") as outfile:
                for lit, ten in enumerate(self._TTlist):
                    for lia in range(len(ten.components)):
                        outfile["ten{}core{}".format(lit, lia)] = ten.components[lia]
                        assert isinstance(ten.basis[lia], PickableBase)
                        outfile["ten{}basis{}".format(lit, lia)] = ten.basis[lia].to_hash()
                    outfile["ten{}len".format(lit)] = len(ten.components)
                outfile["len"] = len(self._TTlist)
        except Exception as ex:
            print(ex)
            return False
        return True
    # endregion

    # region load
    @staticmethod
    def load(path):
        ten_list = []
        try:
            with h5py.File(path, "r") as outfile:
                for lit in range(int(outfile["len"][()])):
                    newcores = []
                    newbasis = []
                    for lia in range(int(outfile["ten{}len".format(lit)][()])):
                        try:
                            newcores.append(np.array(outfile["ten{}core{}".format(lit, lia)][()]))
                            newbasis.append(PickableBase.from_hash(str(outfile["ten{}basis{}".format(lit, lia)][()])))
                        except IndexError as ex:
                            print("load exception")
                            return None
                    ten_list.append(ExtendedTT(newcores, basis=newbasis))
        except IOError as ex:
            print("IOERROR can not import from file " + path + " err: ")
            return None
        except EOFError as ex:
            print("EOFERROR can not import from file " + path + " err: ")
            return None
        except KeyError as ex:
            print("KEYERROR can not import from file " + path + " err: ")
            return None

        return ten_list

    def kl_distance(self,
                    n_samples,
                    sample_path,
                    Z_true,
                    Z_tt
                    ):
        assert self.trafo.post is not None

        def perturbed_prior(x):
            """
            \tilde f_0 = f \circ \tilde T \|det J_T\|
            where \tilde T is the approximated transport, here in the affine case
            \tilde T = H^\frac{1}{2} \cdot + M
            :param x: sample
            :return: transported sample
            """
            f = self.trafo.post
            tilde_T = self.trafo.transport_trafo
            return f(tilde_T(x)) * self.trafo.det_hessian()
        rho1 = lambda x: perturbed_prior(x)
        rho2 = lambda x: self(self.trafo.radial_trafo_inv(x), normalised=False)
        dist = KL.from_path(rho1, rho2, self.trafo.dim, sample_path)
        print("Compute KL dist {} coordinates".format("in radial" if dist.rho2_polar else "in cartesian"))
        return np.log(Z_true/Z_tt) + dist.compute(n_samples)

    def hell_distance(self,
                      n_samples,
                      sample_path,
                      ):
        assert self.trafo.post is not None

        def perturbed_prior(x):
            """
            \tilde f_0 = f \circ \tilde T \|det J_T\|
            where \tilde T is the approximated transport, here in the affine case
            \tilde T = H^\frac{1}{2} \cdot + M
            :param x: sample
            :return: transported sample
            """
            f = self.trafo.post
            tilde_T = self.trafo.transport_trafo
            return f(tilde_T(x)) * self.trafo.det_hessian()
        rho1 = lambda x: perturbed_prior(x)
        rho2 = lambda x: self(self.trafo.radial_trafo_inv(x), normalised=False)
        dist = Hellinger.from_path(rho1, rho2, self.trafo.dim, sample_path)
        print("Compute Hellinger dist {} coordinates".format("in radial" if dist.rho2_polar else "in cartesian"))
        return dist.compute(n_samples)

    def wasserstein_distance(self,
                             n_samples,
                             sample_path1,
                             sample_path2,
                             sinkhorn_eps=0.1,
                             sinkhorn_iter=100,
                             ):
        assert self.trafo.post is not None
        rho1 = lambda x: self.trafo.post(x)

        def rho2(x, cart=None):
            return self(x, cart, normalised=False) # * (self.Z_tt + self.Z_tail)
        dist = Wasserstein.from_path(rho1, rho2, self.trafo.dim, sample_path1, sample_path2,
                                     sinkhorn_eps=sinkhorn_eps, sinkhorn_iter=sinkhorn_iter)
        if True:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            a = np.zeros((n_samples, self.dim))
            b = np.zeros((n_samples, self.dim))
            s1 = dist.rho1_sampler.get(n_samples)
            s2 = dist.rho2_sampler.get(n_samples)

            for lia in range(n_samples):
                a[lia, :] = s1[lia]["x"]
                b[lia, :] = s2[lia]["x"]
            plt.scatter(a[:, 0], a[:, 1], edgecolors='r', c="r", s=2, alpha=0.5, label="truth")
            plt.scatter(b[:, 0], b[:, 1], edgecolors='b', c="b", s=2, alpha=0.5, label="approx")
            plt.legend()
            fig.savefig("scatter_samples.pdf")
        return dist.compute(n_samples)

    def create_trafo_samples(self,
                             n_samples,
                             path):
        print("start MCMC sampler for approximative density")

        def sample_fun(_x):
            retval = np.log(self(self.trafo.radial_trafo_inv(_x), cart=_x, normalised=False))

            if retval is None or math.isnan(retval):
                retval = -np.inf
            return retval
        sampler = MCMC_Sampler(sample_fun,
                               N=n_samples + 1000,
                               burn_in=1000,
                               ndim=self.dim,
                               nwalkers=self.dim * 2 + 2,
                               p0=multivariate_normal.rvs(mean=np.zeros(self.dim), cov=np.eye(self.dim, self.dim),
                                                          size=self.dim * 2 + 2))
        samples = [None] * n_samples
        print("create trafo samples")
        bar = PercentageBar(n_samples)
        for lia, x in enumerate(sampler.get(n_samples)):
            samples[lia] = x.tolist() #{"x" : x.tolist(),
                             # "pc": self.trafo.radial_trafo_inv(x).tolist()}
            bar.next()
        with open(path, "w") as f:
            json.dump(samples, f)

    def plot_posterior_diff(self,
                            title="",
                            path="tmp/plot_r_true.pdf"):
        fig = plt.figure(figsize=(5 * (len(self.trafo.radi) - 1), 15))

        for lia in range(len(self.trafo.radi) - 1):
            grid = np.linspace(self.trafo.radi[lia], self.trafo.radi[lia + 1], num=100)
            f_list = []
            fh_list = []
            for lib in range(len(grid)):
                sample = [grid[lib]] + [0] * (len(self._TTlist[0].n) - 1)
                fh_list.append(self._TTlist[lia](sample))
                trafo_sample = self.trafo.compute_trafo(sample)
                f_list.append(self.trafo.post(trafo_sample))
            plt.subplot(5, len(self.trafo.radi) - 1, lia + 1)
            plt.semilogy(grid, np.abs(np.array(f_list) - np.array(fh_list)) / np.abs(np.array(f_list)), label="rel. diff")
            plt.xlabel("r")
            plt.ylabel("f(r)")
            plt.title(title)
            plt.legend()
            plt. subplot(5, len(self.trafo.radi)-1, (len(self.trafo.radi)-1) + lia + 1)
            plt.semilogy(grid, f_list, '-r', label="truth")
            plt.xlabel("r")
            plt.ylabel("f(r)")
            plt.title(title)
            plt.legend()
            plt. subplot(5, len(self.trafo.radi)-1, 2*(len(self.trafo.radi)-1) + lia + 1)
            plt.semilogy(grid, fh_list, '--b', label="TT")
            plt.xlabel("r")
            plt.ylabel("f(r)")
            plt.title(title)
            plt.legend()

            plt.subplot(5, (len(self.trafo.radi) - 1), 3*(len(self.trafo.radi)-1) + lia + 1)

            plt.semilogy(grid, (np.abs(np.array(f_list) - np.array(fh_list))),
                         label="abs. diff")
            plt.xlabel("r")
            plt.ylabel("f(r)")
            plt.title(title)
            plt.legend()

            plt.subplot(5, (len(self.trafo.radi) - 1), 4*(len(self.trafo.radi)-1) + lia + 1)

            plt.semilogy(grid, (np.abs(np.array(f_list) - np.array(fh_list)) * grid ** (self.dim-1)),
                         label="abs. diff weighted")
            plt.xlabel("r")
            plt.ylabel("f(r)")
            plt.title(title)
            plt.legend()
        plt.tight_layout()
        fig.savefig(path)

    def plot_x_post(self,
                    path,
                    offset=1,
                    num=100):
        if self.dim != 2:
            return False
        fig = plt.figure()
        x = np.linspace(self.trafo.map[0] - offset, self.trafo.map[0] + offset, num=num)
        y = np.linspace(self.trafo.map[1] - offset, self.trafo.map[1] + offset, num=num)
        X, Y = np.meshgrid(x, y)

        rho1 = np.zeros(X.shape)
        rho2 = np.zeros(X.shape)
        from alea.utils.progress.percentage import PercentageBar
        bar = PercentageBar(int(np.prod(X.shape)))
        for lia in range(X.shape[0]):
            for lib in range(X.shape[1]):
                x = X[lia, lib]
                y = Y[lia, lib]
                rho1[lia, lib] = self.trafo.post([x, y])
                rho2[lia, lib] = self(self.trafo.compute_trafo_inv([x, y]), normalised=False)
                bar.next()
        plt.subplot(2, 2, 1)
        plt.contourf(X, Y, rho1)
        plt.subplot(2, 2, 2)
        plt.contourf(X, Y, rho2)
        fig.savefig(path)

        rho1 = np.zeros(X.shape)
        rho2 = np.zeros(X.shape)
        from alea.utils.progress.percentage import PercentageBar
        bar = PercentageBar(int(np.prod(X.shape)))
        h12 = self.trafo.u.dot(np.diag(self.trafo.sigma ** (-0.5)).dot(self.trafo.v))

        x = np.linspace(0 - offset, 0 + offset, num=num)
        y = np.linspace(0 - offset, 0 + offset, num=num)
        X, Y = np.meshgrid(x, y)
        for lia in range(X.shape[0]):
            for lib in range(X.shape[1]):
                z = h12.dot(np.array([X[lia, lib], Y[lia, lib]])) + self.trafo.map
                rho1[lia, lib] = self.trafo.post(z)
                rho2[lia, lib] = self(self.trafo.compute_trafo_inv(z), normalised=False)
                bar.next()
        plt.subplot(2, 2, 3)
        plt.contourf(X, Y, rho1)
        plt.subplot(2, 2, 4)
        plt.contourf(X, Y, rho2)
        fig.savefig(path)

    def dofs(self):
        dofs = 0
        for ten in self._TTlist:
            assert isinstance(ten, ExtendedTT)
            dofs += ten.dofs()
        return dofs

    def average_r(self):
        retval = 0
        for ten in self._TTlist:
            assert isinstance(ten, ExtendedTT)
            retval += np.sum([r for r in ten.r]) / (ten.dim-1)
        return retval / len(self._TTlist)
