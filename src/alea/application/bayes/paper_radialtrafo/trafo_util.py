from __future__ import division, print_function, absolute_import
from scipy.integrate.quadrature import quadrature
import numpy as np
import json
from alea.math_utils.basis import (PickableBase, NumpyPolynomial, FourierSinModes, FourierCosModes,
                                   ConstantFourierMode, ArbitraryPrecisionPolynomial)
from numpy.polynomial.polynomial import Polynomial
from alea.application.bayes.paper_radialtrafo.sampler import PolarSampler, UniformSampler, HashSampler
from numpy.polynomial import polynomial as Poly
import matplotlib.pyplot as plt
import xerus as xe
from scipy.optimize import minimize
from dolfin import *

import math
from scipy.stats import multivariate_normal
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler
from alea.utils.progress.percentage import PercentageBar
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
from alea.math_utils.tensor.extended_tt import ExtendedTT, BasisType
from alea.utils.progress.percentage import PercentageBar
from alea.application.bayes.paper_radialtrafo.orthonormalize import OrthoPoly
from alea.math_utils.tensor.reconstruction import (ADFReconstruction, UQADFReconstruction, TorchTTReconstruction)

from alea.application.bayes.paper_radialtrafo.polynomial import compute_orth_polynomials_radial

count = None

def compute_map(func, x0, verbose=True, bnds=None):
    print("start with x0={}".format(x0))
    if bnds is not None:
        bounds = [(l, u) for l, u in zip(bnds[0], bnds[1])]
        opt_info = minimize(func, x0, method="L-BFGS-b", bounds=bounds)
        hess_inv_op = opt_info["hess_inv"]

        hess_inv = np.zeros(hess_inv_op.shape)
        for lia in range(hess_inv_op.shape[0]):
            ei = np.zeros(hess_inv.shape[0])
            ei[lia] = 1

            hess_inv[:, lia] = hess_inv_op * ei
        print("hess inv: {}".format(hess_inv))
        opt_info["hess_inv"] = hess_inv
    else:
        opt_info = minimize(func, x0, method="BFGS", options={"gtol": 1e-15, "norm": np.Inf})
    if verbose:
        print("  success: {}, n_iter: {}, f_eval: {}, fun: {}". format(opt_info["success"],
                                                                       opt_info["nit"], opt_info["nfev"],
                                                                       opt_info["fun"]))
        if "jac" in opt_info:
            print("  jac: {}".format(opt_info["jac"]))
        print("  msg: {}".format(opt_info["message"]))
        print("  status: {}".format(opt_info["status"]))
    return opt_info


def fourier_modes(noB):

    n_modes = int(noB / 2)

    base_list = [ConstantFourierMode()]
    for k in range(n_modes):
        base_list.append(FourierSinModes(k + 1))
        base_list.append(FourierCosModes(k + 1))

    return PickableBase(base_list)


class RadialTrafo(object):
    def __init__(self,
                 dim,                               # type: int
                 mapoint=None,                      # type: np.ndarray or None
                 hess=None                          # type: np.ndarray or None
                 ):                                 # type: (...) -> None
        """
        Constructor for the transformation of spherical and cartesian coordinates.
        We use the MAP information and the curvature at the MAP point to create a laplace
        approximation and transform this approximation to a unit circle.
        :param dim: Dimension of the parameter space
        :param mapoint: maximal a posteriori point (MAP)/ point of maximal mass/ maximal value of potential
        :param hess: hessian at MAP
        """
        if mapoint is None:
            self.map = np.zeros(dim)
        else:
            if not len(mapoint) == dim:
                raise ValueError("Dimension of MAP point is {} but should be {}".format(len(mapoint), dim))
            self.map = mapoint
        if hess is None:
            self.hess = np.eye(dim, dim)
        else:
            if not hess.shape == (dim, dim):
                raise ValueError("Dimension of hessian is {0} but should be ({1}, {1})".format(hess.shape, dim))
            self.hess = hess
        self.dim = dim
        self.u = None
        self.v = None
        self.sigma = None
        self.radi = None
        self.posterior_ten_list = None
        self.maxdegs = None
        self.radi_onb = None
        self.angle_onb = None
        self.u, self.sigma, self.v = np.linalg.svd(self.hess, full_matrices=False)
        self.post = None

    def radial_trafo(self, hatx):
        theta, r = hatx[1:], hatx[0]
        sins = np.sin(theta)
        coss = np.cos(theta)

        mat = np.zeros((self.dim, self.dim - 1))
        if self.dim == 2:
            mat[0, :] = coss[0]
            mat[1, :] = sins[0]
        else:  # dim > 2
            mat[0, :] = np.concatenate([np.array([coss[0]]), sins[1:]])
            mat[1, :] = sins
            for d in range(2, self.dim):
                mat[d, :] = np.array([1.] * (d - 1) + [coss[d - 1]] + sins[d:].tolist())
        return r * np.prod(mat, axis=1)  # + self.M

    def radial_trafo_inv(self, X):

        a = X[0]
        X[0] = X[1]
        X[1] = a
        # revert
        X = X[::-1]
        r, theta = self.__invpolar(X)
        return np.concatenate([r, theta[::-1]])

    def transport_trafo(self, hatx):
        if self.u is None or self.v is None or self.sigma is None:
            self.u, self.sigma, self.v = np.linalg.svd(self.hess, full_matrices=False)

        h12 = self.u.dot(np.diag(self.sigma ** (-0.5)).dot(self.v))
        return h12.dot(hatx) + self.map

    def transport_trafo_inv(self, hatx):
        if self.u is None or self.v is None or self.sigma is None:
            self.u, self.sigma, self.v = np.linalg.svd(self.hess, full_matrices=False)

        h12 = self.v.T.dot(np.diag(self.sigma ** (0.5)).dot(self.u.T))
        return h12.dot(hatx-self.map)

    def compute_trafo(self,
                      x,                            # type: np.ndarray or list
                      affine=True
                      ):
        """
        method to compute the transformation (r, \phi_1, ... , \phi_{M-1}) -> (x_1, ... x_M)
        :param x: point in tensor space [Radius, Angle_1, ..., Angle_{M-1}]
        :return: point in cartesian coordinates
        """

        m = len(x)
        assert m > 1
        vectorize = False
        r = x[0]
        if isinstance(r, np.ndarray) and len(r.shape) == 2:
            vectorize = True
        phi = x[1:]
        assert m == self.dim

        if False:
            if vectorize:
                retval = np.zeros((m, r.shape[0], r.shape[1]))
            else:
                retval = np.zeros(m)
            for lia in range(m):
                if lia == 0:
                    if vectorize:
                        retval[lia, :, :] = r * np.cos(phi[lia]) * np.prod([np.sin(phi[lib]) for lib in range(1, m-1)],
                                                                           axis=0)
                    else:
                        retval[lia] = r * np.cos(phi[lia]) * np.prod([np.sin(phi[lib]) for lib in range(1, m-1)])
                    continue
                if lia == 1:
                    if vectorize:
                        retval[lia, :, :] = r * np.prod([np.sin(phi[lib]) for lib in range(0, m-1)], axis=0)
                    else:
                        retval[lia] = r * np.prod([np.sin(phi[lib]) for lib in range(0, m-1)])
                    continue

                if vectorize:
                    retval[lia, :, :] = r*np.cos(phi[lia-1])*np.prod([np.sin(phi[lib]) for lib in range(lia, m-1)],
                                                                     axis=0)
                else:
                    retval[lia] = r*np.cos(phi[lia-1])*np.prod([np.sin(phi[lib]) for lib in range(lia, m-1)])
        else:
            retval = self.radial_trafo(x)
        if self.u is None or self.v is None or self.sigma is None:
            self.u, self.sigma, self.v = np.linalg.svd(self.hess, full_matrices=False)

        if affine:
            h12 = self.u.dot(np.diag(self.sigma**(-0.5)).dot(self.v))

            if vectorize:
                raise UserWarning("Why are you here anyway?")
                retval = np.einsum('ij, jkl->ikl', h12, retval)
            else:
                retval = h12.dot(retval)
            # print(retval)
            if vectorize:
                for lia in range(m):
                    retval[lia, :, :] += self.map[lia]
                assert retval.shape == (m, r.shape[0], r.shape[1])
            else:
                retval += self.map
                assert len(retval) == m
            # print(retval)
        return retval

    def det_hessian(self):
        """
        Computes the hessian of the linear transformation, i.e. |\prod_m=1^M \sqrt{\sigma_m}|,
        where \sigma_m is the m-th singular value of the matrix H (hessian)
        :return: float
        """
        return np.abs(np.prod(self.sigma**-0.5))

    def det_spherical_trafo(self):
        """
        computes the determinant of the spherical transformation.
        See https://de.wikipedia.org/wiki/Polarkoordinaten
        :return: list of callables [r^{d-1}, 1, sin(\theta_1), sin(\theta_2)^2, ..., sin(\theta_{n-2})^{n-2}]
        """
        retval = []
        SIN = lambda _k: lambda x: np.abs(np.sin(x)**_k)
        for lia in range(self.dim):
            if lia == 0:
                retval.append(lambda x: np.abs(x**(self.dim-1)))
                continue
            if lia == 1:
                retval.append(lambda x: 1)
                continue
            retval.append(SIN((lia-1)))

        return retval

    def generate_onb(self,
                     radi,
                     maxdegs,
                     _check_onb,
                     prec):
        """
        Computes the orthonormal basis on every circular disc.
        in r-coordinate: Take arbitrary precision polynomials which are orthonormal with respect to
                         the weight function w(r) = r^{n-1}.
                         This choice creates a blow up of the basis as r->0 but is the correct choice.
                         The basis is created on every circular disc `i` up to polynomial degree of `maxdegs[0][i]`
        in \theta_0:     Periodical Fourier modes are employed of maximal degree `maxdegs[1]` on [0, 2\pi]
        otherwise:       Orthonormal polynomials are created on [0, \pi] with respect to the weight function
                         w(\theta_i) = sin(\theta_i)^i (factor in determinant of spherical transformation)
        :param radi: List of radi
        :param maxdegs: maximal degree. list of lists in first component, list otherwise
        :param _check_onb: flag to check if the basis is orthonormal (only applies for anglular basis)
        :param prec: precision to use in arbitrary precision polynomials
        :return: tuple(list[radi basis], angular basis]
        """
        onb_fun_list = self.det_spherical_trafo()
        self.radi_onb = []
        self.angle_onb = []
        for lia, f in enumerate(onb_fun_list):
            if lia == 0:
                for lib in range(len(radi) - 1):

                    subdomain = [radi[lib], radi[lib + 1]]
                    o_basis = compute_orth_polynomials_radial(maxdegs[lia][lib], subdomain, self.dim, prec)

                    self.radi_onb.append(PickableBase([ArbitraryPrecisionPolynomial(o_basis_.coef)
                                                       for o_basis_ in o_basis]))
                continue
            if lia == 1:
                assert maxdegs[lia] % 2 == 1
                #self.angle_onb.append(self._create_poly_onb(f, [0, 2*np.pi], maxdegs[lia], _check_onb=_check_onb))
                self.angle_onb.append(fourier_modes(maxdegs[lia]))
                continue
            self.angle_onb.append(self._create_poly_onb(f, [0, np.pi], maxdegs[lia], _check_onb=_check_onb))
        return self.radi_onb, self.angle_onb

    def reconstruct_tt_uq(self,
                          radi,                        # type: list
                          maxdegs,                     # type: list
                          func,                        # type: callable
                          sample_list=None,            # type: list or None
                          n_samples=None,              # type: int or None
                          _check_onb=False,            # type: bool
                          progress=False,              # type: bool
                          adf_iteration=1000,          # type: int
                          adf_targetresnorm=1e-12,     # type: float
                          adf_minresnormdecrease=0.9,  # type: float
                          adf_verbose=True,            # type: bool
                          adf_initrank=5,              # type: int
                          adf_maxrank=10,              # type: int
                          prec=50,                     # type: int
                          force_recalc=True,           # type: bool
                          save_path=None,
                          use_radial_sampler=True,
                          backend="UQ_ADF",
                          rerun_on_fail=0,             # type: int
                          ):
        self.radi = radi
        self.maxdegs = maxdegs
        self.post = func
        n_circles = len(radi)-1
        M = len(maxdegs)

        assert rerun_on_fail >= 0

        ten_list = []
        radi_fun, angle_fun = self.generate_onb(radi, maxdegs, _check_onb=_check_onb, prec=prec)
        if save_path is not None and not force_recalc:
            print("Try to read tt list")
            ten_list = RadialTT.load(save_path)
            if ten_list is not None:
                self.posterior_ten_list = ten_list
                print("tt list read: {}".format(self.posterior_ten_list))
                return ten_list
            ten_list = []

        def target(theta):
            return func(self.compute_trafo(theta))

        sampler_subd = [[lb, ub] for lb, ub in zip(radi[:-1], radi[1:])]
        if use_radial_sampler:
            sampler = PolarSampler(dim=M, subdomains=sampler_subd)
        elif sample_list is not None:
            sampler = HashSampler(dim=M, subdomains=sampler_subd, hash=sample_list)
        else:
            sampler = UniformSampler(dim=M, subdomains=sampler_subd)

        for lia_circle, fr_list in enumerate(radi_fun):

            tensor_basis = [fr_list] + angle_fun

            resutl = None
            run_counter = 0
            while run_counter <= rerun_on_fail:
                if progress:
                    print("Run {}/{}".format(lia_circle + 1, n_circles))
                curr_sample_list = [sampler.generate(lia_circle) for _ in range(n_samples)]

                if backend.upper() == "UQ_ADF":
                    recon = UQADFReconstruction(adf_iteration, adf_targetresnorm, curr_sample_list, tensor_basis, target)
                    result = recon.run(verbose=adf_verbose, non_uq=True)

                elif backend.upper() == "ADF":
                    recon = ADFReconstruction(adf_iteration, adf_targetresnorm, curr_sample_list, tensor_basis, target)
                    result = recon.run(None, adf_maxrank,
                                       init_rank=adf_initrank,
                                       min_resnorm_decrease=adf_minresnormdecrease,
                                       verbose=adf_verbose)
                elif backend.upper() == "PYTORCH":
                    recon = TorchTTReconstruction(adf_iteration, adf_targetresnorm, curr_sample_list, tensor_basis, target)
                    max_deg = 0
                    for lia in range(len(maxdegs)):
                        if lia == 0:
                            for deg in maxdegs[0]:
                                if deg > max_deg: max_deg = deg
                        else:
                            if maxdegs[lia] > max_deg: max_deg = maxdegs[lia]

                    result = recon.run(max_deg, num_test=0, batch_size=int(len(curr_sample_list)//10),
                                       l2_reg=0.0, max_rank=adf_maxrank, max_epoch=1000,
                                       learn_rate=1e-4, verbose=adf_verbose, device=None)
                else:
                    raise ValueError("backend: {} not supported. use any of [UQ_ADF, ADF, PYTORCH]".format(backend))
                if result is not None:
                    break
                print("Xerus did not converge. Run again {}/{}".format(run_counter, rerun_on_fail))
                run_counter += 1
            ten_list.append(result)
        if save_path is not None:
            radial_tt = RadialTT(ten_list, self)
            save_res = radial_tt.save(save_path)
            print("save result = {}".format(save_res))

        self.posterior_ten_list = ten_list
        return ten_list

    @staticmethod
    def _create_poly_onb(f,
                         intlims,
                         maxdeg,
                         _check_onb=False
                         ):
        onb_poly = OrthoPoly(f, intlims=intlims)
        onb_poly.gen_poly(maxdeg)
        retval = []
        for lic in range(maxdeg):
            normalisation, err = quadrature(lambda x: Polynomial(onb_poly.poly[lic])(x)**2*f(x),
                                            intlims[0], intlims[1], maxiter=100)
            if err > 1e-5:
                print("WARNING: normalisation quadrature failed with error: {}".format(err))
            # print("norm {} = {}".format(lic, normalisation))
            curr_poly = NumpyPolynomial(onb_poly.poly[lic]/np.sqrt(normalisation))
            retval.append(curr_poly)
        if _check_onb:
            if not (check_onb(retval, intlims[0], intlims[1], f)):
                    print("WARNING non orthonormal basis")
        return PickableBase(retval)

    def compute_Z(self):
        norm_list = []
        for f in self.angle_onb:
            norm_list.append(f[0](0))
        Z_tt = 0
        Z_list = []
        for lia, ten in enumerate(self.posterior_ten_list):
            buf_tt = ExtendedTT(ten.components, basis=[BasisType.points] * self.dim)
            # Z_k = -np.exp(-0.5*radi[lia+1]**2) + np.exp(-0.5*radi[lia]**2)
            Z_tt_k = buf_tt([0] * self.dim) / (self.radi_onb[lia][0](0) * np.prod(norm_list)) * self.det_hessian()
            Z_list.append(Z_tt_k)
            Z_tt += Z_tt_k
        # print("Z={} vs Z_tt={}".format(Z, Z_tt))
        self.Z_list = Z_list
        self.Z = Z_tt
        return Z_list, Z_tt

    def compute_bbox(self,
                     min_z_volume):
        max_radi = 1
        for lia, Zi in enumerate(self.Z_list):
            if Zi >= min_z_volume:
                max_radi = lia + 1
        print("maximal circular disc: {} with volume: {}".format([self.radi[max_radi - 1], self.radi[max_radi]],
                                                                 self.Z_list[max_radi - 1]))

        def J(theta, _r, _i, _trafo):
            _rtheta = np.zeros(len(theta) + 1)
            _rtheta[0] = _r
            _rtheta[1:] = theta
            _retval = _trafo.compute_trafo(_rtheta)[_i]
            return _retval

        lower_limit = []
        arg_lower_limit = []
        upper_limit = []
        arg_upper_limit = []
        bnds = [(0, 2 * np.pi)]
        for lia in range(1, self.dim - 1):
            bnds.append((0, np.pi))
        for lia in range(self.dim):
            if lia == 0:
                iv = np.ones(self.dim - 1) * np.pi/2
            else:
                iv = np.ones(self.dim - 1) * np.pi
            res = minimize(J, iv, args=(self.radi[max_radi], lia, self),
                           bounds=bnds, options={"gtol": 1e-10, "ftol": 1e-12})
            # print(res)
            lower_limit.append(res["fun"])
            arg_lower_limit.append(res["x"])
            res = minimize(lambda _x, _r, _i, _trafo: -J(_x, _r, _i, _trafo), iv,
                           args=(self.radi[max_radi], lia, self),
                           bounds=bnds, options={"gtol": 1e-10, "ftol": 1e-12})
            upper_limit.append(-res["fun"])
            if np.abs(upper_limit[-1] - lower_limit[-1]) < 1e-4:
                lower_limit[-1] -= 1e-2
                upper_limit[-1] += 1e-2
            arg_upper_limit.append(res["x"])
            # print(res)
        print("lower : {}".format(lower_limit))
        print("upper: {}".format(upper_limit))
        return lower_limit, upper_limit, max_radi

    def compute_trafo_inv(self, x):
        h12_inv = self.u.dot(np.diag(self.sigma**(0.5)).dot(self.v))
        X = h12_inv.dot(x - self.map)

        # swap x0 and x1
        a = X[0]
        X[0] = X[1]
        X[1] = a
        # revert
        X = X[::-1]

        # apply inverse polarcoordinates in the changed coords
        r, phi = self.__invpolar(X)
        # reverse angles phi
        return np.concatenate([r, phi[::-1]])

    def __invpolar(self,X):
        """
        inverse polar w.r.t to the changed order of coordinates from
        https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates
        :param X:
        :return:
        """
        r = np.linalg.norm(X)

        norms = [np.linalg.norm(X[(i):]) for i in range(len(X) - 1)]

        phi = [math.acos(X[i] / norms[i]) if norms[i] != 0 else 0. for i in range(len(X) - 1)]

        if X[len(X) - 1] < 0:
            phi[-1] = 2 * np.pi - phi[-1]

        return np.array([r]), np.array(phi)

    def create_trafo_samples(self,
                             n_samples,
                             path,
                             func):
        sampler = MCMC_Sampler(lambda x: np.log(func(x)),
                               N=n_samples + 1000,
                               burn_in=1000,
                               ndim=self.dim,
                               nwalkers=self.dim * 2 + 2,
                               p0=multivariate_normal.rvs(mean=self.map, cov=np.linalg.inv(-self.hess),
                                                          size=self.dim * 2 + 2))
        samples = [None] * len(sampler)
        # print("create trafo samples")
        bar = PercentageBar(len(sampler))
        for lia, x in enumerate(sampler.get(len(sampler))):
            samples[lia] = x.tolist() #{"x" : x.tolist(),
                                      # "pc": self.compute_trafo_inv(x).tolist()}
            bar.next()
        with open(path, "w") as f:
            json.dump(samples, f)


def plot_coef_sol_mesh(coef, sol, points, mesh, save_path=None):
    fig = plt.figure(figsize=(16, 4))
    plt.subplot(131)
    im = plot(coef)
    plt.title("True coefficient field")
    plt.colorbar(im)
    plt.subplot(132)
    im = plot(sol)
    for lia in range(len(points)):
        plt.scatter(points[lia][0], points[lia][1], s=12, c="red", marker="x")
    plt.title("True solution with observation nodes")
    plt.colorbar(im)
    plt.subplot(133)
    plt.tight_layout()
    plot(mesh)
    if save_path is None:
        plt.show()
    else:
        fig.savefig(save_path)


def plot_trafo_hessian(f, trafo, num=100, save_path="tmp/trafo.pdf", progress=False):
    from alea.utils.progress.percentage import PercentageBar
    plt.figure(figsize=(14, 8))

    X, Y = np.meshgrid(np.linspace(-1, 1, num=num), np.linspace(-1, 1, num=num))
    Z = np.zeros(X.shape)
    if progress:
        bar = PercentageBar(X.shape[0])
    for lia in range(X.shape[0]):
        for lib in range(X.shape[1]):
            Z[lia, lib] = f([X[lia, lib], Y[lia, lib]])
        if progress:
            bar.next()
    plt.subplot(121)
    plt.title("posterior")
    plt.contourf(X, Y, Z)
    plt.xlabel("x_1")
    plt.ylabel("x_2")
    plt.plot(trafo.map[0], trafo.map[1], 'xr', label="MAP")
    plt.legend()
    plt.subplot(122)
    plt.title("posterior with trafo([0, 1], [0, 2pi])")
    plt.contourf(X, Y, Z)
    plt.xlabel("x_1")
    plt.ylabel("x_2")

    r_list = np.linspace(0, 2, endpoint=True, num=5)
    theta = np.linspace(0, 2 * np.pi, num=num)
    for lia in range(len(r_list) - 1):
        r = np.linspace(r_list[lia], r_list[lia + 1], num=num)
        R, THETA = np.meshgrid(r, theta)
        T = trafo.compute_trafo([R, THETA])
        plt.scatter(T[0, :, :].flatten(), T[1, :, :].flatten(), s=3, alpha=0.3,
                    label="r={}".format((r_list[lia], r_list[lia + 1])))
    plt.legend()
    plt.savefig(save_path)


def plot_onb(trafo, maxdegs, radi, save_path="tmp/onb.pdf"):
    radi_fun, angle_fun = trafo.generate_onb(radi, maxdegs, _check_onb=False, prec=50)

    fig = plt.figure(figsize=(5 * len(radi), 20))
    for lia in range(len(radi) - 1):
        plt.subplot(2, max(len(radi) - 1, trafo.dim-1), lia + 1)
        grid = np.linspace(radi[lia], radi[lia + 1], num=50)
        for lib, f in enumerate(radi_fun[lia]):
            plt.plot(grid, f(grid), label="P_{}^r".format(lib))
        plt.legend()
    for lia in range(trafo.dim - 1):
        plt.subplot(2, max(len(radi) - 1, trafo.dim-1), max(len(radi), trafo.dim) + lia)
        if lia == 0:
            grid = np.linspace(0, 2 * np.pi, num=50, endpoint=True)
        else:
            grid = np.linspace(0, np.pi, num=50, endpoint=True)
        for lib, f in enumerate(angle_fun[lia]):
            plt.plot(grid, f(grid), label="P_{}^\phi".format(lib))
        plt.legend()
    plt.tight_layout()
    fig.savefig(save_path)


def check_onb(basis, a, b, rho):
    gramian = np.zeros((len(basis), len(basis)))
    for lia, f in enumerate(basis):
        for lib, g in enumerate(basis):
            q, err = quadrature(lambda x: f(x)*g(x)*rho(x), a, b, maxiter=100)
            gramian[lia, lib] = q
    if not (np.linalg.norm(gramian - np.eye(len(basis), len(basis))) < 1e-7):
        print("gramian: \n{}".format(gramian))
        return False
    return True


def plot_bbox(post, trafo, max_r, n_circles, lower_limit, upper_limit, dir, num=100):
    plt.figure(figsize=(14, 8))

    X, Y = np.meshgrid(np.linspace(-1, 1, num=num), np.linspace(-1, 1, num=num))
    Z = np.zeros(X.shape)
    bar = PercentageBar(X.shape[0])
    for lia in range(X.shape[0]):
        for lib in range(X.shape[1]):
            Z[lia, lib] = post([X[lia, lib], Y[lia, lib]])
        bar.next()
    plt.subplot(121)
    plt.title("posterior")
    plt.contourf(X, Y, Z)
    plt.xlabel("x_1")
    plt.ylabel("x_2")
    plt.plot(trafo.map[0], trafo.map[1], 'xr', label="MAP")
    plt.legend()
    plt.subplot(122, axisbg="xkcd:royal blue")
    plt.title("posterior with trafo([0, 1], [0, 2pi])")
    plt.contourf(X, Y, Z)
    plt.xlabel("x_1")
    plt.ylabel("x_2")

    r_list = np.linspace(0, max_r, endpoint=True, num=n_circles)
    theta = np.linspace(0, 2 * np.pi, num=50)
    for lia in range(len(r_list) - 1):
        r = np.linspace(r_list[lia], r_list[lia + 1], num=num)
        R, THETA = np.meshgrid(r, theta)
        T = trafo.compute_trafo([R, THETA])
        plt.scatter(T[0, :, :].flatten(), T[1, :, :].flatten(), s=3, alpha=0.3,
                    label="r={}".format((r_list[lia], r_list[lia + 1])))
    plt.plot(lower_limit[0], lower_limit[1], 'sr')
    plt.plot(upper_limit[0], lower_limit[1], 'sr')
    plt.plot(lower_limit[0], upper_limit[1], 'sr')
    plt.plot(upper_limit[0], upper_limit[1], 'sr')

    _x1 = [upper_limit[0], lower_limit[0], lower_limit[0], upper_limit[0], upper_limit[0]]
    _x2 = [lower_limit[1], lower_limit[1], upper_limit[1], upper_limit[1], lower_limit[1]]
    plt.plot(_x1, _x2, '-g', label="arg l0")

    # _x1 = J(arg_upper_limit[0], radi[max_radi], 0, trafo)
    # _x2 = J(arg_upper_limit[0], radi[max_radi], 1, trafo)
    # plt.plot(_x1, _x2, '^', label="arg u0")
    # print("upper x1={}, x2={}".format(_x1, _x2))

    # _x1 = J(arg_lower_limit[1], radi[max_radi], 0, trafo)
    # _x2 = J(arg_lower_limit[1], radi[max_radi], 1, trafo)
    # print("lower x1={}, x2={}".format(_x1, _x2))
    # plt.plot(_x1, _x2, '^', label="arg l1")

    # _x1 = J(arg_upper_limit[1], radi[max_radi], 0, trafo)
    # _x2 = J(arg_upper_limit[1], radi[max_radi], 1, trafo)
    # plt.plot(_x1, _x2, '^', label="arg u1")
    # print("upper x1={}, x2={}".format(_x1, _x2))

    # plt.scatter([J([t], radi[max_radi], 0, trafo) for t in theta], [J([t], radi[max_radi], 1, trafo) for t in theta])

    plt.legend()
    plt.savefig(dir)


def plot_marginals(sc, lower, upper):
    fig = plt.figure(figsize=(14, 8))
    M = sc.shape[1]
    d = sc.shape[0]

    for lia, (a, b) in enumerate(zip(lower, upper)):
        grid = np.linspace(a, b, num=100)

        def rho(x):
            return 1/(b-a)
        poly_list = RadialTrafo._create_poly_onb(rho, intlims=[a, b], maxdeg=d, _check_onb=True)
        plt.subplot(2, M, lia+1)
        for p in poly_list:
            plt.plot(grid, p(grid), label="P_{}".format(lia))
        plt.title("ONB on [{}, {}]".format(a, b))
        print("polylist: {}".format(poly_list))
        new_coef_list = []
        for poly in poly_list:
            coef = np.zeros(d)
            for lib, x in enumerate(poly.coef):
                coef[lib] = x
            print("coef: {} -> newcoef: {} . {} = {}".format(poly.coef, sc[:, lia].T, coef, sc[:, lia].T.dot(coef)))
            new_coef_list.append(sc[:, lia].T.dot(coef))


        plt.subplot(2, M, M + lia+1)
        plt.plot(grid, np.sum([c*p(grid) for c, p in zip(new_coef_list, poly_list)], axis=0))
        plt.xlabel("x_{}".format(lia))
        plt.title("Parameter {}".format(lia))

    fig.savefig("tmp/marginals.pdf")

def plot_marginals_hatfun(sc, lower, upper, true_para, save_path="tmp/marginals.pdf"):
    fig = plt.figure(figsize=(14, 8))
    M = sc.shape[1]
    d = sc.shape[0]

    for lia, (a, b) in enumerate(zip(lower, upper)):
        line = IntervalMesh(d, a, b)
        fs = FunctionSpace(line, 'DG', 0)

        grid = np.linspace(a, b, num=100)

        u = TestFunction(fs)
        v = TrialFunction(fs)
        stiff = inner(u, v)*dx
        rhs = Function(fs)
        rhs.vector()[:] = np.ascontiguousarray(sc[:, lia])
        L = inner(rhs, v)*dx
        sol = Function(fs)
        solve(stiff == L, sol)
        Z = assemble(sol*dx)
        sol.set_allow_extrapolation(True)
        plt.subplot(1, M, lia+1)
        vals = [sol(x)/Z for x in grid]
        plt.plot(grid, vals)
        plt.xlabel("x_{}".format(lia))
        plt.vlines(true_para[lia], 0, max(vals), colors='r', linestyles="--")
        plt.title("Parameter {}".format(lia))

    fig.savefig(save_path)

def plot_tt_trafo(ten_list, radi, num, post, trafo, save_path="tmp/tt_trafo.pdf"):

    plt.figure(figsize=(5*len(ten_list), 14))

    for lia, ten in enumerate(ten_list):
        r = np.linspace(radi[lia], radi[lia + 1], num=num)
        R, THETA = np.meshgrid(r, np.linspace(0, 2 * np.pi, num=num))
        Z = np.zeros(R.shape)
        TT = np.zeros(R.shape)
        print("Evaluate Tensor {}/{}".format(lia + 1, len(ten_list)))
        bar = PercentageBar(R.shape[0])
        for i in range(R.shape[0]):
            for j in range(R.shape[1]):
                Z[i, j] = post(trafo.compute_trafo([R[i, j], THETA[i, j]])) * trafo.det_hessian()
                TT[i, j] = ten([R[i, j], THETA[i, j]]) * trafo.det_hessian()
            bar.next()

        plt.subplot(3, len(ten_list), lia+1)
        plt.title("posterior")
        # plt.contourf(X, Y, Z)
        plt.xlabel("r")
        plt.ylabel("theta")
        im = plt.contourf(R, THETA, Z)
        plt.colorbar(im)
        plt.subplot(3, len(ten_list), len(ten_list) + lia + 1)
        plt.title("TT")
        # plt.contourf(X, Y, Z)
        plt.xlabel("r")
        plt.ylabel("theta")
        im = plt.contourf(R, THETA, TT)
        plt.colorbar(im)

        plt.subplot(3, len(ten_list), 2*len(ten_list) + lia + 1)
        plt.title("diff")
        # plt.contourf(X, Y, Z)
        plt.xlabel("r")
        plt.ylabel("theta")
        im = plt.contourf(R, THETA, np.abs(Z-TT)/np.abs(Z))
        plt.colorbar(im)
    plt.tight_layout()
    plt.savefig(save_path)

class Trafo():
    """

    Trafo.fun(xhat)  = H * polartrafo(xhat) + M
    Trafo.invfun(x)  =    polartrafo^{-1}(inv(H)(x-M))

    """
    def __init__(self, dim, H,M):

        self.dim = dim
        # TODO assert self.dim = len(M),  and dimension of H
        # replace with decomposition  and inverse over eigenvalues
        self.H = H
        self.invH = np.linalg.inv(H)

        self.M = M

    def fun(self, xhat):
        theta, r = xhat[1:], xhat[0]
        sins = np.sin(theta)
        coss = np.cos(theta)

        mat = np.zeros((self.dim, self.dim - 1))
        if self.dim == 2:
            mat[0, :] = coss[0]
            mat[1, :] = sins[0]
        else:  # dim > 2
            mat[0, :] = np.concatenate([np.array([coss[0]]), sins[1:]])
            mat[1, :] = sins
            for d in range(2, self.dim):
                mat[d, :] = np.array([1.] * (d - 1) + [coss[d - 1]] + sins[d:].tolist())

        res = r * np.prod(mat, axis=1)  # + self.M

        res = self.H.dot(res) + self.M
        return res


    def invfun(self, x):

        X = self.invH.dot(x - self.M)

        # swap x0 and x1
        a = X[0]
        X[0] = X[1]
        X[1] = a
        # revert
        X = X[::-1]

        # apply inverse polarcoordinates in the changed coords
        r, phi = self.__invpolar(X)
        # reverse angles phi
        return np.concatenate([r, phi[::-1]])

    def __invpolar(self,X):
        """
        inverse polar w.r.t to the changed order of coordinates from
        https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates
        :param X:
        :return:
        """
        r = np.linalg.norm(X)

        norms = [np.linalg.norm(X[(i):]) for i in range(len(X) - 1)]

        phi = [math.acos(X[i] / norms[i]) if norms[i] != 0 else 0. for i in range(len(X) - 1)]

        if X[len(X) - 1] < 0:
            phi[-1] = 2 * np.pi - phi[-1]

        return np.array([r]), np.array(phi)