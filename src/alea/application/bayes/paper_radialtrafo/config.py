from __future__ import (division, print_function, absolute_import)
import json
import numpy as np
from alea.application.bayes.paper_radialtrafo.problems import (GaussianModel, PoissonModel, ElasticityUpscaling)


class RadialTrafoConfig(object):
    """
    simple json file wrapper to store and obtain configuration data
    TODO: inherit from alea configuration (not existing)
    """
    def __init__(self,
                 path                               # type: str
                 ):
        """
        constructor taking the path where the .json file is stored.
        Full path is used here, i.e. path="directory/to/file.json" or any other file extension
        :param path: str
        """
        with open(path, "r") as f:
            self.data = json.load(f)

    def __getitem__(self, item):
        """
        wrapper for getitem method to always grab the data dictionary containing the config
        :param item: key
        :return: value
        """
        return self.data[item]

    def __setitem__(self, key, value):
        """
        wrapper for the setitem method to always write keys and values to the data dictionary
        :param key: key of the value to set
        :param value: value to set the key to
        :return: None
        """
        self.data[key] = value

    def load(self):
        """
        General load method to store additional data into the config dictionary
        :return: bool
        """
        if self["name"] == "gauss":
            return self.load_gauss_config()
        elif self["name"] == "poisson":
            retval = self.load_poisson_config()
            assert self["dim"] == self["problem"]["M"]
            return retval
        elif self["name"] == "upscaling":
            return self.load_upscaling_config()

        else:
            raise ValueError("config: unknown problem type given in config")

    def load_gauss_config(self):
        """
        Special load method for the Gaussian model
        :return: bool
        """
        self["recon"]["radi"] = np.array(self["recon"]["radi"])
        self["problem"]["mean"] = np.array(self["problem"]["mean"])
        self["problem"]["var"] = np.array(self["problem"]["var"])
        model = GaussianModel(**self["problem"])

        self["recon"]["func"] = model.post
        self["model"] = model

        return True

    def load_poisson_config(self):
        """
        Special load method for the Gaussian model
        :return: bool
        """
        self["recon"]["radi"] = np.array(self["recon"]["radi"])
        model = PoissonModel(**self["problem"])

        self["recon"]["func"] = model.post
        self["model"] = model

        return True
    
    def load_upscaling_config(self):
        """
        Special load method for the upscaling model
        :return: bool
        """
        self["recon"]["radi"] = np.array(self["recon"]["radi"])
        model = ElasticityUpscaling()

        self["recon"]["func"] = model.post
        self["model"] = model

        return True

    @staticmethod
    def write_dummy_config_gauss(path):
        dim = 2
        problem_dict = {
            "mean": [0, 0],
            "var": [[1, 0], [0, 1]]
        }
        radi = np.linspace(0, 10, num=10, endpoint=True)
        n_circles = len(radi) + 1
        max_r_poly = 7

        recon_dict = {
            "radi"                  : radi.tolist(),  # type: list
            "maxdegs"               : [[max_r_poly] * (n_circles - 1)] + [1] * (dim - 1),  # type: list
            "func"                  : None,  # type: callable
            "sample_list"           : None,  # type: list or None
            "n_samples"             : 200,  # type: int or None
            "_check_onb"            : True,  # type: bool
            "progress"              : True,  # type: bool
            "adf_iteration"         : 1000,  # type: int
            "adf_targetresnorm"     : 1e-12,  # type: float
            "adf_minresnormdecrease": 0.9999,  # type: float
            "adf_verbose"           : True,  # type: bool
            "adf_maxrank"           : 10,  # type: int
            "adf_initrank"          : 4,  # type: int
            "force_recalc"          : False,  # type: bool
            "prec"                  : 50,
            "save_path"             : "test.dat",
            "use_radial_sampler"    : True,
            "backend"               : "ADF"

        }
        optimizer_dict = {
            "approximate": False,
            "log_optimizer": True,
            "use_difftool": False
        }
        inf = {
            "name": "gauss",
            "dim": dim,
            "min_z_volume": 1e-15,
            "max_r_poly": max_r_poly,
            "sample_path"   : "gauss_trafo_samples10d.dat",
            "sample_path_tt": "gauss_trafo_samples10d_tt.dat",
            "problem": problem_dict,
            "recon": recon_dict,
            "optimizer": optimizer_dict,
        }
        with open(path, "w") as f:
            json.dump(inf, f, indent=2)

    @staticmethod
    def write_dummy_config_poisson(path):
        dim = 2
        problem_dict = {
            "M": dim,
            "n_points": 9,
            "noise": 1e-6
        }
        radi = np.linspace(0, 10, num=10, endpoint=True)
        n_circles = len(radi) + 1
        max_r_poly = 7

        recon_dict = {
            "radi"                  : radi.tolist(),  # type: list
            "maxdegs"               : [[max_r_poly] * (n_circles - 1)] + [10] * (dim - 1),  # type: list
            "func"                  : None,  # type: callable
            "sample_list"           : None,  # type: list or None
            "n_samples"             : 1000,  # type: int or None
            "_check_onb"            : True,  # type: bool
            "progress"              : True,  # type: bool
            "adf_iteration"         : 1000,  # type: int
            "adf_targetresnorm"     : 1e-12,  # type: float
            "adf_minresnormdecrease": 0.9999,  # type: float
            "adf_verbose"           : True,  # type: bool
            "adf_maxrank"           : 10,  # type: int
            "adf_initrank"          : 4,  # type: int
            "force_recalc"          : False,  # type: bool
            "prec"                  : 50,
            "save_path"             : "test_poisson.dat",
            "use_radial_sampler"    : True,
            "backend"               : "ADF"

        }
        optimizer_dict = {
            "approximate": True,
            "log_optimizer": True,
            "use_difftool": False,
            "init_value": np.zeros(dim).tolist()
        }
        inf = {
            "name": "poisson",
            "dim": dim,
            "min_z_volume": 1e-15,
            "max_r_poly": max_r_poly,
            "sample_path"   : "poisson_trafo_samples10d.dat",
            "sample_path_tt": "poisson_trafo_samples10d_tt.dat",
            "problem": problem_dict,
            "recon": recon_dict,
            "optimizer": optimizer_dict,
        }
        with open(path, "w") as f:
            json.dump(inf, f, indent=2)