from __future__ import division, print_function, absolute_import
import numpy as np
from numpy.polynomial.polynomial import Polynomial
from alea.math_utils.tensor.extended_tt import BasisType
from alea.application.bayes.paper_radialtrafo.moments import moment_generator, moment_indices
from alea.application.bayes.paper_radialtrafo.basis import PwConstant as Basis
from alea.application.bayes.paper_radialtrafo.sampler import PolarSampler, UniformSampler, HashSampler
from alea.application.bayes.paper_radialtrafo.orthonormalize import OrthoPoly, check_onb
from alea.utils.progress.percentage import PercentageBar
from alea.math_utils.basis import PickableBase
from alea.math_utils.tensor.extended_tt import ExtendedTT
from scipy.stats import multivariate_normal
from scipy.special import gamma, gammaincc
from scipy.integrate import quadrature
# DO not include this. Somehow we ended up with a cross ref. That is bad style
# from alea.application.bayes.paper_radialtrafo.trafo_util import RadialTrafo
from alea.application.bayes.paper_radialtrafo.distances import (KL, Wasserstein)
import h5py
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler
from alea.math_utils.tensor.reconstruction import (ADFReconstruction, UQADFReconstruction, TorchTTReconstruction)
from alea.math_utils.basis import (PickableBase, NumpyPolynomial, FourierSinModes, FourierCosModes,
                                   ConstantFourierMode, ArbitraryPrecisionPolynomial)
from alea.application.bayes.paper_radialtrafo.polynomial import compute_orth_polynomials_radial
import json
import math
import matplotlib.pyplot as plt


def polar2cart(hatx):
    theta, r = hatx[1:], hatx[0]
    sins = np.sin(theta)
    coss = np.cos(theta)
    dim = len(hatx)

    mat = np.zeros((dim, dim - 1))
    if dim == 2:
        mat[0, :] = coss[0]
        mat[1, :] = sins[0]
    else:  # dim > 2
        mat[0, :] = np.concatenate([np.array([coss[0]]), sins[1:]])
        mat[1, :] = sins
        for d in range(2, dim):
            mat[d, :] = np.array([1.] * (d - 1) + [coss[d - 1]] + sins[d:].tolist())
    return r * np.prod(mat, axis=1)  # + self.M


def cart2polar(x):
    def __invpolar(X):
        """
        inverse polar w.r.t to the changed order of coordinates from
        https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates
        :param X:
        :return:
        """
        r = np.linalg.norm(X)

        norms = [np.linalg.norm(X[(i):]) for i in range(len(X) - 1)]

        phi = [math.acos(X[i] / norms[i]) if norms[i] != 0 else 0. for i in range(len(X) - 1)]

        if X[len(X) - 1] < 0:
            phi[-1] = 2 * np.pi - phi[-1]

        return np.array([r]), np.array(phi)
    _x = np.zeros(x.shape)
    a = x[0]
    _x[0] = x[1]
    _x[1] = a
    # revert
    _x = _x[::-1]
    r, theta = __invpolar(_x)
    return np.concatenate([r, theta[::-1]])



def fourier_modes(noB):

    n_modes = int(noB / 2)

    base_list = [ConstantFourierMode()]
    for k in range(n_modes):
        base_list.append(FourierSinModes(k + 1))
        base_list.append(FourierCosModes(k + 1))

    return PickableBase(base_list)


def generate_onb(
                 weight_list,
                 radi,
                 maxdegs,
                 _check_onb,
                 prec):
    """
    Computes the orthonormal basis on every circular disc.
    in r-coordinate: Take arbitrary precision polynomials which are orthonormal with respect to
                     the weight function w(r) = r^{n-1}.
                     This choice creates a blow up of the basis as r->0 but is the correct choice.
                     The basis is created on every circular disc `i` up to polynomial degree of `maxdegs[0][i]`
    in \theta_0:     Periodical Fourier modes are employed of maximal degree `maxdegs[1]` on [0, 2\pi]
    otherwise:       Orthonormal polynomials are created on [0, \pi] with respect to the weight function
                     w(\theta_i) = sin(\theta_i)^i (factor in determinant of spherical transformation)
    :param weight_list: List of weight functions
    :param radi: List of radi
    :param maxdegs: maximal degree. list of lists in first component, list otherwise
    :param _check_onb: flag to check if the basis is orthonormal (only applies for anglular basis)
    :param prec: precision to use in arbitrary precision polynomials
    :return: tuple(list[radi basis], angular basis]
    """
    def _create_poly_onb(f,
                         intlims,
                         maxdeg,
                         _check_onb=False
                         ):
        onb_poly = OrthoPoly(f, intlims=intlims)
        onb_poly.gen_poly(maxdeg)
        retval = []
        for lic in range(maxdeg):
            normalisation, err = quadrature(lambda x: Polynomial(onb_poly.poly[lic])(x)**2*f(x),
                                            intlims[0], intlims[1], maxiter=100)
            if err > 1e-5:
                print("WARNING: normalisation quadrature failed with error: {}".format(err))
            # print("norm {} = {}".format(lic, normalisation))
            curr_poly = NumpyPolynomial(onb_poly.poly[lic]/np.sqrt(normalisation))
            retval.append(curr_poly)
        if _check_onb:

            if not (check_onb(retval, intlims[0], intlims[1], f)):
                    print("WARNING non orthonormal basis")
        return PickableBase(retval)

    radi_onb = []
    angle_onb = []

    for lia, f in enumerate(weight_list):
        if lia == 0:
            for lib in range(len(radi) - 1):
                subdomain = [radi[lib], radi[lib + 1]]
                o_basis = compute_orth_polynomials_radial(maxdegs[lia][lib], subdomain, len(maxdegs), prec)

                radi_onb.append(PickableBase([ArbitraryPrecisionPolynomial(o_basis_.coef)
                                              for o_basis_ in o_basis]))
            continue
        if lia == 1:
            assert maxdegs[lia] % 2 == 1
            angle_onb.append(fourier_modes(maxdegs[lia]))
            continue
        angle_onb.append(_create_poly_onb(f, [0, np.pi], maxdegs[lia], _check_onb=_check_onb))
    return radi_onb, angle_onb


def reconstruct_tt_uq(
                      radi,  # type: list
                      maxdegs,  # type: list
                      func,  # type: callable
                      sample_list=None,  # type: list or None
                      n_samples=None,  # type: int or None
                      _check_onb=False,  # type: bool
                      progress=False,  # type: bool
                      adf_iteration=1000,  # type: int
                      adf_targetresnorm=1e-12,  # type: float
                      adf_minresnormdecrease=0.9,  # type: float
                      adf_verbose=True,  # type: bool
                      adf_initrank=5,  # type: int
                      adf_maxrank=10,  # type: int
                      prec=50,  # type: int
                      force_recalc=True,  # type: bool
                      save_path=None,
                      use_radial_sampler=True,
                      backend="ADF"
                      ):

    n_circles = len(radi) - 1
    M = len(maxdegs)


    weight_list = []
    SIN = lambda _k: lambda x: np.abs(np.sin(x) ** _k)
    for lia in range(M):
        if lia == 0:
            weight_list.append(lambda x: np.abs(x ** (M-1)))
            continue
        if lia == 1:
            weight_list.append(lambda x: 1)
            continue
        weight_list.append(SIN((lia - 1)))

    print(weight_list)
    ten_list = []
    radi_fun, angle_fun = generate_onb(weight_list, radi, maxdegs, _check_onb=_check_onb, prec=prec)

    sampler_subd = [[lb, ub] for lb, ub in zip(radi[:-1], radi[1:])]
    if use_radial_sampler:
        sampler = PolarSampler(dim=M, subdomains=sampler_subd)
    elif sample_list is not None:
        sampler = HashSampler(dim=M, subdomains=sampler_subd, hash=sample_list)
    else:
        sampler = UniformSampler(dim=M, subdomains=sampler_subd)

    for lia_circle, fr_list in enumerate(radi_fun):
        if progress:
            print("Run {}/{}".format(lia_circle + 1, n_circles))
        curr_sample_list = [sampler.generate(lia_circle) for _ in range(n_samples)]

        def target(theta):
            return func(theta)

        tensor_basis = [fr_list] + angle_fun

        if backend.upper() == "UQ_ADF":
            recon = UQADFReconstruction(adf_iteration, adf_targetresnorm, curr_sample_list, tensor_basis, target)
            result = recon.run(verbose=adf_verbose, non_uq=True)

        elif backend.upper() == "ADF":
            recon = ADFReconstruction(adf_iteration, adf_targetresnorm, curr_sample_list, tensor_basis, target)
            result = recon.run(None, adf_maxrank,
                               init_rank=adf_initrank,
                               min_resnorm_decrease=adf_minresnormdecrease,
                               verbose=adf_verbose)
        elif backend.upper() == "PYTORCH":
            recon = TorchTTReconstruction(adf_iteration, adf_targetresnorm, curr_sample_list, tensor_basis, target)
            max_deg = 0
            for lia in range(len(maxdegs)):
                if lia == 0:
                    for deg in maxdegs[0]:
                        if deg > max_deg: max_deg = deg
                else:
                    if maxdegs[lia] > max_deg: max_deg = maxdegs[lia]

            result = recon.run(max_deg, num_test=0, batch_size=int(len(curr_sample_list) // 10),
                               l2_reg=0.0, max_rank=adf_maxrank, max_epoch=1000,
                               learn_rate=1e-4, verbose=adf_verbose, device=None)
        else:
            raise ValueError("backend: {} not supported. use any of [UQ_ADF, ADF, PYTORCH]".format(backend))

        ten_list.append(result)

    return ten_list


class RadialTT:

    def __init__(self,
                 TTlist,                            # type: list
                 radi
                 ):
        self._TTlist = TTlist
        self._rad_list = radi
        self.equidistant_radi = True

        self._r0 = self._rad_list[0]
        assert (self._r0 == 0.)

        self._h = self._rad_list[1] - self._rad_list[0]

        self.dim = len(self._TTlist[0].n)
        for i in range(1, len(self._rad_list) - 1):
            if not np.abs(self._h - (self._rad_list[i + 1] - self._rad_list[i])) < 1e-10:
                self.equidistant_radi = False
                break
        self.moments = {}

        def tail_approximation(_pc, _cart=None):
            return 0
            # if _cart is not None:
            #     return multivariate_normal.pdf(_cart, mean=mean, cov=covariance)
            # else:
            #     return multivariate_normal.pdf(trafo.compute_trafo(_pc), mean=mean, cov=covariance)
        self.tail_approximation = tail_approximation
        self.mean_cache = None
        self.cov_cache = None
        self.cov_cache_shift = None
        self.Z_tt = 0
        self.Z_tail = 0

    def __call__(self,
                 pc,                                # type: list or np.array
                 cart=None,                         # type: None
                 normalised=True                    # type: bool
                 ):
        """
        call method that evaluates the radial TT representation
        :param pc: node to evaluate at in polar coordinates
        :param cart: Keine Ahnung was das soll
        :param normalised: flag to return the normalised result
        :return: float
        """
        # assert self.Z_tt > 0
        # assert self.Z_tail > 0
        if normalised:
            z = 1 / (self.Z_tt + self.Z_tail)
        r = pc[0]
        if not self.equidistant_radi:
            retval = -1
            for i in range(len(self._rad_list)-1):
                if self._rad_list[i] <= r <= self._rad_list[i+1]:
                    retval = self._TTlist[i](pc)
                    break
            if retval == -1:
                print("not equidistant and not in area")
                retval = self.tail_approximation(pc, _cart=cart)
            # return 1 / (self.Z_tail + self.Z_tail) * retval
            if normalised:
                return retval * z
            else:
                return retval
        i = int((r - self._r0) / self._h)
        if 0 <= i < len(self._TTlist):
            retval = self._TTlist[i](pc)
        else:
            retval = self.tail_approximation(pc, _cart=cart)
        # return 1/(self.Z_tail + self.Z_tail) * retval
        if normalised:
            return z*retval
        else:
            return retval

    def set_normalisation(self, Z_tt):
        # assert Z_tt > 0
        self.Z_tt = Z_tt

        def int_sin(k):
            if k == 1:
                return 2
            if k == 2:
                return 0.5*np.pi
            return (k-1)/k * int_sin(k-2)

        # c = (2*np.pi)**(-0.5*self.dim) * np.sqrt(np.linalg.det(np.linalg.inv(-self.trafo.hess)))**(-1)
        # c *= 2*np.pi
        # c *= np.prod([int_sin(lia) for lia in range(1, self.dim-2)])
        # T = self.radi[-1]
        # int_r = 2**(self.dim/2 - 1) * T**self.dim * (T**2)**(-self.dim/2) * (gamma(self.dim/2) - gamma(self.dim/2)*gammaincc(self.dim/2, T**2/2))
        self.Z_tail = 0 # 1 - c*int_r
        for l in range(len(self._TTlist)):
            self._TTlist[l] *= 1/(self.Z_tail + self.Z_tt)


    # region save
    def save(self, path):
        try:
            with h5py.File(path, "w") as outfile:
                for lit, ten in enumerate(self._TTlist):
                    for lia in range(len(ten.components)):
                        outfile["ten{}core{}".format(lit, lia)] = ten.components[lia]
                        assert isinstance(ten.basis[lia], PickableBase)
                        outfile["ten{}basis{}".format(lit, lia)] = ten.basis[lia].to_hash()
                    outfile["ten{}len".format(lit)] = len(ten.components)
                outfile["len"] = len(self._TTlist)
        except Exception as ex:
            print(ex)
            return False
        return True
    # endregion

    # region load
    @staticmethod
    def load(path):
        ten_list = []
        try:
            with h5py.File(path, "r") as outfile:
                for lit in range(int(outfile["len"][()])):
                    newcores = []
                    newbasis = []
                    for lia in range(int(outfile["ten{}len".format(lit)][()])):
                        try:
                            newcores.append(np.array(outfile["ten{}core{}".format(lit, lia)][()]))
                            newbasis.append(PickableBase.from_hash(str(outfile["ten{}basis{}".format(lit, lia)][()])))
                        except IndexError as ex:
                            print("load exception")
                            return None
                    ten_list.append(ExtendedTT(newcores, basis=newbasis))
        except IOError as ex:
            print("IOERROR can not import from file " + path + " err: ")
            return None
        except EOFError as ex:
            print("EOFERROR can not import from file " + path + " err: ")
            return None
        except KeyError as ex:
            print("KEYERROR can not import from file " + path + " err: ")
            return None

        return ten_list

    def compute_Z(self):
        angle_onb = self._TTlist[0].basis[1:]
        print(angle_onb)
        radi_onb = []
        for ten in self._TTlist:
            radi_onb.append(ten.basis[0])
        norm_list = []
        for f in angle_onb:
            norm_list.append(f[0](0))
        Z_tt = 0
        Z_list = []
        for lia, ten in enumerate(self._TTlist):
            buf_tt = ExtendedTT(ten.components, basis=[BasisType.points] * self.dim)
            # Z_k = -np.exp(-0.5*radi[lia+1]**2) + np.exp(-0.5*radi[lia]**2)
            Z_tt_k = buf_tt([0] * self.dim) / (radi_onb[lia][0](0) * np.prod(norm_list))
            Z_list.append(Z_tt_k)
            Z_tt += Z_tt_k
        # print("Z={} vs Z_tt={}".format(Z, Z_tt))
        self.Z_list = Z_list
        self.Z = Z_tt
        return Z_list, Z_tt

    def dofs(self):
        dofs = 0
        for ten in self._TTlist:
            assert isinstance(ten, ExtendedTT)
            dofs += ten.dofs()
        return dofs

    def average_r(self):
        retval = 0
        for ten in self._TTlist:
            assert isinstance(ten, ExtendedTT)
            retval += np.sum([r for r in ten.r]) / (ten.dim-1)
        return retval / len(self._TTlist)
