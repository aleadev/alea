from __future__ import division
import unittest
from alea.utils.testing import *
from scipy.stats import multivariate_normal
from alea.application.bayes.paper_radialtrafo.distances import KL, Wasserstein

import time
import numpy as np

np.random.seed(80190)


class TestDistances(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()
        self.n_samples = 1000

    def tearDown(self):
        t = time.time() - self.startTime
        print("{}: {}".format(self.id(), t))

    def test_kl_distance(self):
        dim = 10
        if False:
            print("Test Gaussian noise reduction.")
            print("Target is centered i.i.d. Gauss in {} dimension with noise 1e-4.".format(dim))
            print("  reduce noise and observe KL distance")
            for noise in [10, 20, 50, 100, 1000, 5000, 10000]:
                rho1 = lambda x: multivariate_normal.pdf(x, mean=np.zeros(dim), cov=np.eye(dim, dim) * 1e-4)
                rho2 = lambda x: multivariate_normal.pdf(x, mean=np.zeros(dim), cov=np.eye(dim, dim) / noise)
                dist = KL(rho1, rho2, dim,
                          n_samples=1000,
                          n_burn_in=100,
                          n_walkers=dim*2 + 2,
                          p0=np.random.randn(dim*2+2, dim)*1e-4)
                print("KL distance of {} noise vs 1e-4 noise: {:.6f}".format(1/noise, dist.compute(100)))
        if False:
            print("Test Gaussian mean move.")
            print("Target is i.i.d. unit variance Gauss in {} dimension with mean = [100]*d.".format(dim))
            print("  reduce distance to [100]*d and observe KL distance")
            for mean in [10, 20, 50, 75, 99, 99.99, 100]:
                rho1 = lambda x: multivariate_normal.pdf(x, mean=np.ones(dim)*100, cov=np.eye(dim, dim))
                rho2 = lambda x: multivariate_normal.pdf(x, mean=np.ones(dim)*mean, cov=np.eye(dim, dim))
                dist = KL(rho1, rho2, dim,
                          n_samples=1000,
                          n_burn_in=100,
                          n_walkers=dim*2 + 2,
                          p0=np.random.randn(dim*2+2, dim) + 100)
                print("KL distance of [{}]*d mean vs [100]*d mean: {:.6f}".format(mean, dist.compute(1000)))

    def test_wasserstein_distance(self):
        dim = 2
        if False:
            print("Test Gaussian noise reduction.")
            print("Target is centered i.i.d. Gauss in {} dimension with noise 1e-4.".format(dim))
            print("  reduce noise and observe Wasserstein distance using Sinkhorn")
            for noise in [10, 20, 50, 100, 1000, 5000, 10000]:
                rho1 = lambda x: multivariate_normal.pdf(x, mean=np.zeros(dim), cov=np.eye(dim, dim) * 1e-4)
                rho2 = lambda x: multivariate_normal.pdf(x, mean=np.zeros(dim), cov=np.eye(dim, dim) / noise)
                dist = Wasserstein(rho1, rho2, dim,
                                   n_samples=1000,
                                   n_burn_in=100,
                                   n_walkers=dim*2 + 2,
                                   p0=np.random.randn(dim*2+2, dim)*1e-4,
                                   p0_2=np.random.randn(dim*2+2, dim)*1/noise)
                print("Wasserstein distance of {} noise vs 1e-4 noise: {:.6f}".format(1/noise, dist.compute(100)))
                # dist.visualize("noise{}".format(noise))
        if True:
            print("Test Gaussian mean move.")
            print("Target is i.i.d. unit variance Gauss in {} dimension with mean = [100]*d.".format(dim))
            print("  reduce distance to [100]*d and observe Wasserstein distance using Sinkhorn")
            for mean in [10, 20, 50, 75, 99, 99.99, 100]:
                rho1 = lambda x: multivariate_normal.pdf(x, mean=np.ones(dim)*100, cov=np.eye(dim, dim))
                rho2 = lambda x: multivariate_normal.pdf(x, mean=np.ones(dim)*mean, cov=np.eye(dim, dim))
                dist = Wasserstein(rho1, rho2, dim,
                                   n_samples=1000,
                                   n_burn_in=100,
                                   n_walkers=dim*2 + 2,
                                   p0=np.random.randn(dim*2+2, dim) + 100,
                                   p0_2=np.random.randn(dim * 2 + 2, dim) + mean)
                print("Wasserstein distance of [{}]*d mean vs [100]*d mean: {:.6f}".format(mean, dist.compute(1000)))
                # dist.visualize("mean{}".format(mean))


if __name__ == '__main__':
    print("#" * 20)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestDistances)
    unittest.TextTestRunner(verbosity=0).run(suite)

