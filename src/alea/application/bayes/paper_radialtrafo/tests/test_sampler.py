from __future__ import division
import unittest
from alea.utils.testing import *
from alea.application.bayes.paper_radialtrafo.sampler import PolarSampler, UniformSampler

import time
import numpy as np
from numpy.linalg import norm

np.random.seed(80190)


class TestSampler(unittest.TestCase):

    def setUp(self):
        self.startTime = time.time()
        self.n_samples = 1000

    def tearDown(self):
        t = time.time() - self.startTime
        print("{}: {}".format(self.id(), t))

    def test_radial_sampler(self):
        M = 2
        radi = np.linspace(0, 10, num=5)
        ref_samples = np.load("radial_samples.npy")
        sampler_radi = [[lb, ub] for lb, ub in zip(radi[:-1], radi[1:])]
        samples = np.zeros((M, len(sampler_radi), self.n_samples))
        sampler = PolarSampler(dim=M, subdomains=sampler_radi)
        for lic in range(len(sampler_radi)):
            for lia in range(self.n_samples):
                samples[:, lic, lia] = sampler.generate(lic)
                # print("circle: {}, sample: {} - {}".format(lic, lia, samples[:, lic, lia]))
        if False:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            for lic in range(len(sampler_radi)):
                plt.scatter(samples[0, lic, :], samples[1, lic, :], s=1)
            fig.savefig("polar_samples.pdf")
            np.save("radial_samples.npy", samples)
        self.assertListEqual(list(samples.shape), list(ref_samples.shape), msg="ref sampler: sample shape wrong")
        self.assertLessEqual(np.linalg.norm(samples - ref_samples), 1e-10,
                             msg="radial sampler: samples wrong")

    def test_uniform_sampler(self):
        M = 2
        radi = np.linspace(0, 10, num=5)

        sampler_radi = [[lb, ub] for lb, ub in zip(radi[:-1], radi[1:])]
        ref_samples = np.load("uniform_samples.npy")
        samples = np.zeros((M, len(sampler_radi), self.n_samples))
        sampler = UniformSampler(dim=M, subdomains=sampler_radi)
        for lic in range(len(sampler_radi)):
            for lia in range(self.n_samples):
                samples[:, lic, lia] = sampler.generate(lic)
                # print("circle: {}, sample: {} - {}".format(lic, lia, samples[:, lic, lia]))
        if False:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            for lic in range(len(sampler_radi)):
                plt.scatter(samples[0, lic, :], samples[1, lic, :], s=1)
            fig.savefig("uniform_samples.pdf")
            np.save("uniform_samples.npy", samples)

        self.assertListEqual(list(samples.shape), list(ref_samples.shape), msg="uniform sampler: sample shape wrong")
        self.assertLessEqual(np.linalg.norm(samples - ref_samples), 1e-10,
                             msg="uniform sampler: samples wrong")


if __name__ == '__main__':
    print("#" * 20)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSampler)
    unittest.TextTestRunner(verbosity=0).run(suite)

