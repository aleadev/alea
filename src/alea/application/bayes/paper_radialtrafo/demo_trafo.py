from __future__ import (division, print_function, absolute_import)
import sys
import numpy as np
from alea.application.bayes.paper_radialtrafo.trafo_util import (plot_onb, plot_tt_trafo)
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
from alea.application.bayes.paper_radialtrafo.config import RadialTrafoConfig as Config
import os
if sys.version_info < (3, 0):
    from dolfin import (set_log_level, WARNING)
    set_log_level(WARNING)
else:
    from dolfin import (set_log_level, LogLevel)
    set_log_level(LogLevel.ERROR)

np.random.seed(8011990)

config_file_path = "poisson_conf.json"
if not os.path.isfile(config_file_path):
    # Config.write_dummy_config_gauss(config_file_path)
    Config.write_dummy_config_poisson(config_file_path)

config = Config(config_file_path)

if not config.load():
    raise ValueError("something went wrong in the configuration")

problem = config["model"]

# TODO: use heuristical a priori adaptivity to chose radi

print("#"*20)
print("compute MAP")
trafo = problem.compute_map(**config["optimizer"])
print("#"*20)
print("log: {}, difftool: {}".format(config["optimizer"]["log_optimizer"], config["optimizer"]["use_difftool"]))
print("  MAP = {}".format(trafo.map))
print("  hessian: \n{}".format(trafo.hess))
### ###

if not os.path.exists(config["sample_path_prior"]):
    print("create MCMC samples for error sampling later from true posterior")

    def prior(x):
        """
        :param x: sample
        :return: transported sample
        """
        f = config["model"].prior
        return f(x)

    trafo.create_trafo_samples(10000, config["sample_path_prior"], prior)

print("compute Z approx")
import json
from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler
with open(config["sample_path_prior"], "r") as f:
    inf = json.load(f)
sampler = MCMC_Sampler(log_prob=None,
                       N=None,
                       burn_in=None,
                       ndim=None,
                       nwalkers=None,
                       p0=None,
                       samples=inf
                       )
# Z_true = 1/10000*np.sum([np.exp(-config["model"].Phi(s)) for s in sampler.get(10000)], axis=0)
Z_true = 0.007681517904272642
if not os.path.exists(config["sample_path"]):
    print("create MCMC samples for error sampling later from true posterior")

    def perturbed_prior(x):
        """
        \tilde f_0 = f \circ \tilde T \|det J_T\|
        where \tilde T is the approximated transport, here in the affine case
        \tilde T = H^\frac{1}{2} \cdot + M
        :param x: sample
        :return: transported sample
        """
        f = config["recon"]["func"]
        tilde_T = trafo.transport_trafo
        return f(tilde_T(x)) * trafo.det_hessian()

    trafo.create_trafo_samples(1000, config["sample_path"], perturbed_prior)
plot_onb(trafo, config["recon"]["maxdegs"], config["recon"]["radi"])

ten_list = trafo.reconstruct_tt_uq(**config["recon"])
print("Tensor reconstruction result")
print(" Dimensions: {}".format([t.n for t in ten_list]))
print(" Ranks: {}".format([t.r for t in ten_list]))

rad_tt = RadialTT(ten_list, trafo)


print("Compute Z with TT")
Z_list, Z_tt = trafo.compute_Z()
rad_tt.set_normalisation(Z_tt)
for lia, Z in enumerate(Z_list):
    print("circular disc [{}, {}] Z_TT = {} / {}".format(config["recon"]["radi"][lia], config["recon"]["radi"][lia + 1],
                                                         Z, Z_tt))

# plot_bbox(post, trafo, max_r, n_circles, lower_limit, upper_limit)

print("mean:\n  {}".format(rad_tt.mean()))
print("cov:\n  {}".format(rad_tt.covariance()))
print("Trafo MAP: {}".format(trafo.map))
print("total number of forward evaluations: {}".format(problem.post.num_calls))
print(Z_true)
print(Z_tt)
if config["dim"] == 2 and False:
    rad_tt.plot_x_post("posterior_x.png", offset=0.4, num=25)

if config["dim"] == 2 and False:
    plot_tt_trafo(ten_list, trafo.radi, 25, problem.post, trafo)

kl_dist = rad_tt.kl_distance(1000, config["sample_path"], Z_true, Z_tt)
print("KL distance:", kl_dist)
exit()
if not os.path.exists(config["sample_path_tt"]) or True:
    rad_tt.create_trafo_samples(1000, config["sample_path_tt"])

was_dist = rad_tt.wasserstein_distance(1000, config["sample_path"], config["sample_path_tt"],
                                       sinkhorn_eps=0.01)
print("Wasserstein distance:", was_dist)

rad_tt.plot_posterior_diff("Dimension {}, max r-poly {}".format(config["dim"], config["max_r_poly"]),
                          path="tmp/plot_r_true.pdf")

# rad_tt.compute_marginals2(config["min_z_volume"], Z_tt, samples_per_interval=20, n_base_fun=20)
# TODO: normalize correctly
# TODO: test with different distances
