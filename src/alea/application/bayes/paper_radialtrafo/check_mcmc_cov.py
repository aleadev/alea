from __future__ import (division, print_function, absolute_import)
import sys
import numpy as np
from alea.application.bayes.paper_radialtrafo.radial_tt import RadialTT
from alea.application.bayes.paper_radialtrafo.config import RadialTrafoConfig as Config
import time
import matplotlib.pyplot as plt
import json

import os

if sys.version_info < (3, 0):
    from dolfin import (set_log_level, WARNING)

    set_log_level(WARNING)
else:
    from dolfin import (set_log_level, LogLevel)

    set_log_level(LogLevel.ERROR)

np.random.seed(8011990)
problem_type = "gaussian"

config_file_path = "{}_conf.json".format(problem_type)
d = 3
var = 1e-7
mean = 1
mc_ref = 10000

config = Config(config_file_path)
config["dim"] = d
config["problem"]["mean"] = np.ones(d) * mean
config["problem"]["var"] = np.eye(d, d) * var

config["optimizer"]["approximate"] = False
config["optimizer"]["use_difftool"] = True
config["optimizer"]["log_optimizer"] = True
config["sample_path"] = "{}_sample_d{}.dat".format(problem_type, d)
config["sample_path_tt"] = "{}_sample_d{}_tt.dat".format(problem_type, d, len(config["recon"]["radi"]))
config["sample_path_prior"] = "numerical_examples_radi/{}_sample_prior_d{}.dat".format(problem_type, d)

if not config.load():
    raise ValueError("something went wrong in the configuration")
problem = config["model"]


print("#" * 20)
print("compute MAP")
trafo = problem.compute_map(**config["optimizer"])
print("#" * 20)
print("log: {}, difftool: {}".format(config["optimizer"]["log_optimizer"], config["optimizer"]["use_difftool"]))
print("  MAP = {}".format(trafo.map))
print("  hessian: \n{}".format(trafo.hess))
optimizer_calls = config["model"].post.num_calls


def posterior(x):
    return (config["recon"]["func"](x))


posterior_sample_path = "{}_reference_samples_d{}.dat".format(problem_type, d)
if not os.path.exists(posterior_sample_path):
    print("create MCMC samples for error sampling later from true posterior")

    trafo.create_trafo_samples(mc_ref, posterior_sample_path, posterior)


from alea.application.bayes.paper_radialtrafo.sampler import MCMC_Sampler

with open(posterior_sample_path, "r") as f:
    inf = json.load(f)
sampler = MCMC_Sampler(log_prob=None,
                       N=None,
                       burn_in=None,
                       ndim=None,
                       nwalkers=None,
                       p0=None,
                       samples=inf
                       )

from alea.utils.corner import corner
fig = corner(sampler.get(mc_ref))
fig.savefig("poisson_reference_d{}.pdf".format(d))

samples = np.array(sampler.get(mc_ref))
mean_true = np.mean(samples, axis=0)
mean_approx = trafo.map
print(mean_true)
print(mean_approx)
print(np.linalg.norm(mean_true - mean_approx)/np.linalg.norm(mean_true))

cov_true = np.cov(samples.T, bias=True)
cov_approx = np.linalg.inv(-trafo.hess)
print("cov_true:")
print(cov_true)
print("cov_approx:")
print(cov_approx)
print("cov_true / cov_approx:")
print(cov_true / cov_approx)

print("relative error:")
print(np.linalg.norm(cov_true - cov_approx)/np.linalg.norm(cov_true))