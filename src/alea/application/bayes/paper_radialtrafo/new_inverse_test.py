import numpy as np
from scipy.optimize import minimize


import math

def polar(xhat):

    r = xhat[0]

    x1 = r * np.cos(xhat[1])
    x2 = r * np.sin(xhat[1]) * np.cos(xhat[2])
    x3 = r * np.sin(xhat[1]) * np.sin(xhat[2])

    return [x1,x2,x3]


def inv_reverted_polar(x):

    r = np.linalg.norm(x)

    norms = [ np.linalg.norm(x[(i):]) for i in range(len(x)-1)]

    #print("norms ", norms)


    #ref =  np.array([r, math.acos(x[0] / np.sqrt(x[2]**2+x[1]**2+x[0]**2)),
    #                    math.acos(x[1] / np.sqrt(x[2]**2+x[1]**2))])
    #print("ref = ", ref)


    phi =  [math.acos( x[i] / norms[i]) if norms[i] != 0 else 0. for i in range(len(x)-1) ]

    if x[len(x)-1] < 0:
        phi[-1] = 2*np.pi - phi[-1]

    #v = math.acos(x[len(x)-2] / norms[len(x)-2]) if norms[len(x)-2] != 0 else 0.

    #phi +=  [v] if x[-1] >= 0 else [2*np.pi-v]

    #res = [r]+phi
    #print("res = ", np.array(res))


    return np.array([r]), np.array(phi)


def inv_trafo(x, invH, M):


    X = invH.dot(x-M)

    #print("X      = ", X)

    #r,phi = inv_reverted_polar(X)
    #res = np.concatenate([r,phi])
    #print(" inv ( X ) ", res)

    a = X[0]
    X[0] = X[1]
    X[1] = a
    X = X[::-1]


    #print("rev X  = ", X)
    r, phi = inv_reverted_polar(X)
    res = np.concatenate([r, phi[::-1]])
    print("inv (rev X) ", res)



    #print("swap X = ", X)
    #rphi = inv_reverted_polar(X)
    #res = np.concatenate([r, phi])
    #print(" inv (swapped inv X ) ", rphi)

    exit()

    r, phi_rev = inv_reverted_polar(X)

    rphi = np.concatenate([r, phi_rev[::-1]])

    return rphi



class Trafo():
    """

    Trafo.fun(xhat)  = H * polartrafo(xhat) + M
    Trafo.invfun(x)  =    polartrafo^{-1}(inv(H)(x-M))

    """
    def __init__(self, dim, H,M):

        self.dim = dim
        # TODO assert self.dim = len(M),  and dimension of H
        # replace with decomposition  and inverse over eigenvalues
        self.H = H
        self.invH = np.linalg.inv(H)

        self.M = M

    def fun(self, xhat):
        theta, r = xhat[1:], xhat[0]
        sins = np.sin(theta)
        coss = np.cos(theta)

        mat = np.zeros((self.dim, self.dim - 1))
        if self.dim == 2:
            mat[0, :] = coss[0]
            mat[1, :] = sins[0]
        else:  # dim > 2
            mat[0, :] = np.concatenate([np.array([coss[0]]), sins[1:]])
            mat[1, :] = sins
            for d in range(2, self.dim):
                mat[d, :] = np.array([1.] * (d - 1) + [coss[d - 1]] + sins[d:].tolist())

        res = r * np.prod(mat, axis=1)  # + self.M

        res = self.H.dot(res) + self.M
        return res


    def invfun(self, x):

        X = self.invH.dot(x - self.M)

        # swap x0 and x1
        a = X[0]
        X[0] = X[1]
        X[1] = a
        # revert
        X = X[::-1]

        # apply inverse polarcoordinates in the changed coords
        r, phi = self.__invpolar(X)
        # reverse angles phi
        return np.concatenate([r, phi[::-1]])

    def __invpolar(self,X):
        """
        inverse polar w.r.t to the changed order of coordinates from
        https://en.wikipedia.org/wiki/N-sphere#Spherical_coordinates
        :param X:
        :return:
        """
        r = np.linalg.norm(X)

        norms = [np.linalg.norm(X[(i):]) for i in range(len(X) - 1)]

        phi = [math.acos(X[i] / norms[i]) if norms[i] != 0 else 0. for i in range(len(X) - 1)]

        if X[len(X) - 1] < 0:
            phi[-1] = 2 * np.pi - phi[-1]

        return np.array([r]), np.array(phi)


class testinv():
    def __init__(self, dim):
        self.dim = dim

        self.M = np.zeros((dim, 1))
        self.map = 1.*np.ones(dim)
        self.hess = np.eye(dim, dim) * 1e-2
        self.u, self.sigma, self.v = np.linalg.svd(self.hess, full_matrices=False)
        self.h12 = self.u.dot(np.diag(self.sigma ** (-0.5)).dot(self.v))

    def fun(self, hatx, x = None):
        theta, r = hatx[1:], hatx[0]
        sins = np.sin(theta)
        coss = np.cos(theta)

        mat = np.zeros((self.dim, self.dim - 1))
        if self.dim == 2:
            mat[0, :] = coss[0]
            mat[1, :] = sins[0]
        else:  # dim > 2
            mat[0, :] = np.concatenate([np.array([coss[0]]), sins[1:]])
            mat[1, :] = sins
            for d in range(2, self.dim):
                mat[d, :] =  np.array( [1.] * (d - 1) + [coss[d-1]] + sins[d:].tolist())

        res = r * np.prod(mat, axis = 1) #+ self.M


        res = self.h12.dot(res) + self.map
        if x is not None:
            #print("x.shape ", x.shape)
            ress = np.linalg.norm(res - x)**2
            print("ress = ", ress)
            return ress
        else:
            #print("return the vector", res)
            return res

def main():

    dim = 10

    M = 0. * np.ones(dim)
    H = np.eye(dim, dim)  # * 1e-2

    Psi = Trafo(dim = dim, H = H, M = M)


    for i in range(1):

        r = (5 * np.random.rand(1)).tolist()
        phi0 = (2 * np.pi * np.random.rand(1)).tolist()
        phi_else = (np.pi * np.random.rand(dim - 2)).tolist() if dim > 2 else []

        xhat =  r + phi0 + phi_else
        xhat = np.array(xhat)

        x = Psi.fun(xhat)
        xhat_2 = Psi.invfun(x)

        if np.linalg.norm(xhat - xhat_2) > -1:#1e-10:

            print("            xhat = ", xhat)

            print("Psi(xhat) =  x   = ", x)

            print("invPsi(x) = xhat = ", xhat_2)


main()