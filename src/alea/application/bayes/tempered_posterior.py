from __future__ import (division, print_function)
import numpy as np
from dolfin import (FunctionSpace, UnitIntervalMesh, set_log_level, WARNING)
from multiprocessing import (cpu_count, Pool)
from alea.math_utils.tensor.libxerus.reconstruction_util import reconstruct_project

global_fun = None
set_log_level(WARNING)

def parallel_func(arr):
    _func = global_fun
    return _func(arr)


class TemperedPosterior(object):
    """
    A simple wrapper around a potential function to represent the tempering process,
    which introduces a weight for the natural Bayesian noise to ease the concentration
    effect of the Bayesian posterior density.
    """
    def __init__(self,
                 potential,                         # type: callable
                 beta                               # type: float or list
                 ):                                 # type: (...) -> None
        """
        Constructor function
        :param potential: callable function that takes a sample and returns the value of the Bayesian potential \Phi(y)
        :param beta: tempering factor or list of tempering factors
        """
        self.potential = potential
        self.beta = beta
        if isinstance(beta, list) or isinstance(beta, np.ndarray):
            self.constant = False
        else:
            self.constant = True

    def sample(self,
               y                                    # type: list or np.ndarray
               ):                                   # type: (...) -> float
        """
        Sample function to generate \Phi(y) for some sample y.
        :param y: sample (list)
        :return: function evaluation
        """
        assert self.constant is True                # atm we only consider constant tempering
        return np.exp(-self.beta*self.potential(y))

    def reconstruct_tt(self,
                       n_samples,                   # type: int
                       M,
                       distribution,                # type: callable
                       new_fs=None,                 # type: FunctionSpace or None
                       adf_tol=1e-8,                # type: float
                       adf_iter=10000,              # type: float
                       _poly_dim=5,                 # type: int
                       use_rb=False,                # type: bool
                       ew_tol=1e-16,                # type: float
                       poly_sys="L"                 # type: str
                       ):

        global global_fun
        global_fun = np.random.randn

        recon_dict = {
            "new_fs"   : new_fs,
            "adf_tol"  : adf_tol,
            "adf_iter" : adf_iter,
            "_poly_dim": _poly_dim,
            "use_rb"   : use_rb,
            "ew_tol"   : ew_tol,
            "poly_sys" : poly_sys
        }

        p = Pool(processes=cpu_count())
        samples = np.array(p.map(parallel_func, [M for _ in range(n_samples)]))
        p.close()
        p.join()

        global_fun = self.sample
        p = Pool(processes=cpu_count())
        measurements = np.array(p.map(parallel_func, [samples[lia, :] for lia in range(n_samples)]))
        p.close()
        p.join()

        measurements = np.reshape(measurements, (1, n_samples))
        samples = samples.tolist()

        non_mesh = UnitIntervalMesh(1)
        non_fs = FunctionSpace(non_mesh, 'DG', 0)

        tensor = reconstruct_project(measurements, samples, non_fs, **recon_dict)
        from alea.math_utils.tensor.extended_tt import ExtendedTT
        tensor = ExtendedTT(tensor.components, tensor.basis)
        tensor = tensor.multiply_with_extendedTT(tensor)
        tensor.round()
        tensor = ExtendedTT(tensor.components[1:], tensor.basis[1:])
        tensor.normalise()
        return tensor
