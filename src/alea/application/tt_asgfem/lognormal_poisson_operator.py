from alea.math_utils.param_pde.lognormal_field import (TTLognormalSemidiscreteField,
                                                       TTLognormalFullyDiscreteField)
from alea.math_utils.param_pde.affine_field import AffineField
from dolfin import (FunctionSpace, Function, Expression, DirichletBC, project, Constant)
from alea.application.tt_asgfem.asgfem_util import (compute_fe_matrices, generate_operator_tt)
from alea.math_utils.tensor.smatrix import smatrix
from alea.math_utils.tensor.solver.xerus_als import XerusALS
import xerus as xe
from alea.math_utils.tensor.solver.tt_sparse_als import TTSparseALS
from alea.math_utils.tensor.extended_fem_tt import (ExtendedFEMTT, BasisType)
import tt
import scipy.sparse as sps
import numpy as np


class LognormalPoissonOperator(object):
    # region init
    def __init__(self,
                 coef,                              # type: AffineField
                 coef_degs,                         # type: list
                 coef_ranks,                        # type: list
                 rhs,                               # type: Function or Expression or Constant
                 fs,                                # type: FunctionSpace
                 bc,                                # type: DirichletBC
                 degs,                              # type: list
                 empty=False,                       # type: bool
                 mesh_dofs=100000,                  # type: int
                 quad_degree=7,                     # type: int
                 theta=0.5,                         # type: float
                 rho=1,                             # type: float
                 acc=1e-10,                         # type: float
                 scale=1.0,                         # type: float
                 domain='square'                    # type: str
                 ):
        """
        constructor
        :param coef: continuous coefficient
        :param fs: function space containing the mesh and finite element discretisation
        """
        self.cont_coef = coef
        if isinstance(rhs, Expression) or isinstance(rhs, Constant):
            self.cont_rhs = project(rhs, fs)
        else:
            self.cont_rhs = rhs
        self.fs = fs
        self.bc = bc
        self.affine_coef = coef
        self.semi_discrete_coef = TTLognormalSemidiscreteField(coef, coef_degs, coef_ranks, fs,
                                                               mesh_dofs, quad_degree, theta, rho, acc, scale, domain)
        self.discrete_coef = TTLognormalFullyDiscreteField(self.semi_discrete_coef)
        self.discrete_coef = ExtendedFEMTT(self.discrete_coef.discrete_coef_cores, [BasisType.points] +
                                           [BasisType.NormalisedHermite]*len(coef_degs), fs)

        # TODO: accept extended TT or Lognormal Field or affine field in Hermite polynomials
        self.discrete_rhs = None
        self.degs = degs

        self.ranks = None
        self.iterations = None
        self.tolerance = None
        self.preconditioner = None

        self.tt_bilinearform = None
        self.tt_linearform = None
        self.solution = None

        self.theta_x = None
        self.theta_y = None
        self.resnorm_zeta_weight = None
        self.eta_zeta_weight = None
        self.rank_always = None
        self.sol_rank = None
        self.start_rank = None
        self.new_hdeg = None

        self.new_solution = None
        self.new_mesh = None
        self.new_hdegs = None
        self.new_ranks = None
        self.eta_global = None
        self.zeta_global = None
        self.resnorm = None

        self.solver = None

        self.det_estimator_result = None
        self.res_estimator_result = None
        self.sto_estimator_result = None

        self.solved = False
        self.computed = False
        self.refined = False

        self.computed = False

        if empty is False:
            self._create_discrete_operator()

    # endregion

    # region private def create discrete operator
    def _create_discrete_operator(self):

        fe_matrix_options = {
            "coef": self.discrete_coef.components[0][0, :, :],
            "fs": self.fs,
            "bc": self.bc
        }
        if self.discrete_rhs is not None:
            fe_matrix_options["rhs"] = self.discrete_rhs.components[0][0, :, :]
        else:
            fe_matrix_options["rhs"] = self.cont_rhs

        #                                           # compute FE matrices for piecewise constant coefficients
        fe_matrices = compute_fe_matrices(**fe_matrix_options)

        n_coeff = len(self.degs)
        if n_coeff > self.discrete_coef.dim - 1:
            raise ValueError("Dimension of solution exhibits dimension of coefficient -> exit()")

        opcores = generate_operator_tt(self.discrete_coef.components[1:],
                                       self.discrete_coef.r, self.degs, len(self.degs))

        opcores[0] = []                             # re_init first operator core (it was None anyway)
        # A = None
        for r0, a in enumerate(fe_matrices["A"]):
            opcores[0].append(a)

        # generate lognormal operator tensor

        a = smatrix(opcores)

        # bcdense = np.zeros([D, D])
        # bcdense[bc_dofs, bc_dofs] = 1
        bcopcores = (n_coeff+1) * [[]]
        bcsparse_direct = sps.csr_matrix(fe_matrices["A"][0])
        bcsparse_direct.data = np.zeros(len(bcsparse_direct.data))
        bcsparse_direct[fe_matrices["bc_dofs"], fe_matrices["bc_dofs"]] = 1
        bcopcores[0] = []
        bcopcores[0].append(bcsparse_direct)
        for i in range(n_coeff):
            # noinspection PyTypeChecker
            bcopcores[i + 1] = np.reshape(np.eye(opcores[i + 1].shape[1], opcores[i + 1].shape[2]),
                                          [1, opcores[i + 1].shape[1], opcores[i + 1].shape[2], 1], order='F')
        bcop = smatrix(bcopcores)

        a += bcop
        # a = a.round(1e-16)                          # ! important: do not cut ranks here. Just orthogonalize cores[1:]

        self.tt_bilinearform = a

        b = np.array(fe_matrices["B"])
        if self.discrete_rhs is not None:
            # in the TT case, the linearform is given by the rhs itself + assembled first component.
            # reason is the orthonormality of the polynomials
            self.tt_linearform = self.discrete_rhs.copy()
            self.tt_linearform.components[0] = np.reshape(b, (1, b.shape[0], b.shape[1]))
            self.tt_linearform.n[0] = b.shape[0]
            self.tt_linearform.r[1] = b.shape[1]
        else:
            self.tt_linearform = np.reshape(b, [1, self.tt_bilinearform.n[0], 1], order='F')
            # noinspection PyTypeChecker
            self.tt_linearform = [self.tt_linearform] + [[]]*len(self.degs)
            for i in range(len(self.degs)):
                self.tt_linearform[i + 1] = np.reshape(np.eye(self.tt_bilinearform.n[i + 1], 1),
                                                       [1, self.tt_bilinearform.n[i + 1], 1])

        self.computed = True

    # endregion

    # region def solve
    def solve(self,
              rank,                                 # type: list
              preconditioner,                       # type: str
              iterations,                           # type: int
              tolerance,                            # type: float
              init_value=None,                      # type: xe.TTTensor or ExtendedFEMTT or list
              solver="TTSparseALS",                 # type: str
              solver_options=None,                  # type: dict
              _load=False                           # type: bool
              ):
        """
        starts the solving process
        :param rank: desired rank
        :param preconditioner: name of the used preconditioner
        :param iterations: number of iterations in the solution process
        :param tolerance: desired tolerance
        :param init_value: prescribed start value
        :param solver: solver type. default is Max+Manuel TTSparseALS with supported preconditioner as in tt_sparse_als
                       get precondition. Alternatively use "XerusAls" or "XerusAlsSym"
        :param solver_options: dictionary containing additional information for the solver.
                               SparseTTALS does not need this dict
                               XerusALS needs the "method" and "symmetric" entry (see xerus_als for more information)
        :param _load: flag to load the solution if it exists
        """
        self.ranks = rank
        self.iterations = iterations
        self.tolerance = tolerance
        self.preconditioner = preconditioner

        # if _load and self.load() is True:
        #     self.solved = True
        #     return
        if solver == "TTSparseALS":
            self.solver = TTSparseALS(self.tt_bilinearform, self.tt_linearform, rank, als_iterations=iterations,
                                      convergence=tolerance)
            if max(rank) == 1:
                self.solution = self.solver.solve(start_rank_one=True, preconditioner={"name": preconditioner,
                                                                                       "coef": self.discrete_coef,
                                                                                       "operator": self})
            elif init_value is None or init_value == []:
                self.solution = self.solver.solve(preconditioner={"name": preconditioner,
                                                                  "coef": self.discrete_coef,
                                                                  "operator": self})
            else:

                self.solution = self.solver.solve(preconditioner={"name": preconditioner,
                                                                  "coef": self.discrete_coef,
                                                                  "operator": self},
                                                  init_value=init_value, normalize_init=True)
        elif solver == "XerusALS":
            if solver_options is None:
                method = "ALS"
                symmetric = False
            else:
                assert "method" in solver_options
                assert "symmetric" in solver_options
                method = solver_options["method"]
                symmetric = solver_options["symmetric"]

            self.solver = XerusALS(self.tt_bilinearform, self.tt_linearform, rank, self.fs, iterations=iterations,
                                   convergence=tolerance)
            if max(rank) == 1:
                self.solution = self.solver.solve(method=method, symmetric=symmetric, start_rank_one=True)
            elif init_value is None or init_value == []:
                self.solution = self.solver.solve(method=method, symmetric=symmetric, start_rank_one=False)
            else:
                if isinstance(init_value, ExtendedFEMTT):
                    init_value = init_value.to_xerus_tt()
                self.solution = self.solver.solve(method=method, symmetric=symmetric, start_rank_one=False,
                                                  init_value=init_value, normalize_init=True)
        else:
            raise ValueError("unknown Solver")
        self.solved = True
        if isinstance(self.solution, list):
            self.solution = ExtendedFEMTT(self.solution, basis=self.discrete_coef.basis, fs=self.fs)
        if isinstance(self.solution, tt.vector):
            comp_list = tt.vector.to_list(self.solution)
            self.solution = ExtendedFEMTT(comp_list, basis=self.discrete_coef.basis, fs=self.fs)
        # self.solved = self.save()
    # endregion

    # region def get_filename
    def get_filename(self):
        filename = "LognormalPoissonOperator"
        filename += "discrete_field:{}".format(self.cont_coef.get_filename())
        filename += "hdegs:{}".format(self.degs)
        filename += "ranks:{}".format(self.ranks)
        filename += "iterations:{}".format(self.iterations)
        filename += "tolerance:{}".format(self.tolerance)
        filename += "preconditioner:{}".format(self.preconditioner)
        return filename
    # endregion

    # region def get_hash
    def get_hash(self):
        return hash(self.get_filename())
    # endregion

    # region save forward solution
    def save(self):
        #TODO
        """
        if self.solved is False:
            raise ValueError(" can not save operator if solver was not successfully run")
        try:
            retval = dict()
            retval['solution'] = self.solution
            f = open(FILE_PATH + str(self.get_hash()) + "sol.dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()
        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        """
        return True
    # endregion

    # region load
    def load(self):
        # TODO
        """
        try:
            f = open(FILE_PATH + str(self.get_hash()) + "sol.dat", 'rb')
            tmp_dict = pickle.load(f)
            f.close()
            self.solution = tmp_dict['solution']
        except Exception as ex:
            # print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        """
        return True
    # endregion

    # region save all parameters to a given hash file
    def save_to_hash(self, filename):
        # TODO
        """
        if self.solved is False:
            raise ValueError(" can not save operator if not solved already")
        if self.refined is False:
            raise ValueError(" can not save operator refinement parameters if not already refined")
        try:

            retval = dict()
            retval['solution'] = self.solution
            retval['ranks'] = self.ranks
            retval['iterations'] = self.iterations
            retval['tolerance'] = self.tolerance
            retval['preconditioner'] = self.preconditioner
            retval['max_norms'] = self.discrete_coef.max_norms
            retval['theta'] = self.discrete_coef.theta
            retval['rho'] = self.discrete_coef.rho
            retval['hdegs'] = self.hdegs
            retval['theta_x'] = self.theta_x
            retval['theta_y'] = self.theta_y
            retval['resnorm_zeta_weight'] = self.resnorm_zeta_weight
            retval['eta_zeta_weight'] = self.eta_zeta_weight
            retval['rank_always'] = self.rank_always
            retval['sol_rank'] = self.sol_rank
            retval['start_rank'] = self.start_rank
            retval['new_hdeg'] = self.new_hdeg
            retval['new_solution'] = self.new_solution
            retval['new_hdegs'] = self.new_hdegs
            retval['new_ranks'] = self.new_ranks
            retval['eta_global'] = self.eta_global
            retval['zeta_global'] = self.zeta_global
            retval['resnorm'] = self.resnorm
            retval['solver_iterations'] = self.solver.actual_iterations
            retval['solver_local_error_list'] = self.solver.local_error_list
            retval['solver_conv_list'] = self.solver.first_comp_convergence_list
            retval['det_estimator_result'] = self.det_estimator_result
            retval['m_dofs'] = self.fs.dim()
            retval['coef_sample_path'] = self.coef_sample_path
            retval['coef_hdegs'] = self.cont_coef.hermite_degree
            retval['coef_ranks'] = self.cont_coef.ranks
            retval['coef_m_dofs'] = self.discrete_coef.dofs
            retval["sto_estimator_result"] = self.sto_estimator_result
            retval["res_estimator_result"] = self.res_estimator_result
            retval["operator"] = self.tt_bilinearform

            filename_ad = "theta_x:{}".format(self.theta_x)
            filename_ad += "theta_y:{}".format(self.theta_y)
            filename_ad += "resnormzetaweight:{}".format(self.resnorm_zeta_weight)
            filename_ad += "etazetaweight:{}".format(self.eta_zeta_weight)
            filename_ad += "rank_always:{}".format(self.rank_always)
            filename_ad += "sol_rank:{}".format(self.sol_rank)
            filename_ad += "start_rank:{}".format(self.start_rank)
            filename_ad += "new_hdeg:{}".format(self.new_hdeg)
            filename_ad = str(hash(filename_ad))
            new_filename = "{}{}".format(self.get_hash(), filename_ad)

            f = open(filename, 'a')
            f.write("\n{}".format(new_filename))
            f.close()

            File(FILE_PATH + new_filename + "sol-mesh.xml") << self.discrete_coef.mesh
            File(FILE_PATH + new_filename + "new-sol-mesh.xml") << self.new_mesh
            f = open(FILE_PATH + new_filename + "sol.dat", 'wb')
            pickle.dump(retval, f, 2)
            f.close()

        except Exception as ex:
            print("!!!!! {} exception: {} \n    {}".format(__name__, ex.message, ex))
            return False
        """
        return True
    # endregion

    def compute_det_estimator(self,
                              refine_uniform        # type: bool
                              ):
        """
        computes the deterministic error estimator.
        :param refine_uniform: flag to not compute the estimator but to set all triangles on the same weights
        :return: None
        """
        from estimator_util import compute_det_estimator
        from numpy import ones, sqrt, sum
        if not refine_uniform:
            eta_local = compute_det_estimator(self.rhs_helper.TT, coef_tt, self.solution, self.helper.fs.mesh())
        else:
            eta_local = ones(self.helper.dg0_fs.dim())
        eta_global = sqrt(sum([eta ** 2 for eta in eta_local]))
        if self.estimator_result is None:
            self.estimator_result = {}
        self.estimator_result["eta_local"] = eta_local
        self.estimator_result["eta_global"] = eta_global

    def compute_sto_estimator(self,
                              refine_uniform,       # type: bool
                              no_compute=False      # type: bool
                              ):
        """
        computes the deterministic error estimator.
        :param refine_uniform: flag to not compute the estimator but to set all triangles on the same weights
        :param no_compute: flag to not compute the estimator
        :return: None
        """
        from estimator_util import compute_sto_estimator
        from numpy import ones, sqrt, sum
        if no_compute:
            zeta_local = dict((k, 1) for k in range(self.coef_00_helper.TT.dim-1))
            zeta_global = 0.0000000001
            if self.estimator_result is None:
                self.estimator_result = {}
            self.estimator_result["zeta_local"] = zeta_local
            self.estimator_result["zeta_global"] = zeta_global
            return
        if not refine_uniform:
            coef_tt = [self.coef_00_helper.TT, self.coef_01_helper.TT, self.coef_11_helper.TT]
            zeta_local = compute_sto_estimator(coef_tt, self.solution)
        else:
            zeta_local = ones(self.coef_00_helper.TT.dim-1)
        zeta_global = sqrt(sum([zeta ** 2 for zeta in zeta_local.values()], axis=0))
        if self.estimator_result is None:
            self.estimator_result = {}
        self.estimator_result["zeta_local"] = zeta_local
        self.estimator_result["zeta_global"] = zeta_global

    def compute_res_estimator(self,
                              ):
        from estimator_util import evaluate_resnorm

        resnorm = evaluate_resnorm(self.TT_op.tt_bilinearform,
                                   self.solution,
                                   self.TT_op.tt_linearform,
                                   self.helper.bc)
        self.estimator_result["resnorm"] = resnorm["resnorm"]

    def refine(self,
               mesh,
               poly_dim,                            # type: list
               max_sol_rank,                        # type: int
               theta_x,                             # type: float
               theta_y,                             # type: float
               resnorm_zeta_weight,                 # type: float
               eta_zeta_weight,                     # type: float
               rank_always,                         # type: bool
               refine_uniform                       # type: bool
               ):
        """
        Refine the mesh according to the estimator
        :param theta_x: D\"orfler marking parameter
        :param resnorm_zeta_weight: weights parameter in [0, \infty)
        :param eta_zeta_weight: weight parameter in [0, \infty)
        :param rank_always: flag to always refine the rank
        :param refine_uniform: flag to refine the mesh uniform
        :return:
        """
        from estimator_util import refine_mesh, ttMark_y, tt_add_stochastic_dimension
        from tt_util import has_full_rank
        from dolfin import refine
        if not refine_uniform:
            rank_done = False
            zeta_global = self.estimator_result["zeta_global"]
            eta_global = self.estimator_result["eta_global"]
            resnorm = self.estimator_result["resnorm"]
            print("  is zeta {} >= rszw {} * res {} or rank always {} or rank_done {}".format(zeta_global,
                                                                                              resnorm_zeta_weight,
                                                                                              resnorm,
                                                                                              rank_always,
                                                                                              rank_done))
            print("    or")
            print("  is ezw {} * eta {} >= rszw {} * res {} or rank_done {}".format(eta_zeta_weight,
                                                                                    eta_global,
                                                                                    resnorm_zeta_weight,
                                                                                    resnorm,
                                                                                    rank_done))
            if zeta_global >= resnorm_zeta_weight * resnorm or rank_always or rank_done:
                print("    Option 1")
                refine_deterministic = eta_zeta_weight * eta_global > zeta_global
                refine_stochastic = not refine_deterministic
                if refine_deterministic or refine_stochastic:
                    rank_done = True
            elif eta_zeta_weight * eta_global >= resnorm_zeta_weight * resnorm or rank_done:
                print("    Option 2")
                refine_deterministic = True
                refine_stochastic = False
                if refine_deterministic or refine_stochastic:
                    rank_done = True
            elif has_full_rank(self.solution.components):
                refine_deterministic = False
                refine_stochastic = True
                rank_done = True
            else:  # no refinement
                print("    refine Rank!!! ")
                refine_deterministic = False
                refine_stochastic = False
                rank_done = False
            if refine_deterministic:
                mesh, _ = refine_mesh(self.estimator_result["eta_local"], self.helper.fs, theta_x)
            elif refine_stochastic:
                new_dim, marked_zeta, zeta_ym = ttMark_y(self.estimator_result["zeta_local"],
                                                         {},
                                                         theta_y,
                                                         zeta_global,
                                                         longtail_zeta_marking=True)
                solution, poly_dim, _ = tt_add_stochastic_dimension(self.solution.components,
                                                                    new_dim, poly_dim, 2,
                                                                    new_gpcd=2)
            else:
                max_sol_rank += 1
        else:
            mesh = refine(self.helper.fs.mesh())
        return mesh, poly_dim, max_sol_rank
    # endregion
