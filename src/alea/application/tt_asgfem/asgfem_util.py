from __future__ import (division, print_function)
from dolfin import (FunctionSpace, TrialFunction, TestFunction, assemble, dx, Function,
                    as_backend_type, parameters, inner, nabla_grad)
from alea.math_utils.tensor.extended_tt import (leg_base_change_constant, herm_base_change_constant)
from functools import partial
import warnings
import numpy as np
import scipy.sparse as sps


def compute_fe_matrices(coef, rhs, fs, bc):

    # setup FE functions
    u, v = TrialFunction(fs), TestFunction(fs)
    dof2val = bc.get_boundary_values()
    bc_dofs = dof2val.keys()

    # assemble linear form and apply bc
    if isinstance(rhs, Function):
        b = assemble(rhs * v * dx)
        bc.apply(b)  # TODO: to be certain this maybe should be done explicitly as well
        b = b[:]
    elif len(coef.shape) == 2:
        # setup rhs function
        b = []
        rhs_fun = Function(fs)
        # TODO: parallelize as in alea-testing
        for k in range(rhs.shape[1]):
            rhs_fun.vector()[:] = np.ascontiguousarray(rhs[:, k])
            b_buf = assemble(rhs_fun * v * dx)
            bc.apply(b_buf)
            b.append(b_buf[:])
        b = np.array(b).T
    else:
        raise ValueError("unknown rhs type: {}".format(rhs))

    # use Eigen backend for conversion to scipy sparse matrices
    backend = parameters.linear_algebra_backend
    parameters.linear_algebra_backend = "Eigen"

    c = Function(fs)
    a_list = []
    # TODO: parallelize as in alea-testing
    for k in range(coef.shape[1]):
        c.vector().set_local(coef[:, k])
        a = assemble(inner(c * nabla_grad(u), nabla_grad(v)) * dx)
        bc.zero(a)
        rows, cols, values = as_backend_type(a).data()
        # print("convert to sps matrix")
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            a_list.append(sps.csr_matrix((values, cols, rows)))

    # restore backend
    parameters.linear_algebra_backend = backend

    retval = {
        "A": a_list,
        "bc_dofs": bc_dofs,
        "B": b
    }
    return retval


def generate_operator_tt(U, ranks, hdegs, M, poly="L"):

    if not isinstance(U, list):
        raise ValueError("U should be a list of component tensors")

    # from numpy.polynomial.legendre import leggauss
    # def tri_integral(a, b, c, normalised=True):
    #     poly1 = LegendrePolynomials(normalised=normalised)
    #     poly1 = partial(poly1.eval, n=a, all_degrees=False)
    #     poly2 = LegendrePolynomials(normalised=normalised)
    #     poly2 = partial(poly2.eval, n=b, all_degrees=False)
    #     poly3 = LegendrePolynomials(normalised=normalised)
    #     poly3 = partial(poly3.eval, n=c, all_degrees=False)
    #     nodes, weights = leggauss((a + b + c + 1) * 2)
    #     int_value = np.sum([w * poly1(x=n) * poly2(x=n) * poly3(x=n) * 0.5 for w, n in zip(weights, nodes)])
    #     return int_value
    if poly == "L":
        tri_prod_coef = partial(leg_base_change_constant, normalised=True)
    elif poly == "H":
        tri_prod_coef = partial(herm_base_change_constant, normalised=True)
    else:
        raise ValueError("unknown polynomial type: {}".format(poly))
    # tri_prod_coef = partial(tri_integral, normalised=True)
    cores = U
    d = len(cores) - 1
    opcores = [None]
    for iexp in range(M):
        # print("  {}".format(cores[iexp].shape))
        opcorei = np.zeros([ranks[iexp+1],hdegs[iexp],hdegs[iexp],ranks[iexp+2]])
        for nu1 in range(hdegs[iexp]):
            for nu2 in range(hdegs[iexp]):
                # print("mu={}, nu1={}, nu2={}".format(cores[iexp].shape[1], nu1, nu2))
                opcorei[:, nu1, nu2, :] = sum([cores[iexp][:,mu,:]*tri_prod_coef(mu, nu1, nu2)
                                              for mu in range(cores[iexp].shape[1])])

        opcores.append(opcorei)

    if 0 < M <= d:
        print("Some more dimensions !!!!!!!!!!!!!!!!!")
        exit()
        # buffer_core = np.zeros((opcores[M].shape[0],opcores[M].shape[1], opcores[M].shape[2], 1))
        for lia in range(M, d+1):
            opcores[-1] = np.einsum('lnmk, kt->lnmt', opcores[-1], cores[lia][:, 0, :])
        # print opcores[M].shape
        # buffer_core[:, :, :, 0] = opcores[M]
        # opcores[M] = buffer_core

    return opcores
