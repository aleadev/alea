from __future__ import division
from dolfin import (Function, norm, plot)
import matplotlib.pyplot as plt
import numpy as np


def plot_fem_diff(truth, approx, fs, title_truth="Truth", title_approx="approx"):
    # compute relative difference to show accuracy
    diff = Function(fs)
    diff_vec = (np.abs(truth.vector()[:] - approx.vector()[:])) / norm(truth)
    diff.vector().set_local(diff_vec)

    # start plotting
    fig = plt.figure(figsize=(17, 5))

    fig.add_subplot(131)
    im = plot(truth)
    plt.title(title_truth)
    plt.colorbar(im)

    fig.add_subplot(132)
    im = plot(approx)
    plt.title(title_approx)
    plt.colorbar(im)

    fig.add_subplot(133)
    im = plot(diff)
    plt.title("Difference")
    plt.colorbar(im)

    plt.tight_layout()


def set_fem_fun(vec, fs):
    retval = Function(fs)
    retval.vector().set_local(vec)
    return retval