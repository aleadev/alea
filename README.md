**ALEA** is intended as a research framework for numerical methods in Uncertainty Quantification (UQ).
Its emphasis lies on

* generalised polynomial chaos (gpc) methods
* stochastic Galerkin FEM
* adaptive numerical methods
* tensor methods for UQ

Most of these areas are *work in progress*.
The provided functionality will be extended gradually and demonstrated in related articles.

The framework is written in python and uses [FEniCS][] as its default FEM backend.

[FEniCS]: http://fenicsproject.org/

***

**Usage**

For a quick installation, clone the repository and add *alea/src* to your PYTHONPATH.

As a developer, make sure that the unittests run smoothly. 
Therefore, prior to every commit, run *./bin/test_module*.


***

We are currently preparing the release of the source code.
You can already access it at [alea-testing][].

[alea-testing]: https://bitbucket.org/aleadev/alea-testing
